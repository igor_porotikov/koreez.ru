<?php /* Smarty version 2.6.10, created on 2013-11-12 05:34:33
         compiled from register.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'register.tpl.html', 73, false),array('modifier', 'default', 'register.tpl.html', 73, false),)), $this); ?>


<table border=0 cellpadding="0" cellspacing="0" >
<tr>
<td colspan=2 align=left>
<br>
<?php if ($this->_tpl_vars['order'] != NULL): ?>
<?php echo @STRING_ORDER_CONTINUE_TIP; ?>

<?php endif; ?></td>
</tr>
<tr><td align=left>
<?php if ($this->_tpl_vars['order']): ?>
	<form action="index.php?register=yes&order=yes" method=post  name='RegisterForm'>
<?php else: ?>
	<form action="index.php?register=yes" method=post  name='RegisterForm'>
<?php endif; ?>
<table border=0>
<tr>
<td colspan=3 align=left class="regpad">
<b><u><?php echo @STRING_REGISTRATION_FORM; ?>
</u></b><br>
<br>
<?php echo @STRING_REQUIRED; ?>

<br><br>
<?php if ($this->_tpl_vars['reg_error'] != NULL): ?>
<font color=red><b><?php echo $this->_tpl_vars['reg_error']; ?>
</b>
</font><br><br>
<?php endif; ?>
</td>
</tr>
<!-- STRING AUTHORIZATION FIELDS (LOGIN, PASSWORD) -->
<tr>
<td colspan=3 align=left>
<table bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
 width=80% border=0>
<tr>
<td align="center">
<font color=black><?php echo @STRING_AUTHORIZATION_FIELDS; ?>
</font></td>
</tr></table>
</td>
</tr>
<tr>
<td colspan=2 class="regpad">
<font color=red>*</font> 
<?php echo @CUSTOMER_LOGIN; ?>
</td>
<td><input type="text" name="login" value="<?php echo $this->_tpl_vars['login']; ?>
"></td>
</tr>
<tr>
<td colspan=2 align=left class="regpad"><font color=red>*</font> 
<?php echo @CUSTOMER_PASSWORD; ?>
</td>
<td><input type="password" name="cust_password1" value="<?php echo $this->_tpl_vars['cust_password1']; ?>
">
</td>
</tr>
<tr>
<td colspan=2 align=left class="regpad"><font color=red>*</font> 
			<?php echo @CUSTOMER_CONFIRM_PASSWORD; ?>
</td>
<td>
<input type="password" name="cust_password2" value="<?php echo $this->_tpl_vars['cust_password2']; ?>
">
</td>
</tr>
<!-- GENERAL INFORMATION (FIRST NAME, LAST NAME, EMAIL ) -->
<tr>
<td colspan=3 align=left>
<table bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
 width=80%>
<tr>
<td align="center"><font color=black >
<?php echo @STRING_GENERAL_INFORMATION; ?>
</font></td>
</tr>
</table>
</td>
</tr>
<tr><td colspan=2 align=left class="regpad">
<?php if ($this->_tpl_vars['SessionRefererLogin'] || @CONF_AFFILIATE_PROGRAM_ENABLED == 0): ?>
			<input name="affiliationLogin" type="hidden" value="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['SessionRefererLogin'])) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '&quot;') : smarty_modifier_replace($_tmp, '"', '&quot;')))) ? $this->_run_mod_handler('replace', true, $_tmp, "\'", "'") : smarty_modifier_replace($_tmp, "\'", "'")))) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")))) ? $this->_run_mod_handler('replace', true, $_tmp, ">", "&gt;") : smarty_modifier_replace($_tmp, ">", "&gt;")))) ? $this->_run_mod_handler('default', true, $_tmp, "") : smarty_modifier_default($_tmp, "")); ?>
" />
		<?php endif; ?>
			<font color=red>*</font> 
			<?php echo @CUSTOMER_FIRST_NAME; ?>
		</td>
		<td>
			<input type="text" name="first_name" 
				value="<?php echo $this->_tpl_vars['first_name']; ?>
">
		</td>
	</tr>
	<tr>
		<td colspan=2 align=left class="regpad">
			<font color=red>*</font> 
			<?php echo @CUSTOMER_LAST_NAME; ?>
		</td>
		<td>
			<input type="text" name="last_name" 
				value="<?php echo $this->_tpl_vars['last_name']; ?>
">
		</td>
	</tr>
	<tr>
		<td colspan=2 align=left class="regpad">
			<font color=red>*</font> 
				<?php echo @CUSTOMER_EMAIL; ?>
		</td>
		<td>
			<input type="text" name="email" 
				value="<?php echo $this->_tpl_vars['email']; ?>
">
		</td>
	</tr>

	<tr>
		<td colspan=2 align=right>
			<input type=checkbox 
				name=subscribed4news <?php if ($this->_tpl_vars['subscribed4news'] == 1): ?>checked<?php endif; ?>>		</td>
		<td>
				<?php echo @CUSTOMER_SUBSCRIBE_FOR_NEWS; ?>

		</td>
	</tr> 

	<!-- ADDITIONAL FIELDS (SEE  admin.php?dpt=custord&sub=reg_fields URL) -->
	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['additional_fields']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
	<tr>
		<td colspan=2 align=left class="regpad">
			<?php if ($this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_required']): ?>
				<font color=red>*</font> 
			<?php endif; ?>
			<?php echo $this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_name']; ?>
:		</td>
		<td>
			<input type='text' name='additional_field_<?php echo $this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_ID']; ?>
' 
				value='<?php echo $this->_tpl_vars['additional_field_values'][$this->_sections['i']['index']]['reg_field_value']; ?>
'>
		</td>
	</tr>
	<?php endfor; endif; ?>

	<?php if (! $this->_tpl_vars['SessionRefererLogin'] && @CONF_AFFILIATE_PROGRAM_ENABLED == 1): ?>
		<tr>
			<td colspan=2 align=left>
				<?php echo @CUSTOMER_AFFILIATION; ?>
			</td>
			<td>
				<input type="text" name="affiliationLogin" value="<?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['affiliationLogin'])) ? $this->_run_mod_handler('replace', true, $_tmp, '"', '&quot;') : smarty_modifier_replace($_tmp, '"', '&quot;')))) ? $this->_run_mod_handler('replace', true, $_tmp, "\'", "'") : smarty_modifier_replace($_tmp, "\'", "'")))) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")))) ? $this->_run_mod_handler('replace', true, $_tmp, ">", "&gt;") : smarty_modifier_replace($_tmp, ">", "&gt;")))) ? $this->_run_mod_handler('default', true, $_tmp, "") : smarty_modifier_default($_tmp, "")); ?>
">
			</td>
		</tr>
	<?php endif; ?>
	<tr>
		<td colspan=3 align=left>
			<table bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
 width=80%>
				<tr>
					<td align="center">
						<font color=black >
							<?php if ($this->_tpl_vars['order'] != NULL): ?>
								<?php echo @STRING_ADDRESSES; ?>

							<?php else: ?>
								<?php echo @STRING_ADDRESS; ?>

							<?php endif; ?></font></td>
				</tr>
			</table>
		</td>
	</tr>
<!-- DELIVERY ADDRESS (COUNTRY, AREA(STATE), INDEX, CITY, ADDRESS) -->
	<tr>
		<td colspan=3 align=left>
		<table border=0>
<?php if ($this->_tpl_vars['order'] != NULL): ?>
			<tr>
<td rowspan=7>
	<?php if ($this->_tpl_vars['order'] != NULL): ?>
		<b><?php echo @STRING_SHIPPING_ADDRESS; ?>
</b>
			<?php endif; ?>
			</td>
<td colspan=2 align=left class="regpad">
					<font color=red>*</font> 
						<?php echo @STRING_RECEIVER_FIRST_NAME; ?>
				</td>
	<td>
	<input type=text name='receiver_first_name' value='<?php echo $this->_tpl_vars['receiver_first_name']; ?>
'
							onblur='JavaScript:billingAddressCheckHandler()'
							onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
<tr><td colspan=2 align=left class="regpad">
<font color=red>*</font> <?php echo @STRING_RECEIVER_LAST_NAME; ?>
</td>
<td><input type=text name='receiver_last_name' value='<?php echo $this->_tpl_vars['receiver_last_name']; ?>
'
							onblur='JavaScript:billingAddressCheckHandler()'
							onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_ADDRESS != 2): ?>
			<tr>

				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_ADDRESS == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_ADDRESS; ?>
				</td>
				<td>
					<textarea name="address" rows=4 
						onchange='JavaScript:billingAddressCheckHandler()'><?php echo $this->_tpl_vars['address']; ?>
</textarea>
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='address' value=''>
			<?php endif; ?>


			<?php if (@CONF_ADDRESSFORM_CITY != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_CITY == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_CITY; ?>
				</td>
				<td>
					<input type="text" name="city" 
						value="<?php echo $this->_tpl_vars['city']; ?>
"
						onblur='JavaScript:billingAddressCheckHandler()'
						onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='city' value=''>
			<?php endif; ?>


			<?php if (@CONF_ADDRESSFORM_STATE != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_STATE == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_STATE; ?>
				</td>
				<td>
					<?php if (! $this->_tpl_vars['zones']): ?>
						<input type="text" name="state" 
							value="<?php echo $this->_tpl_vars['state']; ?>
"
							onchange='JavaScript:billingAddressCheckHandler()' >
					<?php else: ?>
						<select name=zoneID
							onchange='JavaScript:billingAddressCheckHandler()' >
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['zones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['zones'][$this->_sections['i']['index']]['zoneID']; ?>

							<?php if ($this->_tpl_vars['zones'][$this->_sections['i']['index']]['zoneID'] == $this->_tpl_vars['zoneID']): ?>
								selected
							<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['zones'][$this->_sections['i']['index']]['zone_name']; ?>

							</option>
							<?php endfor; endif; ?>
						</select>
					<?php endif; ?>
				</td>
			</tr>
			<?php else: ?>
				<?php if (! $this->_tpl_vars['zones']): ?>
					<input type=hidden name='state' value=''>
				<?php else: ?>
					<input type=hidden name='zoneID' value='0'>
				<?php endif; ?>
			<?php endif; ?>


			<?php if (@CONF_ADDRESSFORM_ZIP != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_ZIP == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_ZIP; ?>
				</td>
				<td>
					<input type="text" name="zip" 
						value="<?php echo $this->_tpl_vars['zip']; ?>
"
						onblur='JavaScript:billingAddressCheckHandler()'
						onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='zip' value=''>
			<?php endif; ?>


			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if ($this->_tpl_vars['countries']): ?>
						<font color=red>*</font> 
							<?php echo @CUSTOMER_COUNTRY; ?>

					<?php endif; ?>				</td>
				<td>
					<?php if ($this->_tpl_vars['countries']): ?>
					<select name=countryID
						onchange='JavaScript:billingAddressCheckHandler(); changeCountryHandler();'
					>
						<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID']; ?>

								<?php if ($this->_tpl_vars['countryID'] != NULL): ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == $this->_tpl_vars['countryID']): ?>
										selected
									<?php endif; ?>
								<?php else: ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == @CONF_DEFAULT_COUNTRY): ?>
										selected
									<?php endif; ?>
								<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['country_name']; ?>

							</option>
						<?php endfor; endif; ?>
		 			</select>
					<?php else: ?>
						<input type=hidden name='countryID'	value='NULL'>
					<?php endif; ?> 
				</td>

			</tr>

			</table>

			<?php echo '
			<script language=\'JavaScript\'>
			function billingAddressCheckHandler()
			{
				return;
			}
			</script>
			'; ?>


		</td>
	<tr>




	<?php if ($this->_tpl_vars['order'] != NULL): ?>


	<tr>
		<td colspan=3 align=left>
			
			<table>

			<tr>
				<td rowspan=8>
					<b><?php echo @STRING_BILLING_ADDRESS; ?>
</b>
				</td>
				<td colspan=3 align="left">
					<input type=checkbox name='billing_address_check' value='1'
								onclick='JavaScript:billingAddressCheckHandler()'
					<?php if ($this->_tpl_vars['billing_address_check']): ?>
						checked
					<?php endif; ?>
					>
					<?php echo @STRING_EQUAL_TO_SHIPPING_ADDRESS; ?>
				</td>
			</tr>

			<tr>
				<td colspan=2 align=left class="regpad">
					<font color=red>*</font> 
						<?php echo @STRING_PAYER_FIRST_NAME; ?>
				</td>
				<td>
					<input type=text name='payer_first_name' 
						value='<?php echo $this->_tpl_vars['payer_first_name']; ?>
'>
				</td>
			</tr>


			<tr>
				<td colspan=2 align=left class="regpad">
					<font color=red>*</font> 
						<?php echo @STRING_PAYER_LAST_NAME; ?>
				</td>
				<td>
					<input type=text name='payer_last_name'
						value='<?php echo $this->_tpl_vars['payer_last_name']; ?>
'>
				</td>
			</tr>

			<?php if (@CONF_ADDRESSFORM_ADDRESS != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_ADDRESS == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_ADDRESS; ?>
				</td>
				<td>
					<textarea name="billingAddress" rows=4 
							value='<?php echo $this->_tpl_vars['billingAddress']; ?>
'><?php echo $this->_tpl_vars['billingAddress']; ?>
</textarea>
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingAddress' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_CITY != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_CITY == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_CITY; ?>
				</td>
				<td>
					<input type="text" name="billingCity" 
						value="<?php echo $this->_tpl_vars['billingCity']; ?>
">
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingCity' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_STATE != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_STATE == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_STATE; ?>
				</td>
				<td>
					<?php if (! $this->_tpl_vars['billingZones']): ?>
					<input type="text" name="billingState" 
						value="<?php echo $this->_tpl_vars['billingState']; ?>
">
					<?php else: ?>
						<select name=billingZoneID>
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['billingZones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
								<option value=<?php echo $this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zoneID']; ?>

								<?php if ($this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zoneID'] == $this->_tpl_vars['billingZoneID']): ?>
									selected
								<?php endif; ?>
								>
									<?php echo $this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zone_name']; ?>

								</option>
							<?php endfor; endif; ?>
						</select>
					<?php endif; ?>
				</td>
			</tr>
			<?php else: ?>
				<?php if (! $this->_tpl_vars['billingZones']): ?>
					<input type="hidden" name="billingState" value="">
				<?php else: ?>
					<input type="hidden" name="billingZoneID" value="">
				<?php endif; ?>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_ZIP != 2): ?>
			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if (@CONF_ADDRESSFORM_ZIP == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_ZIP; ?>
				</td>
				<td>
					<input type="text" name="billingZip" 
						value="<?php echo $this->_tpl_vars['billingZip']; ?>
">
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingZip' value=''>
			<?php endif; ?>

			<tr>
				<td colspan=2 align=left class="regpad">
					<?php if ($this->_tpl_vars['countries']): ?>
						<font color=red>*</font> 
							<?php echo @CUSTOMER_COUNTRY; ?>

					<?php endif; ?>				</td>
				<td>
					<?php if ($this->_tpl_vars['countries']): ?>
						<select name=billingCountryID style="width:100px;"
							onchange='JavaScript:changeCountryHandler();'
						>
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID']; ?>

								<?php if ($this->_tpl_vars['billingCountryID'] != NULL): ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == $this->_tpl_vars['billingCountryID']): ?>
										selected
									<?php endif; ?>
								<?php else: ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == @CONF_DEFAULT_COUNTRY): ?>
										selected
									<?php endif; ?>
								<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['country_name']; ?>

							</option>
							<?php endfor; endif; ?>
		 				</select>
					<?php else: ?>
						<input type=hidden name='billingCountryID' value='NULL'>
					<?php endif; ?>
				</td>
			</tr>

			</table>

			<input type=hidden value='' name='billing_address_checkHiddenField'>

			<?php echo '
			<script language=\'JavaScript\'>
				function billingAddressCheckHandler()
				{
					if ( (document.RegisterForm.billingCountryID.value != 
							document.RegisterForm.countryID.value) && 
						 	document.RegisterForm.billing_address_check.checked )
					{
							document.RegisterForm.submit();
							return;
					}
			'; ?>


					document.RegisterForm.payer_first_name.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.payer_last_name.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingCountryID.disabled =
						document.RegisterForm.billing_address_check.checked;
					<?php if (! $this->_tpl_vars['billingZones']): ?>
						document.RegisterForm.billingState.disabled = 
							document.RegisterForm.billing_address_check.checked;
					<?php else: ?>
						document.RegisterForm.billingZoneID.disabled = 
							document.RegisterForm.billing_address_check.checked;						
					<?php endif; ?>
					document.RegisterForm.billingZip.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingCity.disabled =
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingAddress.disabled = 
						document.RegisterForm.billing_address_check.checked;

			<?php echo '
					if ( document.RegisterForm.billing_address_check.checked )
					{
			'; ?>

						document.RegisterForm.payer_first_name.value =
							document.RegisterForm.receiver_first_name.value;
						document.RegisterForm.payer_last_name.value =
							document.RegisterForm.receiver_last_name.value;
						document.RegisterForm.billingCountryID.value =
							document.RegisterForm.countryID.value;
						<?php if (! $this->_tpl_vars['billingZones']): ?>
							document.RegisterForm.billingState.value = 
								document.RegisterForm.state.value;
						<?php else: ?>
							document.RegisterForm.billingZoneID.value = 
								document.RegisterForm.zoneID.value;
						<?php endif; ?>
						document.RegisterForm.billingZip.value = 
							document.RegisterForm.zip.value;
						document.RegisterForm.billingCity.value =
							document.RegisterForm.city.value
						document.RegisterForm.billingAddress.value = 
							document.RegisterForm.address.value; 
			<?php echo '
					}
				}

				billingAddressCheckHandler();
			</script>
			'; ?>


		</td>
	</tr>
		
	<?php endif; ?>
	<?php if (@CONF_ENABLE_CONFIRMATION_CODE): ?>
	<tr>
		<td colspan=3 align=left>
			<div class="small" style="color: black;width:80%; padding: 3px; text-align: left; background-color:#<?php echo @CONF_MIDDLE_COLOR; ?>
">
				<?php echo @STR_CONFIRMATION_CODE; ?>
			</div>
		</td>
	</tr>
	<tr>
		<td align="left">
			<img src="./imgval.php" alt="code" align="right" border="0" />		</td>
		<td>
		</td>
		<td align="left">
			<input name="fConfirmationCode" style="color:#aaaaaa" value="<?php echo @STR_ENTER_CCODE; ?>
" type="text" onfocus="if(this.value=='<?php echo @STR_ENTER_CCODE; ?>
')
			<?php echo '
			{this.style.color=\'#000000\';this.value=\'\';}
			'; ?>
" onblur="if(this.value=='')
			<?php echo '{'; ?>
this.style.color='#aaaaaa';this.value='<?php echo @STR_ENTER_CCODE; ?>
'<?php echo '}'; ?>
" />
		</td>
	</tr>
	<?php endif; ?>
</table>

<?php echo '
		<script language=\'JavaScript\'>

		function changeCountryHandler()
		{
				document.RegisterForm.submit();
		}

		</script>
'; ?>


<p  class="regpad">
<input type="submit" value="<?php echo @OK_BUTTON; ?>
" name=save>


<input type=reset value="<?php echo @RESET_BUTTON; ?>
">
</p>

<?php if ($this->_tpl_vars['order'] != NULL): ?>
	<input type=hidden name=order value=1>
<?php endif; ?>

<?php if ($this->_tpl_vars['order_without_billing_address'] != NULL): ?>
	<input type=hidden name=order_without_billing_address value=1>
<?php endif; ?>

</form>
<?php if ($this->_tpl_vars['reg_updating'] && $this->_tpl_vars['log'] != @ADMIN_LOGIN): ?>
<p>[ <a class=bold href="javascript:confirmUnsubscribe();"><?php echo @TERMINATE_ACCOUNT_LINK; ?>
</a> ]</p>
<?php endif; ?>

	</td>


</tr>
</table>