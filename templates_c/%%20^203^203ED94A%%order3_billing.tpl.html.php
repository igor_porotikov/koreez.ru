<?php /* Smarty version 2.6.10, created on 2013-11-06 08:30:30
         compiled from order3_billing.tpl.html */ ?>

<h1><u><?php echo @STRING_ORDERING; ?>
</u></h1>

<h3><?php echo @STRING_ORDER_PAYMENT; ?>
</h3>

<table border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td><?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '1'): ?>
			<font><?php echo @STRING_BILLING_ADDRESS; ?>
:</font>
			<br>
			<?php if ($this->_tpl_vars['billingAddressID'] > 0): ?><b><?php echo $this->_tpl_vars['strAddress']; ?>
</b><?php else:  echo @STRING_ADDRESS_NOT_SPECIFIED;  endif; ?>
			
				<p><a href='index.php?change_address=yes&shippingAddressID=<?php echo $this->_tpl_vars['shippingAddressID']; ?>
&shippingMethodID=<?php echo $this->_tpl_vars['shippingMethodID']; ?>
&billingAddressID=<?php echo $this->_tpl_vars['billingAddressID']; ?>
&shServiceID=<?php echo $_GET['shServiceID']; ?>
'>
					<?php echo @STRING_CHANGE_ADDRESS; ?>

				</a>
			<?php endif; ?>
		</td>
	</tr>
</table>



<?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '1'): ?>
	<p><?php echo @STRING_SELECT_ORDER_PAYMENT_METHOD_PROMPT; ?>
:</p>
<?php endif; ?>

	<form method=POST name='MainForm'>

	<table border="0" cellspacing="1" cellpadding="4">

		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['payment_methods']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<tr>
			<td valign=top>
				<input type="radio" name="select_payment_method"
				<?php if ($this->_sections['i']['index'] == 0): ?>
					checked
				<?php endif; ?>

				value="<?php echo $this->_tpl_vars['payment_methods'][$this->_sections['i']['index']]['PID']; ?>
"
				>
			</td>

			<td valign=top>
				<b><?php echo $this->_tpl_vars['payment_methods'][$this->_sections['i']['index']]['Name']; ?>
</b>
				<br>
				<?php echo $this->_tpl_vars['payment_methods'][$this->_sections['i']['index']]['description']; ?>

			</td>

		</tr>
		<?php endfor; endif; ?>
	
	</table>

	<?php if (! $this->_tpl_vars['payment_methods']): ?>
		<font color=red><b><?php echo @STRING_NO_PAYMENT_METHODS_TO_SELECT; ?>
...</b></font>
	<?php else: ?>
		<br>
		<?php if ($this->_tpl_vars['billingAddressID'] > 0 || @CONF_ORDERING_REQUEST_BILLING_ADDRESS == '0'): ?>
			<input type="submit" name="continue_button" value="<?php echo @CONTINUE_BUTTON; ?>
 &gt;&gt;">
		<?php else: ?>
			<input type="submit" disabled value="<?php echo @CONTINUE_BUTTON; ?>
 &gt;&gt;">
			<br><font color=red><?php echo @STRING_PLEASE_SPECIFY_ADDRESS; ?>
</font>
		<?php endif; ?>
	<?php endif; ?>

	</form>