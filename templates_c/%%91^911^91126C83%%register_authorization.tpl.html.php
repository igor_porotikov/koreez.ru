<?php /* Smarty version 2.6.10, created on 2013-11-13 13:31:53
         compiled from register_authorization.tpl.html */ ?>

<h1><u><?php echo @STRING_ORDERING; ?>
</u></h1>

<h3><?php echo @STRING_AUTHORIZATION; ?>
</h3>

<table border="0" cellspacing="1" cellpadding="5">
	<tr>
    		<td colspan="2" bgcolor="#CCCCCC">
			<p>
				<strong><?php echo @STRING_AM_NEW_CUSTOMER; ?>
</strong>
			</p>
  		</td>
  	</tr>
  	<tr>
    		<td width="15">
      			<p>&nbsp;</p>
	  </td>
    		<td width="99%">
			<p>
				<?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '1'): ?>
					<a href="index.php?register=yes&order=yes">
						<?php echo @STRING_REGISTER; ?>

					</a>
				<?php endif; ?>
				<?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '0'): ?>
					<a href="index.php?register=yes&order_without_billing_address=yes">
						<?php echo @STRING_REGISTER; ?>

					</a>
				<?php endif; ?>
			</p>
			<?php if (@CONF_QUICK_ORDER): ?>
      				<p>
					<a href="index.php?quick_register=yes"><?php echo @STRING_QUICK_ORDERING; ?>
</a>				
				</p>
			<?php endif; ?>
		</td>
  	</tr>
  	<tr> 
    		<td colspan="2" bgcolor="#CCCCCC">
			<strong><?php echo @STRING_I_AM_REGISTERED_CUSTOMER; ?>
</strong>
		</td>
  	</tr>
  	<tr> 
    		<td width="15">&nbsp;</td>
    		<td>
				<form action="index.php?register_authorization=yes" method="POST">
					<table border=0>
						<tr>
							<td align=right>
								<font><?php echo @CUSTOMER_LOGIN; ?>
</font>
							</td>
							<td>
								<input type="text" class="ss" name="user_login" size="10" value='<?php echo $this->_tpl_vars['user_login']; ?>
' />
							</td>
						</tr>
						<tr>
							<td align=right>
								<font><?php echo @CUSTOMER_PASSWORD; ?>
</font>
							</td>
							<td>
								<input name="user_pw" class="ss" type="password" size="10" />
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" name="login" value="<?php echo @OK_BUTTON; ?>
" /></td>
						</tr>
					</table>
				</form>
				<form action="index.php?register_authorization=yes" method="POST">
					<?php if ($this->_tpl_vars['password_sent_notifycation'] == 'yes'): ?>
						<div><b><?php echo @STRING_PASSWORD_SENT; ?>
</b></div>
					<?php elseif ($this->_tpl_vars['password_sent_notifycation'] == 'no'): ?>
						<div class="error_msg_f"><?php echo @STRING_CANT_FIND_USER_IN_DB; ?>
 (<?php echo $this->_tpl_vars['remind_user_login']; ?>
)!</div>
					<?php endif; ?>
					&nbsp;
					<?php if (! $this->_tpl_vars['remind_password']): ?>
					<a href="index.php?register_authorization=yes&remind_password=yes">
						<?php echo @FORGOT_PASSWORD_LINK; ?>

					</a>
					<?php else: ?>
						<?php echo @STRING_FORGOT_PASSWORD_FIX; ?>

						<input type="text" name="login_to_remind_password" value="<?php echo $this->_tpl_vars['user_login']; ?>
" />
						<input type="hidden" name="remind_password" />
						<input type="submit" value="<?php echo @OK_BUTTON; ?>
" />
					<?php endif; ?>
				</form>
			</td>
  	</tr>
</table>