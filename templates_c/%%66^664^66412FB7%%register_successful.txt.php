<?php /* Smarty version 2.6.10, created on 2013-11-06 08:29:25
         compiled from register_successful.txt */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'register_successful.txt', 9, false),)), $this); ?>
<?php echo @EMAIL_HELLO; ?>
!

<?php echo @EMAIL_YOUVE_BEEN_REGISTERED_AT; ?>
 <?php echo @CONF_SHOP_NAME; ?>


<?php echo @EMAIL_YOUR_REGTRATION_INFO; ?>


<?php if (@CONF_ENABLE_REGCONFIRMATION): ?>
<?php echo ((is_array($_tmp=((is_array($_tmp=@CONF_EMAIL_REGCONFIRMATION)) ? $this->_run_mod_handler('replace', true, $_tmp, "[code]", $this->_tpl_vars['ActCode']) : smarty_modifier_replace($_tmp, "[code]", $this->_tpl_vars['ActCode'])))) ? $this->_run_mod_handler('replace', true, $_tmp, "[codeurl]", $this->_tpl_vars['ActURL']) : smarty_modifier_replace($_tmp, "[codeurl]", $this->_tpl_vars['ActURL'])); ?>


<?php endif; ?>
<?php echo @CUSTOMER_LOGIN; ?>
 <?php echo $this->_tpl_vars['login']; ?>

<?php echo @CUSTOMER_PASSWORD; ?>
 <?php echo $this->_tpl_vars['cust_password']; ?>

<?php echo @CUSTOMER_FIRST_NAME; ?>
 <?php echo $this->_tpl_vars['first_name']; ?>

<?php echo @CUSTOMER_LAST_NAME; ?>
 <?php echo $this->_tpl_vars['last_name']; ?>

<?php echo @CUSTOMER_EMAIL; ?>
 <?php echo $this->_tpl_vars['Email']; ?>

<?php if ($this->_tpl_vars['additional_field_values']): ?>
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['additional_field_values']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<?php echo $this->_tpl_vars['additional_field_values'][$this->_sections['i']['index']]['reg_field_name']; ?>
: <?php echo $this->_tpl_vars['additional_field_values'][$this->_sections['i']['index']]['reg_field_value']; ?>

<?php endfor; endif; ?>	
<?php endif; ?>
<?php if ($this->_tpl_vars['addresses']): ?>
<?php echo @STRING_ADDRESSES_HAS_BEEN_ADDED; ?>
:
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['addresses']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['addresses'][$this->_sections['i']['index']]['addressStr'])) ? $this->_run_mod_handler('replace', true, $_tmp, "<br>", "\n") : smarty_modifier_replace($_tmp, "<br>", "\n")); ?>

<?php endfor; endif; ?>
<?php endif; ?>

<?php echo @EMAIL_SINCERELY; ?>
, <?php echo @CONF_SHOP_NAME; ?>

<?php echo @CONF_SHOP_URL; ?>