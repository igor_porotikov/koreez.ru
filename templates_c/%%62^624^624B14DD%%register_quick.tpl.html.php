<?php /* Smarty version 2.6.10, created on 2013-11-11 00:39:02
         compiled from register_quick.tpl.html */ ?>

<center>
<p>
<table width=70% border=0>

	<tr>
		<td colspan=2 align=center>
			<br>
			<?php if ($this->_tpl_vars['order'] != NULL): ?>
				<?php echo @STRING_ORDER_CONTINUE_TIP; ?>

			<?php endif; ?>
		</td>
	</tr>

	<tr>
		<td align=center>



	<form method=post name='RegisterForm'>

<table border=0 width=70%>
	
	<tr>
		<td colspan=3 align=center>
			<b><u><?php echo @STRING_REGISTRATION_FORM; ?>
</u></b><br><br>
				<?php echo @STRING_REQUIRED; ?>

			<br><br>
			<?php if ($this->_tpl_vars['reg_error'] != NULL): ?>
				<font color=red><b><?php echo $this->_tpl_vars['reg_error']; ?>
</b>
				</font><br><br>
			<?php endif; ?>
		</td>
	</tr>

	<!-- GENERAL INFORMATION (FIRST NAME, LAST NAME, EMAIL ) -->
	<tr>
		<td colspan=3 align=center>
			<table bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
 width=80%>
				<tr>
					<td>
						<font color=black class=small>
							<?php echo @STRING_GENERAL_INFORMATION; ?>

						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=2 align=right>
			<?php if ($this->_tpl_vars['SessionRefererLogin'] || @CONF_AFFILIATE_PROGRAM_ENABLED == 0): ?>
				<input name="affiliationLogin" type="hidden" value="" />
			<?php endif; ?>
			<font color=red>*</font> 
			<?php echo @CUSTOMER_FIRST_NAME; ?>

		</td>
		<td>
			<input type="text" name="first_name" 
				value="<?php echo $this->_tpl_vars['first_name']; ?>
">
		</td>
	</tr>
	<tr>
		<td colspan=2 align=right>
			<font color=red>*</font> 
			<?php echo @CUSTOMER_LAST_NAME; ?>

		</td>
		<td>
			<input type="text" name="last_name" 
				value="<?php echo $this->_tpl_vars['last_name']; ?>
">
		</td>
	</tr>
	<tr>
		<td colspan=2 align=right>
			<font color=red>*</font> 
				<?php echo @CUSTOMER_EMAIL; ?>

		</td>
		<td>
			<input type="text" name="email" 
				value="<?php echo $this->_tpl_vars['email']; ?>
">
		</td>
	</tr>
	<?php if (! $this->_tpl_vars['SessionRefererLogin'] && @CONF_AFFILIATE_PROGRAM_ENABLED == 1): ?>
		<tr>
			<td colspan=2 align=right>
				<?php echo @CUSTOMER_AFFILIATION; ?>

			</td>
			<td>
				<input type="text" name="affiliationLogin" value="<?php echo $this->_tpl_vars['affiliationLogin']; ?>
">
			</td>
		</tr>
	<?php endif; ?>

	<!-- ADDITIONAL FIELDS -->
	<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['additional_fields']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
	<tr>
		<td colspan=2 align=right>
			<?php if ($this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_required']): ?>
				<font color=red>*</font> 
			<?php endif; ?>
			<?php echo $this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_name']; ?>
:
		</td>
		<td>
			<input type='text' name='additional_field_<?php echo $this->_tpl_vars['additional_fields'][$this->_sections['i']['index']]['reg_field_ID']; ?>
' 
				value='<?php echo $this->_tpl_vars['additional_field_values'][$this->_sections['i']['index']]['reg_field_value']; ?>
'>
		</td>
	</tr>
	<?php endfor; endif; ?>

	<tr>
		<td colspan=3 align=center>
			<table bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
 width=80%>
				<tr>
					<td>
						<font color=black class=small>
							<?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '1'): ?>
								<?php echo @STRING_ADDRESSES; ?>

							<?php else: ?>
								<?php echo @STRING_ADDRESS; ?>

							<?php endif; ?>
						</font>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<!-- DELIVERY ADDRESS (COUNTRY, AREA(STATE), INDEX, CITY, ADDRESS) -->
	<tr>
		<td colspan=3 align=center>
			
			<table>

			<tr>

				<td rowspan=7>
					<b><?php echo @STRING_SHIPPING_ADDRESS; ?>
</b>
				</td>


				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @STRING_RECEIVER_FIRST_NAME; ?>

				</td>
				<td>
					<input type=text name='receiver_first_name' 
							value='<?php echo $this->_tpl_vars['receiver_first_name']; ?>
'
							onblur='JavaScript:billingAddressCheckHandler()'
							onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>


			<tr>
				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @STRING_RECEIVER_LAST_NAME; ?>

				</td>
				<td>
					<input type=text name='receiver_last_name' 
							value='<?php echo $this->_tpl_vars['receiver_last_name']; ?>
'
							onblur='JavaScript:billingAddressCheckHandler()'
							onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>

			<?php if (@CONF_ADDRESSFORM_ADDRESS != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_ADDRESS == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_ADDRESS; ?>

				</td>
				<td>
					<textarea name="address" rows=4 
						onchange='JavaScript:billingAddressCheckHandler()'><?php echo $this->_tpl_vars['address']; ?>
</textarea>
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='address' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_CITY != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_CITY == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_CITY; ?>

				</td>
				<td>
					<input type="text" name="city" 
						value="<?php echo $this->_tpl_vars['city']; ?>
"
						onblur='JavaScript:billingAddressCheckHandler()'
						onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='city' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_STATE != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_STATE == 0): ?><font color=red>*</font><?php endif; ?> 
						<?php echo @CUSTOMER_STATE; ?>

				</td>
				<td>
					<?php if (! $this->_tpl_vars['zones']): ?>
						<input type="text" name="state" 
							value="<?php echo $this->_tpl_vars['state']; ?>
"
							onchange='JavaScript:billingAddressCheckHandler()' >
					<?php else: ?>
						<select name=zoneID
							onchange='JavaScript:billingAddressCheckHandler()' >
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['zones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['zones'][$this->_sections['i']['index']]['zoneID']; ?>

							<?php if ($this->_tpl_vars['zones'][$this->_sections['i']['index']]['zoneID'] == $this->_tpl_vars['zoneID']): ?>
								selected
							<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['zones'][$this->_sections['i']['index']]['zone_name']; ?>

							</option>
							<?php endfor; endif; ?>
						</select>
					<?php endif; ?>
				</td>
			</tr>
			<?php else: ?>
				<?php if (! $this->_tpl_vars['zones']): ?>
					<input type=hidden name='state' value=''>
				<?php else: ?>
					<input type=hidden name='zoneID' value='0'>
				<?php endif; ?>
			<?php endif; ?>


			<?php if (@CONF_ADDRESSFORM_ZIP != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_ZIP == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_ZIP; ?>

				</td>
				<td>
					<input type="text" name="zip" 
						value="<?php echo $this->_tpl_vars['zip']; ?>
"
						onblur='JavaScript:billingAddressCheckHandler()'
						onchange='JavaScript:billingAddressCheckHandler()' >
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='zip' value=''>
			<?php endif; ?>


<?php if ($this->_tpl_vars['countries']): ?>
			<tr>

				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @CUSTOMER_COUNTRY; ?>

				</td>
				<td>
					
					<select name=countryID
						onchange='JavaScript:billingAddressCheckHandler(); changeCountryHandler();'
					>
						<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID']; ?>

								<?php if ($this->_tpl_vars['countryID'] != NULL): ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == $this->_tpl_vars['countryID']): ?>
										selected
									<?php endif; ?>
								<?php else: ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == @CONF_DEFAULT_COUNTRY): ?>
										selected
									<?php endif; ?>
								<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['country_name']; ?>

							</option>
						<?php endfor; endif; ?>
		 			</select>

				</td>
			</tr>
<?php else: ?>
				<input type=hidden name='countryID'	value='NULL'>
<?php endif; ?> 
			</table>

			<?php echo '
			<script language=\'JavaScript\'>
			function billingAddressCheckHandler()
			{
				return;
			}
			</script>
			'; ?>


		</td>
	<tr>




	<?php if (@CONF_ORDERING_REQUEST_BILLING_ADDRESS == '1'): ?>

	<tr>
		<td colspan=3 align=center>
			
			<table>

			<tr>
				<td rowspan=8>
					<b><?php echo @STRING_BILLING_ADDRESS; ?>
</b>
				</td>
				<td colspan=3>
					<input type=checkbox name='billing_address_check' value='1'
								onclick='JavaScript:billingAddressCheckHandler()'
					<?php if ($this->_tpl_vars['billing_address_check']): ?>
						checked
					<?php endif; ?>
					>
					<?php echo @STRING_EQUAL_TO_SHIPPING_ADDRESS; ?>

				</td>
			</tr>

			<tr>
				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @STRING_PAYER_FIRST_NAME; ?>

				</td>
				<td>
					<input type=text name='payer_first_name' 
						value='<?php echo $this->_tpl_vars['payer_first_name']; ?>
'>
				</td>
			</tr>


			<tr>
				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @STRING_PAYER_LAST_NAME; ?>

				</td>
				<td>
					<input type=text name='payer_last_name'
						value='<?php echo $this->_tpl_vars['payer_last_name']; ?>
'>
				</td>
			</tr>

			<?php if (@CONF_ADDRESSFORM_ADDRESS != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_ADDRESS == 0): ?><font color=red>*</font><?php endif; ?> 
						<?php echo @CUSTOMER_ADDRESS; ?>

				</td>
				<td>
					<textarea name="billingAddress" rows=4 
							value='<?php echo $this->_tpl_vars['billingAddress']; ?>
'><?php echo $this->_tpl_vars['billingAddress']; ?>
</textarea>
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingAddress' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_CITY != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_CITY == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_CITY; ?>

				</td>
				<td>
					<input type="text" name="billingCity" 
						value="<?php echo $this->_tpl_vars['billingCity']; ?>
">
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingCity' value=''>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_STATE != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_STATE == 0): ?><font color=red>*</font><?php endif; ?>
						<?php echo @CUSTOMER_STATE; ?>

				</td>
				<td>
					<?php if (! $this->_tpl_vars['billingZones']): ?>
					<input type="text" name="billingState" 
						value="<?php echo $this->_tpl_vars['billingState']; ?>
">
					<?php else: ?>
						<select name=billingZoneID>
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['billingZones']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
								<option value=<?php echo $this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zoneID']; ?>

								<?php if ($this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zoneID'] == $this->_tpl_vars['billingZoneID']): ?>
									selected
								<?php endif; ?>
								>
									<?php echo $this->_tpl_vars['billingZones'][$this->_sections['i']['index']]['zone_name']; ?>

								</option>
							<?php endfor; endif; ?>
						</select>
					<?php endif; ?>
				</td>
			</tr>
			<?php else: ?>
				<?php if (! $this->_tpl_vars['zones']): ?>
					<input type=hidden name='billingState' value=''>
				<?php else: ?>
					<input type=hidden name='billingZoneID' value='0'>
				<?php endif; ?>
			<?php endif; ?>

			<?php if (@CONF_ADDRESSFORM_ZIP != 2): ?>
			<tr>
				<td colspan=2 align=right>
					<?php if (@CONF_ADDRESSFORM_ZIP == 0): ?><font color=red>*</font><?php endif; ?>
					<?php echo @CUSTOMER_ZIP; ?>

				</td>
				<td>
					<input type="text" name="billingZip" 
						value="<?php echo $this->_tpl_vars['billingZip']; ?>
">
				</td>
			</tr>
			<?php else: ?>
			<input type=hidden name='billingZip' value=''>
			<?php endif; ?>

<?php if ($this->_tpl_vars['countries']): ?>
			<tr>
				<td colspan=2 align=right>
					<font color=red>*</font> 
						<?php echo @CUSTOMER_COUNTRY; ?>

				</td>
				<td>
					
						<select name=billingCountryID
							onchange='JavaScript:changeCountryHandler();'
						>
							<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['countries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
							<option value=<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID']; ?>

								<?php if ($this->_tpl_vars['billingCountryID'] != NULL): ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == $this->_tpl_vars['billingCountryID']): ?>
										selected
									<?php endif; ?>
								<?php else: ?>
									<?php if ($this->_tpl_vars['countries'][$this->_sections['i']['index']]['countryID'] == @CONF_DEFAULT_COUNTRY): ?>
										selected
									<?php endif; ?>
								<?php endif; ?>
							>
								<?php echo $this->_tpl_vars['countries'][$this->_sections['i']['index']]['country_name']; ?>

							</option>
							<?php endfor; endif; ?>
		 				</select>

				</td>
			</tr>
<?php else: ?>
			<input type=hidden name='billingCountryID' value='NULL'>
<?php endif; ?>

			</table>

			<input type=hidden value='' name='billing_address_checkHiddenField'>

			<?php echo '
			<script language=\'JavaScript\'>
				function billingAddressCheckHandler()
				{
					if ( (document.RegisterForm.billingCountryID.value != 
							document.RegisterForm.countryID.value) && 
						 	document.RegisterForm.billing_address_check.checked )
					{
							document.RegisterForm.submit();
							return;
					}
			'; ?>


					document.RegisterForm.payer_first_name.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.payer_last_name.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingCountryID.disabled =
						document.RegisterForm.billing_address_check.checked;
					<?php if (! $this->_tpl_vars['billingZones']): ?>
						document.RegisterForm.billingState.disabled = 
							document.RegisterForm.billing_address_check.checked;
					<?php else: ?>
						document.RegisterForm.billingZoneID.disabled = 
							document.RegisterForm.billing_address_check.checked;						
					<?php endif; ?>
					document.RegisterForm.billingZip.disabled = 
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingCity.disabled =
						document.RegisterForm.billing_address_check.checked;
					document.RegisterForm.billingAddress.disabled = 
						document.RegisterForm.billing_address_check.checked;

			<?php echo '
					if ( document.RegisterForm.billing_address_check.checked )
					{
			'; ?>

						document.RegisterForm.payer_first_name.value =
							document.RegisterForm.receiver_first_name.value;
						document.RegisterForm.payer_last_name.value =
							document.RegisterForm.receiver_last_name.value;
						document.RegisterForm.billingCountryID.value =
							document.RegisterForm.countryID.value;
						<?php if (! $this->_tpl_vars['billingZones']): ?>
							document.RegisterForm.billingState.value = 
								document.RegisterForm.state.value;
						<?php else: ?>
							document.RegisterForm.billingZoneID.value = 
								document.RegisterForm.zoneID.value;
						<?php endif; ?>
						document.RegisterForm.billingZip.value = 
							document.RegisterForm.zip.value;
						document.RegisterForm.billingCity.value =
							document.RegisterForm.city.value
						document.RegisterForm.billingAddress.value = 
							document.RegisterForm.address.value; 
			<?php echo '
					}
				}

				billingAddressCheckHandler();
			</script>
			'; ?>


		</td>
	</tr>
		
	<?php endif; ?>
	<?php if (@CONF_ENABLE_CONFIRMATION_CODE): ?>
	<tr>
		<td colspan=3 align=center>
			<div class="small" style="color: black;width:80%; padding: 3px; text-align: left; background-color:#<?php echo @CONF_MIDDLE_COLOR; ?>
">
				<?php echo @STR_CONFIRMATION_CODE; ?>

			</div>
		</td>
	</tr>
	<tr>
		<td align="right">
			<img src="./imgval.php" alt="code" align="right" border="0" />
		</td>
		<td>
		</td>
		<td align="left">
			<input name="fConfirmationCode" style="color:#aaaaaa" value="<?php echo @STR_ENTER_CCODE; ?>
" type="text" onfocus="if(this.value=='<?php echo @STR_ENTER_CCODE; ?>
')
			<?php echo '
			{this.style.color=\'#000000\';this.value=\'\';}
			'; ?>
" onblur="if(this.value=='')
			<?php echo '{'; ?>
this.style.color='#aaaaaa';this.value='<?php echo @STR_ENTER_CCODE; ?>
'<?php echo '}'; ?>
" />
		</td>
	</tr>
	<?php endif; ?>
</table>

<?php echo '
		<script language=\'JavaScript\'>

		function changeCountryHandler()
		{
				document.RegisterForm.submit();
		}

		</script>
'; ?>


<p>
<input type="submit" value="<?php echo @OK_BUTTON; ?>
" name=save>


<input type=reset value="<?php echo @RESET_BUTTON; ?>
">
</p>

<input type=hidden name=quick_register value=1>

</form>


	</td>


</tr>
</table>
</center>