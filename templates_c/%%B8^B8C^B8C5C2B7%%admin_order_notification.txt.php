<?php /* Smarty version 2.6.10, created on 2013-11-06 08:31:55
         compiled from admin_order_notification.txt */ ?>
<?php echo @STRING_ORDER_ID; ?>
: <?php echo $this->_tpl_vars['orderID']; ?>

<?php echo @TABLE_CUSTOMER; ?>
: <?php echo $this->_tpl_vars['customer_firstname']; ?>
 <?php echo $this->_tpl_vars['customer_lastname']; ?>

<?php echo @CUSTOMER_EMAIL; ?>
 <?php echo $this->_tpl_vars['customer_email']; ?>

<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['customer_add_fields']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<?php echo $this->_tpl_vars['customer_add_fields'][$this->_sections['i']['index']]['reg_field_name']; ?>
: <?php echo $this->_tpl_vars['customer_add_fields'][$this->_sections['i']['index']]['reg_field_value']; ?>

<?php endfor; endif; ?>
<?php echo @ADMIN_IP_ADDRESS; ?>
: <?php echo $this->_tpl_vars['customer_ip']; ?>

<?php echo @STRING_ORDER_TIME; ?>
: <?php echo $this->_tpl_vars['order_time']; ?>

<?php echo @STRING_CUSTOMER_COMMENTS; ?>
: <?php echo $this->_tpl_vars['customer_comments']; ?>

<?php echo @STRING_ORDER_CONTENT; ?>
:

<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['content']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<?php if ($this->_tpl_vars['content'][$this->_sections['i']['index']]['product_code']): ?>[<?php echo $this->_tpl_vars['content'][$this->_sections['i']['index']]['product_code']; ?>
] <?php endif;  echo $this->_tpl_vars['content'][$this->_sections['i']['index']]['name']; ?>
 (x<?php echo $this->_tpl_vars['content'][$this->_sections['i']['index']]['Quantity']; ?>
): <?php echo $this->_tpl_vars['content'][$this->_sections['i']['index']]['Price']; ?>

<?php endfor; endif; ?>

<?php echo @ADMIN_DISCOUNT; ?>
: <?php echo $this->_tpl_vars['discount']; ?>

<?php echo @STRING_SHIPPING_TYPE; ?>
: <?php echo $this->_tpl_vars['shipping_type']; ?>

<?php echo @ADMIN_SHIPPING_COST; ?>
: <?php echo $this->_tpl_vars['shipping_cost']; ?>

<?php echo @STRING_RECEIVER; ?>
: <?php echo $this->_tpl_vars['shipping_firstname']; ?>
 <?php echo $this->_tpl_vars['shipping_lastname']; ?>

<?php echo @STRING_SHIPPING_ADDRESS; ?>
: <?php if ($this->_tpl_vars['shipping_address'] != ""):  echo $this->_tpl_vars['shipping_address']; ?>
,<?php endif; ?> <?php if ($this->_tpl_vars['shipping_city'] != ""):  echo $this->_tpl_vars['shipping_city']; ?>
,<?php endif; ?> <?php if ($this->_tpl_vars['shipping_state'] != ""):  echo $this->_tpl_vars['shipping_state'];  endif; ?> <?php if ($this->_tpl_vars['shipping_zip'] != ""):  echo $this->_tpl_vars['shipping_zip'];  endif; ?> <?php if ($this->_tpl_vars['shipping_country'] != ""):  echo $this->_tpl_vars['shipping_country'];  endif; ?>

<?php echo @STRING_PAYMENT_TYPE; ?>
: <?php echo $this->_tpl_vars['payment_type']; ?>

<?php echo @ADMIN_PAYER; ?>
: <?php echo $this->_tpl_vars['billing_firstname']; ?>
 <?php echo $this->_tpl_vars['billing_lastname']; ?>

<?php echo @ADMIN_SCORE_DELIVERY_ADDRESS; ?>
: <?php if ($this->_tpl_vars['billing_address'] != ""):  echo $this->_tpl_vars['billing_address']; ?>
,<?php endif; ?> <?php if ($this->_tpl_vars['billing_city'] != ""):  echo $this->_tpl_vars['billing_city']; ?>
,<?php endif; ?> <?php if ($this->_tpl_vars['billing_state'] != ""):  echo $this->_tpl_vars['billing_state'];  endif; ?> <?php if ($this->_tpl_vars['billing_zip'] != ""):  echo $this->_tpl_vars['billing_zip'];  endif; ?> <?php if ($this->_tpl_vars['billing_country'] != ""):  echo $this->_tpl_vars['billing_country'];  endif; ?>

<?php echo @STRING_TAX; ?>
: <?php echo $this->_tpl_vars['total_tax']; ?>

<?php echo @TABLE_TOTAL; ?>
 <?php echo $this->_tpl_vars['order_amount']; ?>