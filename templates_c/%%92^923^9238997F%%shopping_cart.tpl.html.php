<?php /* Smarty version 2.6.10, created on 2013-11-14 23:13:23
         compiled from shopping_cart.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'counter', 'shopping_cart.tpl.html', 56, false),array('modifier', 'replace', 'shopping_cart.tpl.html', 113, false),)), $this); ?>

<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>

<html>

<head>

<link rel=STYLESHEET href="style.css" type="text/css">

<meta http-equiv="Content-Type" content="text/html; charset=<?php echo @DEFAULT_CHARSET; ?>
">
<title><?php echo @CART_TITLE; ?>
</title>

</head>

<body>

<?php endif; ?>

<center>

<?php if ($this->_tpl_vars['cart_content']): ?>

	<table width=75% border=0>

		<tr>
			<td>
				<b><?php echo @CART_TITLE; ?>
:</b>
			</td>

			<td align=right>
				<a href="<?php echo $this->_tpl_vars['cart_php_file']; ?>
?shopping_cart=yes&clear_cart=yes">
					<img src="images/remove.jpg" border=0 > 
						<?php echo @CART_CLEAR; ?>

					
				</a>
			</td>

		</tr>

		<?php if ($this->_tpl_vars['make_more_exact_cart_content']): ?>
		<tr>
			<td colspan=2>
				<font color=red>
					<b><?php echo @STRING_MAKE_MORE_EXACT_CART_CONTENT; ?>
</b>
				</font>
			</td>
		</tr>
		<?php endif; ?>

	</table>

	<?php if ($this->_tpl_vars['cart_amount'] < @CONF_MINIMAL_ORDER_AMOUNT): ?>
	<div id="id_too_small_order_amount" class="error_message"<?php if (! $this->_tpl_vars['minOrder']): ?> style="display:none;"<?php endif; ?>><?php echo @CART_TOO_SMALL_ORDER_AMOUNT; ?>
 <?php echo $this->_tpl_vars['cart_min']; ?>
</div>
	<?php endif; ?>
	<?php echo smarty_function_counter(array('name' => 'product_count','start' => 1,'skip' => 1,'print' => false), $this);?>


	<form action="<?php echo $this->_tpl_vars['cart_php_file']; ?>
?shopping_cart=yes" method=post>

	<table width=<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>100<?php else: ?>75<?php endif; ?>% border=0 cellspacing=1 cellpadding=2 bgcolor=#<?php echo @CONF_DARK_COLOR; ?>
>
		<tr align=center bgcolor=#<?php echo @CONF_MIDDLE_COLOR; ?>
>
			<td>
				<?php echo @TABLE_PRODUCT_NAME; ?>

			</td>
			<td>
				<?php echo @TABLE_PRODUCT_QUANTITY; ?>

			</td>
			<td>
				<?php echo @TABLE_PRODUCT_COST; ?>
, <?php echo $this->_tpl_vars['currency_name']; ?>

			</td>
			<td width=20>
			</td>
		</tr>

	<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>
		<?php $this->assign('ProductsNum', 0); ?>
	<?php endif; ?>
	 <?php unset($this->_sections['i']);
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['cart_content']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>

		<?php echo smarty_function_counter(array('name' => 'product_count','print' => false), $this);?>


		<tr bgcolor=white>
			<td>
				<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>
					<b><?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['name']; ?>
</b>
				<?php else: ?>
					<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['name']; ?>

				<?php endif; ?>
			</td>

			<td align=center>
				<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>
					<?php $this->assign('ProductsNum', $this->_tpl_vars['ProductsNum']+$this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['quantity']); ?>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['session_items']): ?>
					<input type="text" name="count_<?php echo $this->_tpl_vars['session_items'][$this->_sections['i']['index']]; ?>
" size=5 value="<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['quantity']; ?>
">
				<?php else: ?>
					<input type="text" name="count_<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['id']; ?>
" size=5 value="<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['quantity']; ?>
">
				<?php endif; ?>
				<br>
				<?php if ($this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['min_order_amount']): ?>
					<font color=red>
						<b>
							<?php echo @STRING_MIN_ORDER_AMOUNT; ?>
 
								<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['min_order_amount']; ?>
 
							<?php echo @STRING_ITEM; ?>

						</b>
					</font>
				<?php endif; ?>
			</td>

			<td align=center>
				<?php echo ((is_array($_tmp=$this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['cost'])) ? $this->_run_mod_handler('replace', true, $_tmp, '&amp;', '&') : smarty_modifier_replace($_tmp, '&amp;', '&')); ?>

			</td>
			<?php if ($this->_tpl_vars['session_items']): ?>
				<td align=center>
					<a href="<?php echo $this->_tpl_vars['cart_php_file']; ?>
?shopping_cart=yes&remove=<?php echo $this->_tpl_vars['session_items'][$this->_sections['i']['index']]; ?>
"><img src="images/remove.jpg" border=0 alt="<?php echo @DELETE_BUTTON; ?>
">
					</a>
				</td>
			<?php else: ?>
				<td align=center>
					<a href="<?php echo $this->_tpl_vars['cart_php_file']; ?>
?shopping_cart=yes&remove=<?php echo $this->_tpl_vars['cart_content'][$this->_sections['i']['index']]['id']; ?>
"><img src="images/remove.jpg" border=0 alt="<?php echo @DELETE_BUTTON; ?>
">
					</a>
				</td>
			<?php endif; ?>
		</tr>
	 <?php endfor; endif; ?>

	<?php if ($this->_tpl_vars['discount_prompt'] != 0): ?>

		<?php echo smarty_function_counter(array('name' => 'product_count','print' => false), $this);?>


		<?php if ($this->_tpl_vars['discount_prompt'] == 1 && $this->_tpl_vars['discount_percent'] != 0): ?>
		<tr bgcolor=white>
			<td colspan=2>
				<?php echo @ADMIN_DISCOUNT; ?>
 <?php echo $this->_tpl_vars['discount_percent']; ?>

			</td>
			<td align=center>
				<?php echo $this->_tpl_vars['discount_value']; ?>
	
			</td>
			<td>&nbsp;
				
			</td>
		</tr>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['discount_prompt'] == 2): ?>

		<?php echo smarty_function_counter(array('name' => 'product_count','print' => false), $this);?>


		<tr bgcolor=white>
			<td colspan=4>
				<?php echo @STRING_UNREGISTERED_CUSTOMER_DISCOUNT_PROMPT; ?>

			</td>
		</tr>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['discount_prompt'] == 3 && $this->_tpl_vars['discount_percent'] != 0): ?>

		<?php echo smarty_function_counter(array('name' => 'product_count','print' => false), $this);?>


		<tr bgcolor=white>
			<td colspan=2>
				<?php echo @ADMIN_DISCOUNT; ?>
 <?php echo $this->_tpl_vars['discount_percent']; ?>

				<br>
				<?php echo @STRING_UNREGISTERED_CUSTOMER_COMBINED_DISCOUNT_PROMPT; ?>

			</td>
			<td align=center>
				<?php echo $this->_tpl_vars['discount_value']; ?>
	
			</td>
			<td>&nbsp;
				
			</td>
		</tr>
		<?php endif; ?>

	<?php endif; ?>


	<tr bgcolor=white>
		<td>
			<b><?php echo @TABLE_TOTAL; ?>
</b>
		</td>
		<td>
			<br><br>
		</td>
		<td bgcolor=#<?php echo @CONF_LIGHT_COLOR; ?>
 align=center>
			<b><?php echo $this->_tpl_vars['cart_total']; ?>
</b>
			
		</td>
		<td></td>
	</tr>
</table>

	<input type=hidden name=update value=1>
	<input type=hidden name=shopping_cart value=1>

	<p>
	<table width=75% border=0>
		<tr>
			<td align=right>
				<input type="submit" value="<?php echo @UPDATE_BUTTON; ?>
">
			</td>
		</tr>
	</table>
	</form>

	<form action="index.php" method=get>
		<table width=75% border=0>
			<tr>
				<td align=center>
				 <?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>
					<input type="button" 
						value="<?php echo @CLOSE_BUTTON; ?>
" 
						onClick="JavaScript:window.close();">
				 <?php else: ?>
					<input type="button" 
						value="<?php echo @STRING_BACK_TO_SHOPPING; ?>
" 
						onClick="JavaScript:window.location='<?php if ($this->_tpl_vars['back_to_product_url'] != ''):  echo $this->_tpl_vars['back_to_product_url'];  else:  echo $this->_tpl_vars['cart_php_file'];  endif; ?>';">
				 <?php endif; ?>
				</td>
				<td align=center>
					<input type="button" value="<?php echo @CART_PROCEED_TO_CHECKOUT; ?>
" onClick="<?php if ($this->_tpl_vars['cart_amount'] < @CONF_MINIMAL_ORDER_AMOUNT): ?>document.getElementById('id_too_small_order_amount').style.display='block';return false;<?php endif; ?>window<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>.opener<?php endif; ?>.location='index.php?<?php if ($this->_tpl_vars['log'] != NULL): ?>order2_shipping=yes&shippingAddressID=<?php echo $this->_tpl_vars['shippingAddressID'];  else: ?>register_authorization=yes<?php endif; ?>';<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>window.close();<?php endif; ?>">
				</td>
			</tr>
		</table>
	</form>

<?php else: ?>

<p><font><?php echo @CART_EMPTY; ?>
</font>

<?php endif; ?>

</center>

<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>

<script>
<?php echo '
function adjust_cart_window(counter)
{
	var scr_h = screen.availHeight - 50;
	var wnd_h = 215 + counter*30;
	window.resizeTo( 400, Math.min(scr_h, wnd_h) );
	
}
'; ?>

	adjust_cart_window(<?php echo smarty_function_counter(array('name' => 'product_count'), $this);?>
);
	<?php if ($this->_tpl_vars['this_is_a_popup_cart_window']): ?>
		<?php if ($this->_tpl_vars['ProductsNum']): ?>
			window.opener.document.getElementById('shpcrtgc').innerHTML="<?php echo $this->_tpl_vars['ProductsNum']; ?>
 <?php echo @STRING_PRODUCTS; ?>
"
			window.opener.document.getElementById('shpcrtca').innerHTML='<?php echo $this->_tpl_vars['cart_total']; ?>
'
		<?php else: ?>
			window.opener.document.getElementById('shpcrtgc').innerHTML="<?php echo @CART_CONTENT_EMPTY; ?>
"
			window.opener.document.getElementById('shpcrtca').innerHTML="<br />"
		<?php endif; ?>
	<?php endif; ?>
</script>

</body>
</html>

<?php endif; ?>