<?php /* Smarty version 2.6.10, created on 2013-11-06 08:30:13
         compiled from order2_shipping.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'count', 'order2_shipping.tpl.html', 32, false),)), $this); ?>

<h1><u><?php echo @STRING_ORDERING; ?>
</u></h1>
<h3><?php echo @STRING_ORDER_SHIPPING; ?>
</h3>

<table border="0" cellspacing="1" cellpadding="5">
	<tr>
		<td>
			<p><?php echo @STRING_SHIPPING_ADDRESS; ?>
:<br>
			<?php if ($this->_tpl_vars['shippingAddressID'] > 0): ?><b><?php echo $this->_tpl_vars['strAddress']; ?>
</b><?php else:  echo @STRING_ADDRESS_NOT_SPECIFIED;  endif; ?>
			<p>
			<?php if ($this->_tpl_vars['defaultBillingAddressID']): ?>
				<a href='index.php?change_address=yes&shippingAddressID=<?php echo $this->_tpl_vars['shippingAddressID']; ?>
&defaultBillingAddressID=<?php echo $this->_tpl_vars['defaultBillingAddressID']; ?>
'>
			<?php else: ?>
				<a href='index.php?change_address=yes&shippingAddressID=<?php echo $this->_tpl_vars['shippingAddressID']; ?>
'>
			<?php endif; ?>
				<?php echo @STRING_CHANGE_ADDRESS; ?>

			</a>
		</td>
	</tr>
</table>

<p><?php echo @STRING_SELECT_ORDER_SHIPPING_METHOD_PROMPT; ?>
:</p>

<form name='MainForm' method=POST>

	<?php if ($this->_tpl_vars['shipping_methods']): ?>
	<table border="0" cellspacing="1" cellpadding="4">

		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['shipping_methods']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<?php if ($this->_tpl_vars['shipping_costs'][$this->_sections['i']['index']] != "n/a"): ?>
		<?php echo smarty_function_count(array('item' => '_CostsNum','array' => $this->_tpl_vars['shipping_costs'][$this->_sections['i']['index']]), $this);?>

		<tr>

			<td valign=top>
				<input type="radio" name="select_shipping_method"
				onclick="JavaScript:select_shipping_methodClickHandler()"
				value="<?php echo $this->_tpl_vars['shipping_methods'][$this->_sections['i']['index']]['SID']; ?>
"
				<?php if ($this->_tpl_vars['shipping_methods_count'] == 1): ?> checked<?php endif; ?>>
			</td>

			<td valign=top>
				<b><?php echo $this->_tpl_vars['shipping_methods'][$this->_sections['i']['index']]['Name']; ?>
</b>
				<br>
				<?php echo $this->_tpl_vars['shipping_methods'][$this->_sections['i']['index']]['description']; ?>

			</td>

			<td valign=top>
				<?php if ($this->_tpl_vars['_CostsNum'] > 1): ?>
					<select name="shServiceID[<?php echo $this->_tpl_vars['shipping_methods'][$this->_sections['i']['index']]['SID']; ?>
]">
					<?php $_from = $this->_tpl_vars['shipping_costs'][$this->_sections['i']['index']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_Rate']):
?>
						<option value="<?php echo $this->_tpl_vars['_Rate']['id']; ?>
"><?php echo $this->_tpl_vars['_Rate']['name']; ?>
 - <?php echo $this->_tpl_vars['_Rate']['rate']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select>
				<?php else: ?>
					<?php echo $this->_tpl_vars['shipping_costs'][$this->_sections['i']['index']][0]['rate']; ?>

				<?php endif; ?>
			</td>

		</tr>
		<?php endif; ?>
		<?php endfor; endif; ?>

	</table>
	<?php else: ?>
		<font color=red><b><?php echo @STRING_NO_SHIPPING_METHODS_TO_SELECT; ?>
...</b></font>
	<?php endif; ?>

	<?php echo '
	<script language=\'JavaScript\'>
	function select_shipping_methodClickHandler()
	{
	'; ?>


		<?php if ($this->_tpl_vars['shippingAddressID'] > 0): ?>

		 <?php if ($this->_tpl_vars['shipping_methods_count'] > 1): ?>
			document.MainForm.continue_button.disabled = true;
			for( i=0; i<<?php echo $this->_tpl_vars['shipping_methods_count']; ?>
; i++ )
			<?php echo '
			{
				if(document.MainForm.select_shipping_method[i]){
					if ( document.MainForm.select_shipping_method[i].checked )
					{
						document.MainForm.continue_button.disabled = false;
						break;
					}
				}else{
					
					if(document.MainForm.select_shipping_method){
						
						if ( document.MainForm.select_shipping_method.checked ){
							
							document.MainForm.continue_button.disabled = false;
							break;
						}
					}
				}
			}
			'; ?>

		 <?php endif; ?>
		<?php endif; ?>
	<?php echo '
	}

	</script>
	'; ?>



	<p>
		<?php if ($this->_tpl_vars['shipping_methods']): ?>
		  
			<input type="submit" name="continue_button" <?php if ($this->_tpl_vars['shippingAddressID'] == 0): ?>disabled<?php endif; ?> value="<?php echo @CONTINUE_BUTTON; ?>
 &gt;&gt;">
			<?php if ($this->_tpl_vars['shippingAddressID'] == 0): ?><br><font color=red><?php echo @STRING_PLEASE_SPECIFY_ADDRESS; ?>
</font><?php endif; ?>
		<?php endif; ?>
	</p>
<?php if ($this->_tpl_vars['shipping_methods_count'] > 1): ?>
	<script language='JavaScript'>
		select_shipping_methodClickHandler();
	</script>
<?php endif; ?>
</form>