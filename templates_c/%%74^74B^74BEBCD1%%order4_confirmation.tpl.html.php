<?php /* Smarty version 2.6.10, created on 2013-11-06 08:31:55
         compiled from order4_confirmation.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'replace', 'order4_confirmation.tpl.html', 105, false),)), $this); ?>
<?php if ($this->_tpl_vars['order_success']): ?>

	<b>
		<center>
			<?php echo @STRING_ORDER_PLACED; ?>

		</center>
	</b>


	<?php if ($this->_tpl_vars['after_processing_html']): ?>
		<p><?php echo $this->_tpl_vars['after_processing_html']; ?>

	<?php endif; ?>

<?php else: ?>

<?php if ($this->_tpl_vars['orderSum'] == NULL): ?>

	<b><center><?php echo @ERROR_CANT_FIND_REQUIRED_PAGE; ?>
</center></b>

<?php else: ?>


 <?php if ($this->_tpl_vars['orderSum']['orderContentCartProductsCount'] == 0): ?>
	
	<b><center><?php echo @CART_EMPTY; ?>
</center></b>

 <?php else: ?>


<h1><u><?php echo @STRING_ORDERING; ?>
</u></h1>
<h3><?php echo @STRING_ORDER_CONFIRMATION; ?>
</h3>

<?php if ($this->_tpl_vars['payment_error']): ?>
<p><font color=red><b><?php echo @ADMIN_PAYMENT_ERROR;  if ($this->_tpl_vars['payment_error'] != 1): ?>: <?php echo $this->_tpl_vars['payment_error'];  endif; ?></b></font>
<?php endif; ?>
<p>
<table width=95%>

	<tr>
	<td>

	<table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="<?php echo @CONF_DARK_COLOR; ?>
">

		<tr bgcolor="<?php echo @CONF_MIDDLE_COLOR; ?>
"> 
			<td><strong><?php echo @TABLE_PRODUCT_NAME; ?>
</strong></td>
			<td align="center"><strong><?php echo @TABLE_PRODUCT_QUANTITY; ?>
</strong></td>
			<td align="center"><strong><?php echo @STRING_TAX; ?>
</strong></td>
			<td align="center"><strong><?php echo @TABLE_PRODUCT_COST_WITHOUT_TAX; ?>
</strong></td>
		</tr>


		<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['orderSum']['sumOrderContent']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
$this->_sections['i']['start'] = $this->_sections['i']['step'] > 0 ? 0 : $this->_sections['i']['loop']-1;
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = $this->_sections['i']['loop'];
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
		<tr bgcolor=white>
			<td><?php echo $this->_tpl_vars['orderSum']['sumOrderContent'][$this->_sections['i']['index']]['name']; ?>
		</td>
			<td align=center><?php echo $this->_tpl_vars['orderSum']['sumOrderContent'][$this->_sections['i']['index']]['quantity']; ?>
	</td>
			<td align=center><?php echo $this->_tpl_vars['orderSum']['sumOrderContent'][$this->_sections['i']['index']]['tax']; ?>
%		</td>
			<td align=center><?php echo $this->_tpl_vars['orderSum']['sumOrderContent'][$this->_sections['i']['index']]['cost']; ?>
		</td>
		</tr>
		<?php endfor; endif; ?>

	</table>

	</td>
	</tr>

	<tr>
	<td>
<table width="100%" border="0" cellspacing="1" cellpadding="5">

	<?php if ($this->_tpl_vars['orderSum']['discount'] > 0): ?>
	<tr>
		<td width="80%" align="right"><?php echo @ADMIN_DISCOUNT; ?>
:</td>
		<td width="20%" align="right"><?php echo $this->_tpl_vars['orderSum']['discount_percent']; ?>
</td>
	</tr>
	<?php endif; ?>

	<tr>
		<td width="80%" align="right"><?php echo @STRING_PRED_TOTAL; ?>
:</td>
		<td width="20%" align="right"><?php echo $this->_tpl_vars['orderSum']['pred_total_disc']; ?>
</td>
	</tr>
	<tr>
		<td width="80%" align="right"><?php echo @STRING_TAX; ?>
:</td>
		<td width="20%" align="right"><?php echo $this->_tpl_vars['orderSum']['totalTax']; ?>
</td>
	</tr>
	<tr>
		<td width="80%" align="right"><?php echo @STRING_SHIPPING_TYPE; ?>
 (<?php echo $this->_tpl_vars['orderSum']['shipping_name']; ?>
):</td>
		<td width="20%" align="right"><?php echo $this->_tpl_vars['orderSum']['shipping_cost']; ?>
</td>
	</tr>
	<tr>
		<td width="80%" align="right"><strong><?php echo @TABLE_TOTAL; ?>
</strong></td>
		<td width="20%" align="right"><strong><?php echo $this->_tpl_vars['orderSum']['total']; ?>
</strong></td>
	</tr>
</table>

	</td>
	</tr>

</table>

<?php if ($this->_tpl_vars['orderSum']['shipping_name'] != "-"): ?>

	<p>
		<?php echo @STRING_SHIPPING_TYPE; ?>
: 
		<b><?php echo ((is_array($_tmp=$this->_tpl_vars['orderSum']['shipping_name'])) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")); ?>
</b>
	<br>
		<?php echo @STRING_TARGET_SHIPPING_ADDRESS; ?>
:<br>
		<b><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['orderSum']['shipping_address'])) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")))) ? $this->_run_mod_handler('replace', true, $_tmp, "&lt;br>", "<br>") : smarty_modifier_replace($_tmp, "&lt;br>", "<br>")); ?>
</b>
	</p>

<?php endif; ?>

<?php if ($this->_tpl_vars['orderSum']['payment_name'] != "-"): ?>

	<p>
		<?php echo @STRING_PAYMENT_TYPE; ?>
: 
		<b><?php echo ((is_array($_tmp=$this->_tpl_vars['orderSum']['payment_name'])) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")); ?>
</b>
		<br>
		<?php echo @STRING_BILLING_ADDRESS; ?>
:<br>
		<b><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['orderSum']['billing_address'])) ? $this->_run_mod_handler('replace', true, $_tmp, "<", "&lt;") : smarty_modifier_replace($_tmp, "<", "&lt;")))) ? $this->_run_mod_handler('replace', true, $_tmp, "&lt;br>", "<br>") : smarty_modifier_replace($_tmp, "&lt;br>", "<br>")); ?>
</b>
	</p>

<?php endif; ?>

<form name='MainForm' method=POST>

	<p>
		<?php echo $this->_tpl_vars['orderSum']['payment_form_html']; ?>

	</p>

	<p>
		<?php echo @STRING_ORDER_COMMENT; ?>
:<br>
		<textarea name="order_comment" cols="40" rows="5"></textarea>
	</p>

	<p align="left"> 
		<input type="submit" name="submit" value="<?php echo @STRING_FORMALIZE_ORDER; ?>
">
	</p>

	<input type=hidden name='totalUC' value='<?php echo $this->_tpl_vars['totalUC']; ?>
'>

</form>

<?php endif; ?>

<?php endif; ?>

<?php endif; ?>