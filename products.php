<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	//ADMIN :: products managment

	include("./cfg/connect.inc.php");
	include("./includes/database/".DBMS.".php");
	include("./core_functions/category_functions.php");
	include("./core_functions/product_functions.php");
	include("./core_functions/picture_functions.php");
	include("./core_functions/configurator_functions.php");
	include("./core_functions/datetime_functions.php");
	include("./core_functions/tax_function.php");
	include("./core_functions/setting_functions.php" );
	include( "./core_functions/functions.php" );
	include( "./core_functions/country_functions.php" );

	//authorized access check
	session_start();

	@set_time_limit(0);
	MagicQuotesRuntimeSetting();

	//connect 2 database
	db_connect(DB_HOST,DB_USER,DB_PASS) or die (db_error());
	db_select_db(DB_NAME) or die (db_error());

	settingDefineConstants();

	//current language
	include("./cfg/language_list.php");
	if (!isset($_SESSION["current_language"]) ||
		$_SESSION["current_language"] < 0 || $_SESSION["current_language"] > count($lang_list))
			$_SESSION["current_language"] = 0; //set default language
	//include a language file
	if (isset($lang_list[$_SESSION["current_language"]]) &&
		file_exists("languages/".$lang_list[$_SESSION["current_language"]]->filename))
	{
		//include current language file
		include("languages/".$lang_list[$_SESSION["current_language"]]->filename);
	}
	else
	{
		die("<font color=red><b>ERROR: Couldn't find language file!</b></font>");
	}

	include("./checklogin.php");
	if ( CONF_BACKEND_SAFEMODE != 1 && (!isset($_SESSION["log"]) || strcmp($_SESSION["log"],ADMIN_LOGIN))) //unauthorized
	{
		die (ERROR_FORBIDDEN);
	}


	// several function

	// *****************************************************************************
	// Purpose	gets size
	// Inputs    
	// Remarks
	// Returns	
	function GetPictureSize( $filename )
	{
		$size_info=getimagesize("./products_pictures/".$filename);
		return ((string)($size_info[0] + 40 )).", ".((string)($size_info[1] + 40 ));
	}

	// *****************************************************************************
	// Purpose	gets client JavaScript to reload opener page
	// Inputs    
	// Remarks
	// Returns	
	function ReLoadOpener()
	{
		if ( $_GET["productID"]==0 )
			$categoryID=$_POST["categoryID"];
		else
		{
			$q=db_query("select categoryID from ".PRODUCTS_TABLE.
				" where productID='".$_GET["productID"]."'");
			$r=db_fetch_row($q);
			$categoryID=$r["categoryID"];
		}
		echo("<script language='JavaScript'>");
		echo("	try");
		echo("	{");
		echo("		window.opener.location.reload();");
		echo("	}");
		echo("	catch(e) { }");
		echo("</script>");
	}

	// *****************************************************************************
	// Purpose	gets client JavaScript to close page
	// Inputs    
	// Remarks
	// Returns	
	function CloseWindow()
	{
		echo("<script language='JavaScript'>");
		echo("	window.close();\n");
		echo("</script>");
	}


	// *****************************************************************************
	// Purpose	gets client JavaScript to open in new window 
	//							option_value_configurator.php
	// Inputs    
	// Remarks
	// Returns	
	function OpenConfigurator($optionID, $productID)
	{
		$url = "option_value_configurator.php?optionID=".$optionID."&productID=".$productID;
		echo("<script language='JavaScript'>\n");
		echo("		w=400; \n");
		echo("		h=400; \n");
		echo("		link='".$url."'; \n");
		echo("		var win = 'width='+w+',height='+h+',menubar=no,location=no,resizable=yes,scrollbars=yes';\n");
		echo("		wishWin = window.open(link,'wishWin',win);\n");
		echo("</script>\n");
	}

	if ( isset($_GET["delete"]) )
	{

		if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
		{
			Redirect("products.php?safemode=yes&productID=".$_GET["productID"]);
		}
		DeleteProduct( $_GET["productID"] );
		CloseWindow();
		ReLoadOpener();
	}

	if (!isset($_GET["productID"])) $_GET["productID"]=0;
	$_GET["productID"] = (int)$_GET["productID"];
	$productID = $_GET["productID"];

	if (isset($_POST) && count($_POST)>0)
	{
		if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
		{
			Redirect("products.php?safemode=yes&productID=".$productID);
		}
	}

	if ( !isset($_POST["eproduct_available_days"]) )
		$_POST["eproduct_available_days"] = 365;
	if ( !isset($_POST["eproduct_download_times"]) )
		$_POST["eproduct_download_times"] = 1;

	if ( isset($_POST["eproduct_download_times"]) )
		$_POST["eproduct_download_times"] = (int)$_POST["eproduct_download_times"];

	// save product
	if ( isset($_POST["save_product"]) )
	{
		if($_POST["seolink"] == '') $_POST["seolink"] = strToURL($_POST["name"]);
		if ( $_GET["productID"] == 0 )
		{
			$productID = AddProduct( 
				$_POST["categoryID"], $_POST["name"], $_POST["price"], $_POST["description"], 
				$_POST["in_stock"], 
				$_POST["brief_description"], $_POST["list_price"],
				$_POST["product_code"], $_POST["sort_order"],
				isset($_POST["ProductIsProgram"]), "eproduct_filename",
				$_POST["eproduct_available_days"], 
				$_POST["eproduct_download_times"], 
				$_POST["weight"], $_POST["meta_description"], 
				$_POST["meta_keywords"], isset($_POST["free_shipping"]),
				$_POST["min_order_amount"], $_POST["shipping_freight"],
				$_POST["tax_class"],1, $_POST["seolink"], $_POST["meta_title"] );
			$_GET["productID"] = $productID;
			$updatedValues = ScanPostVariableWithId( array( "option_value", "option_radio_type" ) );
			cfgUpdateOptionValue($productID, $updatedValues);
		}
		else
		{
			UpdateProduct( $productID, 
				$_POST["categoryID"], $_POST["name"], $_POST["price"], $_POST["description"], 
				$_POST["in_stock"], $_POST["rating"],
				$_POST["brief_description"], $_POST["list_price"],
				$_POST["product_code"], $_POST["sort_order"],
				isset($_POST["ProductIsProgram"]), "eproduct_filename",
				$_POST["eproduct_available_days"], 
				$_POST["eproduct_download_times"],
				$_POST["weight"], $_POST["meta_description"], 
				$_POST["meta_keywords"], isset($_POST["free_shipping"]),
				$_POST["min_order_amount"], $_POST["shipping_freight"],
				$_POST["tax_class"], 1, $_POST["seolink"], $_POST["meta_title"] );
			$updatedValues = ScanPostVariableWithId( array( "option_value", "option_radio_type" ) );
			cfgUpdateOptionValue($productID, $updatedValues);
		}

		if ( CONF_UPDATE_GCV == '1' )
			update_products_Count_Value_For_Categories(1);



		ReLoadOpener();

		if ( $_POST["save_product_without_closing"]=="0" )
			CloseWindow();
	}

	// save pictures
	if ( isset( $_POST["save_pictures"] ) )
	{
		if ( $_GET["productID"] == 0 )
		{
			$productID = AddProduct( 
				$_POST["categoryID"], $_POST["name"], $_POST["price"], $_POST["description"], 
				$_POST["in_stock"], 
				$_POST["brief_description"], $_POST["list_price"],
				$_POST["product_code"], $_POST["sort_order"], 
				isset($_POST["ProductIsProgram"]), "eproduct_filename",
				$_POST["eproduct_available_days"], 
				$_POST["eproduct_download_times"],
				$_POST["weight"], $_POST["meta_description"], 
				$_POST["meta_keywords"], isset($_POST["free_shipping"]),
				$_POST["min_order_amount"], $_POST["shipping_freight"],
				$_POST["tax_class"] , 1, $_POST["seolink"], $_POST["meta_title"]);
			$_GET["productID"] = $productID;
		}
		
			AddNewPictures( $_GET["productID"], 
				"new_filename", "new_thumbnail", "new_enlarged", $_POST["default_picture"] );
		
		$updatedFileNames = ScanPostVariableWithId( 
			array( "filename", "thumbnail", "enlarged" ) );
		UpdatePictures( $_GET["productID"], $updatedFileNames, $_POST["default_picture"] );

		ReLoadOpener();
	}


	// delete three picture
	if ( isset( $_GET["delete_pictures"] ) )
	{
		if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
		{
			Redirect("products.php?safemode=yes&productID=".$productID);
		}

		DeleteThreePictures( $_GET["photoID"] );
		ReLoadOpener();
	}

	// delete one picture
	if ( isset( $_GET["delete_one_picture"] ) )
	{
		if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
		{
			Redirect("products.php?safemode=yes&productID=".$productID);
		}
		if ( isset( $_GET["filename"] ) )
			DeleteFilenamePicture( $_GET["filename"] );
		if ( isset( $_GET["thumbnail"] ) )
			DeleteThumbnailPicture( $_GET["thumbnail"] );
		if ( isset( $_GET["enlarged"] ) )
			DeleteEnlargedPicture( $_GET["enlarged"] );
		ReLoadOpener();
	}

	// add new product and open configurator 
	// it works when user click "setting..." and new product is added
	if ( isset($_POST["AddProductAndOpenConfigurator"]) )
	{
		$productID = AddProduct( 
				$_POST["categoryID"], $_POST["name"], $_POST["price"], $_POST["description"], 
				$_POST["in_stock"], 
				$_POST["brief_description"], $_POST["list_price"],
				$_POST["product_code"], $_POST["sort_order"], 
				isset($_POST["ProductIsProgram"]), "eproduct_filename",
				$_POST["eproduct_available_days"], 
				$_POST["eproduct_download_times"],
				$_POST["weight"], $_POST["meta_description"], 
				$_POST["meta_keywords"], isset($_POST["free_shipping"]),
				$_POST["min_order_amount"], $_POST["shipping_freight"],
				$_POST["tax_class"], 1, $_POST["seolink"], $_POST["meta_title"] );
		$_GET["productID"] = $productID;
		$updatedValues = ScanPostVariableWithId( array( "option_value", "option_radio_type" ) );
		cfgUpdateOptionValue($productID, $updatedValues);
		OpenConfigurator($_POST["optionID"], $productID);
	}


	// remove product from appended category
	if ( isset($_GET["remove_from_app_cat"]) )
	{
		if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
		{
			Redirect("products.php?safemode=yes&productID=".$productID);
		}
		catRemoveProductFromAppendedCategory( $_GET["productID"], 
				$_GET["remove_from_app_cat"] );
		catUpdateProductCount($_GET["productID"], $_GET["remove_from_app_cat"], -1);
	}

	// add into new appended category
	if ( isset($_POST["add_category"]) )
	{
		if ( $_GET["productID"] == 0 )
		{
			$productID = AddProduct( 
				$_POST["categoryID"], $_POST["name"], $_POST["price"], $_POST["description"], 
				$_POST["in_stock"], 
				$_POST["brief_description"], $_POST["list_price"],
				$_POST["product_code"], $_POST["sort_order"], 
				isset($_POST["ProductIsProgram"]), "eproduct_filename",
				$_POST["eproduct_available_days"], 
				$_POST["eproduct_download_times"],
				$_POST["weight"], $_POST["meta_description"], 
				$_POST["meta_keywords"], isset($_POST["free_shipping"]),
				$_POST["min_order_amount"], $_POST["shipping_freight"],
				$_POST["tax_class"], 1, $_POST["seolink"], $_POST["meta_title"] );
			$_GET["productID"] = $productID;
		}
		catAddProductIntoAppendedCategory($_GET["productID"], 
				$_POST["new_appended_category"] );

		if ( CONF_UPDATE_GCV == '1' )
			catUpdateProductCount($_GET["productID"], $_POST["new_appended_category"]);

	}
	// show product
	if ( $_GET["productID"] != 0 )
	{
		$product = GetProduct($_GET["productID"]);
		$title = $product["name"];
	}
	else
	{
		$product = array();
		$title = ADMIN_PRODUCT_NEW;
		$cat = isset($_GET["categoryID"]) ? $_GET["categoryID"] : 0;
		$product["categoryID"]			= $cat;
		$product["name"]				= "";
		$product["description"]			= "";
		$product["customers_rating"]	= "";
		$product["Price"]				= 0;
		$product["picture"]				= "";
		$product["in_stock" ]			= 0;
		$product["thumbnail" ]			= "";
		$product["big_picture" ]		= "";
		$product["brief_description"]	= "";
		$product["list_price"]			= 0;
		$product["product_code"]		= "";
		$product["sort_order"]			= 0;
		$product["date_added"]			= null;
		$product["date_modified"]		= null;
		$product["eproduct_filename"]			= "";
		$product["eproduct_available_days"]		= 365; 
		$product["eproduct_download_times"]		= 1;
		$product["weight"]				= 0;
		$product["meta_description"]	= "";
		$product["meta_keywords"]		= "";
		$product["meta_title"]		= "";
		$product["seolink		"]		= "";
		$product["free_shipping"]		= 0;
		$product["min_order_amount"]	= 1;
		if ( CONF_DEFAULT_TAX_CLASS == '0' )
			$product["classID"]	= "null";
		else
			$product["classID"] = CONF_DEFAULT_TAX_CLASS;
		$product["shipping_freight"]	= 0;
	}

	// gets ALL product options
	$options = cfgGetProductOptionValue( $_GET["productID"] );
	$options = html_spchars($options);

	// gets pictures
	$picturies = GetPictures( $_GET["productID"] );
	// get appended categories
	$appended_categories = catGetAppendedCategoriesToProduct( $_GET["productID"] );
	// hide/show tables

	$showAppendedParentsTable = 0;
	if ( isset($_POST["AppendedParentsTableHideTable_hidden"]) )
	{
		if ( $_POST["AppendedParentsTableHideTable_hidden"] == "1" )
			$showAppendedParentsTable = 1;
	}
	else if ( isset($_GET["remove_from_app_cat"]) )
		$showAppendedParentsTable = 1;

	$showConfiguratorTable = 0;
	if ( isset($_POST["ConfiguratorHideTable_hidden"]) )
		if ( $_POST["ConfiguratorHideTable_hidden"] == "1" )
			$showConfiguratorTable = 1;

	$showPhotoTable = 0;
	if ( isset($_POST["PhotoHideTable_hidden"]) )
	{
		if ( $_POST["PhotoHideTable_hidden"] == "1" )
			$showPhotoTable = 1;
	}
	else if ( isset($_GET["delete_pictures"]) ||  isset($_GET["delete_one_picture"])  )
		$showPhotoTable = 1;

	$tax_classes = taxGetTaxClasses();

?>

<html>

<head>
<link rel=STYLESHEET href="style1.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo DEFAULT_CHARSET;?>">
<title><?php echo ADMIN_PRODUCT_TITLE;?></title>
<script>
function confirmDelete(question, where)
{
	temp = window.confirm(question);
	if (temp) //delete
	{
		window.location=where;
	}
}
function open_window(link,w,h) //opens new window
{
	var win = "width="+w+",height="+h+",menubar=no,location=no,resizable=yes,scrollbars=yes";
	wishWin = window.open(link,'wishWin',win);
}
function position_this_window()
{
	var x = (screen.availWidth - 795) / 2;
	window.resizeTo(795, screen.availHeight - 50);
	window.moveTo(Math.floor(x),25);
}
</script>
</head>

<body bgcolor=#FFFFE2 onLoad="position_this_window();">
<center>
<p>
<b><?php echo $title; ?></b>
<?php
		if ( isset($_GET["couldntToDelete"]) )
		{
			?>
				<br>
				<font color=red>
					<b><?php echo COULD_NOT_DELETE_THIS_PRODUCT?><b>
				</font>
			<?php
		}
?>

<?php
		if ( isset($_GET["safemode"]) )
		{
			echo "<p><font color=red><b>".ADMIN_SAFEMODE_WARNING."<b></font>";
		}
?>

<form enctype="multipart/form-data" action="products.php?productID=<?php echo $_GET["productID"]?>" 
		method=post name="MainForm">

<table width=100% border=0 cellpadding=3 cellspacing=0>

<tr>
<td align=right><?php echo ADMIN_CATEGORY_PARENT;?></td>
<td>
<select name="categoryID" <?php

	if (CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE == 0) // update list
		echo "onChange=\"window.location='products.php?productID=".$_GET["productID"]."&change_category='+document.MainForm.categoryID.value;\"";

?>>
<?php

	if (CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE == 1) echo "<option value=\"1\">".ADMIN_CATEGORY_ROOT."</option>";

	//show categories select element
	$core_category = (isset($_GET["change_category"])) ? (int)$_GET["change_category"] : $product["categoryID"] ;
	if (CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE == 0)
		$cats = catGetCategoryCompactCList($core_category);
	else
		$cats = catGetCategoryCList();
	for ($i=0; $i<count($cats); $i++)
	{
		echo "<option value=\"".$cats[$i]["categoryID"]."\"";
		if ($core_category == $cats[$i]["categoryID"]) //select category
			echo " selected";
		echo ">";
		for ($j=0;$j<$cats[$i]["level"];$j++) echo "&nbsp;&nbsp;";
		echo $cats[$i]["name"];
		echo "</option>";
	}
?>
</select>
</td>
</tr>

<tr>
	<td align=center colspan=2>
		
		<a href="JavaScript:AppendedParentsTableHideTable();">
			<?php echo ADMIN_CATEGORY_APPENDED_PARENTS;?>
			<input type=hidden name='AppendedParentsTableHideTable_hidden' 
				value='<?php echo $showAppendedParentsTable;?>'>
		</a>

		<script language='javascript'>

			function AppendedParentsTableHideTable()
			{
				if ( AppendedParentsTable.style.display == 'none' ) 
				{
					AppendedParentsTable.style.display = 'block';
					document.MainForm.AppendedParentsTableHideTable_hidden.value='1';
				}
				else
				{
					AppendedParentsTable.style.display = 'none';
					document.MainForm.AppendedParentsTableHideTable_hidden.value='0';
				}
			}
		</script>

		<table border=0 cellpadding=5 cellspacing=1 bgcolor=#C3BD7C
			id='AppendedParentsTable'>
			<tr>
				<td colspan=2 align=center>
					<b><?php echo ADMIN_CATEGORY_APPENDED_PARENTS;?>:</b>
				</td>
			</tr>
			<tr bgcolor=#F5F5C5>
				<td align=center><?php echo ADMIN_CATEGORY_TITLE;?></td>
				<td width=1%>&nbsp;</td>
			</tr>

			<?php
			foreach( $appended_categories as $app_cat )
			{
			?>
			<tr bgcolor=#FFFFE2>
				<td align=center>
					<?php echo $app_cat["category_name"];?>
				</td>
				<td width=1%>
					<a href="javascript:confirmDelete('<?php echo QUESTION_DELETE_CONFIRMATION;?>','products.php?productID=<?php echo $_GET["productID"]?>&remove_from_app_cat=<?php echo $app_cat["categoryID"]?>');">
						<img src="images/remove.jpg" border=0 alt="<?php echo DELETE_BUTTON?>">
					</a>
				</td>
			</tr>
			<?php
			}
			?>

			<tr>
				<td align=center colspan=2>
					<?php echo ADD_BUTTON;?>:
				</td>
			</tr>

			<tr bgcolor=white>
				<td align=center>
					<select name='new_appended_category' <?php

	if (CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE == 0) // update list
		echo "onChange=\"window.location='products.php?productID=".$_GET["productID"]."&change_app_category='+document.MainForm.new_appended_category.value;\"";

?>>
					<?php

						$change_app_category = isset($_GET["change_app_category"]) ? (int)$_GET["change_app_category"] : $product["categoryID"];

						if (CONF_FULLY_EXPAND_CATEGORIES_IN_ADMIN_MODE == 0)
							$cats = catGetCategoryCompactCList($change_app_category);
						else
							$cats = catGetCategoryCList();
						for ($i=0; $i<count($cats); $i++)
						 if ($cats[$i]["categoryID"] > 1)
						 {
							echo "<option value=\"".$cats[$i]["categoryID"]."\"";
							if ($change_app_category == $cats[$i]["categoryID"]) echo " selected";
							echo ">";
							for ($j=0;$j<$cats[$i]["level"];$j++) echo "&nbsp;&nbsp;";
							echo $cats[$i]["name"];
							echo "</option>";
						 }
					?>
					</select>
				</td>
				<td width=1%>&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2 align=center>
					<input type=submit value='<?php echo ADD_BUTTON;?>' name='add_category'>
				</td>
			</tr>
		</table>
		<script language='JavaScript'>
			<?php
			if ( $showAppendedParentsTable == 0 && !isset($_GET["change_app_category"]) )
			{
			?>
				AppendedParentsTable.style.display = 'none';
			<?php
			}
			?>
		</script>

	</td>
</tr>
<tr>
	<td align=right>
		<?php echo ADMIN_PRODUCT_NAME;?>
	</td>
	<td>
		<input type="text" name="name" value="<?php echo str_replace("\"","&quot;",$product["name"]); ?>">
	</td>
</tr>
<tr>
	<td align=right>
		���
	</td>
	<td>
		<input type="text" name="seolink" value="<?php echo str_replace("\"","&quot;",$product["seolink"]); ?>">
	</td>
</tr>

<tr>
	<td align=right>
		<?php echo ADMIN_PRODUCT_CODE;?>
	</td>
	<td>
		<input type="text" name="product_code" 
			value="<?php echo str_replace("\"","&quot;",$product["product_code"] ); ?>">
	</td>
</tr>

<tr>
	<td align=right><?php echo ADMIN_TAX_CLASS;?></td>
	<td>
		<select name='tax_class'>
			<option value='null'><?php echo ADMIN_NOT_DEFINED;?></option>
			<?php
			foreach( $tax_classes as $tax_class )
			{
			?>
				<option value='<?php echo $tax_class["classID"];?>'
				<?php
				if ( $product["classID"] == $tax_class["classID"] )
				{
				?>
					selected
				<?php
				}
				?>
				>
					<?php echo $tax_class["name"];?>
				</option>
			<?php
			}
			?>
		</select>
	</td>
</tr>

<?php
	if ( !is_null($product["date_added"])  )
	{
?>
<tr>
	<td align=right>
		<?php echo ADMIN_DATE_ADDED;?>
	</td>
	<td>
		<?php echo $product["date_added"]?>
	</td>
</tr>
<?php
	}
?>

<?php
	if ( !is_null($product["date_modified"]) )
	{
?>
<tr>
	<td align=right>
		<?php echo ADMIN_DATE_MODIFIED;?>
	</td>
	<td>
		<?php echo $product["date_modified"]?>
	</td>
</tr>

<?php
	}
?>

<?php	
	if ($_GET["productID"]) 
	{ 
?>
<tr>
	<td align=right>
		<?php echo ADMIN_PRODUCT_RATING;?>
	</td>
	<td>
		<input type=text name="rating" 
			value="<?php echo str_replace("\"","&quot;",$product["customers_rating"]); ?>">
	</td>
</tr>

<?php }; ?>

<tr>
	<td align=right>
		<?php echo ADMIN_SORT_ORDER;?>
	</td>
	<td>
		<input type="text" name="sort_order" value="<?php echo $product["sort_order"];?>">
	</td>
</tr>

<tr>
	<td align=right>
		<?php echo ADMIN_PRODUCT_PRICE;?>, <?php echo STRING_UNIVERSAL_CURRENCY;?><br>(<?php echo STRING_NUMBER_ONLY;?>):
	</td>
	<td>
		<input type="text" name="price" value=<?php echo $product["Price"]; ?>>
	</td>
</tr>

<tr>
	<td align=right>
		<?php echo ADMIN_PRODUCT_LISTPRICE;?>, <?php echo STRING_UNIVERSAL_CURRENCY;?><br>(<?php echo STRING_NUMBER_ONLY;?>):
	</td>
	<td>
		<input type="text" name="list_price" value=<?php echo $product["list_price"];?>>
	</td>
</tr>

<?php
	if ($product["in_stock"]<0) $is = 0;
	else $is = $product["in_stock"];
	if (CONF_CHECKSTOCK == 1) {
?>
	<tr>
	<td align=right><?php echo ADMIN_PRODUCT_INSTOCK;?>:</td>
	<td><input type="text" name="in_stock" value="<?php echo $is;?>"></td>
	</tr>
<?php } else { ?>
	<input type=hidden name="in_stock" value="<?php echo $is;?>">
<?php } ?>

<tr>
	<td align=right>
		<?php echo ADMIN_SHIPPING_FREIGHT;?>
	</td>
	<td>
		<input type="text" name="shipping_freight" value=<?php echo $product["shipping_freight"];?>>
	</td>
</tr>

<tr>
	<td align=right><?php echo ADMIN_PRODUCT_WEIGHT;?></td>
	<td><input type=text name='weight' 
		value='<?php echo $product["weight"];?>'> <?php echo CONF_WEIGHT_UNIT;?></td>
</tr>

<tr>
	<td align=right><?php echo ADMIN_FREE_SHIPPING;?></td>
	<td><input type=checkbox name='free_shipping' 
		<?php
		 	if ( $product["free_shipping"] )
			{
		?>
			checked
		<?php
			}
		?>
		value='1'>
	</td>
</tr>

<tr>
	<td align=right><?php echo ADMIN_MIN_ORDER_AMOUNT;?></td>
	<td><input type=text name='min_order_amount' 
			value='<?php echo $product["min_order_amount"];?>'>
	</td>
</tr>






	<!-- ************************ CONFIGUARTOR *********************** -->

	<tr><td align=center colspan=2>


	<center>
		<a href="JavaScript:ConfiguratorHideTable();">
			<?php echo ADMIN_CONFIGURATOR;?>
			<input type=hidden name='ConfiguratorHideTable_hidden' 
				value='<?php echo $showConfiguratorTable;?>'>
		</a>
	</center>

	<script language='javascript'>

		function ConfiguratorHideTable()
		{
			if ( ConfiguratorTable.style.display == 'none' ) 
			{
				ConfiguratorTable.style.display = 'block';
				document.MainForm.ConfiguratorHideTable_hidden.value='1';
			}
			else
			{
				ConfiguratorTable.style.display = 'none';
				document.MainForm.ConfiguratorHideTable_hidden.value='0';
			}
		}

	</script>

	<table id='ConfiguratorTable'>
	<tr><td>


	<script language='JavaScript'>
			function SetOptionValueTypeRadioButton( id, radioButtonState )
			{
				if ( radioButtonState == "UN_DEFINED" )
					document.all["option_radio_type_"+id][0].click();
				else if ( radioButtonState == "ANY_VALUE" )
					document.all["option_radio_type_"+id][1].click();
				else if ( radioButtonState == "N_VALUES" )
					document.all["option_radio_type_"+id][2].click();
			}

			function SetEnabledStateTextValueField( id, radioButtonState )
			{
				if ( radioButtonState == "UN_DEFINED" || 
					radioButtonState == "N_VALUES" )
				{
					document.all["option_value_"+id].disabled=true;
					document.all["option_value_"+id].value="";
				}
				else
					document.all["option_value_"+id].disabled=false;
			}
	</script>
		<?php

		//product extra options
		foreach($options as $option)
		{
			$option_row = $option["option_row"];
			$value_row  = $option["option_value"];
			$ValueCount = $option["value_count"];
		?>
		<table border='0' cellspacing='0' cellpadding='4' width=100%>
			<tr> 
				<td align=left width=25%>
					<b><?php echo $option_row["name"]?></b>:
				</td>
				<td>
					<input name='option_radio_type_<?php echo $option_row["optionID"]?>' 
						type='radio' value="UN_DEFINED" 
						onclick="JavaScript:SetEnabledStateTextValueField(<?php echo $option_row['optionID']?>, 'UN_DEFINED' );"
						<?php
							if ( (is_null($value_row["option_value"]) || $value_row["option_value"] == '') 
								&& $value_row["option_type"]==0 )
							echo "checked";
						?>
					>
				</td>
				<td>
					<?php echo ADMIN_NOT_DEFINED;?>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
				<td valign='top'> 
					<input name='option_radio_type_<?php echo $option_row["optionID"]?>'  
						type='radio' value="ANY_VALUE"
						onclick="JavaScript:SetEnabledStateTextValueField(<?php echo $option_row['optionID']?>, 'ANY_VALUE' );"
						<?php
							if ( $value_row["option_type"]==0 && strlen($value_row["option_value"]) > 0 )
								echo"checked";
						?>
					> 
				</td>
				<td>
					<?php echo ADMIN_ANY_VALUE;?>: 
					<input type=text name='option_value_<?php echo $option_row["optionID"]?>' 
						value='<?php echo str_replace("\"","&quot;",$value_row["option_value"])?>' >
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign='top'>
					<input name='option_radio_type_<?php echo $option_row["optionID"]?>' 
						type='radio' value="N_VALUES"
						onclick="JavaScript:SetEnabledStateTextValueField(<?php echo $option_row['optionID']?>, 'N_VALUES' );"
						<?php
							if ( $value_row["option_type"]==1 )
								echo "checked";
						?>
					>
				</td>
				<td>
					<table cellpadding='0' id='OptionTable_<?php echo $option_row["optionID"]?>'>
						<tr>
							<td>
								<?php echo ADMIN_SELECTING_FROM_VALUES;?> (<?php echo $ValueCount?> <?php echo ADMIN_VARIANTS;?>)
							</td>
						</tr>
						<tr>
							<td>
								<a name="option_value_configurator_<?php echo $option_row['optionID']?>" 
									<?php	
										if ( $_GET["productID"] != 0 ) 
										{
									?>
											href="JavaScript:open_window('option_value_configurator.php?optionID=<?php echo $option_row["optionID"]?>&productID=<?php echo $_GET["productID"]?>',400,400);"
									<?php
										} else
										{	
									?>
											href="JavaScript:AddProductAndOpen_option_value_configurator(<?php echo $option_row["optionID"]?>)"
									<?php
										}
									?>										
									>
									<?php echo ADMIN_SELECT_SETTING;?>...
								</a>

							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=3>
					<hr width="100%" color=black></hr>
				</td>
			</tr>
		</table>

				<?php
		}
?>


		</td></tr>
		</table>

		<script language='JavaScript'>
			<?php
			if ( $showConfiguratorTable == 0 )
			{
			?>
				ConfiguratorTable.style.display = 'none';
			<?php
			}
			?>
		</script>

		<input type="submit" name="AddProductAndOpenConfigurator" 
			value="" width=5>
		<input type="hidden" name="optionID" value="">


		<script language='JavaScript'>

			document.MainForm.AddProductAndOpenConfigurator.style.display = 'none';

			function AddProductAndOpen_option_value_configurator(optionID)
			{
				document.MainForm.optionID.value = optionID;
				document.MainForm.AddProductAndOpenConfigurator.click();
			}

		</script>


		</td>
		</tr>


<tr align=right><td>&nbsp;</td></tr>

<tr>
	<td colspan=2 align=center>

		<center>
			<a href="JavaScript:PhotoHideTable();">
				<?php echo ADMIN_PHOTOS;?>
			</a>
			<input type=hidden name='PhotoHideTable_hidden'
					value='<?php echo $showPhotoTable;?>'>
		</center>

		<script language='javascript'>

		function PhotoHideTable()
		{
			if ( PhotoTable.style.display == 'none' )
			{
				PhotoTable.style.display = 'block';
				document.MainForm.PhotoHideTable_hidden.value='1';
			}
			else
			{
				PhotoTable.style.display = 'none';
				document.MainForm.PhotoHideTable_hidden.value='0';
			}
		}

		</script>

		<table id='PhotoTable'><tr><td>
		
		<table border=0 cellpadding=5 cellspacing=1 bgcolor=#C3BD7C>

			<tr>
				<td colspan=5 align=center>
				<b><?php echo ADMIN_PHOTOS;?></b>
				</td>
			</tr>

			<tr bgcolor=#F5F5C5>
				<td><?php echo ADMIN_DEFAULT_PHOTO;?></td>
				<td><?php echo ADMIN_PRODUCT_PICTURE;?></td>
				<td><?php echo ADMIN_PRODUCT_THUMBNAIL;?></td>
				<td><?php echo ADMIN_PRODUCT_BIGPICTURE;?></td>
				<td width=1%>&nbsp;</td>
			</tr>

			<?php
				foreach( $picturies as $picture )
				{
					echo("<tr bgcolor=#FFFFE2>");
					// default picture radio button
					if ( $picture["default_picture"] == 1 )
					{
						$default_picture_exists = true;
						echo("<td><input type=radio name=default_picture value='".$picture["photoID"].
								"' checked></input></td>");
					}
					else
						echo("<td><input type=radio name=default_picture value='".$picture["photoID"].
								"'></input></td>");

					// conventional picture ( filename field )
					echo("<td>");
					echo("		<input type=text name=filename_".$picture["photoID"].
							" value='".$picture["filename"]."'><br>" );
					if ( file_exists("./products_pictures/".$picture["filename"])
						 && trim($picture["filename"]) != "" )
						echo("		<a class=small href='javascript:open_window(\"products_pictures/".$picture["filename"]."\",".GetPictureSize($picture["filename"]).")'>".ADMIN_PHOTO_PREVIEW."</a>");
					else
						echo(ADMIN_PICTURE_NOT_UPLOADED);
					echo("</td>");

					// small picture ( thumbnail field )
					echo("<td>");
					echo("		<input type=text name=thumbnail_".$picture["photoID"].
							" value='".$picture["thumbnail"]."'><br>" );
					if ( file_exists("./products_pictures/".$picture["thumbnail"]) 
						 && trim($picture["thumbnail"]) != "" )
					{
						echo("		<a class=small href='javascript:open_window(\"products_pictures/".$picture["thumbnail"]."\",".GetPictureSize($picture["thumbnail"]).")'>".ADMIN_PHOTO_PREVIEW."</a>");
						echo("		<a class=small href=\"javascript:confirmDelete('".QUESTION_DELETE_PICTURE."', 'products.php?delete_one_picture=1&thumbnail=".$picture["photoID"]."&productID=".$_GET["productID"]."')\">".DELETE_BUTTON."</a>" );
					}
					else
						echo(ADMIN_PICTURE_NOT_UPLOADED);
					echo("</td>");

					// large picture ( enlarged field )
					echo("<td>");
					echo("		<input type=text name=enlarged_".$picture["photoID"].
							" value='".$picture["enlarged"]."'><br>" );
					if ( file_exists("./products_pictures/".$picture["enlarged"]) 
						 && trim($picture["enlarged"]) != "" )
					{
						echo("		<a class=small href='javascript:open_window(\"products_pictures/".$picture["enlarged"]."\",".GetPictureSize($picture["enlarged"]).")'>".ADMIN_PHOTO_PREVIEW."</a>");
						echo("		<a class=small href=\"javascript:confirmDelete('".QUESTION_DELETE_PICTURE."', 'products.php?delete_one_picture=1&enlarged=".$picture["photoID"]."&productID=".$_GET["productID"]."')\">".DELETE_BUTTON."</a>" );
					}
					else
						echo( ADMIN_PICTURE_NOT_UPLOADED );
					echo("</td>");

					// delete button
					echo("<td>");
					?>		
						<a href=
							"javascript:confirmDelete('<?php echo QUESTION_DELETE_PICTURE?>','products.php?productID=<?php echo $_GET["productID"]?>&photoID=<?php echo $picture["photoID"]?>&delete_pictures=1');">
							<img src="images/remove.jpg" border=0 alt="<?php echo DELETE_BUTTON?>">
						</a>
					<?php
					echo("</td>");
					echo("</tr>");
				}
			?>

			<tr>
				<td colspan=5 align=center>
					<?php echo ADD_BUTTON?>:
				</td>
			</tr>

			<tr bgcolor=#FFFFE2>
				<td width=1%>
					<input type=radio name=default_picture
					<?php
						if ( !isset($default_picture_exists) )
						{	
					?>
						checked
					<?php
						}
					?>
						value=-1
						>
					</input>
				</td>
				<td>
				<input type="file" name="new_filename" width=10></td>
				<td><input type="file" name="new_thumbnail"></td>
				<td><input type="file" name="new_enlarged"></td>
				<td width=1%>&nbsp;</td>
			</tr>

		</table>

		<br>
		<center>
			<input type=submit name="save_pictures" value="<?php echo ADMIN_SAVE_PHOTOS?>">
		</center>

		</td></td></table>

		<script language='JavaScript'>
			<?php
			if ( $showPhotoTable == 0 )
			{
			?>
				PhotoTable.style.display = 'none';
			<?php
			}
			?>
		</script>

	</td>
</tr>

<tr>
	<td colspan=2 align=center>
	</td>
</tr>

<?php
// }
?>


<?php
?>
<tr>
	<td colspan=2 align=center>

		<table id='FileNameTable'>
			<tr>
				<td colspan=3>
					<input type=checkbox name='ProductIsProgram' 
							value='1'
							onclick='JavaScript:ProductIsProgramHandler();'
							<?php
							if ( trim($product["eproduct_filename"]) != "" )
							{
							?>
								checked
							<?php
							}
							?>
							>
							<?php echo ADMIN_PRODUCT_IS_PROGRAM;?>
				</td>
			</tr>

			<script language='JavaScript'>
				function ProductIsProgramHandler()
				{
					document.MainForm.eproduct_filename.disabled = 
							!document.MainForm.ProductIsProgram.checked;
					document.MainForm.eproduct_available_days.disabled = 
							!document.MainForm.ProductIsProgram.checked;
					document.MainForm.eproduct_download_times.disabled = 
							!document.MainForm.ProductIsProgram.checked;
				}
			</script>


			<tr>
				<td>
					<?php echo ADMIN_EPRODUCT_FILENAME;?>
				</td>
				<td>
					<?php echo ADMIN_EPRODUCT_AVAILABLE_DAYS;?>
				</td>
				<td>
					<?php echo ADMIN_EPRODUCT_DOWNLOAD_TIMES;?>
				</td>
			</tr>

			<tr>
				<td>
					<input type='file' name='eproduct_filename' 
							value='<?php echo $product["eproduct_filename"];?>' >
					<br>
					<?php
					if ( file_exists("./products_files/".$product["eproduct_filename"])  &&
							 $product["eproduct_filename"]!=null )
					{
					?>
						(<?php echo $product["eproduct_filename"];?>)
					<?php
					}
					else
					{
					?>
						<?php echo ADMIN_FILE_NOT_UPLOADED;?>
					<?php
					}
					?>
				</td>
				<td>
					<?php
						$valueArray[] = 1;
						$valueArray[] = 2;
						$valueArray[] = 3;
						$valueArray[] = 4;
						$valueArray[] = 5;
						$valueArray[] = 7;
						$valueArray[] = 14;
						$valueArray[] = 30;
						$valueArray[] = 180;
						$valueArray[] = 365;
					?>
					<select name='eproduct_available_days'>
						<?php
						foreach($valueArray as $value)
						{
						?>
							<option value='<?php echo $value;?>'
							<?php
								if ( $product["eproduct_available_days"] == $value )
								{
							?>
									selected
							<?php
								}
							?>
							> <?php echo $value;?> </option>
						<?php
						}
						?>
					</select>
				</td>
				<td>
					<input type=text name='eproduct_download_times' 
						value='<?php echo $product["eproduct_download_times"];?>' >
				</td>
			</tr>
		</table>


		<script language='JavaScript'>
			ProductIsProgramHandler();
		</script>

	</td>
</tr>
<?php
?>

<tr>
<td align=right><?php echo ADMIN_PRODUCT_DESC;?><br>(HTML):</td>
<td><textarea name="description" rows=15 cols=40><?php echo str_replace("<","&lt;",$product["description"]); ?></textarea></td>
</tr>

<tr>
<td align=right><?php echo ADMIN_PRODUCT_BRIEF_DESC;?><br>(HTML):</td>
<td><textarea name="brief_description" rows=7 cols=40><?php echo str_replace("<","&lt;",$product["brief_description"]); ?></textarea></td>
</tr>
<tr>
	<td align=right>
		"�����"
	</td>
	<td>
		<textarea name='meta_title' 
				rows=4 cols=15><?php echo $product["meta_title"];?></textarea>
	</td>
</tr>

<tr>
	<td align=right>
		<?php echo ADMIN_META_DESCRIPTION;?>
	</td>
	<td>
		<textarea name='meta_description' 
				rows=4 cols=15><?php echo $product["meta_description"];?></textarea>
	</td>
</tr>

<tr>
	<td align=right>
		<?php echo ADMIN_META_KEYWORDS;?>
	</td>
	<td>
		<textarea name='meta_keywords' 
				rows=4 cols=15><?php echo $product["meta_keywords"];?></textarea>
	</td>
</tr>

</table>

<?php if ($_GET["productID"]) { ?>

<hr size=1 width=90%>
<center>
<font><b><?php echo STRING_RELATED_ITEMS;?></b></font>
<?php
	$q = db_query("SELECT count(*) FROM ".RELATED_PRODUCTS_TABLE." WHERE Owner='".$_GET["productID"]."'") or die (db_error());
	$cnt = db_fetch_row($q);
	if ($cnt[0] == 0) echo "<p><font>< ".STRING_EMPTY_CATEGORY." ></font></p>";
	else {
		$q = db_query("SELECT productID FROM ".RELATED_PRODUCTS_TABLE." WHERE Owner='".$_GET["productID"]."'") or die (db_error());
		echo "<table>";
		while ($r = db_fetch_row($q))
		{
			$p = db_query("SELECT productID, name FROM ".PRODUCTS_TABLE." WHERE productID=$r[0]") or die (db_error());
			if ($r1 = db_fetch_row($p))
			{
			  echo "<tr>";
			  echo "<td width=100%>$r1[1]</td>";
			  echo "</tr>";
			}
		}
		echo "</table>";
	}
?>
[ <a href="javascript:open_window('wishlist.php?owner=<?php echo $_GET["productID"]; ?>',400,600);"><?php echo EDIT_BUTTON; ?></a> ]
</center>
<hr size=1 width=90%>

<?php } ?>

<p><center>
<input type="submit" name="save_product" value="<?php echo SAVE_BUTTON;?>" width=5>

<input type="button" value="<?php echo CANCEL_BUTTON;?>" onClick="window.close();">
<?php	if ($_GET["productID"]) echo "<input type=button value=\"".DELETE_BUTTON."\" onClick=\"confirmDelete('".QUESTION_DELETE_CONFIRMATION."','products.php?productID=".$_GET["productID"]."&delete=1');\">"; ?>
</center></p>

<input type=hidden name='save_product_without_closing' value='0'>

</form>


</center>
</body>

</html>