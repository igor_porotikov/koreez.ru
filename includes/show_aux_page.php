<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	// show aux page

	if ( isset($show_aux_page) or $aux_page_not_found  )
	{
		if ( isset($show_aux_page_by_url ) ) {
			$show_aux_page_by_url = trim($show_aux_page_by_url);
			if ( substr($show_aux_page_by_url,0,1) == '/' ) {
				$show_aux_page_by_url = substr( $show_aux_page_by_url, 1 );
			}
			if ( substr($show_aux_page_by_url,-1) == '/' ) {
				$show_aux_page_by_url = substr( $show_aux_page_by_url, 0, -1 );
			}
			$aux_page = auxpgGetAuxPageByUrl( $show_aux_page_by_url );
		} else {
			$aux_page = auxpgGetAuxPage( $show_aux_page );
		}

		if ( $aux_page )
		{
			if ( $aux_page["aux_page_text_type"] != 1 )
	 			$aux_page["aux_page_text"] = 
					nl2br(  str_replace("<","&lt;",$aux_page["aux_page_text"]) );
			$smarty->assign("page_body", $aux_page["aux_page_text"] );
			$smarty->assign("show_aux_page", $show_aux_page );
			$smarty->assign("main_content_template", "show_aux_page.tpl.html" );

			$similar_pages = $aux_page["aux_page_similar"] ? auxpgGetPagesByIds( $aux_page["aux_page_similar"] ) : array();
			$child_pages = $aux_page["aux_page_childs"] ? auxpgGetPagesByIds( $aux_page["aux_page_childs"] ) : array();
			$smarty->assign("aux_similar_pages", $similar_pages );
			$smarty->assign("aux_child_pages", $child_pages );
			$smarty->assign("aux_page", $aux_page );
		}
		else
		{
			header('HTTP/1.1 404');
			$aux_page_not_found = auxpgGetAuxPageByUrl( 'page_not_found' );
			if ( $aux_page_not_found ) {
		        $smarty->assign('page_title_manual', $aux_page_not_found['aux_page_name']);
			}
		    
			$smarty->assign("main_content_template", "page_not_found.tpl.html" );
		}

	}

?>