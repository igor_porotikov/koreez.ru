<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	// <head> variables definition: title, meta

	// TITLE & META Keywords & META Description
$page_h1 = CONF_CATALOG_STRING;
	if ( !isset($show_aux_page) ) //not an aux page, e.g. homepage, product/category page, registration form, checkout, etc.
	{

		if (isset($categoryID) && !isset($productID) && $categoryID>0) //category page
		{
			$q = db_query("SELECT name, parent FROM ".CATEGORIES_TABLE." WHERE categoryID<>0 and categoryID<>1 and categoryID='$categoryID'") or die (db_error());
			$r = db_fetch_row($q);
			if ($r)
			{
				$add_title = '';
				if( $r['parent'] > 1){
					$qq = db_query("SELECT name FROM ".CATEGORIES_TABLE." WHERE categoryID='".$r['parent']."'") or die (db_error());
					$rr = db_fetch_row($qq);
					$add_title = $rr['name'].' ';
				}
				$page_title = $add_title.$r[0]." - ".CONF_CATALOG_TITLE;
				$page_h1 = $add_title.$r[0];
			}
			else
			{
				$page_title = CONF_DEFAULT_TITLE;
			}
			$page_title = str_replace( "<", "&lt;", $page_title );
			$page_title = str_replace( ">", "&gt;", $page_title );

			$meta_tags = catGetMetaTags($categoryID);

		}
		else if (isset($productID) && $productID>0) //product information page
			{
				$q = db_query("SELECT name, meta_title FROM ".PRODUCTS_TABLE." WHERE productID='$productID'") or die (db_error());
				$r = db_fetch_row($q);
				if ($r)
				{
					$page_h1 = $r[0];
					if($r['meta_title']) $page_title = $r['meta_title']." - ".CONF_CATALOG_TITLE;	
					else $page_title = $r[0]." - ".CONF_CATALOG_TITLE;
				}
				else
				{
					$page_title = CONF_CATALOG_TITLE;
				}
				$page_title = str_replace( "<", "&lt;", $page_title );
				$page_title = str_replace( ">", "&gt;", $page_title );

				$meta_tags = prdGetMetaTags($productID);
			}
			else // other page
			{
				$page_title = CONF_DEFAULT_TITLE;
				$meta_tags = "";
				if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
					$meta_tags .= "<meta name=\"Description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
				if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
					$meta_tags .= "<meta name=\"KeyWords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\" >\n";
			}

	}
	else // aux page => get title and META information from database
	{
		if ( isset($show_aux_page_by_url ) ) {
			$page = auxpgGetAuxPageByUrl( $show_aux_page_by_url );
		} else {
			$page = auxpgGetAuxPage( $show_aux_page );
		}

		if ( !$page ) {
			$page = array();
		}
	//	$page_title				= $page["aux_page_name"]." - ".CONF_DEFAULT_TITLE;
	$page_title				= $page["aux_page_name"];
	$page_h1 = '';
		$meta_tags = "";
		if  ( $page["meta_description"] != "" )
			$meta_tags .= "<meta name=\"Description\" content=\"".str_replace("\"","&quot;",$page["meta_description"])."\">\n";
		if  ( $page["meta_keywords"] != "" )
			$meta_tags .= "<meta name=\"KeyWords\" content=\"".str_replace("\"","&quot;",$page["meta_keywords"])."\" >\n";
	}

	if(isset($_GET["feedback"])){
		$page_title	= "��������";
		$page_h1 = '��������';
		$meta_tags = "";
		$meta_tags .= "<meta name=\"Description\" content=\"��������. ".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
		$meta_tags .= "<meta name=\"KeyWords\" content=\"��������, ".CONF_HOMEPAGE_META_KEYWORDS."\" >\n";
	}

	$smarty->assign("page_h1",	$page_h1 );
	$smarty->assign("page_title",	$page_title );
	$smarty->assign("page_meta_tags", $meta_tags );


?>