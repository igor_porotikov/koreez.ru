<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
if(!isset($_GET['transaction_result']))return '';

switch ($_GET['transaction_result']){
	
	case 'success':
	case 'failure':
		$smarty->assign('TransactionResult', $_GET['transaction_result']);
		$smarty->assign( "main_content_template", "transaction_result.tpl.html");
		break;
}
?>