<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	// category navigation form
	if ( isset($categoryID) )
		$out = catGetCategoryCompactCList( $categoryID );
	else
		$out = catGetCategoryCompactCList( 1 );
	$smarty->assign( "categories_tree", $out );

?>