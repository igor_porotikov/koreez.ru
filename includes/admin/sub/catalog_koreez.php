<?php
/**
 * Koreez price
 * 
 */
class Koreez_Price {
	var $structure = array();
	var $product_array = array();
	
    /**
     * Constructor
     */	
    function __construct() {
        
    }
    
    /**
     * Load structure
     * @param void
     * @return boolean
     */
    function loadStructure () {
  	    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE );
    	while ( $row=db_fetch_row($q) )
	    {
	    	$this->structure[$row['categoryID']]['name'] = $row['name'];
	    	$this->structure[$row['categoryID']]['seolink'] = strtolower($row['seolink']);
	    	$this->structure[$row['categoryID']]['parent'] = $row['parent'];
	    }
    	return true;
    }
    
    
    /**
     * Get parent category seo link
     * @param int $category_id category id
     * @return string
     */
    function getParentCategorySEO($category_id) {
    	$parent_id = $this->structure[$category_id]['parent'];
    	return str_replace('zapchasti-', '', $this->structure[$parent_id]['seolink']);
    }

    /**
     * Get category ID by seolink
     * @param string $seo seolink
     * @return int
     */
    function getCategoryIDBySeolink( $seo ) {
    	foreach ( $this->structure as $category_id => $category_item ) {
    		if ( eregi($category_item['name'], $seo) ) {
    			//echo "name = ".$category_item['name']."<br>";
    			return $category_id;
    		}
    	}
    	/*
  	    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE seolink like '%$seo%' " );
    	if  ( $row=db_fetch_row($q) )
	    {
	        return $row['categoryID'];
    	}
    	*/
    	return false;
    }
    
    /**
     * Add product
     * @param array $product_array product array
     * @return boolean
     */
    function addProduct ( $product_array ) {
        $query = "insert into ".PRODUCTS_TABLE." set 
            categoryID='".$product_array['categoryID']."', 
            name='".$product_array['name']."', 
            seolink='".$product_array['seolink']."', 
            Price='".$product_array['Price']."', 
            brief_description='".$product_array['brief_description']."', 
            enabled='1', 
            product_code='".$product_array['product_code']."'";
        //echo $query; 
        db_query($query);
        //echo "add product<br>";
        
        return db_insert_id();        
    }

    /**
     * Update product
     * @param array $product_array product array
     * @return boolean
     */
    function updateProduct ( $product_array ) {
    	$query = "update ".PRODUCTS_TABLE." set Price='".$product_array['Price']."' where productID='".$product_array['productID']."'";
    	echo $query."<br>";
        db_query($query);
    	
        echo "update product<br>";
    }
    
    /**
     * Get product id by code
     * @param
     * @return
     */
    function getProductIdByCode ( $product_code ) {
        $query = "select * from ".PRODUCTS_TABLE." where product_code='$product_code'";
  	    $q = db_query($query);
    	if  ( $row=db_fetch_row($q) )
	    {
	    	$this->product_array[$row['productID']]['price'] = $row['Price']; 
	        return $row['productID'];
    	}
    	return false;
        
    }
    
    /**
     * Add category product
     * @param int $category_id
     * @param int $product_id
     * @return boolean
     */
    function addCategoryProduct($category_id, $product_id) {
        $query = "insert into ".CATEGORIY_PRODUCT_TABLE." set
        		categoryID=$category_id,
        		productID=$product_id";
        //echo $query."<br>";
        db_query($query);
    }
    
    /**
     * Concatenate 
     * @param array $content content array
     * @param int $i
     * @return string
     */
    function concatenate ( $content, $i ) {
    	$rs = '';
    	//echo "concatenate i = $i<br>";
    	$counter = 0;
    	for ( $j = $i; $j < count($content); $j++ ) {
    		$counter++;
    		//echo "j = $j<br>";
    		//echo "content_j = ".$content[$j]."<br><br>";
    		
    		$rs .= trim($content[$j]);
    		if ( eregi('���', $content[$j]) ) {
    			$items['string'] = $rs;
    			$items['i'] = $j;
    			return $items;
    		}
    	}
    }
    
    /**
     * Normalize
     * @param array $content content array
     * @return array
     */
    function normalizePrice ( $content ) {
    	$start_push = false;
    	//echo "count = ".count($content)."<br>";
    	for ( $i = 0; $i < count($content); $i++ ) {
    		if ( eregi('"', $content[$i] ) ) {
    			$items = $this->concatenate($content, $i);
    			$ra[] = $items['string'];
    			$i = $items['i'];
    		} else {
    			$ra[] = trim($content[$i]);
    		}
    		
    	}
    	return $ra;
    }
    
    /**
     * Fix model names
     */
    function main_fix_model () {
    	$this->loadStructure();
    	foreach ( $this->structure as $catalog_id => $catalog_array ) {
    		if ( $catalog_array['parent'] > 1 ) {
    			$this->structure[$catalog_array['parent']]['seolink'];
    			$new_seolink = $this->structure[$catalog_array['parent']]['seolink'].'-'.$catalog_array['seolink'];
    			$query = "update ".CATEGORIES_TABLE." set seolink='$new_seolink' where categoryID=$catalog_id;";
    			echo $query.'<br>';
    		}
    	}
    }
    
    /**
     * Main
     */
    function main_fast () {
    	$this->loadStructure();
        $content = file('fast_rub.csv');
        echo '<pre>';
        echo 'stat normilze<br>';
        $content = $this->normalizePrice($content);
        //print_r($content);
        //return;
        
        foreach ( $content as $string ) {
        	//if ( $j++ > 55 ) {
        	//	return;
        	//	
        	//}
            //echo $string;
            //continue;
            $string_array = array();
            
            $string_array = split(";", $string);
            
            $mark[$string_array[1]] = $string_array[1];
            $model[$string_array[1]][$string_array[3]] = $string_array[3];
            $category_id = $this->getCategoryIDBySeolink($string_array[3]);
            //echo $string_array[3].", category_id = ".$category_id."<hr><br>";
            if ( $category_id and $category_id != 1 ) {
            	//$parent_category_seo = $this->getParentCategorySEO($category_id);
            	$current_category_seo =  $this->structure[$category_id]['seolink'];
            	
                $product_array = array();
                $product_array['categoryID'] = $category_id;
                $product_array['name'] = mysql_real_escape_string($string_array[1]);
                $product_array['Price'] = str_replace('���.', '', trim($string_array[4]));
                $product_array['Price'] = str_replace(',', '.', $product_array['Price'] );
                $product_array['brief_description'] = mysql_real_escape_string(trim($string_array[3]));
                $product_array['product_code'] = str_replace('-', '', trim($string_array[0]));

                $product_array['seolink'] = $current_category_seo.'-'.$product_array['product_code'];
                //echo "mark = ".$string_array[1].", category_id = $category_id<br>";
                //print_r($product_array);
                $this->product_processor($product_array);
            }
            
            //print_r($string_array);
        }
        //print_r($mark);
        //print_r($model);
    }
    
    /**
     * Product processor
     * @param array $product_array product array
     * @return mixed 
     */
    function product_processor ( $product_array ) {
    	$product_id = $this->getProductIdByCode($product_array['product_code']);
        if ( $product_id ) {
        	if ( $this->product_array[$product_id]['price'] > $product_array['Price'] ) {
            	echo "product_id = $product_id, current_price = ".$this->product_array[$product_id]['price'].", new price = ".$product_array['Price']."<br>";
            	$product_array['productID'] = $product_id;
            	 
        		$this->updateProduct($product_array);
            }
        } else {
        	//echo $product_array['product_code'].'!<br>';
            $product_id = $this->addProduct($product_array);
            $this->addCategoryProduct($product_array['categoryID'], $product_id);
        }
        return true;
    }
    
    
    /**
     * Main
     */
    function main () {
    	$this->loadStructure();
        $content = file('price2.csv');
        echo '<pre>';
        
        foreach ( $content as $string ) {
            //echo $string;
            //continue;
            $string_array = array();
            
            $string_array = split(";", $string);
            
            $mark[$string_array[1]] = $string_array[1];
            $model[$string_array[1]][$string_array[3]] = $string_array[3];
            $category_id = $this->getCategoryIDBySeolink($string_array[3]);
            //echo $string_array[3].", category_id = ".$category_id."<hr><br>";
            if ( $category_id and $category_id != 1 ) {
                $product_array = array();
                $product_array['categoryID'] = $category_id;
                $product_array['name'] = mysql_real_escape_string($string_array[2]);
                $product_array['Price'] = trim($string_array[4]);
                $product_array['brief_description'] = mysql_real_escape_string(trim($string_array[3]));
                $product_array['product_code'] = str_replace('-', '', trim($string_array[0]));
            	
                $current_category_seo =  $this->structure[$category_id]['seolink'];
                
                $product_array['seolink'] = $current_category_seo.'-'.$product_array['product_code'];
                //echo "mark = ".$string_array[1].", category_id = $category_id<br>";
                $this->product_processor($product_array);
            }
            
            //print_r($string_array);
        }
        //print_r($mark);
        //print_r($model);
    }
}

if (!strcmp($sub, "extra"))
{
	
}
