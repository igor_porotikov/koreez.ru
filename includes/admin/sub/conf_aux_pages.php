<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php

	if (!strcmp($sub, "aux_pages"))
	{
		if ( isset($_GET["delete"]) )
		{
			if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
			{
				Redirect( "admin.php?dpt=conf&sub=aux_pages&safemode=yes" );
			}
			auxpgDeleteAuxPage( $_GET["delete"] );
			Redirect( "admin.php?dpt=conf&sub=aux_pages" );
		}
		if ( isset($_GET["add_new"]) )
		{
			$other_pages = auxpgGetOtherPages();
			$smarty->assign( "other_pages", $other_pages );

			if ( isset($_POST["save"]) )
			{
				$aux_page_url = $_POST["aux_page_url"];
				$aux_page_text_type = 0;
				if ( isset($_POST["aux_page_text_type"]) )
					$aux_page_text_type = 1;

				$paretns = (array)$_POST["parents"];
				$similar = (array)$_POST["similar"];

				$imgName = '';
				$has_image = '';
				if ( $newId && $_FILES["picture"] && $_FILES["picture"]["size"]!=0 && preg_match('/\.(jpg|jpeg|gif|jpe|pcx|bmp)$/i', $_FILES["picture"]["name"], $matches)) {
					$ext = $matches[1];
					$imgName = 'tmp' . rand(1000000,9999999) . '.' . $ext;
					$r = move_uploaded_file( $_FILES["picture"]["tmp_name"], "./page_pictures/".$imgName );
					if ( $r ) {
						$has_image = $ext;
						SetRightsToUploadedFile( "./page_pictures/".$imgName );
					}
		
					if ( !file_exists("./page_pictures/".$imgName) ) {
						$has_image = "";
					}
				}

				$newId = auxpgAddAuxPage( $_POST["aux_page_name"], 
						$_POST["aux_page_text"], $aux_page_text_type,
						$_POST["meta_keywords"], $_POST["meta_description"], $aux_page_url, $parents, $similar, $updateChilds=true, $updateSimilar=(bool)(int)$_POST["update_similar"], $_POST["aux_page_description"], $has_image="" );

				if ( $newId && $imgName) {
					$ext = $has_image;
					$newName = $newId . '.' . $ext;
					$r = @rename( "./page_pictures/".$imgName, "./page_pictures/".$newName );
					if ( $r ) {
						SetRightsToUploadedFile( "./page_pictures/".$newName );
					}
				}

				header("Location: admin.php?dpt=conf&sub=aux_pages");
			}

			$smarty->assign( "add_new", 1 );
		}
		else if ( isset($_GET["edit"]) )
		{

			$other_pages = auxpgGetOtherPages((int)$_GET["edit"]);
			$smarty->assign( "other_pages", $other_pages );
			$aux_page = auxpgGetAuxPage( $_GET["edit"] );

			if ( isset($_POST["save"]) )
			{
				if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
				{
					Redirect( "admin.php?dpt=conf&sub=aux_pages&safemode=yes&edit=".$_GET["edit"] );
				}

				$aux_page_url = $_POST["aux_page_url"];
				$aux_page_text_type = 0;
				if ( isset($_POST["aux_page_text_type"]) )
					$aux_page_text_type = 1;

				$parents = (array)$_POST["parents"];
				$similar = (array)$_POST["similar"];

				$has_image = $aux_page['aux_page_image'];

				if ( $_POST['delete_picture'] ) {
					$imgName = (int)$_GET["edit"] . '.' . $has_image;
					@unlink("./page_pictures/".$imgName);
					$has_image = '';
				}

				if ( $_FILES["picture"] && $_FILES["picture"]["size"]!=0 && preg_match('/\.(jpg|jpeg|gif|jpe|pcx|bmp)$/i', $_FILES["picture"]["name"], $matches)) {
					$ext = $matches[1];
					$imgName = (int)$_GET["edit"] . '.' . $ext;
					$r = move_uploaded_file( $_FILES["picture"]["tmp_name"], "./page_pictures/".$imgName );
					if ( $r ) {
						$has_image = $ext;
						SetRightsToUploadedFile( "./page_pictures/".$imgName );
					}
		
					if ( !file_exists("./page_pictures/".$imgName) ) {
						$has_image = "";
					}

				}

				auxpgUpdateAuxPage( $_GET["edit"], $_POST["aux_page_name"], 
					$_POST["aux_page_text"], $aux_page_text_type,
					$_POST["meta_keywords"], $_POST["meta_description"], $aux_page_url, $parents, $similar, $updateChilds=true, $updateSimilar=(bool)(int)$_POST["update_similar"], $_POST["aux_page_description"], $has_image );

				header("Location: admin.php?dpt=conf&sub=aux_pages");
			}

			$aux_page = auxpgGetAuxPage( $_GET["edit"] );

			$aux_page["parents"] = $aux_page["aux_page_parents"]!=='' ? explode( ',', $aux_page["aux_page_parents"] ) : array();
			$aux_page["similar"] = $aux_page["aux_page_similar"]!=='' ? explode( ',', $aux_page["aux_page_similar"] ) : array();

			$smarty->assign( "aux_page", $aux_page );

			$smarty->assign( "edit", 1 );
		}
		else
		{
			$aux_pages = auxpgGetAllPageAttributes();
			$aux_pages_assign = array();
			foreach ( $aux_pages as $key=>$val ) {
				$val["parents"] = $val["aux_page_parents"]!=='' ? explode( ',', $val["aux_page_parents"] ) : array();
				$val["childs"] = $val["aux_page_childs"]!=='' ? explode( ',', $val["aux_page_childs"] ) : array();
				$val["similar"] = $val["aux_page_similar"]!=='' ? explode( ',', $val["aux_page_similar"] ) : array();
				$val["parents_count"] = count( $val["parents"] );
				$val["child_count"] = count( $val["childs"] );
				$val["similar_count"] = count( $val["similar"] );
				$aux_pages_assign[$val['aux_page_ID']] = $val;
			}
			foreach ( $aux_pages_assign as $key=>$val ) {
				$val["childs_list"] = array();
				foreach ( $val["childs"] as $id ) {
					$val["childs_list"][] = $aux_pages_assign[$id];
				}
				$val["similar_list"] = array();
				foreach ( $val["similar"] as $id ) {
					$val["similar_list"][] = $aux_pages_assign[$id];
				}
				$aux_pages_assign[$key] = $val;
			}
			unset( $aux_pages );
			$smarty->assign( "aux_pages", $aux_pages_assign );
		}

		//set sub-department template
		$smarty->assign("admin_sub_dpt", "conf_aux_pages.tpl.html");		
	}

?>