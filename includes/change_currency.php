<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	// currency selection form

	if (  isset($_POST["current_currency"]) )
	{
		currSetCurrentCurrency( $_POST["current_currency"] );
		
		//$url = "index.php";
		$url = $_SERVER['HTTP_REFERER'];
		$paramGetVars = "";
		foreach( $_GET as $key => $value )
		{
			if ( $paramGetVars == "" )
				$paramGetVars .= "?".$key."=".$value;
			else 
				$paramGetVars .= "&".$key."=".$value;
		}

		Redirect( $url.$paramGetVars );
	}

?>