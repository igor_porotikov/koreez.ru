function mouseOver(n) {
  document.getElementById('before_img'+n).style.background='url(/images/works/'+n+'before.jpg) 0 0 no-repeat'; 
  document.getElementById('before_text'+n).style.color='#000';
  document.getElementById('after_text'+n).style.color='#d3d3d3';
};

function mouseOut(n) {
  document.getElementById('before_img'+n).style.background='url(/images//works/'+n+'after.jpg) 0 0 no-repeat'; 
  document.getElementById('before_text'+n).style.color='#d3d3d3';
  document.getElementById('after_text'+n).style.color='#000';
};

function jump_sitebill ( anchorname ) {
	if ( anchorname == "undefined" ) {
		return;
	}
	var anchorarray = new Array();
	anchorarray['general-dentistry'] = 'general';
	anchorarray['24-dentistry'] = '24';
	anchorarray['pediatric-dentistry'] = 'children';

	if ( anchorarray[anchorname] ) {
		window.location.hash = anchorarray[anchorname]; 
	}
}

function sss ( search_string ) {
	document.getElementById('search_name').value=search_string;
	document.forms["search_form"].submit();
}