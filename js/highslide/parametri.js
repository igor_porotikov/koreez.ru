  hs.graphicsDir = '/js/highslide/graphics/';     // ���� � ����������  
  hs.outlineType = 'rounded-white';
  hs.transitions = ['expand'];                  // ������ ����� �������, ����� ��������� ��������: expand, fade, crossfade. � ����� ����� ����� ��� hs.transitions = ['expand/fade','expand/fade/crossfade'], ��� ������ �������� � ����� ��������� ������, ������ ����� ����� �������  
  hs.fadeInOut = true;                          // "���������" ��� �������/�������� ��������                       
  hs.numberPosition = 'caption';                // ������� ����� ��������� �������� caption � heading, ��� ������������� ������� ������/�����  
  hs.dimmingOpacity = 0;                     // ������������ ����  
  hs.align = 'auto';                            // ������������ ������ ������������ ������, ��������� �������� center/left/right/top/bottom, � ��� �� �� ���������� � �����, ���� auto � ������������� �� ���� �����  
  if (hs.addSlideshow) hs.addSlideshow({  
  interval: 5000,                       // �������� ���� � ���������  
  repeat: false,                        // ���������/�� ���������  
  useControls: true,                    // ������������ �� ������ ���������?  
  overlayOptions: {  
  opacity: .6,                  // ������������ ������ ���������  
  position: 'bottom center',     // ������� ������ ���������  
  hideOnMouseOut: false          // ������ ���������, ���� ������ ������ ���� �� ������           
  }  
  });  
