<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
/**
 * @connect_module_class_name DHLShippingModule
 *
 */

class DHLShippingModule extends ShippingRateCalculator{

	var $ShippingServices;
	var $LogFile = './temp/dhl_errors.log';
	
	function _writeLogMessage($_Param1, $_Param2){
	
		if($this->_getSettingValue('CONF_SHIPPING_DHL_ERROR_LOG'))
			ShippingRateCalculator::_writeLogMessage($_Param1, $_Param2);
	}
	
	function _InitVars(){
		
		$this->title = DHLSHIPPINGMODULE_TTL;
		$this->description = DHLSHIPPINGMODULE_DSCR;
		$this->sort_order = 0;
		$this->Settings = array(
			'CONF_SHIPPING_DHL_TEST_MODE',
			'CONF_SHIPPING_DHL_ERROR_LOG',
			'CONF_SHIPPING_DHL_LOGIN_ID',
			'CONF_SHIPPING_DHL_PASSWORD',
			'CONF_SHIPPING_DHL_ACCOUNT_NUMBER',
			'CONF_SHIPPING_DHL_SHIPPING_KEY',
			'CONF_SHIPPING_DHL_ISHIPPING_KEY',
			'CONF_SHIPPING_DHL_DUTIABLE',
			'CONF_SHIPPING_DHL_SHIPDATE',
			'CONF_SHIPPING_DHL_SHIPMENT_TYPE',
			'CONF_SHIPPING_DHL_AP',
			'CONF_SHIPPING_DHL_USD_CURRENCY',
			'CONF_SHIPPING_DHL_SERVICES',
			);
			
		$this->ShippingServices = array(
			1 => array(
				'id' => 1,
				'name' => 'Express',
				'xmlCode'=> 'E',
				'max weight' => 150,
				'number' => 0,
			),
			2 => array(
				'id' => 2,
				'name' => 'Express 10:30 AM',
				'xmlCode'=> 'E',
				'spCode' => '1030',
				'max weight' => 150,
				'number' => 1,
			),
			3 => array(
				'id' =>3,
				'name' => 'Express Saturday',
				'xmlCode'=> 'E',
				'spCode' => 'SAT',
				'max weight' => 150,
				'number' => 2,
			),
			4 => array(
				'id' => 4,
				'name' => 'Next Afternoon ',
				'xmlCode'=> 'N',
				'max weight' => 150,
				'number' => 3,
			),
			5 => array(
				'id' => 5,
				'name' => 'Ground',
				'xmlCode'=> 'G',
				'max weight' => 150,
				'number' => 4,
			),
			6 => array(
				'id' => 6,
				'name' => 'Second Day Service',
				'xmlCode'=> 'S',
				'max weight' => 150,
				'number' => 5,
			),
			7 => array(
				'id' => 7,
				'name' => 'International delivery',
				'xmlCode'=> 'IE',
				'number' => 7,
			),
		);
	}
	
	function _initSettingFields(){
		
		$this->SettingsFields['CONF_SHIPPING_DHL_DUTIABLE'] = array(
			'settings_value' 		=> 1, 
			'settings_title' 			=> DHL_CNF_DUTIABLE_TTL, 
			'settings_description' 	=> DHL_CNF_DUTIABLE_DSCR, 
			'settings_html_function' 	=> 'setting_CHECK_BOX(', 
			'sort_order' 			=> 0,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_AP'] = array(
			'settings_value' 		=> 0, 
			'settings_title' 			=> DHL_CNF_AP_TTL, 
			'settings_description' 	=> DHL_CNF_AP_DSCR, 
			'settings_html_function' 	=> 'setting_CHECK_BOX(', 
			'sort_order' 			=> 0,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_USD_CURRENCY'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_USD_CURRENCY_TTL, 
			'settings_description' 	=> DHL_CNF_USD_CURRENCY_DSCR, 
			'settings_html_function' 	=> 'setting_CURRENCY_SELECT(', 
			'sort_order' 			=> 0,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_TEST_MODE'] = array(
			'settings_value' 		=> 1, 
			'settings_title' 			=> DHL_CNF_TEST_MODE_TTL, 
			'settings_description' 	=> DHL_CNF_TEST_MODE_DSCR, 
			'settings_html_function' 	=> 'setting_CHECK_BOX(', 
			'sort_order' 			=> 0,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_ERROR_LOG'] = array(
			'settings_value' 		=> 1, 
			'settings_title' 			=> DHL_CNF_ERROR_LOG_TTL, 
			'settings_description' 	=> DHL_CNF_ERROR_LOG_DSCR, 
			'settings_html_function' 	=> 'setting_CHECK_BOX(', 
			'sort_order' 			=> 0,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_PASSWORD'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_PASSWORD_TTL, 
			'settings_description' 	=> DHL_CNF_PASSWORD_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,true,', 
			'sort_order' 			=> 10,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_LOGIN_ID'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_LOGIN_ID_TTL, 
			'settings_description' 	=> DHL_CNF_LOGIN_ID_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,true,', 
			'sort_order' 			=> 10,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_ACCOUNT_NUMBER'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_ACCOUNT_NUMBER_TTL, 
			'settings_description' 	=> DHL_CNF_ACCOUNT_NUMBER_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,true,', 
			'sort_order' 			=> 10,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_SHIPPING_KEY'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_SHIPPING_KEY_TTL, 
			'settings_description' 	=> DHL_CNF_SHIPPING_KEY_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,true,', 
			'sort_order' 			=> 20,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_ISHIPPING_KEY'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_ISHIPPING_KEY_TTL, 
			'settings_description' 	=> DHL_CNF_ISHIPPING_KEY_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,true,', 
			'sort_order' 			=> 20,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_SHIPDATE'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_SHIPDATE_TTL, 
			'settings_description' 	=> DHL_CNF_SHIPDATE_DSCR, 
			'settings_html_function' 	=> 'setting_TEXT_BOX(0,', 
			'sort_order' 			=> 30,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_SHIPMENT_TYPE'] = array(
			'settings_value' 		=> 'P', 
			'settings_title' 			=> DHL_CNF_SHIPMENT_TYPE_TTL, 
			'settings_description' 	=> DHL_CNF_SHIPMENT_TYPE_DSCR, 
			'settings_html_function' 	=> 'setting_SELECT_BOX(DHLShippingModule::_getShipmentType(),', 
			'sort_order' 			=> 40,
		);
		$this->SettingsFields['CONF_SHIPPING_DHL_SERVICES'] = array(
			'settings_value' 		=> '', 
			'settings_title' 			=> DHL_CNF_SERVICES_TTL, 
			'settings_description' 	=> DHL_CNF_SERVICES_DSCR, 
			'settings_html_function' 	=> 'setting_CHECKBOX_LIST(DHLShippingModule::_getServices(),', 
			'sort_order' 			=> 6,
		);
	}
	
	function _prepareXMLQuery(&$_Services,  $order, $address){

		$XMLQuery = 
		'<?xml version="1.0"?>
		<eCommerce action="Request" version="1.1">
			<Requestor>
				<ID>'.$this->_getSettingValue('CONF_SHIPPING_DHL_LOGIN_ID').'</ID>
				<Password>'.$this->_getSettingValue('CONF_SHIPPING_DHL_PASSWORD').'</Password>
			</Requestor>';

		$OrderWeight = '';
		$OrderAmount = ceil($this->_convertCurrency($order['order_amount'], 0, $this->_getSettingValue('CONF_SHIPPING_DHL_USD_CURRENCY')));
		
		if($this->_getSettingValue('CONF_SHIPPING_DHL_SHIPMENT_TYPE')!='L'){
		
			$OrderWeight = $this->_getOrderWeight($order);
			if(!$OrderWeight) return '';
			
			$OrderWeight = ceil($this->_convertMeasurement($OrderWeight, CONF_WEIGHT_UNIT, 'LBS'));
			
		}
		
		$DestCountry = cnGetCountryById($address['countryID']);
		$DestCountry = $DestCountry['country_iso_2'];
		
		$DestProvinceCode = znGetSingleZoneById($address['zoneID']);
		$DestProvinceCode = $DestProvinceCode['zone_code'];

		$International = $address['countryID']==CONF_COUNTRY?false:true;
		
		$_TC = count($_Services)-1;
		for(;$_TC>=0;$_TC--){
			
			if(!($this->_getSettingValue('CONF_SHIPPING_DHL_SERVICES')&pow(2, $this->ShippingServices[$_Services[$_TC]['id']]['number'])))continue;
			if($International && $_Services[$_TC]['id']!=7)continue;
			if(!$International && $_Services[$_TC]['id']==7)continue;
			if(0&&isset($this->ShippingServices[$_Services[$_TC]['id']]['max weight']) && $OrderWeight>$this->ShippingServices[$_Services[$_TC]['id']]['max weight']){
			
				$this->_writeLogMessage(0, str_replace('{%WEIGHT%}', $OrderWeight, DHL_TXTER_OVERWEIGHT));
				continue;
			}
			$XMLQuery .= '
				<'.($International?'IntlShipment action="RateEstimate" version="1.0"':'Shipment action="RateEstimate" version="1.0"').'>
					<ShippingCredentials>
						<ShippingKey>'.$this->_getSettingValue($International?'CONF_SHIPPING_DHL_ISHIPPING_KEY':'CONF_SHIPPING_DHL_SHIPPING_KEY').'</ShippingKey>
						<AccountNbr>'.$this->_getSettingValue('CONF_SHIPPING_DHL_ACCOUNT_NUMBER').'</AccountNbr>
					</ShippingCredentials>
					<ShipmentDetail>
						<ShipDate>'.date("Y-m-d", time()+3600*24*$this->_getSettingValue('CONF_SHIPPING_DHL_SHIPDATE')).'</ShipDate>
						<Service>
							<Code>'.$this->ShippingServices[$_Services[$_TC]['id']]['xmlCode'].'</Code>
						</Service>
						<ShipmentType>
							<Code>'.$this->_getSettingValue('CONF_SHIPPING_DHL_SHIPMENT_TYPE').'</Code>
						</ShipmentType>'.(isset($this->ShippingServices[$_Services[$_TC]['id']]['spCode'])?'
						<SpecialServices>
							<SpecialService>
								<Code>'.$this->ShippingServices[$_Services[$_TC]['id']]['spCode'].'</Code>
							</SpecialService>
						</SpecialServices>
						':'').($OrderWeight?'<Weight>'.$OrderWeight.'</Weight>
						':'').
						($this->_getSettingValue('CONF_SHIPPING_DHL_AP')?'
						<AdditionalProtection>
							<Code>AP</Code>
							<Value>'.$OrderAmount.'</Value>
						</AdditionalProtection>
						':'').
						($International?'
						<ContentDesc>SS goods</ContentDesc>
						':'').
					'</ShipmentDetail>
					'.($International?
					'<Dutiable>
					 <DutiableFlag>'.($this->_getSettingValue('CONF_SHIPPING_DHL_DUTIABLE')?'Y':'N').'</DutiableFlag>
					 <CustomsValue>'.$OrderAmount.'</CustomsValue>
					</Dutiable> '
					:'').
					'<Billing>
						<Party>
							<Code>S</Code>
						</Party>
					'.($International?
						'<DutyPaymentType>S</DutyPaymentType>':'').'
					</Billing>
					<Receiver>
						<Address>
							'.($International?'
							<Street><![CDATA['.($address['address']).']]></Street>
							<City><![CDATA['.($address['city']).']]></City>
							':'').'
							<State>'.$DestProvinceCode.'</State>
							<Country>'.$DestCountry.'</Country>
							<PostalCode>'.$address['zip'].'</PostalCode>
						</Address>
						'.($International?'
						':'').'
					</Receiver>
					<TransactionTrace>'.$_Services[$_TC]['id'].'</TransactionTrace>
				</'.($International?'IntlShipment':'Shipment').'>
				';
		}
		$XMLQuery .= '
		</eCommerce>';
if(0){
		header('Content-type: application/xml');
		print $XMLQuery;
		die;
}
		return $XMLQuery;
	}
	
	function _sendXMLQuery($_XMLQuery){

		if(!$_XMLQuery)return '';
	
		if ( !($ch = curl_init()) ){
			
			$this->_writeLogMessage(MODULE_LOG_CURL, 'Curl error: '.ERR_CURLINIT);
			return ERR_CURLINIT;
		}

		if ( curl_errno($ch) != 0 ){
			
			$this->_writeLogMessage(MODULE_LOG_CURL, 'Curl error: '.curl_errno($ch));
			return ERR_CURLINIT;
		}

		if($this->_getSettingValue('CONF_SHIPPING_DHL_TEST_MODE'))
			$url = 'https://eCommerce.airborne.com/ApiLandingTest.asp';
		else
			$url = 'https://eCommerce.airborne.com/ApiLanding.asp';
			
		@curl_setopt($ch, CURLOPT_URL, $url );
		@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		@curl_setopt($ch, CURLOPT_HEADER, 0);
		@curl_setopt($ch, CURLOPT_POST, 1);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $_XMLQuery);
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		@curl_setopt( $ch, CURLOPT_TIMEOUT, 20 );

		$result = @curl_exec($ch);
		if ( curl_errno($ch) != 0){
			
			$this->_writeLogMessage(MODULE_LOG_CURL, 'Curl error: '.curl_errno($ch));
			return ERR_CURLEXEC;
		}

		curl_close($ch);

if(0){
		header('Content-type: application/xml');
		print $result;
		die;
}
		return $result;
	}
	
	function _parseXMLAnswer( $_XMLResponse ){

		$Rates 		= array();
		if(!$_XMLResponse)return $Rates;
		$_t = new xml2array();
		@list($XMLArray) = $_t->parse($_XMLResponse);
		
		if(!isset($XMLArray['children']))return $Rates;
		
		$_TC = count($XMLArray['children'])-1;
		for(;$_TC>=0; $_TC--){
		
			if($XMLArray['children'][$_TC]['name']=='FAULTS'){
			
				$_t = &$XMLArray['children'][$_TC]['children'];
				$_jj = count($_t)-1;
				for(;$_jj>=0;$_jj--){
				
					if($_t[$_jj]['name']!='FAULT')continue;
					$__ll = count($_t[$_jj]['children'])-1;
					$Fault = array(
						'CODE' => '',
						'DESCRIPTION' => '',
						'SOURCE' => '',
					);
					for(;$__ll>=0;$__ll--){
					
						$Fault[$_t[$_jj]['children'][$__ll]['name']] = isset($_t[$_jj]['children'][$__ll]['tagData'])?$_t[$_jj]['children'][$__ll]['tagData']:'';
					}
					$this->_writeLogMessage(0, "\r\n".'Code: '.$Fault['CODE']."\r\n".'Desc: '.$Fault['DESCRIPTION']);
				}
			}elseif($XMLArray['children'][$_TC]['name']=='SHIPMENT' || $XMLArray['children'][$_TC]['name']=='INTLSHIPMENT'){
			
				$__TC = count($XMLArray['children'][$_TC]['children'])-1;
				$_tRate = array(
					'id'=>'',
					'name'=>'',
					'rate'=>0,
				);
				$SpecialCode = '';
				for(;$__TC>=0; $__TC--){
				
					switch($XMLArray['children'][$_TC]['children'][$__TC]['name']){
					
						case 'SHIPMENTDETAIL':
						case 'ESTIMATEDETAIL':
					
							$_t = &$XMLArray['children'][$_TC]['children'][$__TC]['children'];
							$_jj = count($_t)-1;
							for(;$_jj>=0;$_jj--){
							
								switch($_t[$_jj]['name']){
									case 'RATEESTIMATE':
										$___TC = count($_t[$_jj]['children'])-1;
										for(;$___TC>=0; $___TC--){
										
											if($_t[$_jj]['children'][$___TC]['name']=='TOTALCHARGEESTIMATE'){
											
												$_tRate['rate'] = $this->_convertCurrency($_t[$_jj]['children'][$___TC]['tagData'], $this->_getSettingValue('CONF_SHIPPING_DHL_USD_CURRENCY'), 0);
											}
										}
										break;
								}
							}
							break;
						case 'TRANSACTIONTRACE':
						
							if(isset($XMLArray['children'][$_TC]['children'][$__TC]['tagData']))$_tRate['id'] = $XMLArray['children'][$_TC]['children'][$__TC]['tagData'];
							break;
						case 'FAULTS':
						
							$_t = &$XMLArray['children'][$_TC]['children'][$__TC]['children'];
							$_jj = count($_t)-1;
							for(;$_jj>=0;$_jj--){
							
								if($_t[$_jj]['name']!='FAULT')continue;
								$__ll = count($_t[$_jj]['children'])-1;
								$Fault = array(
									'CODE' => '',
									'DESC' => '',
									'SOURCE' => '',
								);
								for(;$__ll>=0;$__ll--){
								
									$Fault[$_t[$_jj]['children'][$__ll]['name']] = isset($_t[$_jj]['children'][$__ll]['tagData'])?$_t[$_jj]['children'][$__ll]['tagData']:'';
								}
								$this->_writeLogMessage(0, "\r\n".'Code: '.$Fault['CODE']."\r\n".'Desc: '.$Fault['DESC']."\r\n".'Source: '.$Fault['SOURCE']);
							}
							break;
					}
				}
				
				if(isset($this->ShippingServices[$_tRate['id']]['name']))
					$_tRate['name'] = $this->ShippingServices[$_tRate['id']]['name'];
				else
					$_tRate['name'] = $_tRate['id'];
					
				if($_tRate['rate']>0)$Rates[$_tRate['id']][] = $_tRate;
			}
		}
		return $Rates;
	}

	function getShippingServices($_Type = '', $_ID = null){
		
		$_ShippingServices = &$this->ShippingServices;
		
		if(isset($_ID))return $_ShippingServices[$_ID];
		
		$ShippingTypes = $this->_getShippingTypes();
		
		if (!in_array($_Type, array_keys($ShippingTypes)) && $_Type) return array();
		
		if(!$_Type)return  $_ShippingServices;
		else{
			
			$_tRet = array();
			foreach ($ShippingTypes[$_Type] as $_ind){
				
				$_tRet[$_ind] = $_ShippingServices[$_ind];
			}
			return $_tRet;
		}
	}

	function _getShipmentType(){
		return 'Package:P,Letter:L';
	}

	function _getServices(){
	
		$_t = new DHLShippingModule();
		$_Servs = $_t->getShippingServices();
		$_boxDescr = array();
		foreach ($_Servs as $_Serv){
			
			$_boxDescr[$_Serv['number']] = $_Serv['name'];
		}
		return $_boxDescr;
	}
}
?>