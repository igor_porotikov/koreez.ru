<?php
define('FEDEXSHIPPINGMODULE_TTL', 
	'FedEx');
define('FEDEXSHIPPINGMODULE_DSCR', 
	'FedEx Ship Manager Direct. ������ ��������� �������� � FedEx.<br>���������� ������� �������� �� www.fedex.com');
	
define('FEDEX_CNF_ACCOUNT_NUMBER_TTL', 
	'Account number');
define('FEDEX_CNF_ACCOUNT_NUMBER_DSCR', 
	'����� ������� ������ (��������) � FedEx');
	
define('FEDEX_CNF_METER_NUMBER_TTL', 
	'Meter number');
define('FEDEX_CNF_METER_NUMBER_DSCR', 
	'���� � ��� ��� Meter number, �������� ��� ���� ������. Meter number ����� ������������ �������������.');
	
define('FEDEX_CNF_PACKAGING_TTL', 
	'��������');
define('FEDEX_CNF_PACKAGING_DSCR', 
	'� ������, ���� �� ����������� \'FedEx Ground\' ���������� ������� \'Your packaging\'');
	
define('FEDEX_CNF_CARRIER_TTL', 
	'������');
define('FEDEX_CNF_CARRIER_DSCR', 
	'�������� ������ FedEx');
	
define('FEDEX_CNF_STATE_OR_PROVINCE_CODE_TTL', 
	'����/��������� �����������');
define('FEDEX_CNF_STATE_OR_PROVINCE_CODE_DSCR', 
	'������������ ����, ���� ������ ����������� ��� ��� ������<br />
	������� �������� �����/���������, �� ������� �� ����������� ������.');
	
define('FEDEX_CNF_POSTAL_CODE_TTL', 
	'�������� ��� (������, ZIP-���) �����������');
define('FEDEX_CNF_POSTAL_CODE_DSCR', 
	'������������ ����, ���� ������ ����������� ��� ��� ������<br />
	������� ������� ������ ����� ����������� �������.');
	
define('FEDEX_CNF_COUNTRY_CODE_TTL', 
	'������ ����������');
define('FEDEX_CNF_COUNTRY_CODE_DSCR', 
	'������� ������ ����������� �������.');	
	
define('FEDEX_CNF_ADDRESS_TTL', 
	'�����');
define('FEDEX_CNF_ADDRESS_DSCR', 
	'������� ��� �����<br />
	���������� ���������� ��� ������������ Meter number');
define('FEDEX_CNF_CITY_TTL', 
	'�����');
define('FEDEX_CNF_CITY_DSCR', 
	'���������� ���������� ��� ������������ Meter number');
define('FEDEX_CNF_PHONE_NUMBER_TTL', 
	'����� ��������');
define('FEDEX_CNF_PHONE_NUMBER_DSCR', 
	'111-222-3333<br />
	���������� ���������� ��� ������������ Meter number');
define('FEDEX_CNF_NAME_TTL', 
	'���� ���');
define('FEDEX_CNF_NAME_DSCR', 
	'���������� ���������� ��� ������������ Meter number');
define('FEDEX_CNF_ERROR_LOG_TTL', 
	'�������� ������ ��������� ������� ������� FedEx');
define('FEDEX_CNF_ERROR_LOG_DSCR', 
	'� ������ ������ ������� ��������� ��������, ��������� �� ������ ������������ � ���� temp/fedex_errors.log');

define('FEDEX_CNF_TESTMODE_TTL','�������� �����');
define('FEDEX_CNF_TESTMODE_DSCR','');
define('FEDEX_CNF_CURRENCY_TTL','������ "������� ���"');
define('FEDEX_CNF_CURRENCY_DSCR','��������� ��������, ������������� FedEx, ����������� � �������� ���. �������� ������ ������ ��������, ������� ������������ ����� ������� ��� ��� ����������� ��������� ��������� �������� � ������ ������.');
?>