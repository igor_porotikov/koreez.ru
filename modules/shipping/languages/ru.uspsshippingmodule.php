<?php
define('SHIPPING_MODULE_USPS_TTL','USPS');
define('SHIPPING_MODULE_USPS_DSCR','USPS Web Tools. ������ ��������� �������� � USPS. �������� ������ �� ���.<br>���������� ������� �������� �� www.usps.com<br>��������� ���������� �� ��������� � ������� ����� ������ ��������� � ����������� ������������');

define('USPS_CONF_ZIPORIGINATION_TTL','�������� ������ (Zip) �����������');
define('USPS_CONF_ZIPORIGINATION_DSCR','������������ ����� 5 ��������');

define('USPS_CONF_USERID_TTL','USPS User ID');
define('USPS_CONF_USERID_DSCR','����� � ����� ������� ������ USPS');

define('USPS_CONF_PACKAGESIZE_TTL','������ ��������');
define('USPS_CONF_PACKAGESIZE_DSCR','������ ��� ���������� ��������� (������ ���). ��������� �� www.usps.com');

define('USPS_CONF_MACHINABLE_TTL','Machinable');
define('USPS_CONF_MACHINABLE_DSCR', '����� �� ����� ������������������ ������������� �������? ������ ��� ���������� ��������� (������ ���). ��������� �� www.usps.com');

define('USPS_CONF_DOMESTIC_SERVS_TTL','��������� ������� ���������� ���������');
define('USPS_CONF_DOMESTIC_SERVS_DSCR','������ ���');

define('USPS_CONF_INTERNATIONAL_SERVS_TTL','���������� ������� ������������� ���������');
define('USPS_CONF_INTERNATIONAL_SERVS_DSCR','');

define('USPSSHIPPINGMODULE_CFG_ENABLE_ERROR_LOG_TTL', '�������� ������ ��������� ������� ������� USPS');
define('USPSSHIPPINGMODULE_CFG_ENABLE_ERROR_LOG_DSCR', '� ������ ������ ������� ��������� ��������, ��������� �� ������ ������������ � ���� temp/usps_errors.log');

define('USPS_CONF_USD_CURRENCY_TTL', '������ "������� ���"');
define('USPS_CONF_USD_CURRENCY_DSCR', '��������� ��������, ������������� USPS, ����������� � �������� ���. �������� ������ ������ ��������, ������� ������������ ����� ������� ��� ��� ����������� ��������� ��������� �������� � ������ ������.');
?>