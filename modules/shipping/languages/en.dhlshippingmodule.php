<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
define('DHLSHIPPINGMODULE_TTL', 'DHL');
define('DHLSHIPPINGMODULE_DSCR', 'DHL Airborne. Real-time shipping rate calculations.<br>Your need to have an account with www.dhl.com to make this module work');
define('DHL_CNF_ACCOUNT_NUMBER_TTL', 'Account number');
define('DHL_CNF_ACCOUNT_NUMBER_DSCR', 'Please enter your DHL account number');
define('DHL_CNF_ISHIPPING_KEY_TTL', 'International Shipping Key');
define('DHL_CNF_ISHIPPING_KEY_DSCR', 'Please enter Shipping Key provided to you by DHL');
define('DHL_CNF_SHIPPING_KEY_TTL', 'Domestic Shipping Key');
define('DHL_CNF_SHIPPING_KEY_DSCR', 'Please enter Shipping Key provided to you by DHL');
define('DHL_CNF_DUTIABLE_TTL', 'Dutiable');
define('DHL_CNF_DUTIABLE_DSCR', 'For international shipments only');
define('DHL_CNF_SHIPDATE_TTL', 'Ship in X days');
define('DHL_CNF_SHIPDATE_DSCR', 'Enter the number of days from the day of order in which the package is to be picked up by DHL');
define('DHL_CNF_SHIPMENT_TYPE_TTL', 'Shipment type');
define('DHL_CNF_SHIPMENT_TYPE_DSCR', 'Select shipment package type');
define('DHL_CNF_TEST_MODE_TTL', 'Test environment');
define('DHL_CNF_TEST_MODE_DSCR', 'Enable to run DHL module in test environment; and disable when moving to production');
define('DHL_CNF_LOGIN_ID_TTL', 'API System ID');
define('DHL_CNF_LOGIN_ID_DSCR', 'Enter your DHL API System ID');
define('DHL_CNF_PASSWORD_TTL', 'API Password');
define('DHL_CNF_PASSWORD_DSCR', 'Enter your DHL API password');
define('DHL_CNF_SERVICES_TTL', 'Available services');
define('DHL_CNF_SERVICES_DSCR', 'Select DHL services to be offered to customer');
define('DHL_CNF_AP_TTL','Additional protection');
define('DHL_CNF_AP_DSCR','Enable if you want quote rates with additional protection against potential loss or damage included');
define('DHL_CNF_USD_CURRENCY_TTL','USD currency type');
define('DHL_CNF_USD_CURRENCY_DSCR','Shipping charges calculated by DHL server are denominated in USD. Specify currency type in your shopping cart which is assumed as USD to make shipping charges recalculated properly (according to USD exchange rate)');
define('DHL_CNF_ERROR_LOG_TTL','Enable DHL error log');
define('DHL_CNF_ERROR_LOG_DSCR','If enabled, DHL error response codes will be saved into temp/dhl_errors.log file');

define('DHL_TXTER_OVERWEIGHT','Maximum allowed weight exceeded ({%WEIGHT%})');
?>