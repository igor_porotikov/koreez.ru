<?php
define('UPSSHIPPINGMODULE_TTL',
	'UPS');
define('UPSSHIPPINGMODULE_DSCR',
	'UPS OnLine Tools. ������ ��������� �������� � UPS.<br>���������� ������� �������� �� www.ups.com');
	
define('UPSSHIPPINGMODULE_CFG_PACKAGE_TYPE_TTL',
	'��� ��������');
define('UPSSHIPPINGMODULE_CFG_PACKAGE_TYPE_DSCR',
	'');
define('UPSSHIPPINGMODULE_CFG_PICKUP_TYPE_TTL',
	'Pickup type');
define('UPSSHIPPINGMODULE_CFG_PICKUP_TYPE_DSCR',
	'������ �������� ������� � UPS');
define('UPSSHIPPINGMODULE_CFG_ACCESSLICENSENUMBER_TTL',
	'XML Access Key');
define('UPSSHIPPINGMODULE_CFG_ACCESSLICENSENUMBER_DSCR',
	'������� XML ���� �������, ��������������� ��������� UPS');
define('UPSSHIPPINGMODULE_CFG_USERID_TTL',
	'UPS User ID');
define('UPSSHIPPINGMODULE_CFG_USERID_DSCR',
	'��� ������������� (��� ������������ � UPS)');
define('UPSSHIPPINGMODULE_CFG_PASSWORD_TTL',
	'UPS Password');
define('UPSSHIPPINGMODULE_CFG_PASSWORD_DSCR',
	'������ ������� � ������ UPS-��������');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_COUNTRY_ID_TTL',
	'������ �����������');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_COUNTRY_ID_DSCR',
	'�������� ������ ����������� (������ ��������-��������)');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_CITY_TTL',
	'����� �����������');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_CITY_DSCR',
	'�����, �� �������� ����� ������������� �������� �������');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_POSTALCODE_TTL',
	'�������� ��� (������, ZIP-���) �����������');
define('UPSSHIPPINGMODULE_CFG_SHIPPER_POSTALCODE_DSCR',
	'������ ����������� (������ ��������-��������). ����������� ��� ����������');
define('UPSSHIPPINGMODULE_CFG_ENABLE_ERROR_LOG_TTL', '�������� ������ ��������� ������� ������� UPS');
define('UPSSHIPPINGMODULE_CFG__ENABLE_ERROR_LOG_DSCR', '� ������ ������ ������� ��������� ��������, ��������� �� ������ ������������ � ���� temp/ups_errors.log');

define('UPSSHIPPINGMODULE_CFG_USD_CURRENCY_TTL', '������ "������� ���"');
define('UPSSHIPPINGMODULE_CFG_USD_CURRENCY_DSCR', '��������� ��������, ������������� UPS, ����������� � �������� ���. �������� ������ ������ ��������, ������� ������������ ����� ������� ��� ��� ����������� ��������� ��������� �������� � ������ ������.');

define('UPSSHIPPINGMODULE_CFG_CUSTOMER_CLASSIFICATION_TTL', '������������� ����������� ������ ��������');
define('UPSSHIPPINGMODULE_CFG_CUSTOMER_CLASSIFICATION_DSCR', '������� "�� ���������", ����� ������������ �������� UPS �� ���������');
?>