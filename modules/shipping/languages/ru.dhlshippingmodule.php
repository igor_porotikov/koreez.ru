<?php
define('DHLSHIPPINGMODULE_TTL', 'DHL');
define('DHLSHIPPINGMODULE_DSCR', 'DHL Airborne. ������ ��������� �������� � DHL.<br>���������� ������� �������� �� www.dhl.com');
define('DHL_CNF_ACCOUNT_NUMBER_TTL', 'Account number');
define('DHL_CNF_ACCOUNT_NUMBER_DSCR', '����� ������� ������ (��������) � DHL');
define('DHL_CNF_SHIPPING_KEY_TTL', 'Domestic Shipping key');
define('DHL_CNF_SHIPPING_KEY_DSCR', '������� Shipping Key, ��������������� ��� DHL');
define('DHL_CNF_ISHIPPING_KEY_TTL', 'International Shipping key');
define('DHL_CNF_ISHIPPING_KEY_DSCR', '������� Shipping Key, ��������������� ��� DHL');
define('DHL_CNF_DUTIABLE_TTL', '�������� ��������� ���������� ��������');
define('DHL_CNF_DUTIABLE_DSCR', '������ ��� ������������� ��������');
define('DHL_CNF_SHIPDATE_TTL', '����������� ����� X ����');
define('DHL_CNF_SHIPDATE_DSCR', '������� ���������� ����, �� ��������� ������� � ������� ���������� ������ DHL ������ ������� �������');
define('DHL_CNF_SHIPMENT_TYPE_TTL', '��������');
define('DHL_CNF_SHIPMENT_TYPE_DSCR', '�������� ������ ��������� �������');
define('DHL_CNF_TEST_MODE_TTL', '�������� �����');
define('DHL_CNF_TEST_MODE_DSCR', '');
define('DHL_CNF_LOGIN_ID_TTL', 'API System ID');
define('DHL_CNF_LOGIN_ID_DSCR', '������� API System ID, ��������������� DHL');
define('DHL_CNF_PASSWORD_TTL', 'API Password');
define('DHL_CNF_PASSWORD_DSCR', '������� API Password, ��������������� DHL');
define('DHL_CNF_SERVICES_TTL', '��������� �������');
define('DHL_CNF_SERVICES_DSCR', '�������� ������� DHL, ������� ����� ���������� ���������� ��� ������ ��� ���������� ������');
define('DHL_CNF_AP_TTL','����������� �����������');
define('DHL_CNF_AP_DSCR','��������, ���� �� ������, ����� ��������� �������� ������������� � ������ ��������� ��������� �� ������ � ������');
define('DHL_CNF_USD_CURRENCY_TTL','������ "������� ���"');
define('DHL_CNF_USD_CURRENCY_DSCR','��������� ��������, ������������� DHL, ����������� � �������� ���. �������� ������ ������ ��������, ������� ������������ ����� ������� ��� ��� ����������� ��������� ��������� �������� � ������ ������.');
define('DHL_CNF_ERROR_LOG_TTL','�������� ������ ��������� ������� ������� DHL');
define('DHL_CNF_ERROR_LOG_DSCR','� ������ ������ ������� ��������� ��������, ��������� �� ������ ������������ � ���� temp/dhl_errors.log');

define('DHL_TXTER_OVERWEIGHT','�������� ����������� ���������� ��� ({%WEIGHT%})');
?>