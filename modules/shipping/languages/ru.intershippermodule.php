<?php
define('INTERSHIPPERMODULE_TTL', 'InterShipper');
define('INTERSHIPPERMODULE_DSCR', '������ ��������� �������� � UPS, USPS, DHL, FedEx.<br>��������� ������� �������� �� www.intershipper.com');

define('INTERSHIPPER_CFG_USERNAME_TTL', '��� ������������ � ������� InterShipper');
define('INTERSHIPPER_CFG_USERNAME_DSCR', '������� ���������� � ����� ������� ������ � InterShipper');

define('INTERSHIPPER_CFG_PASSWORD_TTL', '������ ��� ������� ������  � ������� InterShipper');
define('INTERSHIPPER_CFG_PASSWORD_DSCR', '������� ���������� � ����� ������� ������ � InterShipper');

define('INTERSHIPPER_CFG_CLASSES_TTL', '���� ��������');
define('INTERSHIPPER_CFG_CLASSES_DSCR', '�������� ������������ ������������ ���� (������) ��������');

define('INTERSHIPPER_CFG_SHIPMETHOD_TTL', '��� ������� ������� � ������������ ��������');
define('INTERSHIPPER_CFG_SHIPMETHOD_DSCR', '�������� ������ �������� ����������� � ��������-�����������');

define('INTERSHIPPER_CFG_SHMOPTION_TTL', '�������������� ���������� � ������� ��������� ������� ��������� ��������');
define('INTERSHIPPER_CFG_SHMOPTION_DSCR', '������� �������������� ���������� � ����������� �� ���������� ������� �������� ����������� �����������');

define('INTERSHIPPER_CFG_PACKAGING_TTL', '��������');
define('INTERSHIPPER_CFG_PACKAGING_DSCR', '�������� ������ �������� ����������� (�������)');

define('INTERSHIPPER_CFG_CONTENTS_TTL', '���������� �������');
define('INTERSHIPPER_CFG_CONTENTS_DSCR', '��������������� ��� ������������ �������');

define('INTERSHIPPER_CFG_INSURANCE_TTL', '��������� �������');
define('INTERSHIPPER_CFG_INSURANCE_DSCR', '������� ������� �� ��������� ������ (������: 10%), ������ ����� (������: 15.96) ��� �������� ���� ������, ���� ��������� �� �����');

define('INTERSHIPPER_CFG_USD_TTL', '������ "������� ���"');
define('INTERSHIPPER_CFG_USD_DSCR', '��������� ��������, ������������� �������� InterShipper, ����������� � �������� ���. �������� ������ ������ ��������, ������� ������������ ����� ������� ��� ��� ����������� ��������� ��������� �������� � ������ ������.');

define('INTERSHIPPER_CFG_CARRIERS_TTL', '��������-�����������');
define('INTERSHIPPER_CFG_CARRIERS_DSCR', '�������� ��������� �� ��������, �������� ������� �� �����������. ��������� �������� ����� ������� ����� ������������� ����� ������ �� ��������� ��������.');

define('INTERSHIPPER_CFG_STATE_TTL', 
	'����/��������� �����������');
define('INTERSHIPPER_CFG_STATE_DSCR', 
	'������� ����/���������, �� ������� ������������ ������');
	
define('INTERSHIPPER_CFG_POSTAL_TTL', 
	'�������� ��� (������, ZIP-���) �����������');
define('INTERSHIPPER_CFG_POSTAL_DSCR', 
	'������� �������� ������ (zip) ����� ����������� �������');
	
define('INTERSHIPPER_CFG_COUNTRY_TTL', 
	'������ �����������');
define('INTERSHIPPER_CFG_COUNTRY_DSCR', 
	'InterShipper ����������� ��������� �������� ������ ��� ����������� � ���������� ���. �������� ��� � ������ �����');
	
define('INTERSHIPPER_CFG_CITY_TTL', 
	'�����');
define('INTERSHIPPER_CFG_CITY_DSCR', 
	'������� �������� ������, �� �������� ����� ������������� �����������');
	
	
define('INTERSHIPPER_TXT_CARRIER_ACCOUNT', 
	'������� ���� ��� ������������ ������� ������ � ���� ��������:<br>(���� � ��� ��� ������� ������, �������� ���� ������)');
define('INTERSHIPPER_TXT_CARRIER_INVOICED', 
	'� ������� ����� ��������������� �� ��������-�����������');

?>