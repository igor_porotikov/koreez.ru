<?php
// -------------------------INITIALIZATION-----------------------------//
	//include core files
	include("./cfg/connect.inc.php");
	include("./includes/database/".DBMS.".php");
	include("./cfg/language_list.php");
	include("./core_functions/functions.php");
	include("./core_functions/category_functions.php");
	include("./core_functions/cart_functions.php");
	include("./core_functions/product_functions.php");
	include("./core_functions/statistic_functions.php");
	include("./core_functions/reg_fields_functions.php" );
	include("./core_functions/registration_functions.php" );
	include("./core_functions/country_functions.php" );
	include("./core_functions/zone_functions.php" );
	include("./core_functions/datetime_functions.php" );
	include("./core_functions/order_status_functions.php" );
	include("./core_functions/order_functions.php" );
	include("./core_functions/aux_pages_functions.php" );
	include("./core_functions/picture_functions.php" ); 
	include("./core_functions/configurator_functions.php" );
	include("./core_functions/option_functions.php" );
	include("./core_functions/search_function.php" );
	include("./core_functions/discount_functions.php" ); 
	include("./core_functions/custgroup_functions.php" ); 
	include("./core_functions/shipping_functions.php" );
	include("./core_functions/payment_functions.php" );
	include("./core_functions/tax_function.php" ); 
	include("./core_functions/currency_functions.php" );
	include("./core_functions/module_function.php" );
	include("./core_functions/crypto/crypto_functions.php");
	include("./core_functions/quick_order_function.php" ); 
	include("./core_functions/setting_functions.php" );
	include("./core_functions/subscribers_functions.php" );
	include("./core_functions/version_function.php" );
	include("./core_functions/discussion_functions.php" );
	include("./core_functions/order_amount_functions.php" ); 
	include("./core_functions/linkexchange_functions.php" ); 
	include("./core_functions/affiliate_functions.php" );

	include('./classes/xml2array.php');
	include('./classes/class.virtual.shippingratecalculator.php');
	include('./classes/class.virtual.paymentmodule.php');

	include('./classes/class.virtual.smsmail.php');
	include('./modules/smsmail/class.smsnotify.php');


	MagicQuotesRuntimeSetting();

	

	//init Smarty
	require 'smarty/smarty.class.php'; 
	$smarty = new Smarty; //core smarty object
	$smarty_mail = new Smarty; //for e-mails

	//select a new language?
	if (isset($_POST["lang"]))
		$_SESSION["current_language"] = $_POST["lang"];

	//current language session variable
	if (!isset($_SESSION["current_language"]) ||
		$_SESSION["current_language"] < 0 || $_SESSION["current_language"] > count($lang_list))
			$_SESSION["current_language"] = 0; //set default language
	//include a language file
	if (isset($lang_list[$_SESSION["current_language"]]) &&
		file_exists("languages/".$lang_list[$_SESSION["current_language"]]->filename))
	{
		//include current language file
		include("languages/".$lang_list[$_SESSION["current_language"]]->filename);
	}
	else
	{
		die("<font color=red><b>ERROR: Couldn't find language file!</b></font>");
	}

	//connect to the database
	db_connect(DB_HOST,DB_USER,DB_PASS) or die (db_error());
	db_select_db(DB_NAME) or die (db_error());

	settingDefineConstants();

class Koreez_Price {
	var $structure = array();
    /**
     * Constructor
     */	
    function __construct() {
        
    }
    
    /**
     * Load structure
     * @param void
     * @return boolean
     */
    function loadStructure () {
  	    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE );
    	while ( $row=db_fetch_row($q) )
	    {
	    	$this->structure[$row['categoryID']]['name'] = $row['name'];
	    	$this->structure[$row['categoryID']]['seolink'] = $row['seolink'];
    	}
    	return true;
    }
    
    

    /**
     * Get category ID by seolink
     * @param string $seo seolink
     * @return int
     */
    function getCategoryIDBySeolink( $seo ) {
    	foreach ( $this->structure as $category_id => $category_item ) {
    		if ( eregi($category_item['name'], $seo) ) {
    			echo "name = ".$category_item['name']."<br>";
    			return $category_id;
    		}
    	}
    	/*
  	    $q = db_query("SELECT * FROM ".CATEGORIES_TABLE." WHERE seolink like '%$seo%' " );
    	if  ( $row=db_fetch_row($q) )
	    {
	        return $row['categoryID'];
    	}
    	*/
    	return false;
    }
    
    /**
     * Add product
     * @param array $product_array product array
     * @return boolean
     */
    function addProduct ( $product_array ) {
        $query = "insert into ".PRODUCTS_TABLE." set 
            categoryID='".$product_array['categoryID']."', 
            name='".$product_array['name']."', 
            seolink='".$product_array['seolink']."', 
            Price='".$product_array['Price']."', 
            brief_description='".$product_array['brief_description']."', 
            enabled='1', 
            product_code='".$product_array['product_code']."'";
        //echo $query; 
        db_query($query);
        //echo "add product<br>";
        
        return db_insert_id();        
    }

    /**
     * Update product
     * @param array $product_array product array
     * @return boolean
     */
    function updateProduct ( $product_array ) {
        //echo "update product<br>";
    }
    
    /**
     * Get product id by code
     * @param
     * @return
     */
    function getProductIdByCode ( $product_code ) {
        $query = "select * from ".PRODUCTS_TABLE." where product_code='$product_code'";
  	    $q = db_query($query);
    	if  ( $row=db_fetch_row($q) )
	    {
	        return $row['productID'];
    	}
    	return false;
        
    }
    
    /**
     * Add category product
     * @param int $category_id
     * @param int $product_id
     * @return boolean
     */
    function addCategoryProduct($category_id, $product_id) {
        $query = "insert into ".CATEGORIY_PRODUCT_TABLE." set
        		categoryID=$category_id,
        		productID=$product_id";
        //echo $query."<br>";
        db_query($query);
    }
    
    /**
     * Main
     */
    function main () {
    	$this->loadStructure();
        $content = file('price1.csv');
        echo '<pre>';
        
        foreach ( $content as $string ) {
            //echo $string;
            //continue;
            $string_array = array();
            
            $string_array = split(";", $string);
            
            $mark[$string_array[1]] = $string_array[1];
            $model[$string_array[1]][$string_array[3]] = $string_array[3];
            $category_id = $this->getCategoryIDBySeolink($string_array[3]);
            //echo $string_array[3].", category_id = ".$category_id."<hr><br>";
            if ( $category_id and $category_id != 1 ) {
                $product_array = array();
                $product_array['categoryID'] = $category_id;
                $product_array['name'] = mysql_real_escape_string($string_array[2]);
                $product_array['Price'] = trim($string_array[4]);
                $product_array['brief_description'] = mysql_real_escape_string(trim($string_array[3]));
                $product_array['product_code'] = trim($string_array[0]);
                //echo "mark = ".$string_array[1].", category_id = $category_id<br>";
                
                $product_id = $this->getProductIdByCode($product_array['product_code']);
                if ( $product_id ) {
                    $this->updateProduct($product_array);
                } else {
                    $product_id = $this->addProduct($product_array);
                    $this->addCategoryProduct($category_id, $product_id);
                }
                
            }
            
            //print_r($string_array);
        }
        print_r($mark);
        print_r($model);
    }
}
$koreez_price = new Koreez_Price();
$koreez_price->main();
?>