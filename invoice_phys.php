<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	//��������� �� ������

	// -------------------------INITIALIZATION-----------------------------//

	//make sure that URL does not contain something like index.php/?parameter1=1&... //

	//include core files
	include("./cfg/connect.inc.php");
	include("./includes/database/".DBMS.".php");
	include("./cfg/language_list.php");
	include("./core_functions/functions.php");
	include("./core_functions/category_functions.php");
	include("./core_functions/cart_functions.php");
	include("./core_functions/product_functions.php");
	include("./core_functions/statistic_functions.php");
	include("./core_functions/reg_fields_functions.php" );
	include("./core_functions/registration_functions.php" );
	include("./core_functions/country_functions.php" );
	include("./core_functions/zone_functions.php" );
	include("./core_functions/datetime_functions.php" );
	include("./core_functions/order_status_functions.php" );
	include("./core_functions/order_functions.php" );
	include("./core_functions/aux_pages_functions.php" );
	include("./core_functions/picture_functions.php" ); 
	include("./core_functions/configurator_functions.php" );
	include("./core_functions/option_functions.php" );
	include("./core_functions/search_function.php" );
	include("./core_functions/discount_functions.php" ); 
	include("./core_functions/custgroup_functions.php" ); 
	include("./core_functions/shipping_functions.php" );
	include("./core_functions/payment_functions.php" );
	include("./core_functions/tax_function.php" ); 
	include("./core_functions/currency_functions.php" );
	include("./core_functions/module_function.php" );
	include("./core_functions/crypto/crypto_functions.php");
	include("./core_functions/quick_order_function.php" ); 
	include("./core_functions/setting_functions.php" );
	include("./core_functions/subscribers_functions.php" );
	include("./core_functions/version_function.php" );
	include("./core_functions/discussion_functions.php" );
	include("./core_functions/order_amount_functions.php" ); 

	include('./classes/class.virtual.paymentmodule.php');

	session_start();

	MagicQuotesRuntimeSetting();

	//init Smarty
	require 'smarty/smarty.class.php'; 
	$smarty = new Smarty; //core smarty object
	$smarty_mail = new Smarty; //for e-mails

	//connect to the database
	db_connect(DB_HOST,DB_USER,DB_PASS) or die (db_error());
	db_select_db(DB_NAME) or die (db_error());

	settingDefineConstants();

	//set Smarty include files dir
	$smarty->template_dir = "./modules/templates/";

	//assign core Smarty variables
	if (!isset($_GET["orderID"]) || !isset($_GET["order_time"]) || !isset($_GET["customer_email"]) || !isset($_GET["moduleID"]))
	{
		die ("����� �� ������ � ���� ������");
	}

	$InvoiceModule = modGetModuleObj($_GET['moduleID'], PAYMENT_MODULE);
	$_GET["orderID"] = (int) $_GET["orderID"];

	$sql = '
		SELECT COUNT(*) FROM '.ORDERS_TABLE.' 
		WHERE orderID='.$_GET["orderID"].' AND order_time="'.base64_decode($_GET["order_time"]).'" AND customer_email="'.base64_decode($_GET["customer_email"]).'"
	';
	$q = db_query($sql) or die (db_error());
	$row = db_fetch_row($q);

	if ($row[0] == 1) //����� ������ � ���� ������
	{
		$order = ordGetOrder( $_GET["orderID"] );

		//define smarty vars
		$smarty->hassign( "billing_lastname", $order["billing_lastname"] );
		$smarty->hassign( "billing_firstname", $order["billing_firstname"] );
		$smarty->hassign( "billing_city", $order["billing_city"] );
		$smarty->hassign( "billing_address", $order["billing_address"] );
		if ($InvoiceModule->is_installed())
		{
			$smarty->assign('InvoiceModule', $InvoiceModule);
			$smarty->assign( "invoice_description", str_replace("[orderID]", (string)$_GET["orderID"], $InvoiceModule->_getSettingValue('CONF_PAYMENTMODULE_INVOICE_PHYS_DESCRIPTION')) );
		}
		else //�������� �� �����
		{
			die ("������ ������ �� ���������� �� ����������");
		}

		//����� ���������
		$q = db_query("select order_amount_string from SS__module_payment_invoice_phys where orderID=".$_GET["orderID"]);
		$row = db_fetch_row($q);
		if ($row) //����� ������� � ����� � ��������� ���������
		{
			$smarty->assign( "invoice_amount", $row[0] );
		}
		else //����� �� ������� - ���������� � ������� ������
		{
			$smarty->assign( "invoice_amount", show_price($order["order_amount"]) );
		}


	}
	else
	{
		die ("����� �� ������ � ���� ������");
	}

	//show Smarty output
	$smarty->display("invoice_phys.tpl.html"); 

?>