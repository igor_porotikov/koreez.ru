<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgGetAllPageAttributes()
{
 	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." ORDER BY aux_page_ID");
	$data = array();
	while( $row = db_fetch_row( $q ) )
	{
		$row["aux_page_url"] = TransformDataBaseStringToText( $row["aux_page_url"] );
		$row["aux_page_name"] = TransformDataBaseStringToText( $row["aux_page_name"] );
		$row["aux_page_description"]	= TransformDataBaseStringToText( $row["aux_page_description"] );
		$row["meta_keywords"]		= TransformDataBaseStringToText( $row["meta_keywords"] );
		$row["meta_description"]	= TransformDataBaseStringToText( $row["meta_description"] );
		$data[] = $row;
	}
	return $data;	
}

function auxpgGetPagesByIds( $ids )
{
	if ( !$ids ) {
		return false;
	}
	if ( !is_array($ids) && !is_string($ids) ) {
		return false;
	}
 	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID IN (".(is_array($ids) ? implode(',',$idsArray) : $ids ).") ORDER BY aux_page_ID");
	$data = array();
	while( $row = db_fetch_row( $q ) )
	{
		$row["aux_page_url"] = TransformDataBaseStringToText( $row["aux_page_url"] );
		$row["aux_page_name"] = TransformDataBaseStringToText( $row["aux_page_name"] );
		$row["aux_page_description"]	= TransformDataBaseStringToText( $row["aux_page_description"] );
		$row["meta_keywords"]		= TransformDataBaseStringToText( $row["meta_keywords"] );
		$row["meta_description"]	= TransformDataBaseStringToText( $row["meta_description"] );
		$data[] = $row;
	}
	return $data;	
}


// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgGetAuxPage( $aux_page_ID )
{
	$aux_page_ID = (int) $aux_page_ID;
  	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID=$aux_page_ID" );
	if  ( $row=db_fetch_row($q) )
	{
		$row["aux_page_name"] = TransformDataBaseStringToText( $row["aux_page_name"] );
		if ( $row["aux_page_text_type"] == 1 )
			$row["aux_page_text"] = TransformDataBaseStringToHTML_Text( $row["aux_page_text"] );
		else
			$row["aux_page_text"] = TransformDataBaseStringToText( $row["aux_page_text"] );
		$row["aux_page_url"] = TransformDataBaseStringToText( $row["aux_page_url"] );
		$row["aux_page_description"]	= TransformDataBaseStringToText( $row["aux_page_description"] );
		$row["meta_keywords"]		= TransformDataBaseStringToText( $row["meta_keywords"] );
		$row["meta_description"]	= TransformDataBaseStringToText( $row["meta_description"] );
	}
	return $row;
}


// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgGetAuxPageByUrl( $aux_page_url )
{
	$aux_page_ID = (int) $aux_page_ID;
  	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE aux_page_url='$aux_page_url'" );
	if  ( $row=db_fetch_row($q) )
	{
		$row["aux_page_name"] = TransformDataBaseStringToText( $row["aux_page_name"] );
		if ( $row["aux_page_text_type"] == 1 )
			$row["aux_page_text"] = TransformDataBaseStringToHTML_Text( $row["aux_page_text"] );
		else
			$row["aux_page_text"] = TransformDataBaseStringToText( $row["aux_page_text"] );
		$row["aux_page_url"] = TransformDataBaseStringToText( $row["aux_page_url"] );
		$row["aux_page_description"]	= TransformDataBaseStringToText( $row["aux_page_description"] );
		$row["meta_keywords"]		= TransformDataBaseStringToText( $row["meta_keywords"] );
		$row["meta_description"]	= TransformDataBaseStringToText( $row["meta_description"] );
	}
	return $row;
}


// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgUpdateAuxPage( 	$aux_page_ID, $aux_page_name, 
				$aux_page_text, $aux_page_text_type,
				$meta_keywords, $meta_description, $aux_page_url = '',
				$parents = null, $similar = null, $updateChilds = null, $updateSimilar = null,
				$aux_page_description = '', $aux_page_image = ''  )
{
	$aux_page_ID = (int) $aux_page_ID;
	$aux_page_name		= TransformStringToDataBase( $aux_page_name );
	$meta_keywords		= TransformStringToDataBase( $meta_keywords );
	$meta_description	= TransformStringToDataBase( $meta_description );
	$aux_page_text		= TransformStringToDataBase( $aux_page_text );
	$aux_page_url		= TransformStringToDataBase( $aux_page_url );
	$aux_page_description	= TransformStringToDataBase( $aux_page_description );
	$aux_page_image		= TransformStringToDataBase( $aux_page_image );

	db_query("UPDATE ".AUX_PAGES_TABLE.
		 " SET 	aux_page_name='$aux_page_name', ".
		 " 	aux_page_text='$aux_page_text', ".
		 " 	aux_page_text_type=$aux_page_text_type, ".
		 " 	aux_page_url='$aux_page_url', ".
		 " 	aux_page_description='$aux_page_description', ".
		 " 	aux_page_image='$aux_page_image', ".
		 (is_array($parents) ? "aux_page_parents='".implode(",",$parents)."', " : '').
		 (is_array($similar) ? "aux_page_similar='".implode(",",$similar)."', " : '').
		 " 	meta_keywords='$meta_keywords', ".
		 " 	meta_description='$meta_description' ".
		 " WHERE aux_page_ID = $aux_page_ID");

	if ( is_array($parents) && $updateChilds ) {
		auxpgUpdateChildsAfterEdit( $aux_page_ID, $parents );
	}
	if ( is_array($similar) && $updateSimilar ) {
		auxpgUpdateSimilarAfterEdit( $aux_page_ID, $similar );
	}
}

// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgAddAuxPage( 	$aux_page_name, 
				$aux_page_text, $aux_page_text_type,
				$meta_keywords, $meta_description, $aux_page_url = '',
				$parents = null, $similar = null, $updateChilds = null, $updateSimilar = null,
				$aux_page_description = '', $aux_page_image = ''  )
{
	$aux_page_name		= TransformStringToDataBase( $aux_page_name );
	$meta_keywords		= TransformStringToDataBase( $meta_keywords );
	$meta_description	= TransformStringToDataBase( $meta_description );
	$aux_page_text		= TransformStringToDataBase( $aux_page_text );
	$aux_page_url		= TransformStringToDataBase( $aux_page_url );
	$aux_page_parents	= is_array($parents) ? implode(",",$parents) : '';
	$aux_page_similar	= is_array($similar) ? implode(",",$similar) : '';
	$aux_page_description	= TransformStringToDataBase( $aux_page_description );
	$aux_page_image		= TransformStringToDataBase( $aux_page_image );
	
	db_query( "INSERT INTO ".AUX_PAGES_TABLE.
		" ( aux_page_name, aux_page_text, aux_page_text_type, aux_page_url, meta_keywords, meta_description, aux_page_parents, aux_page_similar, aux_page_description, aux_page_image )  ".
		" values( '$aux_page_name', '$aux_page_text', $aux_page_text_type, '$aux_page_url', ".
		" '$meta_keywords', '$meta_description', '$aux_page_parents', '$aux_page_similar', '$aux_page_description', '$aux_page_image' ) " );

	$newId = db_insert_id();
	if ( is_array($parents) && $updateChilds ) {
		auxpgUpdateChildsAfterEdit( $newId, $parents );
	}
	if ( is_array($similar) && $updateSimilar ) {
		auxpgUpdateSimilarAfterEdit( $newId, $similar );
	}
	
	return $newId;
}


// *****************************************************************************
// Purpose	
// Inputs   
// Remarks		
// Returns	
function auxpgDeleteAuxPage( $aux_page_ID )
{
	$aux_page_ID = (int) $aux_page_ID;
	db_query("DELETE FROM ".AUX_PAGES_TABLE.
		" WHERE aux_page_ID=$aux_page_ID");
}


function auxpgGetOtherPages( $thisPageId = null )
{
	$other_pages = auxpgGetAllPageAttributes();
	if ( $thisPageId !== null ) {
		foreach ($other_pages as $key=>$val) {
			if ( $val['aux_page_ID'] == $thisPageId ) {
				unset( $other_pages[$key] );
				break;
			}
		}
	}
	return $other_pages;
}

function auxpgUpdateChildsAfterEdit( $changedChildId, $newParents = array() )
{
	$aux_page_ID = $changedChildId;
	$parents = $newParents;

	 	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE ".($parents ? "aux_page_ID IN (".implode(",",$parents).") OR " : "" )."$aux_page_ID IN (aux_page_childs)");
		while( $row = db_fetch_row( $q ) )
		{
			$rowId = $row['aux_page_ID'];
			$rowChilds = $row['aux_page_childs']!=='' ? explode(',',$row['aux_page_childs']) : array();
			$changed = false;
			if ( in_array( $aux_page_ID, $rowChilds ) ) {
				// ��� ���� ����� �����������
				if ( in_array( $rowId, $parents ) ) {
					// ��� ������� ��������, ������ �� ������
				} else {
					// ��� ������ ��������, ���������
					$changed = true;
					foreach ( $rowChilds as $key=>$val ) {
						if ( $val['aux_page_ID'] == $aux_page_ID ) {
							unset($rowChilds[$key]);
							break;
						}
					}
				}

			} else {
				// ����� �����������
				if ( in_array( $rowId, $parents ) ) {
					// ��� ����� ��������
					$rowChilds[] = $aux_page_ID;
					$changed = true;
				}
			}
			//var_dump($row);
			//echo "<br />";
			if ( $changed ) {
				db_query("UPDATE ".AUX_PAGES_TABLE." SET aux_page_childs='".implode(',',$rowChilds)."' WHERE aux_page_ID=".$rowId);
				//echo 'updated: '.implode(',',$rowChilds).'<br />';
			}
		}
}


function auxpgUpdateSimilarAfterEdit( $changedChildId, $newSimilar = array() )
{
	$aux_page_ID = $changedChildId;
	$similar = $newSimilar;

	 	$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE ".($newSimilar ? "aux_page_ID IN (".implode(",",$similar).") OR " : "" )."$aux_page_ID IN (aux_page_similar)");
		while( $row = db_fetch_row( $q ) )
		{
			$rowId = $row['aux_page_ID'];
			$rowSimilar = $row['aux_page_similar']!=='' ? explode(',',$row['aux_page_similar']) : array();
			$changed = false;
			if ( in_array( $aux_page_ID, $rowSimilar ) ) {
				// ��� ���� ����� ������� ��������
				if ( in_array( $rowId, $similar ) ) {
					// ��� ������� ������� ��������, ������ �� ������
				} else {
					// ��� ������ ������� ��������, ���������
					$changed = true;
					foreach ( $rowSimilar as $key=>$val ) {
						if ( $val['aux_page_ID'] == $aux_page_ID ) {
							unset($rowSimilar[$key]);
							break;
						}
					}
				}

			} else {
				// ����� ������� ��������
				if ( in_array( $rowId, $similar ) ) {
					// ��� ����� ��������
					$rowSimilar[] = $aux_page_ID;
					$changed = true;
				}
			}
			//var_dump($row);
			//echo "<br />";
			if ( $changed ) {
				db_query("UPDATE ".AUX_PAGES_TABLE." SET aux_page_similar='".implode(',',$rowSimilar)."' WHERE aux_page_ID=".$rowId);
				//echo 'updated: '.implode(',',$rowSimilar).'<br />';
			}
		}
}