<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	function schIsAllowProductsSearch( $categoryID )
	{
		$q = db_query("select allow_products_search from ".CATEGORIES_TABLE.
			" where categoryID=$categoryID");
		if ( $row = db_fetch_row($q) )
			return $row["allow_products_search"];
		return false;		
	}


	function schUnSetOptionsToSearch( $categoryID )
	{
		$q = db_query( "select optionID from ".CATEGORY_PRODUCT_OPTIONS_TABLE.
			" where categoryID=$categoryID " );
		$data = array();
		while( $row = db_fetch_row($q) )
			$data[] = $row["optionID"];

		foreach( $data as $val )
		{
			db_query( " delete from ".CATEGORY_PRODUCT_OPTION_VARIANTS.
				" where categoryID=$categoryID AND optionID=$val" );

			db_query( " delete from ".CATEGORY_PRODUCT_OPTIONS_TABLE.
				" where categoryID=$categoryID AND optionID=$val" );
		}
	}

	function schSetOptionToSearch( $categoryID, $optionID, $set_arbitrarily )
	{
		db_query( "insert into ".CATEGORY_PRODUCT_OPTIONS_TABLE.
				" ( categoryID, optionID, set_arbitrarily ) ".
				" values( $categoryID, $optionID, $set_arbitrarily ) " );
	}

	function schOptionIsSetToSearch( $categoryID, $optionID )
	{
		$res = array();
		$q = db_query( "select set_arbitrarily from ".CATEGORY_PRODUCT_OPTIONS_TABLE.
				" where categoryID=$categoryID AND optionID=$optionID" );
		if ( $row = db_fetch_row($q) )
		{
			$res["isSet"] = 1;
			$res["set_arbitrarily"] = $row["set_arbitrarily"];
		}
		else
			$res["isSet"] = 0;
		return $res;
	}

	function schUnSetVariantsToSearch( $categoryID, $optionID )
	{
		db_query( " delete from ".CATEGORY_PRODUCT_OPTION_VARIANTS.
			    " where categoryID=$categoryID AND optionID=$optionID" );
	}


	function schSetVariantToSearch( $categoryID, $optionID, $variantID )
	{
		db_query( "insert into ".CATEGORY_PRODUCT_OPTION_VARIANTS.
				" ( optionID, categoryID, variantID )  ".
				" values( $optionID, $categoryID, $variantID ) " );
	}	


	function schVariantIsSetToSearch( $categoryID, $optionID, $variantID )
	{
		$q = db_query( "select count(*) from ".
				CATEGORY_PRODUCT_OPTION_VARIANTS.
				" where categoryID=$categoryID AND optionID=$optionID AND ".
				"  variantID=$variantID " );
		$row = db_fetch_row($q);
		return ( $row[0] != 0 );
		
	}

?>