<?php
/*****************************************************************************
 *                                                                           *
 * Shop-Script PREMIUM                                                       *
 * Copyright (c) 2005 WebAsyst LLC. All rights reserved.                     *
 *                                                                           *
 *****************************************************************************/
?><?php
	//printable invoice

	// -------------------------INITIALIZATION-----------------------------//

	//include core files
	include("./cfg/connect.inc.php");
	include("./includes/database/".DBMS.".php");
	include("./cfg/language_list.php");
	include("./core_functions/functions.php");
	include("./core_functions/category_functions.php");
	include("./core_functions/cart_functions.php");
	include("./core_functions/product_functions.php");
	include("./core_functions/statistic_functions.php");
	include("./core_functions/reg_fields_functions.php" );
	include("./core_functions/registration_functions.php" );
	include("./core_functions/country_functions.php" );
	include("./core_functions/zone_functions.php" );
	include("./core_functions/datetime_functions.php" );
	include("./core_functions/order_status_functions.php" );
	include("./core_functions/order_functions.php" );
	include("./core_functions/aux_pages_functions.php" );
	include("./core_functions/picture_functions.php" ); 
	include("./core_functions/configurator_functions.php" );
	include("./core_functions/option_functions.php" );
	include("./core_functions/search_function.php" );
	include("./core_functions/discount_functions.php" ); 
	include("./core_functions/custgroup_functions.php" ); 
	include("./core_functions/shipping_functions.php" );
	include("./core_functions/payment_functions.php" );
	include("./core_functions/tax_function.php" ); 
	include("./core_functions/currency_functions.php" );
	include("./core_functions/module_function.php" );
	include("./core_functions/crypto/crypto_functions.php");
	include("./core_functions/quick_order_function.php" ); 
	include("./core_functions/setting_functions.php" );
	include("./core_functions/subscribers_functions.php" );
	include("./core_functions/version_function.php" );
	include("./core_functions/discussion_functions.php" );
	include("./core_functions/order_amount_functions.php" ); 

	session_start();

	MagicQuotesRuntimeSetting();

	//init Smarty
	require 'smarty/smarty.class.php'; 
	$smarty = new Smarty; //core smarty object

	//current language session variable
	if (!isset($_SESSION["current_language"]) ||
		$_SESSION["current_language"] < 0 || $_SESSION["current_language"] > count($lang_list))
			$_SESSION["current_language"] = 0; //set default language
	//include a language file
	if (isset($lang_list[$_SESSION["current_language"]]) &&
		file_exists("languages/".$lang_list[$_SESSION["current_language"]]->filename))
	{
		//include current language file
		include("languages/".$lang_list[$_SESSION["current_language"]]->filename);
	}
	else
	{
		die("<font color=red><b>ERROR: Couldn't find language file!</b></font>");
	}

	//connect to the database
	db_connect(DB_HOST,DB_USER,DB_PASS) or die (db_error());
	db_select_db(DB_NAME) or die (db_error());

	settingDefineConstants();

	if ((int)CONF_SMARTY_FORCE_COMPILE) //this forces Smarty to recompile templates each time someone runs invoice.php
	{
		$smarty->force_compile = true;
	}

	//authorized access check
	include("./checklogin.php");

	//set Smarty include files dir
	$smarty->template_dir = "./templates/frontend/".$lang_list[$_SESSION["current_language"]]->template_path;

	$error = "";

	// validate order and user
	if ( CONF_BACKEND_SAFEMODE != 1 && (!isset($_SESSION["log"]) || !isset($_GET["orderID"])) )
	{
		$error = ERROR_FORBIDDEN;
	}
	else
	{

		$orderID = (int) $_GET["orderID"];
		$order = ordGetOrder( $orderID );

		$order["discount_value"] = round((float)$order["order_discount"] * $order["clear_total_priceToShow"])/100;

		if (!$order)
		{
			$error = ERROR_CANT_FIND_REQUIRED_PAGE;
		}
		else //order was found in the database
		{
			//administrator is allowed to access all orders invoices
			//and if logged user is not administrator, check if order belongs to this user.

			if (CONF_BACKEND_SAFEMODE != 1 && strcmp($_SESSION["log"], ADMIN_LOGIN) && ($order["customerID"] != regGetIdByLogin($_SESSION["log"]))) //attempt to view orders of other customers
			{
				$error = ERROR_FORBIDDEN;
			}
			else // show invoice
			{
				$orderContent = ordGetOrderContent( $orderID );
				$smarty->hassign( "orderContent", $orderContent );
				$smarty->hassign( "order", $order );
			}

		}
	}
	$smarty->assign("error", $error);

	//show Smarty output
	$smarty->display("invoice.tpl.html"); 

?>