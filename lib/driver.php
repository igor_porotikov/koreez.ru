<?php
/**
 * Driver class
 */
if (  !defined('UPLOADIFY_TABLE')  ) 
{
    define('UPLOADIFY_TABLE', 'SS_uploadify');
}

if (  !defined('IMAGE_TABLE')  ) 
{
    define('IMAGE_TABLE', 'SS_image');
}


if (  !defined('FRUCTUS_DOCUMENT_ROOT')  ) 
{
    define('FRUCTUS_DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
}

class Driver extends Fructus {
    var $component_url = 'index.php';
    var $config_array;
    var $uploadify_dir = '/storage/upl/';
    var $storage_dir = '/storage/uploads/';

    /**
     * Load uploadify images
     * @param string $session_code session code
     * @return array
     */
    function load_uploadify_images ( $session_code ) {
        $ra = array();
        $query = "select * from ".UPLOADIFY_TABLE." where session_code='$session_code'";
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ra[] = $this->db->row['file_name'];
        }
        return $ra;
    }
    
    /**
     * Get first image by item id
     * @param int $record_id
     * @param int $item_id
     * @param string $table_name
     * @param string $key 
     * @return int
     */
    function getFirstImageByItemID ( $record_id, $item_id, $table_name, $key ) {
    	$query = "select i.* from ".$table_name."_image as li, image as i where li.".$key."=$record_id and li.image_id=i.image_id order by sort_order";
    	//echo $query.'<br>';
    	$this->db->exec($query);
    	//print_r($ra);
    	$i = 0;
    	while ( $this->db->fetch_assoc() ) {
    		if ( $i+1 == $item_id ) {
    			return $this->db->row;
    		}
    		$i++;
    	}
    	return false;
    }
    
    /**
     * Delete image
     * @param string $table_name table name
     * @param int $image_id image id
     * @return boolean
     */
    function deleteImage ( $table_name, $image_id ) {
    	//delete records from land_image
    	$query = "delete from SS_".$table_name."_image where image_id=$image_id";
    	$this->db->exec($query);
    	
    	//delete image files
    	$this->deleteImageFiles( $image_id );
    	
    	//delete image records
    	$query = "delete from ".IMAGE_TABLE." where image_id=$image_id";
    	$this->db->exec($query);
		return true;    	
    }
    
    /**
     * Delete image files
     * @param $image_id image id
     * @return boolean
     */
    function deleteImageFiles( $image_id ) {
    	$path = FRUCTUS_DOCUMENT_ROOT.$this->storage_dir;
    	
    	$query = "select * from ".IMAGE_TABLE." where image_id=$image_id";
    	$this->db->exec($query);
    	while ( $this->db->fetch_assoc() ) {
    		$preview = $this->db->row['preview'];
    		$normal = $this->db->row['normal'];
            unlink($path.$preview);
            unlink($path.$normal);
    	}
    	return true;
    }
    
    
    /**
     * Delete uploadify images
     * @param string $session_code session code
     * @return array
     */
    function delete_uploadify_images ( $session_code ) {
    	$uploadify_path = FRUCTUS_DOCUMENT_ROOT.$this->uploadify_dir;
        
        $ra = array();
        $query = "select * from ".UPLOADIFY_TABLE." where session_code='$session_code'";
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ra[] = $this->db->row['file_name'];
        }
        if ( count($ra) > 0 ) {
            foreach ( $ra as $image_name ) {
                if ( is_file($uploadify_path.$image_name) ) {
                    unlink($uploadify_path.$image_name);
                }
            }
        }
        $query = "delete from ".UPLOADIFY_TABLE." where session_code='$session_code'";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Edit image
     * @param string $action action
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record ID
     * @return boolean
     */
    function editImageMulti( $action, $table_name, $key, $record_id ) {
    	$path = FRUCTUS_DOCUMENT_ROOT.'/storage/uploads/';
    	$uploadify_path = FRUCTUS_DOCUMENT_ROOT.$this->uploadify_dir;
    	
    	$ra = array();
    	$this->writeLog(__METHOD__.", table_name = $table_name, $key = $record_id");
    	
    	//echo '<pre>';
    	//print_r($_REQUEST);
    	//echo '</pre>';
    	//echo "default-id = ".$this->getRequestValue('default-id')."<br>";
    	//echo "default-id = ".$_REQUEST['default-id']."<br>";
    	//update image
    	$images = $this->load_uploadify_images($_REQUEST['PHPSESSID']);
    	$this->writeLog(__METHOD__.", images = ".var_export($images, true));
    	
        foreach ( $images as $image_name ) {
            $i++;
            $need_prv=0;
            $preview_name='';   
            if ( !empty($image_name) ) { 
                $arr=split('\.',$image_name);
                $ext=strtolower($arr[count($arr)-1]);
                $preview_name="img".getmypid().'_'.time()."_".$i.".".$ext;
                $prv="prv".getmypid().'_'.time()."_".$i.".".$ext;
                $preview_name_tmp="_tmp".getmypid().'_'.time()."_".$i.".".$ext;
                
                
                $big_width = $this->getConfigValue($action.'_image_big_width');
                $big_height = $this->getConfigValue($action.'_image_big_height');
                
                $preview_width = $this->getConfigValue($action.'_image_preview_width');
                $preview_height = $this->getConfigValue($action.'_image_preview_height');
                
                //echo '$path.$preview_name = '.$path.$preview_name.'<br>';
                $this->writeLog(__METHOD__.", uploadify_path = ".$uploadify_path.$image_name);
                $this->writeLog(__METHOD__.", path = ".$path.$preview_name);
                
                list($width,$height)=$this->makePreview($uploadify_path.$image_name, $path.$preview_name, $big_width,$big_height, $ext,1);
                list($w,$h)=$this->makePreview($uploadify_path.$image_name, $path.$prv, $preview_width,$preview_height, $ext,'width');
                
                
                //echo $path.$preview_name_tmp."<br>";
                $ra[$i]['preview'] = $prv;
                $ra[$i]['normal'] = $preview_name;
                
                /*
                //update image record
                $query="update rent set img".$i."='".mysql_real_escape_string($preview_name)."' where rent_id=".$record_id;
                //echo $query.'<br>';
                sql_query($query);
                
                //update preview record
                $query="update rent set img".$i."_preview='".mysql_real_escape_string($prv)."' where rent_id=".$record_id;
                //echo $query.'<br>';
                sql_query($query);
                */
                
                //if (!empty($need_prv)) {sql_query("update {$table_name} set preview='".sql_escape($prv)."' where card_id=".$record_id);}
            } 
        }
        $this->add_image_records($ra, $table_name, $key, $record_id);
        $this->delete_uploadify_images($_REQUEST['PHPSESSID']);
        return $ra;
    }
    
    /**
     * Reorder image
     * @param $action
     * @param $image_id
     * @param $key 
     * @param $key_value
     * @param $direction
     * @return mixed
     */
    function reorderImage($action, $image_id, $key, $key_value, $direction) {
    	//get current image info
    	$query = "select ".$action."_image_id, sort_order from ".DB_PREFIX.'_'.$action."_image where image_id=$image_id";
    	//echo $query.'<br>';
    	$this->db->exec($query);
    	$this->db->fetch_array();
    	$rr = $this->db->row;
    	
    	$record_image_id = $rr[0];
    	//echo "record_image_id = $record_image_id<br>";
    	$sort_order = $rr[1];
    	//echo "sort_order = $sort_order<br>";
    	if ( $direction == 'down' ) {
	    	//get next image id
    		$query = "select ".$action."_image_id, sort_order from ".DB_PREFIX.'_'.$action."_image where sort_order > $sort_order and $key = $key_value order by sort_order asc";
    	    $this->db->exec($query);
    	    $this->db->fetch_array();
    	    $rr = $this->db->row;
    	    
    		//$rr = sql_select_array($query);
    		
    		$next_record_image_id = $rr[0];
    		//echo "next_record_image_id = $next_record_image_id<br>";
    		if ( !isset($next_record_image_id) ) {
    			return;
    		}
    		$next_sort_order = $rr[1];
    		//echo "next_sort_order = $next_sort_order<br>";
    		
	    	//replace
    		$query = "update ".DB_PREFIX.'_'.$action."_image set sort_order=$next_sort_order where ".$action."_image_id=$record_image_id";
    	    $this->db->exec($query);
    		//sql_query($query);
    	
    		$query = "update ".DB_PREFIX.'_'.$action."_image set sort_order=$sort_order where ".$action."_image_id=$next_record_image_id";
    	    $this->db->exec($query);
    		//sql_query($query);
    		
    	}
    	
    	if ( $direction == 'up' ) {
	    	//get next image id
    		$query = "select ".$action."_image_id, sort_order from ".DB_PREFIX.'_'.$action."_image where sort_order < $sort_order and $key = $key_value order by sort_order desc";
    	
    	    $this->db->exec($query);
    	    $this->db->fetch_array();
    	    $rr = $this->db->row;
    		//$rr = sql_select_array($query);
    		$next_record_image_id = $rr[0];
    		//echo "next_record_image_id = $next_record_image_id<br>";
    		if ( !isset($next_record_image_id) ) {
    			return;
    		}
    		$next_sort_order = $rr[1];
    		//echo "next_sort_order = $next_sort_order<br>";
    	
	    	//replace
    		$query = "update ".DB_PREFIX.'_'.$action."_image set sort_order=$next_sort_order where ".$action."_image_id=$record_image_id";
    	    $this->db->exec($query);
    		//sql_query($query);
    	
    		$query = "update ".DB_PREFIX.'_'.$action."_image set sort_order=$sort_order where ".$action."_image_id=$next_record_image_id";
    	    $this->db->exec($query);
    		//sql_query($query);
    		
    	}
    	
    	//get next image
    	
    }
    
    
    /**
     * Get image list admin
     * @param string $action action
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record id
     * @return string
     */
    function getImageListAdmin ( $action, $table_name, $key, $record_id ) {
    	global $__db_prefix;
    	$url = $this->storage_dir;
    	
    	
    	$query = "select i.* from SS_".$table_name."_image as li, ".IMAGE_TABLE." as i where li.".$key."=$record_id and li.image_id=i.image_id order by li.sort_order";
    	//echo $query;
    	
    	$this->db->exec($query);
    	while ( $this->db->fetch_assoc() ) {
	    	$up_link = "?action=".$action."&do=edit&".$key."=$record_id&subdo=up_image&image_id=".$this->db->row['image_id'];
	    	$down_link = "?action=".$action."&do=edit&".$key."=$record_id&subdo=down_image&image_id=".$this->db->row['image_id'];
	    		
	    		
	    	$up_link_img = '<a href="'.$up_link.'"><img src="/img/up.gif" border="0" alt="������" title="������"></a>';
	    	$down_link_img = '<a href="'.$down_link.'"><img src="/img/down.gif" border="0" alt="����" title="����"></a>';
    	    
    		$delete_link = "?action=".$action."&do=edit&".$key."=$record_id&subdo=delete_image&image_id=".$this->db->row['image_id'];
    		$rs .= '<div class="preview_admin" style="padding: 2px; border: 1px solid gray;">
    		<table border="0">
    		<tr>
    		<td>
    		<img src="'.$url.''.$this->db->row['preview'].'" border="0" align="left"/><br>
    		</td>
    		<td>
    		<a href="'.$delete_link.'" onclick="return confirm(\'������������� ������ ������� �����������?\');">�������</a>
    		'.$up_link_img.'
    		'.$down_link_img.'
    		</td>
    		</tr>
    		</table>
    		</div>';
    		$rs .= '<div style="clear: both;"></div>';
    	}
    	
    	return $rs;
    }
    
    /**
     * Make preview
     * @param
     * @return
     */
    function makePreview ( $src, $dst, $width, $height, $ext='jpg', $md=0 ) {
        if ($ext=='jpg'){$source_img=ImageCreateFromJPEG($src);}elseif($ext=='png'){$source_img=ImageCreateFromPNG($src);}  
        $w_src=imagesx($source_img);
        $h_src=imagesy($source_img);
        //echo '$w_src = '.$w_src.'<br>';
        if ($w_src>$h_src) {$mode='width';}else{$mode='height';}
        if ($md=='height') {$mode='height';}
        if ($md=='width') {$mode='width';}
        $ratio=1;
        if ($mode=='width') {
            if ($w_src>$width){$ratio=$w_src/$width;}
        } else {
            $tmp=$width;$width=$height;$height=$tmp;
            if ($h_src>$height){$ratio=$h_src/$height;}
        }
        $width_tmp=intval($w_src/$ratio);
        $height_tmp=intval($h_src/$ratio); 
        //echo "width_tmp = $width_tmp, height_tmp = $height_tmp<br>";
        $tmp_img=imagecreatetruecolor($width_tmp,$height_tmp);
        imagecopyresampled($tmp_img, $source_img,0,0,0,0,$width_tmp,$height_tmp,$w_src,$h_src);
        if ($ext=='jpg'){
        	//echo 'image jpg, dst = '.$dst.'<br>';
        	if ( !imagejpeg($tmp_img,$dst, 100) ) {
        		//echo "failed to create jpg<br>";	
        	}
        } elseif ( $ext=='png' ) {
        	imagepng($tmp_img,$dst, 100);
        } 
        ImageDestroy($source_img);
        ImageDestroy($tmp_img);
        // ImageDestroy($preview_img);
        return array($width,$height);
    }
    
    /**
     * Add image data records
     * @param array $images images
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record id
     * @return boolean
     */
    function add_image_records ( $images, $table_name, $key, $record_id ) {
    	//print_r($images);
    	foreach ( $images as $item_id => $item_array ) {
    		$query = "insert into ".IMAGE_TABLE." (normal, preview) values ('".$item_array['normal']."', '".$item_array['preview']."')";
    		//echo 'q = '.$query.'<br>';
    		$this->writeLog(__METHOD__.", q = $query");
    		$this->db->exec($query);
    		$image_id = $this->db->last_insert_id();
    		$this->writeLog(__METHOD__.", image_id = $image_id");
    		$this->add_table_image_record($table_name, $key, $record_id, $image_id);
    	}
    }
    
    /**
     * Add table_image record
     * @param int $record_id record id
     * @param int $image_id image id
     * @return boolean
     */
    function add_table_image_record($table_name, $key, $record_id, $image_id) {
    	$query = "insert into SS_".$table_name."_image (".$key.", image_id, sort_order) values ('".$record_id."', '".$image_id."', '".$image_id."')";
    	//echo 'q = '.$query.'<br>';
    	$this->writeLog(__METHOD__.", q = $query");
    	$this->db->exec($query);
    	return true;
    }
    

    /**
     * Get component url
     * @param void
     * @return string
     */
    function get_component_url () {
        return $this->component_url;
    }
}
?>
