<?php
/**
 * Order sender
 */
define('MANAGER_GROUP_ID', 3);
class Order_Sender extends Order_Manager {
    /**
     * Constructor
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Send order table
     * @param 
     * @return 
     */
    function send_order_table () {
        error_reporting(E_ALL);
        ini_set('display_errors','On');
        
        require_once (FRUCTUS_DOCUMENT_ROOT.'/lib/system/mailer/mailer.php');
        $mailer = new Mailer('kondin@etown.ru', 'info@priem-mobile.ru');
        //$mailer->send($this->grid());
        $mailer->send_smtp($this->grid());
    }
    
    /**
     * Send confirm
     * @param int $orderID
     * @return boolean
     */
    function send_confirm ( $orderID ) {
        require_once (FRUCTUS_DOCUMENT_ROOT.'/lib/system/mailer/mailer.php');
        require_once (FRUCTUS_DOCUMENT_ROOT.'/lib/backend/order/order_confirmation.php');
        require_once (FRUCTUS_DOCUMENT_ROOT.'/lib/system/mailer/mailer.php');
        $order_confirmation = new Order_Confirmation();
        $mailer = new Mailer();
        
        //$user_id = '1';
        /*
        $user_array[1] = 'kondin@etown.ru';
        $user_array[2] = 'egocenter@yandex.ru';
        $user_array[3] = 'ctrlaltdel@mail.ru';
        */
        $users = $this->load_users_by_group_id(MANAGER_GROUP_ID);
        //echo '<pre>';
        //print_r($users);
        //exit;
        
        
        
        foreach ( $users as $user_id => $user_array ) {
            
            $code = $order_confirmation->add_confirm($orderID, $user_id);
            $activate_url = 'http://'.$_SERVER['SERVER_NAME'].'/activation.php?code='.$code;
            $msg = '��� ������������� ���������� ������ �� ������: <br>';
            $msg .= '<a href="'.$activate_url.'">'.$activate_url.'</a>';
            $subject = '������������� ������';
            $from = 'dmn1c@yandex.ru';
            $to = $user_array['Email'];
        
            //$mailer->send($this->grid());
            $mailer->send_smtp( $to, $from, $subject, $msg );
        }
    }
    
    /**
     * Load users by group id
     * @param int $group_id group id
     * @return array
     */
    function load_users_by_group_id ( $group_id ) {
        $query = "select * from SS_customers where custgroupID=$group_id";
        $this->db->exec($query);
        $ra = array();
        while ( $this->db->fetch_assoc() ) {
            $ra[$this->db->row['customerID']] = $this->db->row;
        }
        return $ra;
    }
    
}
?>
