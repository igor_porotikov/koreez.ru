<?php
/**
 * Order confirmation class
 */
define('NEW_STATUS', 2);
define('IN_PROGRESS_STATUS', 3);
class Order_Confirmation extends Order_Manager {
    /**
     * Construct
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Confirm
     * @param
     * @return
     */
    function confirm () {
        $query = "";
    }
    
    /**
     * Add confirm
     * @param int $order_id order id
     * @param int $user_id user id
     * @return string
     */
    function add_confirm ( $order_id, $user_id ) {
        $confirm_code = md5($this->generate_confirm(50));
        $status = 0;
        $query = "insert into SS_orders_confirm (orderID, customerID, confirm_code, status) values ('$order_id', '$user_id', '$confirm_code', '$status')";
        $this->db->exec($query);
        return $confirm_code;
    }
    
    /**
     * Activate order
     * @param string $code code
     * @return string
     */
    function activate ( $code ) {
        //check activation code
        $query = "select orderID from SS_orders_confirm where confirm_code='$code'";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        $orderID = $this->db->row['orderID'];
        if ( $orderID == '' ) {
            return '�������� ��� ���������';
        }
        
        //load all active activations for this code
        $query = "select customerID from SS_orders_confirm where orderID=$orderID and status=1";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        if ( $this->db->row['customerID'] ) {
            //else print message about "this order already activated"
            return '����� ��� �����������';
        }
        
        //if order not activated - activate it
        $query = "update SS_orders_confirm set status=1 where confirm_code='$code' and orderID=$orderID";
        $this->db->exec($query);
        
        //update order status
        $query = "update SS_orders set statusID=".IN_PROGRESS_STATUS." where orderID=$orderID";
        $this->db->exec($query);
        
        
        return '�������, ����� � ������� '.$orderID.' ������� �����������';
        
    }

    /**
     * Generate confirm
     * @param int $number
     * @return string
     */
    function generate_confirm($number)  {
    $arr = array('a','b','c','d','e','f',
                 'g','h','i','j','k','l',
                 'm','n','o','p','r','s',
                 't','u','v','x','y','z',
                 'A','B','C','D','E','F',
                 'G','H','I','J','K','L',
                 'M','N','O','P','R','S',
                 'T','U','V','X','Y','Z',
                 '1','2','3','4','5','6',
                 '7','8','9','0','.',',',
                 '(',')','[',']','!','?',
                 '&','^','%','@','*','$',
                 '<','>','/','|','+','-',
                 '{','}','`','~');
    // ���������� ������
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
      // ��������� ��������� ������ �������
      $index = rand(0, count($arr) - 1);
      $pass .= $arr[$index];
    }
    return $pass;
  }    
}
?>