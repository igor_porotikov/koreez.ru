<?php
/**
 * Map manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Map_Manager extends Fructus {
    /**
     * Constructor
     */
    function Map_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $page_structure = $this->loadPageStructure(true);
                //echo '<pre>';
                //echo 'page_structure = ';
                //print_r($page_structure['childs'][$this->getRequestValue('aux_page_ID')]);
                //exit;
                if ( count($page_structure['childs'][$this->getRequestValue('aux_page_ID')]) > 0 and $this->getRequestValue('confirm') != 'yes' ) {
                    $rs .= '<div align="center">�� ������ ������� �������� �� ����� �����, �� � ��� ���� ��������� ��������.<br> ��� �������� ����� ������� ����� ��� ��������� ��������<br></div>';
                    $rs .= '<br><br><div align="center"><a href="?action=map&do=delete&confirm=yes&aux_page_ID='.$this->getRequestValue('aux_page_ID').'">�������</a> | <a href="?action=map">������</a></div>';
                    return $rs;
                }
                
                
                $this->deleteRecord($this->getRequestValue('aux_page_ID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;
            
            case 'toggle':
                $this->toggle($this->getRequestValue('aux_page_ID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
                $hash = $this->load($this->getRequestValue('aux_page_ID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $rs = $this->getForm();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Toggle expand/collapse page
     * @param int $aux_page_ID aux page ID
     * @return boolean
     */
    function toggle ( $aux_page_ID ) {
        if ( $_SESSION['toggle'][$aux_page_ID] != 'open' ) {
            $_SESSION['toggle'][$aux_page_ID] = 'open';
        } else {
            $_SESSION['toggle'][$aux_page_ID] = 'close';
        }
        return true;
    }
    
    /**
     * Delete record
     * @param int $record_id record ID
     * @return boolean
     */
    function deleteRecord ( $record_id ) {
        
        $query = "update ".AUX_PAGES_TABLE." set in_map = 0, parent_id = 0 where aux_page_ID=$record_id";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Edit record
     * @param void
     * @return boolean
     */
    function editRecord () {
        global $__db_prefix;
        
        $query = "update ".AUX_PAGES_TABLE." set 
        	parent_id='".$this->getRequestValue('parent_id')."' 
        where aux_page_ID=".$this->getRequestValue('aux_page_ID')."";
        
        
        $this->db->exec($query);
        
        //$this->addParentsToMap($this->getRequestValue('aux_page_ID'));
        
        return true;
    }
    
    /**
     * Add parents to map
     * @param int $aux_page_ID aux page ID
     * @return boolean
     */
    function addParentsToMap ( $aux_page_ID ) {
        $page_structure = $this->loadPageStructure();
        echo 'start add<br>';
        $parent_id = $page_structure['page'][$aux_page_ID]['parent_id'];
        $query = "update ".AUX_PAGES_TABLE." set in_map = 1 where aux_page_id = $parent_id ";
        $this->db->exec($query);
        
        $page_structure = $this->loadPageStructure();
        $parent_id = $page_structure['page'][$aux_page_ID]['parent_id'];
        
        echo "parent_id = $parent_id<br>";
        while ( $parent_id > 0 ) {
            $query = "update ".AUX_PAGES_TABLE." set in_map = 1 where aux_page_id = $parent_id ";
            echo $query.'<br>';
            $this->db->exec($query);
            $parent_id = $page_structure['page'][$parent_id]['parent_id'];
        }
        return true;
    }
    
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        
        $query = "select * from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('parent_id', $this->db->row['parent_id']);
        return $this->db->row;
    }
    
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        global $__db_prefix;
        $aux_page_ID = $this->getRequestValue('aux_page_ID');
        $parent_id = $this->getRequestValue('parent_id');
        $aux_page_url = $this->getRequestValue('aux_page_url');
        $categoryID = $this->getRequestValue('categoryID');
        
        $query = "update ".AUX_PAGES_TABLE." set parent_id = '$parent_id', in_map=1 where aux_page_ID='$aux_page_ID'";
        
        //$this->addParentsToMap($aux_page_ID);
        
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        if ( $this->getRequestValue('aux_page_ID') == $this->getRequestValue('parent_id') ) {
            $this->riseError('������ ������� �������� �������� � ���� ��������');
            return false;
        }
        return true;
    }
    
    /**
     * Get category select box
     * @param int $current_category_id category ID
     * @return string
     */
    function getCategorySelectBox ( $current_category_id ) {
    	//echo '$current_category_id = '.$current_category_id;
        $category_structure = $this->loadCategoryStructure();
        //echo '<pre>';
        //print_r($category_structure['childs']);
        $level = 0;
        $rs = '';
        $rs .= '<select name="categoryID">';
        foreach ( $category_structure['childs'][1] as $categoryID => $items ) {
            //echo $categoryID;
            $rs .= $this->getChildNodes($categoryID, $category_structure, $level + 1, $current_category_id);
        }
        $rs .= '</select>';
        return $rs;
    }
    
    /**
     * Get page select box
     * @param int $current_page_id page ID
     * @param string $name name
     * @param boolean $in_map in map flag
     * @param boolean $with_main with main flag 
     * @return string
     */
    function getPageSelectBox ( $current_page_id, $name, $in_map = false, $with_main = true ) {
        //echo 'with main flag = '.$with_main."<br>";
    	//echo '$current_category_id = '.$current_category_id;
        $page_structure = $this->loadPageStructure( $in_map );
        //echo '<pre>';
        //print_r($page_structure);
        $level = 1;
        $rs = '';
        $rs .= '<select name="'.$name.'">';
        if ( $with_main ) {
            $rs .= '<option value="0">�������</option>';
        }
        
        if ( is_array($page_structure) ) {
            foreach ( $page_structure['childs'][0] as $item_id => $page_id ) {
                //echo $categoryID;
                $rs .= $this->getChildNodesPage($page_id, $page_structure, $level + 1, $current_page_id);
            }
        }
        $rs .= '</select>';
        return $rs;
    }
    
    /**
     * Get page table box
     * @param int $current_page_id page ID
     * @return string
     */
    function getPageTable ( $current_page_id ) {
    	//echo '$current_category_id = '.$current_category_id;
        $page_structure = $this->loadPageStructure(true);
        //echo '<pre>';
        //print_r($page_structure['childs']);
        $level = -1;
        $rs = '';
        $rs .= '<table border="0">';
        $rs .= '<tr>';
        $rs .= '<td class="row1"><b>�������</b></td>';
        $rs .= '<td></td>';
        $rs .= '</tr>';
        if ( is_array($page_structure) ) {
            foreach ( $page_structure['childs'][0] as $item_id => $page_id ) {
                //echo "item_id = $item_id, page_id = $page_id<br>";
                //echo $categoryID;
                $rs .= $this->getChildNodesPageTable($page_id, $page_structure, $level + 1, $current_page_id);
            }
        }
        $rs .= '</table>';
        return $rs;
    }

    /**
     * Get child nodes pages for table 
     * @param $page_id
     * @param $page_structure
     * @param $level
     * @param $current_page_id
     * @return string
     */
    function getChildNodesPageTable($page_id, $page_structure, $level, $current_page_id) {
        //echo "page_id = $page_id<br>";
        $rs = $this->get_page_row($level, $page_id, $page_structure);
    	if ( is_array($page_structure['childs'][$page_id]) ) {
            foreach ( $page_structure['childs'][$page_id] as $item_id => $child_id ) {
                //echo 'start pring childs child_id => '.$child_id.'<br>';
                //print_r($category_structure['catalog'][$child_id]);
                //$rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$page_structure['page'][$child_id]['aux_page_name'].'</option>';
                //print_r($category_structure['childs'][$child_id]);
                if ( $_SESSION['toggle'][$page_id] == 'open' ) {
                    $rs .= $this->getChildNodesPageTable($child_id, $page_structure, $level + 1, $current_page_id);
                }
            }
    	}
        return $rs;
    }
    
    /**
     * Get page row
     * @param int $level level
     * @param int $child_id child ID
     * @param array $page_structure page structure
     * @return string
     */
    function get_page_row ( $level, $child_id, $page_structure ) {
        $this->j++;
        if ( ceil($this->j/2) > floor($this->j/2)  ) {
            $row_class = "row1";
        } else {
            $j = 0;
            $row_class = "row2";
        }
        $rs = '';
        $rs .= '<tr>';
        $rs .= '<td class="'.$row_class.'">'.str_repeat('<img src="/img/16x16.gif" border="0" width="16" height="16">', $level);
        if ( count($page_structure['childs'][$child_id]) > 0 ) {
            $rs .= '<a href="?action=map&do=toggle&aux_page_ID='.$child_id.'">';
            if (  $_SESSION['toggle'][$child_id] == 'open' ) {
                $rs .= '<img src="/img/tree_collapse.gif" border="0" alt="�������" title="�������" width="16" height="16">';
            } else {
                $rs .= '<img src="/img/tree_expand.gif" border="0" alt="��������" title="��������" width="16" height="16">';
            }
            $rs .= '</a> ';
        } else {
            $rs .= '<img src="/img/16x16.gif" border="0" width="16" height="16">';
        }
            
        if ( $page_structure['page'][$child_id]['aux_page_short'] != '' ) {
            $rs .= $page_structure['page'][$child_id]['aux_page_short'];
        } else {
            $rs .= $page_structure['page'][$child_id]['aux_page_name'];
        }
        $rs .= '</td>';
        $rs .= '<td class="'.$row_class.'">';
        $rs .= '<a href="?action=map&do=new&parent_id='.$child_id.'"><img src="/img/add.png" border="0" alt="�������� ��������" title="�������� ��������" width="16" height="16"></a> ';
        $rs .= '<a href="?action=map&do=delete&aux_page_ID='.$child_id.'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0" alt="������� �� �����" title="������� �� �����" width="16" height="16"></a>';
        $rs .= '</td>';
        $rs .= '</tr>';
        
        return $rs;
    }

    /**
     * Get child nodes pages 
     * @param $page_id
     * @param $page_structure
     * @param $level
     * @param $current_page_id
     * @return string
     */
    function getChildNodesPage($page_id, $page_structure, $level, $current_page_id) {
        if ( $current_page_id == $page_id ) {
            $selected = " selected ";
        } else {
        	$selected = "";
        }
        //print_r($category_structure['catalog'][$child_id]);
        $rs .= '<option value="'.$page_id.'" '.$selected.'>'.str_repeat(' . ', $level);
        	
        if ( $page_structure['page'][$page_id]['aux_page_short'] != '' ) {
            $rs .= $page_structure['page'][$page_id]['aux_page_short'];
        } else {
            $rs .= $page_structure['page'][$page_id]['aux_page_name'];
        }
        	
        $rs .= '</option>';
        
    	if ( is_array($page_structure['childs'][$page_id]) ) {
            foreach ( $page_structure['childs'][$page_id] as $item_id => $child_id ) {
                //print_r($category_structure['childs'][$child_id]);
                $rs .= $this->getChildNodesPage($child_id, $page_structure, $level + 1, $current_page_id);
            }
    	}
        return $rs;
    }
    
    function getChildNodes($categoryID, $category_structure, $level, $current_category_id) {
    	if ( !is_array($category_structure['childs'][$categoryID]) ) {
    		return '';
    	}
        foreach ( $category_structure['childs'][$categoryID] as $child_id ) {
        	if ( $current_category_id == $child_id ) {
        		$selected = " selected ";
        	} else {
        		$selected = "";
        	}
            //print_r($category_structure['catalog'][$child_id]);
            $rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$category_structure['catalog'][$child_id]['name'].'</option>';
            //print_r($category_structure['childs'][$child_id]);
            if ( count($category_structure['childs'][$child_id]) > 0 ) {
                $rs .= $this->getChildNodes($child_id, $category_structure, $level + 1, $current_category_id);
            }
        }
        return $rs;
    }
    
    /**
     * Load category structure
     * @param void
     * @return array
     */
    function loadCategoryStructure () {
        $query = "SELECT * FROM ".CATEGORIES_TABLE." ";
        //echo $query;
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ret['catalog'][$this->db->row['categoryID']] = $this->db->row;
            $ret['childs'][$this->db->row['parent']][] = $this->db->row['categoryID'];
        }
        return $ret;
    }
    
    /**
     * Load page structure
     * @param int $in_map in map flag
     * @return array
     */
    function loadPageStructure ( $in_map = false ) {
        if ( $in_map === true ) {
            $query = "SELECT * FROM ".AUX_PAGES_TABLE." where in_map = $in_map order by aux_page_ID";
        } else {
            $query = "SELECT * FROM ".AUX_PAGES_TABLE." where in_map = 0 order by aux_page_name_ID";
        }
        //echo $query.'<br>';
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ret['page'][$this->db->row['aux_page_ID']] = $this->db->row;
            $ret['childs'][$this->db->row['parent_id']][] = $this->db->row['aux_page_ID'];
        }
        return $ret;
    }
    
    
    
    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
    	</script>
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>������������ ��������</td>';
        $rs .= '<td>'.$this->getPageSelectBox($this->getRequestValue('parent_id'), 'parent_id', true).'</td>';
        $rs .= '</tr>';
        
        
        $rs .= '<tr>';
        $rs .= '<td>�������� ��� ���������� � �����</td>';
        $rs .= '<td>'.$this->getPageSelectBox($this->getRequestValue('aux_page_ID'), 'aux_page_ID', false, false).'</td>';
        $rs .= '</tr>';
        
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="aux_page_ID" value="'.$this->getRequestValue('aux_page_ID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="map">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=map">&laquo; ����� � �����</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $_SESSION;
        global $__db_prefix;
        
        $rs = '';
        $rs .= $this->getPageTable(0);
        return $rs;
        
        $query = "SELECT * FROM ".AUX_PAGES_TABLE." where in_map = 1 ";
        //echo $query;
        $this->db->exec($query);

        $rs = '<div align="left"><table border="0" width="20%">';
        $rs .= '<td ><b>���������</b><td>';
        $rs .= '<td><td>';
        $rs .= '</tr>';
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'" nowrap width="99%">'.$this->db->row['aux_page_name'].'<td>';
            $rs .= '<td width="10%" nowrap><a href="?action=map&do=edit&aux_page_ID='.$this->db->row['aux_page_ID'].'"><img src="/img/edit.gif" border="0"></a> <a href="?action=map&do=delete&aux_page_ID='.$this->db->row['aux_page_ID'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0"></a><td>';
            $rs .= '</tr>';
        }
        $rs .= '</table></div>';
        return $rs;
    }
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="?action=map&do=new">�������� �������� � �����</a>';
        return $rs;
    }
}
?>
