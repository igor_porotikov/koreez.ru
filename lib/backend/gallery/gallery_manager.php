<?php
/**
 * Gallery component
 */
if (  !defined('GALLERY_TABLE')  ) 
{
    define('GALLERY_TABLE', 'SS_gallery');
}

class Gallery_Manager extends Driver {
    /**
     * Constructor
     */
    function __construct() {
        //$params['component_url'] = '/portfolio_manager/';
        $this->Fructus($params);
    }
    
    /**
     * Main
     */
    function main () {
        //global $init;
        
        switch ( $this->getRequestValue('do', 'default') ) {
            case 'edit_done':
                $data = $this->initFromRequest();
                if ( !$this->check_data($data) ) {
                    $rs = $this->getForm('edit');
                    return $rs;
                }
                $this->edit_record($data);
            break;
            
            case 'edit':
            	if ( $this->getRequestValue('subdo') == 'delete_image' ) {
            		$this->deleteImage('gallery', $this->getRequestValue('image_id'));
            	}
            	
            	if ( $this->getRequestValue('subdo') == 'up_image' ) {
            		$this->reorderImage('gallery', $this->getRequestValue('image_id'), 'gallery_id', $this->getRequestValue('gallery_id'),'up');
            	}
            	
            	if ( $this->getRequestValue('subdo') == 'down_image' ) {
            		$this->reorderImage('gallery', $this->getRequestValue('image_id'), 'gallery_id', $this->getRequestValue('gallery_id'), 'down');
            	}
                
                $this->load($this->getRequestValue('gallery_id'));
                $rs = $this->getForm('edit');
                return $rs;
            break;
            
            case 'done':
                $data = $this->initFromRequest();
                if ( !$this->check_data($data) ) {
                    $rs = $this->getForm();
                    return $rs;
                }
                $this->add_record( $data );
            break;
            
            case 'new':
                $rs = $this->getForm();
                return $rs;
            break;
        }
        $rs = $this->getTopMenu();
        $rs .= $this->grid();
        return $rs;
    }
    
    /**
     * Edit record
     * @param array $data data
     * @return boolean
     */
    function edit_record ( $data ) {
        $query = "update ".GALLERY_TABLE." set title='".$data['title']."', short_description='".$data['short_description']."', long_description='".$data['long_description']."',  create_date='".$data['create_date']."' where gallery_id='".$data['gallery_id']."'";
        //echo $query;
        $this->db->exec($query);
        $this->editImageMulti('gallery', 'gallery', 'gallery_id', $this->getRequestValue('gallery_id'));
        return true;
    }
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        global $debug_mode;
        
        $query = "select * from ".GALLERY_TABLE." where gallery_id=$record_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('gallery_id', $this->db->row['gallery_id']);
        $this->setRequestValue('title', $this->db->row['title']);
        $this->setRequestValue('short_description', $this->db->row['short_description']);
        $this->setRequestValue('long_description', $this->db->row['long_description']);
        $this->setRequestValue('create_date', $this->db->row['create_date']);
    }
    
    /**
     * Load array
     * @param int $record_id record ID
     * @return boolean
     */
    function load_array ( $record_id ) {
        global $debug_mode;
        
        $query = "select * from ".GALLERY_TABLE." where gallery_id=$record_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row;
    }
    
    
    
    /**
     * Add record
     * @param array $data
     * @return void
     */
    function add_record( $data ) {
        $query = "insert into ".GALLERY_TABLE." (title, short_description, long_description, create_date) values ('".$data['title']."', '".$data['short_description']."', '".$data['long_description']."', '".$data['create_date']."')";
        $this->writeLog(__METHOD__.", query = $query");
        //echo $query;
        $this->db->exec($query);
        
        $this->editImageMulti('gallery', 'gallery', 'gallery_id', $this->db->last_insert_id());
    }
    
    /**
     * Check data
     * @param array
     * @return boolean
     */
    function check_data ( $data ) {
        if ( $data['title'] == '' ) {
            $this->RiseError('�� ������� ��������');
            return false;
        }
        return true;
    }
    
    
    /**
     * Init from request
     * @param void
     * @return array
     */
    function initFromRequest () {
        $array = array();
        
        $array['gallery_id'] = $this->getRequestValue('gallery_id');
        $array['title'] = $this->getRequestValue('title');
        $array['short_description'] = $this->getRequestValue('short_description');
        $array['long_description'] = $this->getRequestValue('long_description');
        $array['create_date'] = time();
        //echo '<pre>';
        //print_r($array);
        return $array;
    }
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="'.$this->get_component_url().'?action=gallery&do=new">�������� �������</a>';
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $config, $init;
        
        $query = "select * from ".GALLERY_TABLE." order by create_date desc";

        $rs .= '<table border="0" width="100%">';
        $rs .= '<tr>';
        $rs .= '<td class="row_title">������������</td>';
        $rs .= '<td class="row_title">��������</td>';
        $rs .= '<td class="row_title">����</td>';
        $rs .= '<td class="row_title"></td>';
        $rs .= '<tr>';
        
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            
            $edit_link = '<a href="?action=gallery&do=edit&gallery_id='.$this->db->row['gallery_id'].'"><img src="/img/edit.gif" border="0"></a>';
            $delete_link = '<a href="?action=gallery&do=delete&gallery_id='.$this->db->row['gallery_id'].'"><img src="/img/delete.gif" border="0"></a>';
            
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'">'.$this->db->row['title'].'</td>';
            $rs .= '<td class="'.$row_class.'">'.$this->db->row['short_description'].'</td>';
            $rs .= '<td class="'.$row_class.'">'.$this->db->row[''].'</td>';
            $rs .= '<td class="'.$row_class.'">'.$edit_link.' '.$delete_link.'</td>';
            $rs .= '<tr>';
        }
        $rs .= '</table>';
        return $rs;
    }
    
    /**
     * Get form
     * @param string $action action
     * @return string
     */
    function getForm ( $action = 'new' ) {
        global $debug_mode;
        if ( $debug_mode ) {
            $this->writeLog(__METHOD__);
        }
        
        $rs .= '<form method="post" action="'.$this->get_component_url().'" name="rentform" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td></td>';
            $rs .= '<td><span class="error">'.$this->GetErrorMessage().'</span></td>';
            $rs .= '</tr>';
        }
        
        $rs .= '<tr>';
        $rs .= '<td class="left_column">�������� <span class="error">*</span>:</td>';
        $rs .= '<td><input type="text" name="title" value="'.$this->getRequestValue('title').'" size="60"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td class="left_column">������� ��������:</td>';
        $rs .= '<td><textarea name="short_description" id="short_description" style="width: 450px; height: 150px;">'.$this->getRequestValue('short_description').'</textarea></td>';
        $rs .= '</tr>';
        
        /*
        $rs .= '<tr>';
        $rs .= '<td class="left_column">��������� ��������:</td>';
        $rs .= '<td><textarea name="long_description" style="width: 450px; height: 150px;">'.$this->getRequestValue('long_description').'</textarea></td>';
        $rs .= '</tr>';
        */
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2">';
        $rs .= '<h2>����������</h2>';
        $rs .= $this->getImageListAdmin('gallery', 'gallery', 'gallery_id', $this->getRequestValue('gallery_id'));
        $rs .= '</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2">';
        $rs .= $this->getUploadifyPlugin($_REQUEST['PHPSESSID']);
        //$rs .= $this->getFlashListAdmin('elit', 'elit', 'elit_id', $this->getRequestValue('elit_id'));
        $rs .= '</td>';
        $rs .= '</tr>';
        
        
        $rs .= '<tr>';
        $rs .= '<td></td>';
        $rs .= '<input type="hidden" name="pub_status" value="'.$this->getRequestValue('pub_status').'">';
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="gallery_id" value="'.$this->getRequestValue('gallery_id').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="done">';
        }
        $rs .= '<input type="hidden" name="action" value="gallery">';
        
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        $rs .= '</table>';
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Get uploadify plugin
     * @param string $session_code session code
     * @return string
     */
    function getUploadifyPlugin ( $session_code ) {
        $rs = '';
        $rs .= '
<link href="/js/upl/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/js/upl/swfobject.js"></script>
<script type="text/javascript" src="/js/upl/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript">
var uploadedfiles = 0;
var maxQueueSize = 8;
var queueSize = 0;
$(document).ready(function() {
  $(\'#file_upload\').uploadify({
    \'uploader\'  : \'/js/upl/uploadify.swf\',
    \'script\'    : \'/js/upl/uploadify.php?session='.$session_code.'\',
    \'cancelImg\' : \'/js/upl/cancel.png\',
    \'folder\'    : \'/storage/upl\',
    \'auto\'      : true,
	\'fileExt\': \'*.jpg\',
	\'multi\': true,	
	\'queueSizeLimit\': 8,
	\'sizeLimit\': 2000000,
	\'buttonText\': \'select image ...\',	
    \'onComplete\': function(event, queueID, fileObj, response, data) {
    					queueSize++;
    					if ( response == \'max_file_size\' ) {
    						alert(\'���� ������� �������. ������������ ������ ����� '.ini_get('upload_max_filesize').' \');
    						return false;
    					}
    					if ( response == \'wrong_ext\' ) {
    						alert(\'����� ��������� ������ ����� � ����������� *.jpg\');
    						return false;
    					}
    					if ( queueSize > maxQueueSize ) {
    						alert(\'�� ��������� ������������ ���������� ������\');
    						return false;
    					}
    					addFileNotify(queueSize);
    				}
                    
    });
});
function addFileNotify ( queueSize ) {
	$(\'#filenotify\').html( \'�� ������� ���������: \' + queueSize + \' ����(��)\' );
}
</script>
�������� �����������: <input id="file_upload" name="file_upload" type="file" />
<div id="filenotify"></div>
        ';
        return $rs;
    }
}
?>
