<?php
class Review_Manager extends Review {

	public function __construct(){
		parent::__construct();
	}
	
	public function main(){
		$do = $this->getRequestValue('do');
		switch($do){
			case 'edit' : {
				if(!isset($_POST['submit'])){
					$id=$this->getRequestValue('rev_id');
					if(isset($id) AND $id!=''){
						if($data=$this->loadReview($id)){
							$ret=$this->getForm($data);
						}else{
							$ret=$this->getReviewGrid();
						}
					}else{
						$ret=$this->getReviewGrid();
					}
				}else{
					//var_dump(1);
					$data=array();
					$data['id']=$this->getRequestValue('id');
					$data['user_nick']=$this->getRequestValue('user_nick');
					$data['review']=$this->getRequestValue('review');
					$data['email']=$this->getRequestValue('email');
					$data['admin_review']=$this->getRequestValue('admin_review');
					$data['mark']=$this->getRequestValue('mark');
					if(isset($_POST['is_published'])){
						$data['is_published']=1;
					}else{
						$data['is_published']=0;
					}
					//$data['is_published']=$this->getRequestValue('is_published');
					$this->editReview($data);
					$ret=$this->getReviewGrid();
				}
				break;
			}
			case 'delete' : {
				$id=$this->getRequestValue('rev_id');
				if(isset($id) AND $id!=''){
					$this->deleteReview($id);
				}
				$ret=$this->getReviewGrid();
				break;
			}
			default : {
				$ret=$this->getReviewGrid();
			}
		}
		return $ret;
	}
	
	
	private function getReviewGrid(){
		$query="SELECT * FROM ".REVIEWS_TABLE." ORDER BY add_date DESC";
		//echo $query;
		$this->db->exec($query);
		$text.='<table>';
		$text.='<tr><td><b>id</b></td><td><b>�����</b></td><td><b>E-mail</b></td><td><b>���� ����������</b></td><td><b>�����</b></td><td><b>����� ������</b></td><td><b>������������</b></td><td><b>������</b></td>';
		while($this->db->fetch_assoc()){
			$j++;
			if ( ceil($j/2) > floor($j/2)  ) {
				$row_class = "row1";
			} else {
				$j = 0;
				$row_class = "row2";
			}
			//print_r($this->db->row);
			$text.='<tr class="'.$row_class.'" nowrap width="99%">';
			$text.='<td>'.$this->db->row['id'].'</td>';
			$text.='<td>'.$this->db->row['user_nick'].'</td>';
			$text.='<td>'.$this->db->row['email'].'</td>';
			$text.='<td>'.date('d-m-Y H:i',$this->db->row['add_date']).'</td>';
			$text.='<td>'.$this->db->row['review'].'</td>';
			$text.='<td>'.$this->db->row['admin_review'].'</td>';
			$text.='<td>';
			if($this->db->row['is_published']==1){
				$text.='��';
			}else{
				$text.='���';
			}
			$text.='</td>';
			$text.='<td>'.$this->getMarkTitle($this->db->row['mark']).'</td>';
			$text.='<td width="10%" nowrap><a href="?action=review&do=edit&rev_id='.$this->db->row['id'].'"><img src="/img/edit.gif" border="0"></a> <a href="?action=review&do=delete&rev_id='.$this->db->row['id'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif"></a></td>';
			$text.='</tr>';
		}
		$text.='</table>';
		return $text;
	}
	
	private function deleteReview($id){
		$query='DELETE FROM '.REVIEWS_TABLE.' WHERE id='.$id;
		$this->db->exec($query);
	}
	
	private function getForm($data=NULL){
        $ret = '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
      		$(document).ready(function() {
        		$("#input").cleditor();
        		$("#input1").cleditor();
			});
    	</script>
        ';
	    
		$ret.='<form action="/admin/index.php?action=review&do=edit" method="post">';
		//$ret.='<legend>�������� �����</legend>';
		$ret.='<table>';
		if((isset($data['is_published'])) AND ($data['is_published']==1)){
			$ret.='<tr><td>�����������</td><td><input type="checkbox" name="is_published" checked="checked" /></td></tr>';
		}else{
			$ret.='<tr><td>�����������</td><td><input type="checkbox" name="is_published" /></td></tr>';
		}
		
		$ret.='<tr><td>���</td><td><input type="text" name="user_nick" value="'.$data['user_nick'].'" /></td></tr>';
		//$ret.='<tr><td>���� ���</td><td><select name="sex"><option value="n">�� ������</option><option value="m">�������</option><option value="f">�������</option></select></td></tr>';
		$ret.='<tr><td>�����</td><td><textarea id="input1" cols="50" rows="10" name="review">'.$data['review'].'</textarea></td></tr>';
		$ret.='<tr><td>������</td><td>';
		if ( $data['mark'] == 0 ) {
		    $mark0_checked = 'checked';
		} elseif ( $data['mark'] == 1 ) {
		    $mark1_checked = 'checked';
		} elseif ( $data['mark'] == 2 ) {
		    $mark2_checked = 'checked';
		}
		$ret .= ' <input type="radio" name="mark" value="0" '.$mark0_checked.'/> �����������';
		$ret .= ' <input type="radio" name="mark" value="1" '.$mark1_checked.'/> ���������';
		$ret .= ' <input type="radio" name="mark" value="2" '.$mark2_checked.'/> �� �����������';
		
		$ret.='</td></tr>';
		$ret.='<tr><td>����� ������</td><td><textarea id="input" cols="50" rows="10" name="admin_review">'.$data['admin_review'].'</textarea></td></tr>';
		$ret.='<tr><td>E-mail</td><td><input type="text" name="email" value="'.$data['email'].'" /></td></tr>';
		$ret.='<tr><td></td><td><input type="submit" name="submit" value="���������" /></td></tr>';
		$ret.='</table>';
		$ret.='<input type="hidden" name="id" value="'.$data['id'].'" />';
		$ret.='</form>';
		return $ret;
	}
	
	private function loadReview($id){
		$query="SELECT * FROM ".REVIEWS_TABLE." WHERE id=".$id;
		$this->db->exec($query);
		if(!$this->db->success OR ($this->db->num_rows()==0)){
			return FALSE;
		}else{
			$this->db->fetch_assoc();
			$data=$this->db->row;
			return $data;
		}
	}
	
	private function editReview($data){
		$query="UPDATE ".REVIEWS_TABLE." SET user_nick='".$data['user_nick']."', review='".$data['review']."', email='".$data['email']."', is_published='".$data['is_published']."', admin_review='".$data['admin_review']."', mark='".$data['mark']."' WHERE id=".$data['id'];
		//echo $query;
		$this->db->exec($query);
		if($this->db->success){
			return TRUE;
		}else{
			return FALSE;
		}
	}

}