<?php
/**
 * Config manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 * @url http://www.sitebill.ru
 */
class Config_Manager extends Fructus {

	private $dev_status=false;
	private $is_shop = true;

	/**
	 * Constructor
	 */
	function __construct() {
		$this->Fructus();
		
	}
	
	/**
	 * Main
	 */
	function main () {
		
		$cnf_mode=$this->getRequestValue('cnf_mode');
		switch($cnf_mode){
			case 'addnew' : {
				$conf_new_param_name=$this->getRequestValue('conf_new_param_name');
				$conf_new_param_value=$this->getRequestValue('conf_new_param_value');
				$conf_new_param_title=$this->getRequestValue('conf_new_param_title');
				if(($conf_new_param_name!='') OR ($conf_new_param_name!=NULL)){
					if($this->addParamToConfig($conf_new_param_name,$conf_new_param_value,$conf_new_param_title)){
						$rs = $this->getForm();
						$rs.=$this->getAddForm();
					}else{
						//error
					}
				}else{
					$rs = $this->getForm();
					$rs.=$this->getAddForm('retry');
				}
				break;				
			}
			
			case 'save' : {
				$post=$this->getRequestValue('conf_param_value');
				foreach($post as $k=>$v){
					$this->updateParamToConfig($k,$v);
				}
				$rs='������������ ���������. ��������� �� �������� ��������?';
				$rs.='<form method="post">';
				$rs .= '<input type="submit" name="cnf_submit" value="�������">';
				$rs .= '<input type="hidden" name="action" value="config">';
				$rs .= '</form>';
				//$rs = $this->getForm();
				//$rs.=$this->getAddForm();
				//$rs=print_r($post);
				break;
			}
			
			case 'editone' : {
				$submit=$this->getRequestValue('cnf_submit');
				if(($submit!='')OR($submit!=NULL)){
					$data['id']=$this->getRequestValue('conf_edit_param_id');
					$data['key']=$this->getRequestValue('conf_edit_param_key');
					$data['title']=$this->getRequestValue('conf_edit_param_title');
					$data['value']=$this->getRequestValue('conf_edit_param_value');
					$this->updateParamFull($data);
					$rs='�������� �����������. <a href="?action=config">���������</a> �� �������� ��������?';
				}else{
					$param_id=$this->getRequestValue('param_id');
					$data=array();
					$query='SELECT * FROM '.DB_PREFIX.'_config WHERE id='.$param_id;
					$this->db->exec($query);
					while ($this->db->fetch_assoc()){
						$data['id']=$this->db->row['id'];
						$data['key']=$this->db->row['config_key'];
						$data['title']=$this->db->row['title'];
						$data['value']=$this->db->row['value'];
					}
					$rs.=$this->getEditOneForm($data);
				}
				break;
			}
			
			case 'delete' : {
				$param_id=$this->getRequestValue('param_id');
				if(($param_id!='') OR ($param_id!=NULL)){
					$this->deleteParamFromConfig($param_id);
				}
				$rs='������������ ��������. <a href="?action=config">���������</a> �� �������� ��������?';
				break;
			}
			
			default : {
				$rs=$this->getForm();
				$rs.=$this->getAddForm();
			}
		}
		return $rs;
	}
	
	/**
	 * Get config form
	 */
	function getForm () {
		$tabs[1] = array('meta_title_product_detailed', 'meta_title_catalog1', 'meta_title_catalog2', 'meta_description_catalog1', 'meta_description_catalog2', 'meta_description_product_detailed');
		$tabs[2] = array('random_template_mode', 'template');
$rs .= '
<script type="text/javascript">

			$(function(){
				// Tabs
				$(\'#tabs\').tabs();
        });
</script>
';		
		$rs .= '<form method="post">';
        
		
		
		$query='SELECT * FROM '.DB_PREFIX.'_config ORDER BY sort_order';
		$this->db->exec($query);
		while ($this->db->fetch_assoc()){
		    if ( in_array($this->db->row['config_key'], $tabs[1]) ) {
		        $tabs_items[1]['content'][] = $this->db->row;
		    }
		    
		    if ( in_array($this->db->row['config_key'], $tabs[2]) ) {
		        $tabs_items[2]['content'][] = $this->db->row;
		    }
		    
		}
		
		
		$rs .= '
<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
			<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#tabs-1">����������� ����</a></li>
				<li class="ui-state-default ui-corner-top"><a href="#tabs-2">��������� ��������</a></li>
				<li class="ui-state-default ui-corner-top"><a href="#tabs-3">�������</a></li>
			</ul>
		';
		
		//start generate tabs
		for ( $i = 1; $i <= 3; $i++ ) {
		    $rs .= '<div id="tabs-'.$i.'" class="ui-tabs-panel ui-widget-content ui-corner-bottom" style="overflow: auto;">';
		    $rs .= '<div id="es_full"><table border="0">';
		    $rs .= '<thead><th>��� ���������</th><th>��������</th><th>��������</th><th></th><th></th></thead>';

		    if ( is_array($tabs_items[$i]['content']) ) {
    		    foreach ( $tabs_items[$i]['content'] as $item_id => $item ) {
	    		    $rs .= '<tr>';
		    	    $rs .= '<td>'.$item['config_key'].'</td>';
			        $rs .= '<td>'.$item['title'].'</td>';
			        $rs .= '<td><input size="50" type="text" name="conf_param_value['.$item['id'].']" value="'.$item['value'].'"></td>';
			        $rs .= '<td>'.($this->dev_status?'<a href="?action=config&cnf_mode=delete&param_id='.$item['id'].'">�������</a>':'').'</td>';
			        $rs .= '<td>'.($this->dev_status?'<a href="?action=config&cnf_mode=editone&param_id='.$item['id'].'">�������</a>':'').'</td>';
			        $rs .= '</tr>';
        			//$this->config_array[$this->db->row['config_key']]=$this->db->row['value'];
	        	}
		    }
		    
		    //echo '<pre>';
    		//print_r($tabs_items);

	    	$rs .= '</table></div>';
	    	
		    if ( $this->is_shop and $i == 1 ) {
	            $rs .= '<div id="es_full">';
	            $rs .= '�������� ��������� ����������<br>';
	            $rs .= '<b>$shop_name</b> - �������� ��������<br>';
	            $rs .= '<b>$category_name1</b> - ������������ ��������� 1-�� ������<br>';
	            $rs .= '<b>$category_name2</b> - ������������ ��������� 2-�� ������<br>';
	            $rs .= '<b>$product_name</b> - ������������ ������, ������� ������ ���������������<br>';
	            $rs .= '</div>';
	        }
	    	
		
		    $rs .= '</div>';
		}
		$rs .= '</div>';
		
		

		$rs .= '<input type="hidden" name="action" value="config">';
		$rs .= '<input type="hidden" name="cnf_mode" value="save">';

		$rs .= '<table border="0">';
		$rs .= '<tr>';
		$rs .= '<td></td><td></td><td><input type="submit" name="cnf_submit" value="���������"></td>';
		$rs .= '</tr>';
		$rs .= '</table>';
		
		$rs .= '</form>';
		
		return $rs;
	}
	
	function getAddForm ($mode=NULL) {
		if($this->dev_status){
			$rs = '<h1>����� ��������</h1>';
			$rs .= '<form method="post">';
			$rs .= '<table border="0">';
			$rs .= '<thead><th>��� ���������</th><th>��������</th><th>��������</th></thead>';
			$rs .= '<tr>';
			$rs .= '<td><input type="text" name="conf_new_param_name" value="'.($mode!==NULL?$this->getRequestValue('conf_new_param_name'):'').'"></td>';
			$rs .= '<td><input type="text" name="conf_new_param_title" value="'.($mode!==NULL?$this->getRequestValue('conf_new_param_title'):'').'"></td>';
			$rs .= '<td><input type="text" name="conf_new_param_value" value="'.($mode!==NULL?$this->getRequestValue('conf_new_param_value'):'').'"></td>';
			$rs .= '</tr>';
			$rs .= '<tr>';
			$rs .= '<td></td><td><input type="submit" name="cnf_submit" value="��������"></td>';
			$rs .= '</tr>';
			$rs .= '<input type="hidden" name="action" value="config">';
			$rs .= '<input type="hidden" name="cnf_mode" value="addnew">';
			$rs .= '</table>';
			$rs .= '</form>';
		}else{
			$rs='';
		}

		return $rs;
	}
	
	function getEditOneForm ($data) {
		if($this->dev_status){
			$rs = '<h1>������ ���������</h1>';
			$rs .= '<form method="post">';
			$rs .= '<table border="0">';
			$rs .= '<thead><th>��� ���������</th><th>��������</th><th>��������</th></thead>';
			$rs .= '<tr>';
			$rs .= '<td><input type="text" name="conf_edit_param_key" value="'.$data['key'].'"></td>';
			$rs .= '<td><input type="text" name="conf_edit_param_title" value="'.$data['title'].'"></td>';
			$rs .= '<td><input type="text" name="conf_edit_param_value" value="'.$data['value'].'"></td>';
			$rs .= '</tr>';
			$rs .= '<tr>';
			$rs .= '<td></td><td><input type="submit" name="cnf_submit" value="��������"></td>';
			$rs .= '</tr>';
			$rs .= '<input type="hidden" name="action" value="config">';
			$rs .= '<input type="hidden" name="conf_edit_param_id" value="'.$data['id'].'">';
			$rs .= '<input type="hidden" name="cnf_mode" value="editone">';
			$rs .= '</table>';
			$rs .= '</form>';
		}else{
			$rs='';
		}

		return $rs;
	}
	
	function addParamToConfig($conf_new_param_name,$conf_new_param_value,$conf_new_param_title){
		$query="INSERT INTO ".DB_PREFIX."_config VALUES ('','".$this->validateParam($conf_new_param_name)."','".$this->validateParam($conf_new_param_value)."','".$this->validateParam($conf_new_param_title)."')";
		$this->db->exec($query);
		return TRUE;
	}
	
	function updateParamToConfig($conf_param_id,$conf_param_value){
		$query="UPDATE ".DB_PREFIX."_config SET value='".$this->validateParam($conf_param_value)."' WHERE id='".$conf_param_id."'";
		$this->db->exec($query);
		return TRUE;
	}
	
	function updateParamFull($data){
		$query="UPDATE ".DB_PREFIX."_config SET config_key='".$this->validateParam($data['key'])."', value='".$this->validateParam($data['value'])."', title='".$this->validateParam($data['title'])."' WHERE id=".$data['id'];
		$this->db->exec($query);
		return TRUE;
	}
	
	function deleteParamFromConfig($param_id){
		$query="DELETE FROM ".DB_PREFIX."_config WHERE id=".$param_id;
		$this->db->exec($query);
		return TRUE;
	}
	
	function loadConfig(){
		$query='SELECT * FROM '.DB_PREFIX.'_config';
		$this->db->exec($query);
		while ($this->db->fetch_assoc()){
			$this->config_array[$this->db->row['config_key']]=$this->db->row['value'];
		}		
	}
	
	function validateParam($param){
		$rs=$param;
		$rs=htmlspecialchars($rs);
		$rs=str_replace(array('\'','"','`'),'',$rs);
		$rs=trim($rs);
		$rs=addslashes($rs);
		return $rs;
	}
}
?>
