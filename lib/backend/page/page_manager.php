<?php
/**
 * Page manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Page_Manager extends Fructus {
    /**
     * Constructor
     */
    function Page_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $this->deleteRecord($this->getRequestValue('aux_page_ID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
            	if ( $this->getRequestValue('subdo') == 'delete_additional' ) {
            		$this->delete_additional_records($this->getRequestValue('aux_page_ID'), $this->getRequestValue('page_id'), $this->getRequestValue('key'));
            	}
                
            	if ( $this->getRequestValue('subdo') == 'add_additional' ) {
            		$this->add_additional_records($this->getRequestValue('aux_page_ID'), $this->getRequestValue('page_id'), $this->getRequestValue('key'));
            	}
            	
                if ( $this->getRequestValue('subdo') == 'add_gallery' ) {
            		$this->add_gallery($this->getRequestValue('aux_page_ID'), $this->getRequestValue('gallery_id'));
            	}
            	if ( $this->getRequestValue('subdo') == 'delete_gallery' ) {
            		$this->delete_gallery($this->getRequestValue('aux_page_ID'), $this->getRequestValue('gallery_id'));
            	}
            	
                
                $hash = $this->load($this->getRequestValue('aux_page_ID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $rs = $this->getForm();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Add additional records to the page by KEY
     * @param int $aux_page_ID 
     * @param int $page_id
     * @param string $key
     * @return void
     */
    function add_additional_records ( $aux_page_ID, $page_id, $key ) {
        if ( $page_id == '' ) {
            return false;
        }
        $page_array = $this->get_page_array($key, $aux_page_ID);
        if ( !$page_array ) {
            $page_array = array();
        }
        //print_r($page_array);
        array_push($page_array, $page_id);
        sort($page_array);
        $query = "update ".AUX_PAGES_TABLE." set $key='".implode(',', $page_array)."' where aux_page_ID=$aux_page_ID";
        //echo $query;
        $this->db->exec($query);
        return true;  
    }
    
    /**
     * Delete additional records from the page by KEY
     * @param int $aux_page_ID 
     * @param int $page_id
     * @param string $key
     * @return void
     */
    function delete_additional_records ( $aux_page_ID, $page_id, $key ) {
        if ( $page_id == '' ) {
            return false;
        }
        $page_array = $this->get_page_array($key, $aux_page_ID);
        if ( !$page_array ) {
            $page_array = array();
        }
        $ra = array();
        foreach ( $page_array as $record_id ) {
            if ( $record_id != $page_id ) {
                $ra[] = $record_id; 
            }
        }
        //print_r($page_array);
        //array_push($page_array, $page_id);
        sort($ra);
        $query = "update ".AUX_PAGES_TABLE." set $key='".implode(',', $ra)."' where aux_page_ID=$aux_page_ID";
        //echo $query;
        $this->db->exec($query);
        return true;  
    }
    
    
    /**
     * Add gallery
     * @param int $page_id
     * @param int $gallery_id
     */
    function add_gallery ( $page_id, $gallery_id ) {
        if ( $page_id > 0 and $gallery_id > 0 ) {
            $query = "insert into ".GALLERY_PAGE_TABLE." (gallery_id, page_id) values ($gallery_id, $page_id)";
            //echo $query;
            $this->db->exec($query);
        }
    }
    
    /**
     * Delete gallery
     * @param int $page_id
     * @param int $gallery_id
     */
    function delete_gallery ( $page_id, $gallery_id ) {
        $query = "delete from ".GALLERY_PAGE_TABLE." where gallery_id=$gallery_id and page_id=$page_id";
        //echo $query;
        $this->db->exec($query);
    }
    
    
    /**
     * Delete record
     * @param int $record_id record ID
     * @return boolean
     */
    function deleteRecord ( $record_id ) {
        
        $query = "delete from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Edit record
     * @param void
     * @return boolean
     */
    function editRecord () {
        global $__db_prefix;
        
        $query = "update ".AUX_PAGES_TABLE." set 
        	aux_page_name='".$this->getRequestValue('aux_page_name')."', 
        	aux_page_text='".$this->getRequestValue('aux_page_text')."', 
        	
        	aux_page_h1='".$this->getRequestValue('aux_page_h1')."', 
        	aux_page_short='".$this->getRequestValue('aux_page_short')."', 
        	
        	meta_keywords='".$this->getRequestValue('meta_keywords')."', 
        	meta_description='".$this->getRequestValue('meta_description')."', 
        	categoryID='".$this->getRequestValue('categoryID')."', 
        	aux_page_url='".$this->getRequestValue('aux_page_url')."' 
        where aux_page_ID=".$this->getRequestValue('aux_page_ID')."";
        
        
        $this->db->exec($query);
        
        return true;
    }
    
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        
        $query = "select * from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('aux_page_name', $this->db->row['aux_page_name']);
        $this->setRequestValue('aux_page_text', $this->db->row['aux_page_text']);
        $this->setRequestValue('aux_page_url', $this->db->row['aux_page_url']);
        
        $this->setRequestValue('aux_page_h1', $this->db->row['aux_page_h1']);
        $this->setRequestValue('aux_page_short', $this->db->row['aux_page_short']);
        
        $this->setRequestValue('meta_keywords', $this->db->row['meta_keywords']);
        $this->setRequestValue('meta_description', $this->db->row['meta_description']);
        $this->setRequestValue('categoryID', $this->db->row['categoryID']);
        return $this->db->row;
    }
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load_array ( $record_id ) {
        
        $query = "select * from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        return $this->db->row;
    }
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        global $__db_prefix;
        $aux_page_name = $this->getRequestValue('aux_page_name');
        $aux_page_text = $this->getRequestValue('aux_page_text');
        $aux_page_url = $this->getRequestValue('aux_page_url');
        $aux_page_h1 = $this->getRequestValue('aux_page_h1');
        $aux_page_short = $this->getRequestValue('aux_page_short');
        
        $meta_keywords = $this->getRequestValue('meta_keywords');
        $meta_description = $this->getRequestValue('meta_description');
        $categoryID = $this->getRequestValue('categoryID');
        
        $query = "insert into ".AUX_PAGES_TABLE." (aux_page_name, aux_page_text, aux_page_url, categoryID, meta_keywords, meta_description, aux_page_h1, aux_page_short) values ('$aux_page_name', '$aux_page_text', '$aux_page_url', '$categoryID', '$meta_keywords', '$meta_description', '$aux_page_h1', '$aux_page_short')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        if ( $this->getRequestValue('aux_page_name') == '' ) {
            $this->riseError('�� ������� ��������');
            return false;
        }
        return true;
    }
    
    /**
     * Get category select box
     * @param int $current_category_id category ID
     * @return string
     */
    function getCategorySelectBox ( $current_category_id ) {
    	//echo '$current_category_id = '.$current_category_id;
        $category_structure = $this->loadCategoryStructure();
        //echo '<pre>';
        //print_r($category_structure['childs']);
        $level = 0;
        $rs = '';
        $rs .= '<select name="categoryID">';
        foreach ( $category_structure['childs'][1] as $categoryID => $items ) {
            //echo $categoryID;
            $rs .= $this->getChildNodes($categoryID, $category_structure, $level + 1, $current_category_id);
        }
        $rs .= '</select>';
        return $rs;
    }
    
    function getChildNodes($categoryID, $category_structure, $level, $current_category_id) {
    	if ( !is_array($category_structure['childs'][$categoryID]) ) {
    		return '';
    	}
        foreach ( $category_structure['childs'][$categoryID] as $child_id ) {
        	if ( $current_category_id == $child_id ) {
        		$selected = " selected ";
        	} else {
        		$selected = "";
        	}
            //print_r($category_structure['catalog'][$child_id]);
            $rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$category_structure['catalog'][$child_id]['name'].'</option>';
            //print_r($category_structure['childs'][$child_id]);
            if ( count($category_structure['childs'][$child_id]) > 0 ) {
                $rs .= $this->getChildNodes($child_id, $category_structure, $level + 1, $current_category_id);
            }
        }
        return $rs;
    }
    
    /**
     * Load category structure
     * @param void
     * @return array
     */
    function loadCategoryStructure () {
        $query = "SELECT * FROM ".CATEGORIES_TABLE." ";
        //echo $query;
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ret['catalog'][$this->db->row['categoryID']] = $this->db->row;
            $ret['childs'][$this->db->row['parent']][] = $this->db->row['categoryID'];
        }
        return $ret;
    }
    
    
    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
    	</script>
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>���������</td>';
        $rs .= '<td>'.$this->getCategorySelectBox($this->getRequestValue('categoryID')).'</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��������</td>';
        $rs .= '<td><input type="text" name="aux_page_name" size="50" value="'.$this->getRequestValue('aux_page_name').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��������� H1</td>';
        $rs .= '<td><input type="text" name="aux_page_h1" size="50" value="'.$this->getRequestValue('aux_page_h1').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>�������� �������� ��������</td>';
        $rs .= '<td><input type="text" name="aux_page_short" size="50" value="'.$this->getRequestValue('aux_page_short').'"></td>';
        $rs .= '</tr>';
        
        
        $login = $this->getLoginByUserID($_SESSION['user_id']);
        
        if ( $login != 'Admin_k' ) {
            $rs .= '<input type="hidden" name="aux_page_url" value="'.$this->getRequestValue('aux_page_url').'">';
        } else {
            $rs .= '<tr>';
            $rs .= '<td>SEO-link</td>';
            $rs .= '<td><input type="text" name="aux_page_url" size="50" value="'.$this->getRequestValue('aux_page_url').'"></td>';
            $rs .= '</tr>';
        }
        
        $rs .= '<tr>';
        $rs .= '<td>Meta keywords</td>';
        $rs .= '<td><textarea name="meta_keywords" rows="10" cols="45">'.$this->getRequestValue('meta_keywords').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>Meta description</td>';
        $rs .= '<td><textarea name="meta_description" rows="10" cols="45">'.$this->getRequestValue('meta_description').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>�����</td>';
        $rs .= '<td><textarea name="aux_page_text" id="input" rows="10" cols="45">'.$this->getRequestValue('aux_page_text').'</textarea></td>';
        $rs .= '</tr>';
        
        
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="aux_page_ID" value="'.$this->getRequestValue('aux_page_ID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="page">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=page">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        $rs .= '</form>';
        
        $rs .= '<tr>';
        $rs .= '<td>��������</td>';
        $rs .= '<td>'.$this->get_gallery_add_form($this->getRequestValue('aux_page_ID'), 'page', $action).'</td>';
        $rs .= '</tr>';

        $rs .= '<tr>';
        $rs .= '<td>������������ ��������</td>';
        $rs .= '<td>'.$this->get_additional_add_form('aux_page_parents', $this->getRequestValue('aux_page_ID'), 'page', $action).'</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>�������� ��������</td>';
        $rs .= '<td>'.$this->get_additional_add_form('aux_page_childs', $this->getRequestValue('aux_page_ID'), 'page', $action).'</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>������� ��������<br>(��������)</td>';
        $rs .= '<td>'.$this->get_additional_add_form('aux_page_similar', $this->getRequestValue('aux_page_ID'), 'page', $action).'</td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        
        return $rs;
    }
    
    /**
     * Get parents add form
     * @param string $key key
     * @param int $page_id page id
     * @param string $action
     * @param string $do
     * @return string
     */
    function get_additional_add_form ( $key, $page_id, $action, $do ) {
        $page_array = $this->get_page_array($key, $page_id);
        
        $rs .= $this->get_page_list($page_array, $page_id, $key);
        if ( !is_array($page_array) ) {
            $page_array = array();
        }
        array_push($page_array, $page_id);
        sort($page_array);
        
        $rs .= '<form method="get">';
        $rs .= $this->get_page_select_box( $page_array );
        $rs .= '<input type="hidden" name="action" value="'.$action.'">';
        $rs .= '<input type="hidden" name="aux_page_ID" value="'.$page_id.'">';
        $rs .= '<input type="hidden" name="do" value="'.$do.'">';
        $rs .= '<input type="hidden" name="key" value="'.$key.'">';
        $rs .= '<input type="hidden" name="subdo" value="add_additional">';
        $rs .= '<input type="submit" value="��������">';
        $rs .= '</form>';
        return $rs;
    }
    
    /**
     * Get page select box
     * @param array $page_array except page list
     * @return string
     */
    function get_page_select_box ( $page_array ) {
        $query = "select * from ".AUX_PAGES_TABLE;
        if ( is_array($page_array) ) {
            $query .= " where aux_page_ID not in (".implode(' , ', $page_array).")";   
        }
        //echo $query;
        $this->db->exec($query);
        $rs = '<select name="page_id">';
        while ( $this->db->fetch_assoc() ) {
            $rs .= '<option value="'.$this->db->row['aux_page_ID'].'">'.$this->db->row['aux_page_name'].'</option>';
        }
        $rs .= '</select>';
        return $rs;
    }
    
    
    /**
     * Get page array
     * @param string $key
     * @param int $page_id
     * @return array
     */
    function get_page_array ( $key, $page_id ) {
        $query = "select $key from ".AUX_PAGES_TABLE." where aux_page_ID=$page_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        if ( $this->db->row[$key] != '' ) {
            $items = explode(',', $this->db->row[$key]);
        }
        if ( is_array($items) ) {
            return $items;
        }
        return false;
    }
    
    /**
     * Get gallery add form
     * @param int $page_id page id
     * @param string $action action
     * @return string
     */
    function get_gallery_add_form ( $page_id, $action, $do ) {
        $gallery_array = $this->load_gallery_page_array($page_id);
        //echo '<pre>';
        //print_r($gallery_array);
        $rs .= $this->get_gallery_list($gallery_array, $page_id);
        
        
        $rs .= '<form method="get">';
        $rs .= $this->get_gallery_select_box( $gallery_array );
        $rs .= '<input type="hidden" name="action" value="'.$action.'">';
        $rs .= '<input type="hidden" name="aux_page_ID" value="'.$page_id.'">';
        $rs .= '<input type="hidden" name="do" value="'.$do.'">';
        $rs .= '<input type="hidden" name="subdo" value="add_gallery">';
        $rs .= '<input type="submit" value="��������">';
        $rs .= '</form>';
        return $rs;
    }

    /**
     * Get page list
     * @param array $page_array 
     * @param int $page_id page id
     * @param string $key
     * @return string
     */
    function get_page_list ( $page_array, $page_id, $key ) {
        if ( !is_array($page_array) ) {
            return '';
        }
        $rs .= '<table border="0">';
        $rs .= '<tr><td colspan="2"><b>��������� ��������</b></td></tr>';
        foreach ( $page_array as  $item_id => $record_id ) {
            $page_info = $this->load_array($record_id);
            //$gallery_info = $gallery_manager->load_array($gallery_id);
            $rs .= '<tr>';
            $rs .= '<td class="row1" width="99%">'; 
            $rs .= $page_info['aux_page_name'];
            $rs .= '</td>';
            $rs .= '<td class="row1">'; 
            $rs .= '<a href="?action=page&do=edit&aux_page_ID='.$page_id.'&subdo=delete_additional&key='.$key.'&page_id='.$record_id.'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}" ><img src="/img/delete.gif" border="0" title="�������" alt="�������"></a>';
            $rs .= '</td>';
            $rs .= '</tr>';
        }
        $rs .= '</table>';
        return $rs;
    }
    
    /**
     * Get gallery list
     * @param array $gallery_array 
     * @param int $page_id page id
     * @return string
     */
    function get_gallery_list ( $gallery_array, $page_id ) {
        if ( count($gallery_array) < 1 ) {
            return '';
        }
        require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/driver.php');
		require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/backend/gallery/gallery_manager.php');
        $gallery_manager = new Gallery_Manager();

        $rs .= '<table border="0">';
        $rs .= '<tr><td colspan="2"><b>��������� �������</b></td></tr>';
        foreach ( $gallery_array as  $item_id => $gallery_id ) {
            $gallery_info = $gallery_manager->load_array($gallery_id);
            $rs .= '<tr>';
            $rs .= '<td class="row1" width="99%">'; 
            $rs .= $gallery_info['title'];
            $rs .= '</td>';
            $rs .= '<td class="row1">'; 
            $rs .= '<a href="?action=page&do=edit&aux_page_ID='.$page_id.'&subdo=delete_gallery&gallery_id='.$gallery_id.'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0" title="��������� ������� �� ��������" alt="��������� ������� �� ��������"></a>';
            $rs .= '</td>';
            $rs .= '</tr>';
        }
        $rs .= '</table>';
        return $rs;
    }
    
    /**
     * Load gallery page array
     * @param int $page_id page ID
     * @return array
     */
    function load_gallery_page_array ( $page_id ) {
        $ra = array();
        $query = "select * from ".GALLERY_PAGE_TABLE." where page_id=$page_id";
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ra[] = $this->db->row['gallery_id'];
        }
        return $ra;
    }
    
    /**
     * Get gallery select box
     * @param array $gallery_array except gallery list
     * @return string
     */
    function get_gallery_select_box ( $gallery_array ) {
        $query = "select * from ".GALLERY_TABLE;
        if ( count($gallery_array) > 0 ) {
            $query .= " where gallery_id not in (".implode(' , ', $gallery_array).")";   
        }
        //echo $query;
        $this->db->exec($query);
        $rs = '<select name="gallery_id">';
        while ( $this->db->fetch_assoc() ) {
            $rs .= '<option value="'.$this->db->row['gallery_id'].'">'.$this->db->row['title'].'</option>';
        }
        $rs .= '</select>';
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $_SESSION;
        global $__db_prefix;
        
        $query = "SELECT * FROM ".AUX_PAGES_TABLE." ";
        //echo $query;
        $this->db->exec($query);

        $rs = '<div align="left"><table border="0" width="20%">';
        $rs .= '<td ><b>���������</b><td>';
        $rs .= '<td><td>';
        $rs .= '</tr>';
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'" nowrap width="99%">'.$this->db->row['aux_page_name'].'<td>';
            $rs .= '<td width="10%" nowrap><a href="?action=page&do=edit&aux_page_ID='.$this->db->row['aux_page_ID'].'"><img src="/img/edit.gif" border="0"></a> <a href="?action=page&do=delete&aux_page_ID='.$this->db->row['aux_page_ID'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0"></a><td>';
            $rs .= '</tr>';
        }
        $rs .= '</table></div>';
        return $rs;
    }
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="?action=page&do=new">�������� ��������</a>';
        return $rs;
    }
}
?>
