<?php
/**
 * News manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class News_Manager extends Fructus {
    /**
     * Constructor
     */
    function News_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $this->deleteRecord($this->getRequestValue('NID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
            	if ( $this->getRequestValue('subdo') == 'delete_image' ) {
            		$this->deleteImage('news', $this->getRequestValue('image_id'));
            	}
            	
                $hash = $this->load($this->getRequestValue('NID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $this->setRequestValue('add_date', date('d.m.Y'));
                $rs = $this->getForm();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Delete record
     * @param int $record_id record ID
     * @return boolean
     */
    function deleteRecord ( $record_id ) {
        
        $query = "delete from ".NEWS_TABLE." where NID=$record_id";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Edit record
     * @param void
     * @return boolean
     */
    function editRecord () {
        global $__db_prefix;
        
        $query = "update ".NEWS_TABLE." set 
        	title='".$this->getRequestValue('title')."', 
        	textToPublication='".$this->getRequestValue('textToPublication')."', 
        	add_date='".$this->getRequestValue('add_date')."', 
        	add_stamp='".strtotime($this->getRequestValue('add_date'))."' 
        where NID=".$this->getRequestValue('NID')."";
        
        $this->db->exec($query);
        
        $this->editImageMulti('news', 'news', 'news_id', $this->getRequestValue('NID'));
        
        
        return true;
    }
    
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        
        $query = "select * from ".NEWS_TABLE." where NID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('title', $this->db->row['title']);
        $this->setRequestValue('textToPublication', $this->db->row['textToPublication']);
        $this->setRequestValue('add_date', $this->db->row['add_date']);
        $this->setRequestValue('add_stamp', $this->db->row['add_stamp']);
    }
    
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        global $__db_prefix;
        $title = $this->getRequestValue('title');
        $textToPublication = $this->getRequestValue('textToPublication');
        $add_date = $this->getRequestValue('add_date');
        $add_stamp = strtotime($this->getRequestValue('add_date'));
        
        $query = "insert into ".NEWS_TABLE." (title, textToPublication, add_date, add_stamp) values ('$title', '$textToPublication', '$add_date', '$add_stamp')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        if ( $this->getRequestValue('title') == '' ) {
            $this->riseError('�� ������ ���������');
            return false;
        }
        if ( $this->getRequestValue('textToPublication') == '' ) {
            $this->riseError('�� �������� ����� �������');
            return false;
        }
        if ( $this->getRequestValue('add_date') == '' ) {
            $this->riseError('�� ������� ����');
            return false;
        }
        return true;
    }
    
    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
      		$(document).ready(function() {
        		$("#input").cleditor();
      		});
			$(function() {
    			$("#date").datepicker({
            		showOn: \'button\',
            		buttonImage: \'/img/calendar.gif\',
            		buttonImageOnly: true
    			});
    			$("#anim").change(function() { 
        			$(\'#date\').datepicker(\'option\', {showAnim: $(this).val()});
    			});
			});      		
    	</script>
        <script type="text/javascript" src="/js/image_admin.js"></script>         
    	
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>���������</td>';
        $rs .= '<td><input type="text" name="title" size="50" value="'.$this->getRequestValue('title').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>�����</td>';
        $rs .= '<td><textarea name="textToPublication" id="input" rows="10" cols="45">'.$this->getRequestValue('textToPublication').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>����</td>';
        $rs .= '<td><input type="text" size="10" name="add_date" id="date" value="'.$this->getRequestValue('add_date').'"></td>';
        $rs .= '</tr>';

        $rs .= '<tr>';
        $rs .= '<td></td>';
        $rs .= '<td><div id="img_list">'.$this->getImageListAdmin ( 'news', 'news', 'news_id', $this->getRequestValue('NID') ).'</div></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td></td>';
        $rs .= '<td><div id="imgarea"></div></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td></td>';
        $rs .= '<td style="text-align: center;"><input type="hidden" id="default-id" name="default-id" value="0"><a href="javascript:{}" onclick="addInput()">�������� ����</a></td>';
        $rs .= '</tr>';
        
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="NID" value="'.$this->getRequestValue('NID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="news">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=news">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $_SESSION;
        global $__db_prefix;
        
        $query = "SELECT * FROM ".NEWS_TABLE." order by add_stamp desc";
        //echo $query;
        $this->db->exec($query);

        $rs = '<div align="left"><table border="0" width="20%">';
        $rs .= '<td ><b>���������</b><td>';
        $rs .= '<td ><b>����</b><td>';
        $rs .= '<td><td>';
        $rs .= '</tr>';
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'" nowrap width="90%">'.$this->db->row['title'].'<td>';
            $rs .= '<td class="'.$row_class.'" width="10%">'.$this->db->row['add_date'].'<td>';
            $rs .= '<td width="10%" nowrap><a href="?action=news&do=edit&NID='.$this->db->row['NID'].'"><img src="/img/edit.gif" border="0"></a> <a href="?action=news&do=delete&NID='.$this->db->row['NID'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0"></a><td>';
            $rs .= '</tr>';
        }
        $rs .= '</table></div>';
        return $rs;
    }
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="?action=news&do=new">�������� �������</a>';
        return $rs;
    }
}
?>
