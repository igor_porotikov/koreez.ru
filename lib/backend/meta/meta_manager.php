<?php
/**
 * Meta manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Meta_Manager extends Fructus {
    /**
     * Constructor
     */
    function Meta_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $this->deleteRecord($this->getRequestValue('aux_page_ID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
                $hash = $this->load($this->getRequestValue('aux_page_ID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $rs = $this->getForm();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Delete record
     * @param int $record_id record ID
     * @return boolean
     */
    function deleteRecord ( $record_id ) {
        
        $query = "delete from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Edit record
     * @param void
     * @return boolean
     */
    function editRecord () {
        global $__db_prefix;
        
        $query = "update ".AUX_PAGES_TABLE." set 
        	aux_page_name='".$this->getRequestValue('aux_page_name')."', 
        	aux_page_text='".$this->getRequestValue('aux_page_text')."', 
        	categoryID='".$this->getRequestValue('categoryID')."', 
        	aux_page_url='".$this->getRequestValue('aux_page_url')."' 
        where aux_page_ID=".$this->getRequestValue('aux_page_ID')."";
        
        
        $this->db->exec($query);
        
        return true;
    }
    
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        
        $query = "select * from ".AUX_PAGES_TABLE." where aux_page_ID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('aux_page_name', $this->db->row['aux_page_name']);
        $this->setRequestValue('aux_page_text', $this->db->row['aux_page_text']);
        $this->setRequestValue('aux_page_url', $this->db->row['aux_page_url']);
        $this->setRequestValue('categoryID', $this->db->row['categoryID']);
    }
    
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        global $__db_prefix;
        $aux_page_name = $this->getRequestValue('aux_page_name');
        $aux_page_text = $this->getRequestValue('aux_page_text');
        $aux_page_url = $this->getRequestValue('aux_page_url');
        $categoryID = $this->getRequestValue('categoryID');
        
        $query = "insert into ".AUX_PAGES_TABLE." (aux_page_name, aux_page_text, aux_page_url, categoryID) values ('$aux_page_name', '$aux_page_text', '$aux_page_url', '$categoryID')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        if ( $this->getRequestValue('aux_page_name') == '' ) {
            $this->riseError('�� ������� ��������');
            return false;
        }
        return true;
    }
    
    /**
     * Get category select box
     * @param int $current_category_id category ID
     * @return string
     */
    function getCategorySelectBox ( $current_category_id ) {
    	//echo '$current_category_id = '.$current_category_id;
        $category_structure = $this->loadCategoryStructure();
        //echo '<pre>';
        //print_r($category_structure['childs']);
        $level = 0;
        $rs = '';
        $rs .= '<select name="categoryID">';
        foreach ( $category_structure['childs'][1] as $categoryID => $items ) {
            //echo $categoryID;
            $rs .= $this->getChildNodes($categoryID, $category_structure, $level + 1, $current_category_id);
        }
        $rs .= '</select>';
        return $rs;
    }
    
    function getChildNodes($categoryID, $category_structure, $level, $current_category_id) {
    	if ( !is_array($category_structure['childs'][$categoryID]) ) {
    		return '';
    	}
        foreach ( $category_structure['childs'][$categoryID] as $child_id ) {
        	if ( $current_category_id == $child_id ) {
        		$selected = " selected ";
        	} else {
        		$selected = "";
        	}
            //print_r($category_structure['catalog'][$child_id]);
            $rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$category_structure['catalog'][$child_id]['name'].'</option>';
            //print_r($category_structure['childs'][$child_id]);
            if ( count($category_structure['childs'][$child_id]) > 0 ) {
                $rs .= $this->getChildNodes($child_id, $category_structure, $level + 1, $current_category_id);
            }
        }
        return $rs;
    }
    
    /**
     * Load category structure
     * @param void
     * @return array
     */
    function loadCategoryStructure () {
        $query = "SELECT * FROM ".CATEGORIES_TABLE." ";
        //echo $query;
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ret['catalog'][$this->db->row['categoryID']] = $this->db->row;
            $ret['childs'][$this->db->row['parent']][] = $this->db->row['categoryID'];
        }
        return $ret;
    }
    
    
    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
    	</script>
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>���������</td>';
        $rs .= '<td>'.$this->getCategorySelectBox($this->getRequestValue('categoryID')).'</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��������</td>';
        $rs .= '<td><input type="text" name="aux_page_name" size="50" value="'.$this->getRequestValue('aux_page_name').'"></td>';
        $rs .= '</tr>';
        
        $login = $this->getLoginByUserID($_SESSION['user_id']);
        
        if ( $login != 'Admin_k' ) {
            $rs .= '<input type="hidden" name="aux_page_url" value="'.$this->getRequestValue('aux_page_url').'">';
        } else {
            $rs .= '<tr>';
            $rs .= '<td>SEO-link</td>';
            $rs .= '<td><input type="text" name="aux_page_url" size="50" value="'.$this->getRequestValue('aux_page_url').'"></td>';
            $rs .= '</tr>';
        }
        
        
        $rs .= '<tr>';
        $rs .= '<td>�����</td>';
        $rs .= '<td><textarea name="aux_page_text" id="input" rows="10" cols="45">'.$this->getRequestValue('aux_page_text').'</textarea></td>';
        $rs .= '</tr>';
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="aux_page_ID" value="'.$this->getRequestValue('aux_page_ID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="page">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=page">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $_SESSION;
        global $__db_prefix;
        
        $query = "SELECT * FROM ".AUX_PAGES_TABLE." ";
        //echo $query;
        $this->db->exec($query);

        $rs = '<div align="left"><table border="0" width="20%">';
        $rs .= '<td ><b>���������</b><td>';
        $rs .= '<td><td>';
        $rs .= '</tr>';
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'" nowrap width="99%">'.$this->db->row['aux_page_name'].'<td>';
            $rs .= '<td width="10%" nowrap><a href="?action=page&do=edit&aux_page_ID='.$this->db->row['aux_page_ID'].'"><img src="/img/edit.gif" border="0"></a> <a href="?action=page&do=delete&aux_page_ID='.$this->db->row['aux_page_ID'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0"></a><td>';
            $rs .= '</tr>';
        }
        $rs .= '</table></div>';
        return $rs;
    }
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        //$rs = '<a href="?action=page&do=new">�������� ��������</a>';
        return $rs;
    }
}
?>
