<?php
/**
 * Main admin module
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Admin_Main extends Fructus {
	/**
	 * Constructor
	 */
	function __construct() {
		$this->Fructus();
	}
	
	/**
	 * 
	 */
	function main () {
		$rs .= $this->getMenu();
        if ( $_REQUEST['action'] != ''  ) {
            if ( !$this->getAccess($_SESSION['user_id'], $_REQUEST['action']) ) {
                return '������ ��������';
            }
	        if ( $_REQUEST['action'] == 'grandprice' ) {
    	        require_once('../lib/backend/grandprice/grandprice_manager.php');
                $Grandprice_Manager = new Grandprice_Manager();
                $rs .= $Grandprice_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'news' ) {
                require_once('../lib/backend/news/news_manager.php');
                $News_Manager = new News_Manager();
                $rs .= $News_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'order' ) {
                require_once('../lib/driver.php');
                require_once('../lib/backend/order/order_manager.php');
                $Order_Manager = new Order_Manager();
                $rs .= $Order_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'catalog' ) {
                require_once('../lib/backend/catalog/catalog_manager.php');
                $Catalog_Manager = new Catalog_Manager();
                $rs .= $Catalog_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'product' ) {
                require_once('../lib/backend/catalog/catalog_manager.php');
                require_once('../lib/backend/product/product_manager.php');
                $Product_Manager = new Product_Manager();
                $rs .= $Product_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'page' ) {
                require_once('../lib/backend/page/page_manager.php');
                $Page_Manager = new Page_Manager();
                $rs .= $Page_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'gallery' ) {
                require_once('../lib/driver.php');
                require_once('../lib/backend/gallery/gallery_manager.php');
                $Gallery_Manager = new Gallery_Manager();
                $rs .= $Gallery_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'map' ) {
                require_once('../lib/backend/page/page_manager.php');
                require_once('../lib/backend/map/map_manager.php');
                $Map_Manager = new Map_Manager();
                $rs .= $Map_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'meta' ) {
                require_once('../lib/backend/meta/meta_manager.php');
                $Meta_Manager = new Meta_Manager();
                $rs .= $Meta_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'review' ) {
                require_once('../lib/frontend/review/review.php');
                require_once('../lib/backend/review/review_manager.php');
                $Review_Manager = new Review_Manager();
                $rs .= $Review_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'config' ) {
                require_once('../lib/backend/config/config_manager.php');
                $Config_Manager = new Config_Manager();
                $rs .= $Config_Manager->main();
                return $rs;
            } elseif( $_REQUEST['action'] == 'logout' ) {
            	unset($_SESSION['user_id']);
            	$rs .= '�� �����';
            	$rs .= '<script>';
            	$rs .= "location.href='?'";
            	$rs .= '</script>';
                return $rs;
            } elseif( $_REQUEST['action'] == 'users' ) {
                require_once('../lib/backend/users/users_manager.php');
                $Users_Manager = new Users_Manager();
                $rs .= $Users_Manager->main();
                return $rs;
            }
        }
		
		return $rs;
	}
	
	/**
	 * Get menu
	 */
	function getMenu () {
	    if ( $this->getAccess($_SESSION['user_id'], 'grandprice') ) {
            $menu['grandprice']['title'] = '������';
            $menu['grandprice']['href'] = 'index.php?action=grandprice';
	    }
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'catalog') ) {
	        $menu['catalog']['title'] = '�������';
            $menu['catalog']['href'] = 'index.php?action=catalog';
	    }
	    
	    /*
	    if ( $this->getAccess($_SESSION['user_id'], 'product') ) {
	        $menu['product']['title'] = '������';
            $menu['product']['href'] = 'index.php?action=product';
	    }
	    */
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'order') ) {
	        $menu['order']['title'] = '������';
            $menu['order']['href'] = 'index.php?action=order';
	    }
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'page') ) {
	        $menu['page']['title'] = '��������';
            $menu['page']['href'] = 'index.php?action=page';
	    }
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'gallery') ) {
	        $menu['gallery']['title'] = '�������';
            $menu['gallery']['href'] = 'index.php?action=gallery';
	    }
	    
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'news') ) {
	        $menu['news']['title'] = '�������';
            $menu['news']['href'] = 'index.php?action=news';
	    }

	    /*
	    if ( $this->getAccess($_SESSION['user_id'], 'meta') ) {
	        $menu['meta']['title'] = '������� meta';
            $menu['meta']['href'] = 'index.php?action=meta';
	    }
	    */
	    
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'users') ) {
	        $menu['users']['title'] = '������������';
            $menu['users']['href'] = 'index.php?action=users';
	    }
	    
	    /*
	    if ( $this->getAccess($_SESSION['user_id'], 'review') ) {
	        $menu['review']['title'] = '������';
            $menu['review']['href'] = 'index.php?action=review';
	    }
	    */
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'map') ) {
	        $menu['map']['title'] = '����� �����';
            $menu['map']['href'] = 'index.php?action=map';
	    }
	    
	    if ( $this->getAccess($_SESSION['user_id'], 'config') ) {
	        $menu['config']['title'] = '���������';
            $menu['config']['href'] = 'index.php?action=config';
	    }
	    

	    $menu['logout']['title'] = '�����';
        $menu['logout']['href'] = 'index.php?action=logout';
    
	    $menu['main']['title'] = '������� �� ����';
        $menu['main']['href'] = '/';
        $menu['main']['target'] = '_blank';
        
        
        foreach ( $menu as $menu_key => $menu_item ) {
            if ( $menu_item['href'] != '' ) {
                $menu_string .= '<li><a href="'.$menu_item['href'].'" target="'.$menu_item['target'].'">'.$menu_item['title'].'</a></li>';
            } else {
                $menu_string .= '<li>'.$menu_item['title'].'</li>';
            }
        }
        
        
        $rs = '
<div class="menu" align="center">
        <ul>
                    '.$menu_string.'
        </ul>
</div>
';
        return $rs;
	}
}
?>
