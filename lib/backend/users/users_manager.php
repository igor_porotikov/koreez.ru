<?php
/**
 * Users manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 * @url http://www.sitebill.ru
 */
class Users_Manager extends Fructus {
	/**
	 * Constructor
	 */
	function Users_Manager() {
		$this->Fructus();
	}
	
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'edit':
                $hash = $this->load($this->getRequestValue('customerID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    if ( $this->getRequestValue('new_pass') != '' ) {
                        $this->editPassword($this->getRequestValue('customerID'), $this->getRequestValue('new_pass'));
                    }
                    $this->edit_data($this->getRequestValue('customerID'));
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $rs = $this->getForm();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="?action=users&do=new">�������� ������������</a>';
        return $rs;
    }
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        $fructus_auth = new Fructus_Auth();
        
        global $__db_prefix;
        $login = $this->getRequestValue('login');
        $custgroupID = $this->getRequestValue('custgroupID');
        $Email = $this->getRequestValue('Email');
        $first_name = $this->getRequestValue('first_name');
        $last_name = $this->getRequestValue('last_name');
        
        $password = $fructus_auth->cryptPasswordCrypt($this->getRequestValue('new_pass'), null);
        
        $query = "insert into ".CUSTOMERS_TABLE." (login, cust_password, custgroupID, Email, first_name, last_name) values ('$login', '$password', '$custgroupID', '$Email', '$first_name', '$last_name')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Edit data
     * @param int $user_id user id
     * @return boolean
     */
    function edit_data ( $user_id ) {
        $query = "update SS_customers set custgroupID='".$this->getRequestValue('custgroupID')."', Email='".$this->getRequestValue('Email')."', first_name='".$this->getRequestValue('first_name')."', last_name='".$this->getRequestValue('last_name')."' where customerID=$user_id";
        //echo $query;
        $this->db->exec($query);
        return true;
    }
    
    /**
     * Edit password
     * @param int $user_id user id
     * @param string $password password
     * @return boolean
     */
    function editPassword ( $user_id, $password ) {
        $fructus_auth = new Fructus_Auth();
        
        $password = $fructus_auth->cryptPasswordCrypt($password, null);
        
        $query = "update ".CUSTOMERS_TABLE." set cust_password='".$password."' where customerID=$user_id";
        $this->db->exec($query);
        return true;
    }
    
    
    /**
     * Check password
     * @param int $user_id user id
     * @param string $password password
     * @return boolean
	 */
    function checkPassword ( $user_id, $password ) {
        global $__db_prefix;
    	
    	$query = "select user_id from ".CUSTOMERS_TABLE." where customerID=$user_id and cust_password='".md5($password)."'";
    	//echo $query;
    	
    	$this->db->exec($query);
		$this->db->fetch_assoc();
		if ( $this->db->row['user_id'] > 0 ) {
			return true;
		}    	
		return false;
    }
    
    /**
     * Check crypt
     * @param string $password password
     * @return boolean
	 */
    function checkCrypt ( $password ) {
        if ( strlen($password) < 5 ) {
            $this->riseError('����� ������ ������ ���� ������ 5 ��������');
            return false;
        }
		return true;
    }
    
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        global $__user;
        
        if ( $this->getRequestValue('login') == '' ) {
            $this->riseError('�� ������� ��� ������������');
            return false;
        }
        
        if ( $this->getRequestValue('Email') == '' ) {
            $this->riseError('�� ������ email ������������');
            return false;
        }
        
        if ( $this->getRequestValue('new_pass') != '' ) {
       	    if ( !$this->checkCrypt( $this->getRequestValue('new_pass') ) ) {
                return false;
            }
        }
       	
        if ( $this->getRequestValue('new_pass') != $this->getRequestValue('new_pass_retype') ) {
            $this->riseError('����� ������ �� ���������');
            return false;
        }
        return true;
    }
    
    /**
     * Get group select box
     * @param int $group_id
     * @return string
     */
    function get_group_selext_box ( $group_id ) {
        $query = "select * from SS_custgroups order by sort_order";
        $this->db->exec($query);
        $rs = '<select name="custgroupID">';
        while ( $this->db->fetch_assoc() ) {
            if ( $group_id == $this->db->row['custgroupID'] ) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            $rs .= '<option value="'.$this->db->row['custgroupID'].'" '.$selected.'>'.$this->db->row['custgroup_name'].'</option>';
        }
        $rs .= '</select>';
        return $rs;
    }
    
    

    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>         
        <script src="/js/interface.js"></script> 
        ';
        $rs .= '<form method="post" action="index.php">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: left;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>��� ������������</td>';
        $rs .= '<td width="90%">';
        if ( $action == 'edit' ) {
            $rs .= '<b>'.$this->getRequestValue('login').'</b>';
            $rs .= '<input type="hidden" name="login" value="'.$this->getRequestValue('login').'">';
        } else {
            $rs .= '<input type="text" name="login" value="'.$this->getRequestValue('login').'">';
        }
        
        $rs .= '</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>������</td>';
        $rs .= '<td>'.$this->get_group_selext_box($this->getRequestValue('custgroupID')).'</td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>Email</td>';
        $rs .= '<td><input type="text" name="Email" value="'.$this->getRequestValue('Email').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>���</td>';
        $rs .= '<td><input type="text" name="first_name" value="'.$this->getRequestValue('first_name').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>�������</td>';
        $rs .= '<td><input type="text" name="last_name" value="'.$this->getRequestValue('last_name').'"></td>';
        $rs .= '</tr>';
        
        
        $rs .= '<tr>';
        $rs .= '<td>����� ������</td>';
        $rs .= '<td><input type="password" name="new_pass"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td nowrap>��������� ����� ������</td>';
        $rs .= '<td><input type="password" name="new_pass_retype"></td>';
        $rs .= '</tr>';
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="customerID" value="'.$this->getRequestValue('customerID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="users">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=users">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        
        $query = "select * from ".CUSTOMERS_TABLE." where customerID=$record_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        
        $this->setRequestValue('login', $this->db->row['Login']);
        $this->setRequestValue('custgroupID', $this->db->row['custgroupID']);
        $this->setRequestValue('Email', $this->db->row['Email']);
        $this->setRequestValue('first_name', $this->db->row['first_name']);
        $this->setRequestValue('last_name', $this->db->row['last_name']);
    }
    
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
        global $_SESSION;
        global $__db_prefix;
        
        $query = "select * from ".CUSTOMERS_TABLE." order by customerID asc";
        $this->db->exec($query);

        $rs = '<div align="left"><table border="0" width="20%">';
        $rs .= '<td ><b>�����</b><td>';
        $rs .= '<td><td>';
        $rs .= '</tr>';
        while ( $this->db->fetch_assoc() ) {
            $j++;
            if ( ceil($j/2) > floor($j/2)  ) {
                $row_class = "row1";
            } else {
                $j = 0;
                $row_class = "row2";
            }
            $rs .= '<tr>';
            $rs .= '<td class="'.$row_class.'" nowrap width="99%">'.$this->db->row['Login'].'<td>';
            $rs .= '<td width="10%" nowrap><a href="?action=users&do=edit&customerID='.$this->db->row['customerID'].'"><img src="/img/edit.gif" border="0"></a><td>';
            $rs .= '</tr>';
        }
        $rs .= '</table></div>';
        return $rs;
    }
	
}
?>