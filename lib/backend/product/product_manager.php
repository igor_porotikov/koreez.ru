<?php
/**
 * Product manager
 */
class Product_Manager extends Catalog_Manager {
    /**
     * Constructor
     */
    function Product_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $rs = $this->deleteRecord($this->getRequestValue('productID'));
                $rs .= $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
                $this->load($this->getRequestValue('productID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $this->setRequestValue('date', date('d.m.Y'));
                $rs = $this->getForm();
            break;
            
            case 'toggle':
                $this->toggle($this->getRequestValue('categoryID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
	
	function deleteRecord ($record_id) {
		$ret='';
		$query = 'DELETE FROM '.PRODUCTS_TABLE.' WHERE productID='.$record_id;
		//echo $query;
		$this->db->exec($query);
		if(!$this->db->success){
			$ret='������ ��� ��������';
		}
		return $ret;
	}
	
	function addRecord () {

		$name = $this->getRequestValue('name');
		$categoryid = $this->getRequestValue('parent');
        $meta_title = $this->getRequestValue('meta_title');
        $seolink = $this->getRequestValue('seolink');
		$sort_order = $this->getRequestValue('sort_order');
		$description = $this->getRequestValue('description');
        $meta_keywords = $this->getRequestValue('meta_keywords');
        $meta_description = $this->getRequestValue('meta_description');

        $query = "insert into ".PRODUCTS_TABLE." (name, categoryID, description, seolink, meta_keywords, meta_description, sort_order, meta_title) values ('$name', '$categoryid', '$description', '$seolink', '$meta_keywords', '$meta_description', '$sort_order', '$meta_title')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;	
	}
	
	function editRecord () {
		$query = "UPDATE ".PRODUCTS_TABLE." SET 
        	name='".$this->getRequestValue('name')."',
			categoryID='".$this->getRequestValue('parent')."',
			meta_title='".$this->getRequestValue('meta_title')."',
			seolink='".$this->getRequestValue('seolink')."',
        	sort_order='".$this->getRequestValue('sort_order')."', 
        	description='".$this->getRequestValue('description')."', 
			meta_description='".$this->getRequestValue('meta_description')."', 
        	meta_keywords='".$this->getRequestValue('meta_keywords')."'
        WHERE productID=".$this->getRequestValue('productid')."";
        //echo $query;
        $this->db->exec($query);
        //$this->editImage($this->getRequestValue('categoryID'));
        
        return true;
	}
	
    function checkData () {
		if ( $this->getRequestValue('name') == '' ) {
            $this->riseError('�� ������ ���������');
            return false;
        }
        return true;
	}
	
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
      		$(document).ready(function() {
        		$("#input").cleditor();
      		});
    	</script>
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>���������</td>';
        $rs .= '<td>'.$this->getCatalogSelectBox($this->getRequestValue('categoryid')).'</td>';
        $rs .= '</tr>';

        $rs .= '<tr>';
        $rs .= '<td>��������</td>';
        $rs .= '<td><input type="text" name="name" size="50" value="'.$this->getRequestValue('name').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>���-������</td>';
        $rs .= '<td><input type="text" name="seolink" size="50" value="'.$this->getRequestValue('seolink').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>������� ����������</td>';
        $rs .= '<td><input type="text" name="sort_order" size="50" value="'.$this->getRequestValue('sort_order').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>����� ��������</td>';
        $rs .= '<td><textarea name="meta_title" rows="5" cols="45">'.$this->getRequestValue('meta_title').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��� META keywords</td>';
        $rs .= '<td><textarea name="meta_keywords" rows="5" cols="45">'.$this->getRequestValue('meta_keywords').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��� META description</td>';
        $rs .= '<td><textarea name="meta_description" rows="5" cols="45">'.$this->getRequestValue('meta_description').'</textarea></td>';
        $rs .= '</tr>';
        
        
        $rs .= '<tr>';
        $rs .= '<td>��������<br>(HTML)</td>';
        $rs .= '<td><textarea name="description" id="input" rows="10" cols="45">'.$this->getRequestValue('description').'</textarea></td>';
        $rs .= '</tr>';
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="productid" value="'.$this->getRequestValue('productid').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="product">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=product">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Load
     * @param int $record_id
     * @return void
     */
	function load ( $record_id ) {
        $query = 'SELECT * FROM '.PRODUCTS_TABLE.' WHERE productID='.$record_id;
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        //echo 'on load parent = '.$this->db->row['parent'].'<br>';
        $this->setRequestValue('productid', $this->db->row['productID']);
        $this->setRequestValue('categoryid', $this->db->row['categoryID']);
        $this->setRequestValue('name', $this->db->row['name']);
        $this->setRequestValue('seolink', $this->db->row['seolink']);
        $this->setRequestValue('description', $this->db->row['description']);
        $this->setRequestValue('meta_keywords', $this->db->row['meta_keywords']);
        $this->setRequestValue('meta_description', $this->db->row['meta_description']);
        $this->setRequestValue('meta_title', $this->db->row['meta_title']);
        $this->setRequestValue('first_descr', $this->db->row['first_descr']);
    }
    
	
}
?>
