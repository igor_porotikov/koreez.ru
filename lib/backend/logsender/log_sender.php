<?php
/**
 * Log sender
 * @author Kondin Dmitriy <kondin@etown.ru>
 * @url http://www.sitebill.ru
 */
class Log_Sender extends Fructus {
	var $bzip2 = '/usr/bin/bzip2';
	var $zip = '';
    /**
     * Construct
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Main
     */
    function main () {
    	$this->mail_multipart('kondin@etown.ru', 'Apache log', 'this is apache log', '/var/spool/userlogs/g29553/pcquality.ru.2011-05-11.error.log.bz2');
    	return __METHOD__;
    }
    
    /**
     * Create zip
     * @param
     * @return
     */
    function createZip ( $filename ) {
    	exec($this->bzip2.' -dc '.$filename.' > test.log');
    	exec('');
    }
    
	/**
 	 * Mail multipart
 	 * @param string $to
 	 * @param string $thm subject
 	 * @param string $html email message
 	 * @param string $path path to attached file
 	 * @return boolean
 	 */
	function mail_multipart($to, $thm, $html, $path) {
    	$fp = fopen($path,"r"); 
	    if (!$fp) {
    	    return false; 
    	} 
    	$file = fread($fp, filesize($path)); 
    	fclose($fp); 

    	//$from_domain = str_replace('www.', '', CONF_SHOP_URL);
    	$from_mail = 'logsender@pcquality.ru';
    
    	$boundary = "--".md5(uniqid(time())); // ���������� ����������� 
    	$headers .= "MIME-Version: 1.0\n";
    	$headers .= "From: $from_mail\n"; 
    	$headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
	    //$headers .= "From: backup@".CONF_SHOP_URL."\r\n"; 
    	$multipart .= "--$boundary\n"; 
    	$kod = 'windows-1251'; // ��� $kod = 'windows-1251'; 
    	$multipart .= "Content-Type: text/html; charset=$kod\n"; 
    	$multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n"; 
    	$multipart .= "$html\n\n"; 
    
    	$path_array = split('/',$path);
    	$email_file_name = array_pop($path_array);
    

    	$message_part = "--$boundary\n"; 
    	$message_part .= "Content-Type: application/octet-stream\n"; 
    	$message_part .= "Content-Transfer-Encoding: base64\n"; 
    	$message_part .= "Content-Disposition: attachment; filename = \"".$email_file_name."\"\n\n"; 
    	$message_part .= chunk_split(base64_encode($file))."\n"; 
    	$multipart .= $message_part."--$boundary--\n"; 

    	if(!mail($to, $from_domain.': auto backup', $multipart, $headers)) 
    	{
      		return false; 
    	}
    	return true; 
	}
    
}
?>
