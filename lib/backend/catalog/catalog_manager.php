<?php
/**
 * Catalog manager
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Catalog_Manager extends Fructus {
    /**
     * Constructor
     */
    function Catalog_Manager() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        switch ( $this->getRequestValue('do') ) {
            case 'delete':
                $rs = $this->deleteRecord($this->getRequestValue('categoryID'));
                $rs .= $this->getTopMenu();
                $rs .= $this->grid();
            break;

            case 'edit':
                $hash = $this->load($this->getRequestValue('categoryID'));
                $rs = $this->getForm('edit');
            break;
            
            case 'edit_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm('edit');
                } else {
                    $this->editRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new_done':
                if ( !$this->checkData() ) {
                    $rs = $this->getForm();
                } else {
                    $this->addRecord();
                    $rs = $this->getTopMenu();
                    $rs .= $this->grid();
                }
            break;
            
            case 'new':
                $this->setRequestValue('date', date('d.m.Y'));
                $rs = $this->getForm();
            break;
            
            case 'toggle':
                $this->toggle($this->getRequestValue('categoryID'));
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
            break;
            
            default:
                $rs = $this->getTopMenu();
                $rs .= $this->grid();
        }                
        return $rs;
    }
    
    /**
     * Toggle expand/collapse catalog
     * @param int $categoryID category ID
     * @return boolean
     */
    function toggle ( $categoryID ) {
        if ( $_SESSION['toggle'][$categoryID] != 'open' ) {
            $_SESSION['toggle'][$categoryID] = 'open';
        } else {
            $_SESSION['toggle'][$categoryID] = 'close';
        }
        return true;
    }
    
    /**
     * Delete record
     * @param int $record_id record ID
     * @return boolean
     */
    function deleteRecord ( $record_id ) {
        //global $__db_prefix;
		$ret='';
		$catalog_structure = $this->loadCatalogStructure();
        if(count($catalog_structure['childs'][$record_id])==0){
			$query = 'DELETE FROM '.CATEGORIES_TABLE.' WHERE categoryID='.$record_id;
			//echo $query;
			$this->db->exec($query);
			if(!$this->db->success){
				$ret='������ ��� ��������';
			}
		}else{
			$ret='������� �� ������. ������ ��������';
		}
		
        return $ret;
    }
    
    
    /**
     * Edit record
     * @param void
     * @return boolean
     */
    function editRecord () {
        //global $__db_prefix;
        
        $query = "update ".CATEGORIES_TABLE." set 
        	name='".$this->getRequestValue('name')."',
			parent='".$this->getRequestValue('parent')."',
			meta_title='".$this->getRequestValue('meta_title')."',
			seolink='".$this->getRequestValue('seolink')."',
        	sort_order='".$this->getRequestValue('sort_order')."', 
        	first_descr='".$this->getRequestValue('first_descr')."', 
        	description='".$this->getRequestValue('description')."', 
			meta_description='".$this->getRequestValue('meta_description')."', 
        	meta_keywords='".$this->getRequestValue('meta_keywords')."'
        where categoryID=".$this->getRequestValue('categoryID')."";
        
        $this->db->exec($query);
        $this->editImage($this->getRequestValue('categoryID'));
        
        return true;
    }
    
    /**
     * Edit image
     * @param int $record_id record id
     * @return boolean
     */
    function editImage ( $record_id ) {
        global $sitebill_document_root;
        global $__db_prefix;
        
            $need_prv=0;
            $preview_name='';
            $i = '';   
            if (!empty($_FILES['img'.$i]['name'])) { 
                $arr=split('\.',$_FILES['img'.$i]['name']);
                $ext=strtolower($arr[count($arr)-1]);
                $preview_name="img".getmypid().'_'.time()."_".$i.".".$ext;
                $prv="prv".getmypid().'_'.time()."_".$i.".".$ext;
                $preview_name_tmp="_tmp".getmypid().'_'.time()."_".$i.".".$ext;
                move_uploaded_file($_FILES['img'.$i]['tmp_name'], $sitebill_document_root.'/img/data/'.$preview_name_tmp);  
                list($width,$height)=$this->makePreview($sitebill_document_root.'/img/data/'.$preview_name_tmp, $sitebill_document_root.'/img/data/'.$preview_name, 600,400, $ext,1);
                list($w,$h)=$this->makePreview($sitebill_document_root.'/img/data/'.$preview_name_tmp, $sitebill_document_root.'/img/data/'.$prv, 130,130, $ext,'width');
                unlink($sitebill_document_root.'/img/data/'.$preview_name_tmp);
                
                //update image record
                $query="update ".$__db_prefix."_catalog set img".$i."='".mysql_real_escape_string($preview_name)."' where catalog_id=".$record_id;
                //echo $query.'<br>';
                $this->db->exec($query);
                
                //update preview record
                $query="update ".$__db_prefix."_catalog set img".$i."_preview='".mysql_real_escape_string($prv)."' where catalog_id=".$record_id;
                //echo $query.'<br>';
                $this->db->exec($query);
            } 
    }
    
    
    
    /**
     * Load
     * @param int $record_id record ID
     * @return boolean
     */
    function load ( $record_id ) {
        $query = "select * from ".CATEGORIES_TABLE." where categoryID=$record_id";
        //echo $query;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        //echo 'on load parent = '.$this->db->row['parent'].'<br>';
        
        $this->setRequestValue('parent', $this->db->row['parent']);
        $this->setRequestValue('name', $this->db->row['name']);
        $this->setRequestValue('seolink', $this->db->row['seolink']);
        $this->setRequestValue('description', $this->db->row['description']);
        $this->setRequestValue('meta_keywords', $this->db->row['meta_keywords']);
        $this->setRequestValue('meta_description', $this->db->row['meta_description']);
        $this->setRequestValue('meta_title', $this->db->row['meta_title']);
        $this->setRequestValue('first_descr', $this->db->row['first_descr']);
    }
    
    
    /**
     * Add record
     * @param void
     * @return string
     */
    function addRecord () {
        //global $__db_prefix;
		$name = $this->getRequestValue('name');
		$parent = $this->getRequestValue('parent');
        $meta_title = $this->getRequestValue('meta_title');
        $seolink = $this->getRequestValue('seolink');
		$sort_order = $this->getRequestValue('sort_order');
		$first_descr = $this->getRequestValue('first_descr');
		$description = $this->getRequestValue('description');
        $meta_keywords = $this->getRequestValue('meta_keywords');
        $meta_description = $this->getRequestValue('meta_description');

        $query = "insert into ".CATEGORIES_TABLE." (name, parent, description, seolink, meta_keywords, meta_description, sort_order, meta_title, first_descr) values ('$name', '$parent', '$description', '$seolink', '$meta_keywords', '$meta_description', '$sort_order', '$meta_title', '$first_descr')";
        //echo $query;
        $record_id = $this->db->exec($query);
        if ( !$record_id ) {
            $this->riseError('������ ���������� ������ � �������');
            return false;
        }
        //$this->editImage($record_id);
        
        return $rs;
    }
    
    /**
     * Check data
     * @param void
     * @return boolean
     */
    function checkData () {
        if ( $this->getRequestValue('name') == '' ) {
            $this->riseError('�� ������ ���������');
            return false;
        }
        return true;
    }
    
    /**
     * Get form
     * @param string $action action (default 'new')
     * @return string
     */
    function getForm ( $action = 'new' ) {
        $rs .= '
        <link href="/css/jquery-ui-1.8.custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/js/jquery.ui.core.js"></script>         
        <script type="text/javascript" src="/js/jquery.ui.datepicker.js"></script>

        <link rel="stylesheet" type="text/css" href="/js/cleditor/jquery.cleditor.css" />
    	<script type="text/javascript" src="/js/cleditor/jquery.cleditor.min.js"></script>
    	<script type="text/javascript">
      		$(document).ready(function() {
        		$("#input").cleditor();
      		});
    	</script>
        ';
        $rs .= '<form method="post" action="index.php" enctype="multipart/form-data">';
        $rs .= '<table border="0">';
        
        $rs .= '<tr>';
        $rs .= '<td colspan="2" style="text-align: center;"><b>���� ���������� <span class="error">*</span> ����������� ��� ����������</b></td>';
        $rs .= '</tr>';
        
        if ( $this->GetError() ) {
            $rs .= '<tr>';
            $rs .= '<td colspan="2"><span class="error" style="color: red;">'.$this->GetError().'</span></td>';
            $rs .= '</tr>';
        }
        $rs .= '<tr>';
        $rs .= '<td>�������� ������������� �������</td>';
        $rs .= '<td>'.$this->getCatalogSelectBox($this->getRequestValue('parent')).'</td>';
        $rs .= '</tr>';

        $rs .= '<tr>';
        $rs .= '<td>�������� ���������</td>';
        $rs .= '<td><input type="text" name="name" size="50" value="'.$this->getRequestValue('name').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>���-�������</td>';
        $rs .= '<td><input type="text" name="seolink" size="50" value="'.$this->getRequestValue('seolink').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>���� �������� ��� ������� ��������</td>';
        $rs .= '<td><input type="text" name="first_descr" size="50" value="'.$this->getRequestValue('first_descr').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>������� ����������</td>';
        $rs .= '<td><input type="text" name="sort_order" size="50" value="'.$this->getRequestValue('sort_order').'"></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>����� ��������</td>';
        $rs .= '<td><textarea name="meta_title" rows="5" cols="45">'.$this->getRequestValue('meta_title').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��� META keywords</td>';
        $rs .= '<td><textarea name="meta_keywords" rows="5" cols="45">'.$this->getRequestValue('meta_keywords').'</textarea></td>';
        $rs .= '</tr>';
        
        $rs .= '<tr>';
        $rs .= '<td>��� META description</td>';
        $rs .= '<td><textarea name="meta_description" rows="5" cols="45">'.$this->getRequestValue('meta_description').'</textarea></td>';
        $rs .= '</tr>';
        
        
        $rs .= '<tr>';
        $rs .= '<td>��������<br>(HTML)</td>';
        $rs .= '<td><textarea name="description" id="input" rows="10" cols="45">'.$this->getRequestValue('description').'</textarea></td>';
        $rs .= '</tr>';
        
        if ( $action == 'edit' ) {
            $rs .= '<input type="hidden" name="do" value="edit_done">';
            $rs .= '<input type="hidden" name="categoryID" value="'.$this->getRequestValue('categoryID').'">';
        } else {
            $rs .= '<input type="hidden" name="do" value="new_done">';
        }
        
        $rs .= '<input type="hidden" name="action" value="catalog">';
        
        $rs .= '<tr>';
        $rs .= '<td><a href="?action=catalog">&laquo; ����� � ������</a></td>';
        $rs .= '<td><input type="submit" value="���������"></td>';
        $rs .= '</tr>';
        
        $rs .= '</table>';
        
        $rs .= '</form>';
        
        return $rs;
    }
    
    /**
     * Load catalog structure
     * @param void
     * @return array
     */
    function loadCatalogStructure ( ) {
        $query = "SELECT * FROM ".CATEGORIES_TABLE." order by sort_order";
        //echo $query.'<br>';
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $ret['catalog'][$this->db->row['categoryID']] = $this->db->row;
            $ret['childs'][$this->db->row['parent']][] = $this->db->row['categoryID'];
        }
        foreach ( $ret['catalog'] as $categoryID => $c_a ) {
            $ret['catalog'][$categoryID]['total'] = $this->load_product_count($categoryID);
        }
		//print_r($ret);
        return $ret;
    }
    
    /**
     * Load product count
     * @param int $categoryID category id
     * @return int
     */
    function load_product_count ( $categoryID ) {
        $query = "select count(productID) as total from SS_products where categoryID = $categoryID";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row['total'];
        
    }
    
    /**
     * Get catalog table box
     * @param boolean $edit = true
     * @param boolean $product = false
     * @return string
     */
    function getCatalogTable ( $edit = true, $product = false ) {
        $catalog_structure = $this->loadCatalogStructure();
        //echo '<pre>';
        //print_r($catalog_structure);
        $level = -1;
        $rs = '';
        $rs .= '<table border="0">';
        $rs .= '<tr>';
        $rs .= '<td class="row1"><b>������</b></td>';
        if ( $edit ) {
            $rs .= '<td></td>';
        }
        $rs .= '</tr>';
        if ( is_array($catalog_structure) ) {
            foreach ( $catalog_structure['childs'][1] as $item_id => $catalog_id ) {
                //echo "item_id = $item_id, page_id = $page_id<br>";
                //echo $categoryID;
                $rs .= $this->getChildNodesCatalogTable($catalog_id, $catalog_structure, $level + 1, $edit, $product);
            }
        }
        $rs .= '</table>';
        return $rs;
    }
    
    /**
     * Get catalog select box
     * @param int $parent_id parent ID
     * @return string
     */
    function getCatalogSelectBox ( $parent_id ) {
        //echo 'parent_id = '.$parent_id.'<br>';
        $catalog_structure = $this->loadCatalogStructure();
        //echo '<pre>';
        //print_r($catalog_structure['childs']);
        $level = 0;
        $rs = '';
        $rs .= '<select name="parent">';
        $rs .= '<option value="1">������</option>';
        if ( is_array($catalog_structure) ) {
            foreach ( $catalog_structure['childs'][1] as $item_id => $catalog_id ) {
                //echo "item_id = $item_id, page_id = $page_id<br>";
                //echo $categoryID;
                $rs .= $this->getChildNodesCatalogSelectBox($catalog_id, $catalog_structure, $level + 1, $parent_id);
            }
        }
        $rs .= '</select>';
        return $rs;
    }
    
    /**
     * Get child nodes catalog for select box 
     * @param $catalog_id
     * @param $catalog_structure
     * @param $level
     * @param int $parent_id parent ID
     * @return string
     */
    function getChildNodesCatalogSelectBox ( $catalog_id, $catalog_structure, $level, $parent_id ) {
        //echo "catalog_id = $catalog_id<br>";
        $rs = $this->get_catalog_option_item($level, $catalog_id, $catalog_structure, $parent_id);
    	if ( is_array($catalog_structure['childs'][$catalog_id]) ) {
            foreach ( $catalog_structure['childs'][$catalog_id] as $item_id => $child_id ) {
                //echo 'start pring childs child_id => '.$child_id.'<br>';
                //print_r($category_structure['catalog'][$child_id]);
                //$rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$page_structure['page'][$child_id]['aux_page_name'].'</option>';
                //print_r($category_structure['childs'][$child_id]);
                $rs .= $this->getChildNodesCatalogSelectBox($child_id, $catalog_structure, $level + 1, $parent_id);
            }
    	}
        return $rs;
    }
    
    /**
     * Get child nodes catalog for table 
     * @param $catalog_id
     * @param $catalog_structure
     * @param $level
     * @param boolean $edit
     * @param boolean $product
     * @return string
     */
    function getChildNodesCatalogTable ( $catalog_id, $catalog_structure, $level, $edit = true, $product = false ) {
        //echo "catalog_id = $catalog_id<br>";
        $rs = $this->get_catalog_row($level, $catalog_id, $catalog_structure, $edit, $product);
    	if ( is_array($catalog_structure['childs'][$catalog_id]) ) {
            foreach ( $catalog_structure['childs'][$catalog_id] as $item_id => $child_id ) {
                //echo 'start pring childs child_id => '.$child_id.'<br>';
                //print_r($category_structure['catalog'][$child_id]);
                //$rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$page_structure['page'][$child_id]['aux_page_name'].'</option>';
                //print_r($category_structure['childs'][$child_id]);
                if ( $_SESSION['toggle'][$catalog_id] == 'open' ) {
                    $rs .= $this->getChildNodesCatalogTable($child_id, $catalog_structure, $level + 1, $edit, $product);
                }
            }
    	}
        return $rs;
    }
    
    /**
     * Get catalog option item
     * @param int $level level
     * @param int $child_id child ID
     * @param array $catalog_structure catalog structure
     * @param int $parent_id parent ID
     * @return string
     */
    function get_catalog_option_item ( $level, $child_id, $catalog_structure, $parent_id ) {
        $rs = '';
        //echo 'child_id = '.$child_id.', parent_id = '.$parent_id.'<br>';
        if ( $child_id == $parent_id ) {
            $selected = 'selected';
        }
        $rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$catalog_structure['catalog'][$child_id]['name'].'</option>';
        
        return $rs;
    }
    
    /**
     * Get catalog row
     * @param int $level level
     * @param int $child_id child ID
     * @param array $catalog_structure catalog structure
     * @param boolean $edit edit flag
     * @param boolean $product product
     * @return string
     */
    function get_catalog_row ( $level, $child_id, $catalog_structure, $edit = true, $product = false ) {
        $action = $this->getRequestValue('action');
        $this->j++;
        if ( ceil($this->j/2) > floor($this->j/2)  ) {
            $row_class = "row1";
        } else {
            $j = 0;
            $row_class = "row2";
        }
        $rs = '';
        $rs .= '<tr>';
        $rs .= '<td class="'.$row_class.'">'.str_repeat('<img src="/img/16x16.gif" border="0" width="16" height="16">', $level);
        if ( count($catalog_structure['childs'][$child_id]) > 0 ) {
            $rs .= '<a href="?action='.$action.'&do=toggle&categoryID='.$child_id.'">';
            if (  $_SESSION['toggle'][$child_id] == 'open' ) {
                $rs .= '<img src="/img/tree_collapse.gif" border="0" alt="�������" title="�������" width="16" height="16">';
            } else {
                $rs .= '<img src="/img/tree_expand.gif" border="0" alt="��������" title="��������" width="16" height="16">';
            }
            $rs .= '</a> ';
        } else {
            $rs .= '<img src="/img/16x16.gif" border="0" width="16" height="16">';
        }
        
        if ( $product ) {
            $rs .= '<a href="?action='.$action.'&show_category='.$child_id.'">'.$catalog_structure['catalog'][$child_id]['name'].' ('.$catalog_structure['catalog'][$child_id]['total'].')'.'</a>';
        } else {
            $rs .= $catalog_structure['catalog'][$child_id]['name'];
        }
            

        $rs .= '</td>';
        if ( $edit ) {
            $rs .= '<td class="'.$row_class.'">';
            $rs .= '<a href="?action=catalog&do=edit&categoryID='.$child_id.'"><img src="/img/edit.gif" border="0" alt="�������������" title="�������������" width="16" height="16"></a> ';
            //$rs .= '<a href="?action=catalog&do=new&parent_id='.$child_id.'"><img src="/img/add.png" border="0" alt="��������" title="��������" width="16" height="16"></a> ';
            $rs .= '<a href="?action=catalog&do=delete&categoryID='.$child_id.'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0" alt="�������" title="�������" width="16" height="16"></a>';
            $rs .= '</td>';
        }
        $rs .= '</tr>';
        
        return $rs;
    }
    
    /**
     * Grid
     * @param void
     * @return string
     */
    function grid () {
		$catalog_structure = $this->loadCatalogStructure();
		$rs = '<table>';
		$rs .= '<tr>';
		$rs .= '<td class="row_title">���������</td>';
		$rs .= '<td class="row_title">������</td>';
		$rs .= '</tr>';
		$rs .= '<tr>';
		$rs .= '<td style="vertical-align: top;">'.$this->getCatalogTable(true, true).'</td>';
		$rs .= '<td style="vertical-align: top;">'.$this->product_grid($catalog_structure).'</td>';
		$rs .= '</tr>';
		$rs .= '</table>';
		
		return $rs;
    }
    
    /**
     * Product grid
     * @param array $catalog_structure catalog structure
     * @return string
     */
    function product_grid ( $catalog_structure ) {
        if ( $this->getRequestValue('show_category') > 0 ) {
            $_SESSION['show_category'] = $this->getRequestValue('show_category'); 
        }
		//echo '<pre>';
		//print_r($catalog_structure);
		if ( $_SESSION['show_category'] > 0 ) {
		    $query = 'SELECT * FROM '.PRODUCTS_TABLE.' where categoryID='.$_SESSION['show_category'];
		    $this->db->exec($query);
		    if($this->db->success){
			    $rs.='<table>';
			    while($this->db->fetch_assoc()){
			    
                $j++;
                if ( ceil($j/2) > floor($j/2)  ) {
                    $row_class = "row1";
                } else {
                    $j = 0;
                    $row_class = "row2";
                }
		    
			    
			    $rs.='<tr>';
			    $rs.='<td class="'.$row_class.'">'.$this->db->row['name'].'</td>';
			    $rs.='<td class="'.$row_class.'">'.$catalog_structure['catalog'][$this->db->row['categoryID']]['name'].'</td>';
			    $rs.='<td class="'.$row_class.'">';
			    $rs.='<a href="?action=product&do=edit&productID='.$this->db->row['productID'].'"><img src="/img/edit.gif" border="0" alt="�������������" title="�������������" width="16" height="16"></a> ';
			    $rs.='<a href="?action=product&do=delete&productID='.$this->db->row['productID'].'" onclick="if ( confirm(\'������� ��� ������ �������?\') ) {return true;} else {return false;}"><img src="/img/delete.gif" border="0" alt="�������" title="�������" width="16" height="16"></a>';
			    $rs.='</td>';
			    $rs.='</tr>';
			    }
			    $rs.='</table>';
		    }
		}
        return $rs;
    }
	
	
    
    
    /**
     * Get top menu
     * @param void
     * @return string
     */
    function getTopMenu () {
        $rs = '<a href="?action=catalog&do=new">�������� ������</a> | <a href="?action=product&do=new">�������� �����</a> ';
        return $rs;
    }
}
?>
