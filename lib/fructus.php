<?php
define('DB_PREFIX', 'SS');
define('FRUCTUS_DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
if (  !defined('GALLERY_TABLE')  ) 
{
    define('GALLERY_TABLE', 'SS_gallery');
}
if (  !defined('GALLERY_PAGE_TABLE')  ) 
{
    define('GALLERY_PAGE_TABLE', 'SS_gallery_page');
}
if (  !defined('AUX_PAGES_TABLE')  ) 
{
	define('AUX_PAGES_TABLE', 'SS_aux_pages');

}

/**
 * Fructus parent class
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Fructus {
    /**
     * Error message
     */
    var $error_message = false;
    
    /**
     * 
     * Config array
     * @var array
     */
    var $config_array = array();
    
    /**
     * Constructor
     */
    function Fructus() {
        $this->db = new Db( DB_HOST, DB_NAME, DB_USER, DB_PASS );
        $this->loadConfig();
    }
    
	/**
    * Add include path into INCLUDE PATH environment variable
    * @param string $additionalPath - additional include path
    * @return void
    */
    function addIncludePath ( $additionalPath ) {
        if ( preg_match('/WIN/', PHP_OS) ) {
            $separator = ';';
        } else {
            $separator = ':';
        }
        $include_path = ini_get("include_path");
        $include_path .= $separator.$additionalPath;
        ini_set("include_path", $include_path );
    }
    
    /**
     * Get session template
     * @param void
     * @return string
     */
    function get_session_template () {
        srand((double)microtime() *1000000);
        //unset($_SESSION['template']);
        $templates = array('tmpl0', 'variant');
        
        if ( $_COOKIE['check_cookie'] == '' and preg_match('/'.$_SERVER['SERVER_NAME'].'/', $_SERVER['HTTP_REFERER']) ) {
            return $templates[0];
        }
        //SERVER_NAME;
        //echo $_REQUEST[''];
        //echo $_SERVER['HTTP_REFERER'];
        
        if ( $_COOKIE['template'] == '' ) {
             $key = array_rand($templates);
             setcookie("template",$templates[$key],0x6FFFFFFF);
             setcookie("check_cookie",1,0x6FFFFFFF);
             return $templates[$key];
             //$_COOKIE['template'] = $templates[$key];
        }
        
        return $_COOKIE['template'];
    }
    
    /**
     * Get month array
     * @param
     * @return
     */
    function getMonthArray() {
        $month = array('������', '�������', '����', '������', '���', '����', '����', '������', '��������', '�������', '������', '�������');
        return $month;
    }
    
    /**
     * Get assoc array from query string
     * @param string $query query
     * @return array
     */
    function get_assoc_array_from_query ( $query ) {
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row;
    }

    /**
     * this functions converts it to format selected in the administrative mode
     * @param string $dt is a datetime string in MySQL default format (e.g. 2005-12-25 23:59:59)
     * @return string 
     */
    function format_datetime($dt)
    {
        $dformat = (!strcmp(CONF_DATE_FORMAT,"DD.MM.YYYY")) ? "d.m.Y H:i:s" : "m/d/Y h:i:s A";
        $a = @date($dformat, strtotime($dt));
        return $a;
    }

    
    /**
     * Get access
     * @param int $user_id user id
     * @param string $molekula molekula
     * @param string $atom atom
     * @return boolean
     */
    function getAccess ( $user_id, $molekula, $atom = '' ) {
        $login = $this->getLoginByUserID($user_id);
        //echo $_SERVER['SERVER_NAME'].', atom = '.$atom.'<br>';
        if ( $molekula == 'logout' ) {
            return true;
        }
        
        if ( preg_match('/grandmetal/', $_SERVER['SERVER_NAME']) and $molekula == 'grandprice' ) {
            return true;
        }
        
        if ( preg_match('/grandmetal/', $_SERVER['SERVER_NAME']) and $molekula == 'news' ) {
            return false;
        }
        
        if ( $login != 'Admin_k' and $molekula != 'news' ) {
            return false;
        }
        return true;
    }
    
    /**
     * Edit image
     * @param string $action action
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record ID
     * @return boolean
     */
    function editImageMulti( $action, $table_name, $key, $record_id ) {
    	$path = FRUCTUS_DOCUMENT_ROOT.'/images/';
    	$ra = array();
    	
    	//echo '<pre>';
    	//print_r($_REQUEST);
    	//echo '</pre>';
    	//echo "default-id = ".$this->getRequestValue('default-id')."<br>";
    	//echo "default-id = ".$_REQUEST['default-id']."<br>";
    	//update image
        for ( $i=1; $i<=$_REQUEST['default-id']; $i++ ) {
            $need_prv=0;
            $preview_name='';   
            if (!empty($_FILES['img'.$i]['name'])) { 
                $arr=split('\.',$_FILES['img'.$i]['name']);
                $ext=strtolower($arr[count($arr)-1]);
                $preview_name="img".getmypid().'_'.time()."_".$i.".".$ext;
                $prv="prv".getmypid().'_'.time()."_".$i.".".$ext;
                $preview_name_tmp="_tmp".getmypid().'_'.time()."_".$i.".".$ext;
                move_uploaded_file($_FILES['img'.$i]['tmp_name'], $path.$preview_name_tmp);  
                
                $big_width = $this->getConfigValue($action.'_image_big_width');
                $big_height = $this->getConfigValue($action.'_image_big_height');
                
                $preview_width = $this->getConfigValue($action.'_image_preview_width');
                $preview_height = $this->getConfigValue($action.'_image_preview_height');
                
                //echo '$path.$preview_name = '.$path.$preview_name.'<br>';
                list($width,$height)=$this->makePreview($path.$preview_name_tmp, $path.$preview_name, $big_width,$big_height, $ext,1);
                list($w,$h)=$this->makePreview($path.$preview_name_tmp, $path.$prv, $preview_width,$preview_height, $ext,'width');
                
                
                //echo $path.$preview_name_tmp."<br>";
                unlink($path.$preview_name_tmp);
                $ra[$i]['preview'] = $prv;
                $ra[$i]['normal'] = $preview_name;
                
                /*
                //update image record
                $query="update rent set img".$i."='".mysql_real_escape_string($preview_name)."' where rent_id=".$record_id;
                //echo $query.'<br>';
                sql_query($query);
                
                //update preview record
                $query="update rent set img".$i."_preview='".mysql_real_escape_string($prv)."' where rent_id=".$record_id;
                //echo $query.'<br>';
                sql_query($query);
                */
                
                //if (!empty($need_prv)) {sql_query("update {$table_name} set preview='".sql_escape($prv)."' where card_id=".$record_id);}
            } 
        }
        $this->add_image_records($ra, $table_name, $key, $record_id);
        return $ra;
    }
    
    /**
     * Add image data records
     * @param array $images images
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record id
     * @return boolean
     */
    function add_image_records ( $images, $table_name, $key, $record_id ) {
    	//print_r($images);
    	foreach ( $images as $item_id => $item_array ) {
    		$query = "insert into ".DB_PREFIX."_image (normal, preview) values ('".$item_array['normal']."', '".$item_array['preview']."')";
    		//echo 'q = '.$query.'<br>';
    		$this->writeLog(__METHOD__.", q = $query");
    		$image_id = $this->db->exec($query);
    		$this->add_table_image_record($table_name, $key, $record_id, $image_id);
    	}
    }

    
    /**
     * Get login by user ID
     * @param int $user_id user id
     * @return string
     */
    function getLoginByUserID ( $user_id ) {
        $query = "select Login from ".CUSTOMERS_TABLE." where customerID=".$user_id;
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row['Login'];
    }
    
    /**
     * Get ID from URI
     * @param string $uri uri
     * @return int
     */
    function getIDfromURI ( $uri ) {
    	preg_match('/(?P<digit>\d+)/', $uri, $matches);
    	if ( $matches['digit'] > 0 ) {
    		return $matches['digit'];
    	}
    	return false;
    }
    
    
    /**
     * Load config
     * @param 
     * @return
     */
    function loadConfig () {
    	$this->config_array['news_image_big_width'] = 350;
    	$this->config_array['news_image_big_height'] = 350;
    	
    	$this->config_array['news_image_preview_width'] = 84;
    	$this->config_array['news_image_preview_height'] = 84;

    	$this->config_array['gallery_image_big_width'] = 800;
    	$this->config_array['gallery_image_big_height'] = 600;
    	
    	$this->config_array['gallery_image_preview_width'] = 200;
    	$this->config_array['gallery_image_preview_height'] = 200;
    	
    	$this->config_array['news_image_preview_height'] = 84;
    	
    	
		$query='SELECT * FROM '.DB_PREFIX.'_config ORDER BY config_key';
		$this->db->exec($query);
		while ($this->db->fetch_assoc()){
			$this->config_array[$this->db->row['config_key']] = $this->db->row['value']; 			
		}
    }

    /**
     * Get config value
     * @param string $key key
     * @return string
     */
    function getConfigValue ( $key ) {
    	if ( isset($this->config_array[$key]) ) {
    	    //echo "cfg: $key = ".$this->config_array[$key]."<br>";
    		return $this->config_array[$key];
    	}
    	return false;
    }
    
    /**
     * Get value
     * @param string $key key
     * @return string
     */
    function getRequestValue( $key ) {
        $value = (isset($_GET[$key])) ? $_GET[$key] : $_POST[$key];
        return $value;
    }
    
    /**
     * Set request value
     * @param string $key key
     * @param string $value value
     * @return void
     */
    function setRequestValue ( $key, $value ) {
        $_POST[$key] = $value;
        return;
    }
    
    
    /**
     * Rise error 
     * @param string $error error message
     * @return void
     */
    function riseError ( $error_message ) {
        $this->error_message = $error_message;
        $this->error_state = true;
    }
    
    /**
     * Get error message
     * @param void
     * @return string
     */
    function GetErrorMessage () {
        return $this->error_message;
    }
    
    /**
     * Get error 
     * @param void
     * @return boolean
     */
    function getError ( ) {
        return $this->error_message;
    }
    
    /**
     * Get debug mode
     * @param void
     * @return boolean
     */
    function getDebugMode() {
        return DEBUG_MODE;
    }
    
    /**
     * Set debug mode 
     * @param boolean
     * @return void
     */
    function setDebugMode ( $debug_mode ) {
        return;
    }
    
    /**
     * Add customer log
     * @param $log
     * @return void
     */
    function stAddCustomerLog( $log )
    {
	    $customerID=$log;
		//$customerID =  regGetIdByLogin( $log );
	    if ( $customerID != null )
	    {
		    $ipAddress = $_SERVER["REMOTE_ADDR"];
		    $this->db->exec( " insert into ".CUSTOMER_LOG_TABLE.
			  "  (customerID, customer_ip, customer_logtime) ".
			  "  values( $customerID, '$ipAddress', '".$this->get_current_time()."' ) " );
	    }
    }
	
	function get_current_time(){
		return date("Y-m-d H:i:s", time());
	}
    
    
    
    /**
     * Transform
     * @param $str
     * @return string
     */
    function TransformStringToDataBase( $str )
    {
	    if (is_array($str))
	    {
		    foreach ($str as $key => $val)
		    {
			    $str[$key] = stripslashes($val);
		    }
		    $str = str_replace("\\","\\\\",$str);
	    }
	    else
	    {
		    $str = str_replace("\\","\\\\",stripslashes($str));
	    }
	    return str_replace( "'", "''", $str );
    }
    
    
    /**
     * Delete image
     * @param string $table_name table name
     * @param int $image_id image id
     * @return boolean
     */
    function deleteImage ( $table_name, $image_id ) {
    	//delete records from land_image
    	$query = "delete from ".DB_PREFIX."_".$table_name."_image where image_id=$image_id";
    	$this->db->exec($query);
    	
    	//delete image files
    	$this->deleteImageFiles( $image_id );
    	
    	//delete image records
    	$query = "delete from ".DB_PREFIX."_image where image_id=$image_id";
    	$this->db->exec($query);
		return true;    	
    }
    
    /**
     * Delete image files
     * @param $image_id image id
     * @return boolean
     */
    function deleteImageFiles( $image_id ) {
    	$path = FRUCTUS_DOCUMENT_ROOT.'/images/';
    	
    	$query = "select * from ".DB_PREFIX."_image where image_id=$image_id";
    	$this->db->exec($query);
    	while ( $this->db->fetch_assoc() ) {
    		$preview = $this->db->row['preview'];
    		$normal = $this->db->row['normal'];
            unlink($path.$preview);
            unlink($path.$normal);
    	}
    	return true;
    }
    
    function TransformDataBaseStringToText( $str )
    {
        $str = str_replace( "&", "&#38;", $str );
        $str = str_replace( "'", "&#39;", $str );
        $str = str_replace( "<", "&lt;",  $str );
        $str = str_replace( ">", "&gt;",  $str );
        return $str;
    }

    function TransformDataBaseStringToHTML_Text( $str )
    {
        return $str;
    }

    
    /**
     * Show navigator
     * @param $a
     * @param $offset
     * @param $q
     * @param $path
     * @return string
     */
    function ShowNavigator($a, $offset, $q, $path )
    {
        //shows navigator [prev] 1 2 3 4 � [next]
        //$a - count of elements in the array, which is being navigated
        //$offset - current offset in array (showing elements [$offset ... $offset+$q])
        //$q - quantity of items per page
        //$path - link to the page (f.e: "index.php?categoryID=1&")

        if ($a > $q) //if all elements couldn't be placed on the page
        {

            //[prev]
            if ($offset>0) $out .= "<a class=no_underline href=\"".$path."offset=".($offset-$q)."\">&lt;&lt; ".STRING_PREVIOUS."</a> &nbsp;&nbsp;";

            //digital links
            $k = $offset / $q;

            //not more than 4 links to the left
            $min = $k - 5;
            if ($min < 0) { $min = 0; }
            else {
                if ($min >= 1)
                { //link on the 1st page
                    $out .= "<a class=no_underline href=\"".$path."offset=0\">1</a> &nbsp;&nbsp;";
                    if ($min != 1) { $out .= "... &nbsp;"; };
                }
            }

            for ($i = $min; $i<$k; $i++)
            {
                $m = $i*$q + $q;
                if ($m > $a) $m = $a;

                $out .= "<a class=no_underline href=\"".$path."offset=".($i*$q)."\">".($i+1)."</a> &nbsp;&nbsp;";
            }

            //# of current page
            if (strcmp($offset, "show_all"))
            {
                $min = $offset+$q;
                if ($min > $a) $min = $a;
                $out .= "<font class=faq><b>".($k+1)."</b></font> &nbsp;&nbsp;";
            }
            else
            {
                $min = $q;
                if ($min > $a) $min = $a;
                $out .= "<a class=no_underline href=\"".$path."offset=0\">1</a> &nbsp;&nbsp;";
            }

            //not more than 5 links to the right
            $min = $k + 6;
            if ($min > $a/$q) { $min = $a/$q; };
            for ($i = $k+1; $i<$min; $i++)
            {
                $m = $i*$q+$q;
                if ($m > $a) $m = $a;

                $out .= "<a class=no_underline href=\"".$path."offset=".($i*$q)."\">".($i+1)."</a> &nbsp;&nbsp;";
            }

            if ($min*$q < $a) { //the last link
                if ($min*$q < $a-$q) $out .= " ... &nbsp;&nbsp;";
                $out .= "<a class=no_underline href=\"".$path."offset=".($a-$a%$q)."\">".(floor($a/$q)+1)."</a> &nbsp;&nbsp;";
            }

            //[next]
            if (strcmp($offset, "show_all"))
            if ($offset<$a-$q) $out .= "<a class=no_underline href=\"".$path."offset=".($offset+$q)."\">".STRING_NEXT." &gt;&gt;</a> ";

            //[show all]
            if (strcmp($offset, "show_all"))
            $out .= " |&nbsp; <a class=no_underline href=\"".$path."show_all=yes\">".STRING_SHOWALL."</a>";
            else
            $out .= " |&nbsp; <B>".STRING_SHOWALL."</B>";
        }
        return $out;
    }

    
    /**
     * Add table_image record
     * @param int $record_id record id
     * @param int $image_id image id
     * @return boolean
     */
    function add_table_image_record($table_name, $key, $record_id, $image_id) {
    	$query = "insert into ".DB_PREFIX."_".$table_name."_image (".$key.", image_id) values ('".$record_id."', '".$image_id."')";
    	//echo 'q = '.$query.'<br>';
    	$this->db->exec($query);
    	return true;
    }
    
    /**
     * Get image list admin
     * @param string $action action
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record id
     * @return string
     */
    function getImageListAdmin ( $action, $table_name, $key, $record_id ) {
    	global $__db_prefix;
    	
    	$query = "select i.* from ".DB_PREFIX."_".$table_name."_image as li, ".DB_PREFIX."_image as i where li.".$key."=$record_id and li.image_id=i.image_id";
    	//echo $query;
    	
    	$this->db->exec($query);
    	while ( $this->db->fetch_assoc() ) {
    		if ( $action == 'news' ) {
    			$delete_link = "?action=".$action."&do=edit&NID=$record_id&subdo=delete_image&image_id=".$this->db->row['image_id'];
    		} else {
    			$delete_link = "?action=".$action."&do=edit&".$key."=$record_id&subdo=delete_image&image_id=".$this->db->row['image_id'];
    		}
    		$rs .= '<div class="preview_admin" style="padding: 2px; border: 1px solid gray;"><img src="/images/'.$this->db->row['preview'].'" border="0" /><br><a href="'.$delete_link.'" onclick="return confirm(\'������������� ������ ������� �����������?\');">�������</a></div>';
    		
    	}
    	return $rs;
    }
    
    /**
     * Get image array
     * @param string $table_name table name
     * @param string $key key
     * @param int $record_id record id
     * @return string
     */
    function getImageArray ( $table_name, $key, $record_id ) {
    	$query = "select i.* from ".DB_PREFIX."_".$table_name."_image as li, ".DB_PREFIX."_image as i where li.".$key."=$record_id and li.image_id=i.image_id order by li.sort_order";
    	//echo $query;
    	
    	$this->db->exec($query);
    	$i = 0;
    	$ra = array();
    	while ( $this->db->fetch_assoc() ) {
    	    $ra[$i]['normal'] = $this->db->row['normal'];
    	    $ra[$i]['preview'] = $this->db->row['preview'];
    	    $i++;
    	}
    	return $ra;
    }
    
    /**
     * Send notification
     * @param string $email email address
     * @param string $subject subject of the message
     * @param string $message notification message
     * @return boolean
     */
    function sendEmailMessage ( $email, $subject, $message ) {
        global $config;

        //print_r($email);
        //skip empty email
        if ( $email == '' ) {
        	return true;
        }
        $toArray = array();
        $toArray[] = $email;
        //$toArray[] = 'dmn@newod.ru';

        $to = implode(',', array_filter($toArray));
        //echo $to;
        //echo '<pre>';
        $params = array(
            'host' => $config->smtp_host,
            'auth' => $config->smtp_auth,
            'username' => $config->smtp_username,
            'password' => $config->smtp_password,
            'debug' => $config->smtp_debug
        );
        $hdrs = array(
            'Content-type' => 'text/html; charset=windows-1251',
            'From'    => $config->smtp_from,
            'To'      => $email,
            'Subject' => $subject
        );
        
        //require_once($config->lib_dir.'/pear/Mail/smtp.php');
        require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/pear/Mail.php');
        //Create our SMTP-based mailer object.
        $mailer = &Mail::factory('sendmail', $params);
        
        if (PEAR::isError($e = $mailer->send($to, $hdrs, $message))) {
            //$this->RiseError( $e->getMessage() );
            //echo $e->getMessage() . "\n";
            //return false;
            //die($e->getMessage() . "\n");
        }
        return true;
        //echo '</pre>';
    }
    
    /**
     * Write log message
     * @param string $message message
     * @return void
     */
    function writeLog ( $message ) {
        global $debug_log;
        $debug_log = $_SERVER['DOCUMENT_ROOT'].'/debug.log';
        if (!$handle = @fopen($debug_log, 'a')) {
            echo "Cannot open error log file ($debug_log)";
            return;
        }
        $error_message = date("m.d.Y H:i:s ").$message."\n";
        if (fwrite($handle, $error_message) === FALSE) {
            echo "Cannot write to erro log file ($debug_log)";
            return;
        }
        fclose($handle);
        return;
    }
    
    /**
     * Make preview
     * @param
     * @return
     */
    function makePreview ( $src, $dst, $width, $height, $ext='jpg', $md=0 ) {
        if ($ext=='jpg'){$source_img=ImageCreateFromJPEG($src);}elseif($ext=='png'){$source_img=ImageCreateFromPNG($src);}  
        $w_src=imagesx($source_img);
        $h_src=imagesy($source_img);
        if ($w_src>$h_src) {$mode='width';}else{$mode='height';}
        if ($md=='height') {$mode='height';}
        if ($md=='width') {$mode='width';}
        $ratio=1;
        if ($mode=='width') {
            if ($w_src>$width){$ratio=$w_src/$width;}
        } else {
            $tmp=$width;$width=$height;$height=$tmp;
            if ($h_src>$height){$ratio=$h_src/$height;}
        }
        $width_tmp=intval($w_src/$ratio);
        $height_tmp=intval($h_src/$ratio); 
        $tmp_img=imagecreatetruecolor($width_tmp,$height_tmp);
        imagecopyresampled($tmp_img, $source_img,0,0,0,0,$width_tmp,$height_tmp,$w_src,$h_src);
        if ($ext=='jpg'){imagejpeg($tmp_img,$dst);}elseif($ext=='png'){imagepng($tmp_img,$dst);} 
        ImageDestroy($source_img);
        ImageDestroy($tmp_img);
        // ImageDestroy($preview_img);
        return array($width,$height);
    }
}
?>