<?php
class Db {
    var $host;
    var $dbname;
    var $login;
    var $password;

    var $id = false;
    var $error = "";
    var $success = false;
    var $errno = 0;
    var $query = "";
    var $res = false;
    var $row;

    /**
     * Constructor of the class
     * @param string $db_host db host
     * @param string $db_name db name
     * @param string $db_user db user
     * @param string $db_pass db pass
     * @return void
     */
    function Db ( $db_host, $db_name, $db_user, $db_pass ) {
        $this->host = $db_host;
        $this->dbname = $db_name;
        $this->login = $db_user;
        $this->password = $db_pass;
        $this->connect();
    }

    /**
    *Connection function doing connect to database,
    *if connect success then return success flag, else call
    *critical_error function
    *@param void
    *@return boolean success flag if connection success and false if faild
    */
    function connect() # connect to MySQL server and select given database
    {
        $this->close();
        $this->id = mysql_connect($this->host, $this->login, $this->password);
        
        if ($this->id === false) {
            $this->success = false;
            $this->error = mysql_error();
            $this->errno = mysql_errno();
        }
        else
        {
            $success = mysql_select_db($this->dbname, $this->id);
            if (!$success)
            {
                $this->error = mysql_error();
                $this->errno = mysql_errno();
            }
/*
                  $this->exec("set character_set_client='cp1251'");
                  $this->exec("set character_set_results='cp1251'");
                  $this->exec("set collation_connection='cp1251'");
*/
        mysql_query("SET NAMES cp1251");
//                mysql_query ("set character_set='koi8r'");
//                mysql_query ("set character_set_results='koi8_ru'");
//                mysql_query ("set collation_connection='cp1251_general_ci'");
        }
        return $this->success;
    }

    /**
    * Function execute SQL - query and return success if query executed success
    * @param string $query - Query string
    * @return boolean status of function false/true
    */
    function exec($query) #execute a query, set success and res vars as result
    {
        $this->success = true;
        $this->error = "";
        $this->errno = 0;

        $this->query = $query;

        $this->res = mysql_query($this->query, $this->id);

        if ( mysql_error($this->id) != "" )
        {
            $this->success = false;
            $this->error = mysql_error($this->id);
            $this->errno = mysql_errno($this->id);
            return false;
        }
        return mysql_insert_id();
        return $this->success;
    }

    /**
    *Fetch one row of result to $this->row variable
    *@param void
    *@return array hash-array with result
    */
    function fetch_assoc()
    {
        $this->row = @mysql_fetch_array($this->res, MYSQL_ASSOC);
        return $this->row;
    }
    
    /**
     * Fetch one row of result to $this->row variable
     * @param void
     * @return array hash-array with result
     */
    function fetch_array()
    {
        //$this->row = @mysql_fetch_array($this->res, MYSQL_ASSOC);
        $this->row = mysql_fetch_array($this->res);
        
        return $this->row;
    }
    
    

    /**
    *Free result's memory
    *@param void
    *@return void
    */
    function free_result()
    {
        if ($this->res)
        {
            mysql_free_result($this->res);
            $this->res = false;
            $this->success = true;
            $this->error = "";
            $this->errno = 0;
        }
    }

    /**
    *Get last inserted auto-increment column
    *@param void
    *@return mixed if success return $row['id'] - row identificator, else false flag
    */
    function last_insert_id()
    {
        if($this->res)
        {
            if($res = mysql_query("select LAST_INSERT_ID() as id",$this->id))
            {
                $row=mysql_fetch_assoc($res);
                return $row['id'];
                mysql_free_result($res);
            }
            return false;
        }
    }

    /**
    *Rewind fetched rows set
    *@param void
    *@return mixed if success return internal result pointer
    */
    function rewind()
    {
        if($this->res)
            return mysql_data_seek($this->res,0);
        return false;
    }

    /**
    *Get number of rows affected by previous query
    *@param void
    *@return mixed if success return number of rows in result, else return 0
    */
    function num_rows()
    {
        if($this->res)
            return mysql_num_rows($this->res);
        return 0;
    }

    /**
    *Close connection to MySQL server
    *@param void
    *@return void
    */
    function close()
    {
        $this->free_result();
        if ($this->id)
        {
            mysql_close($this->id);
            $this->id = false;
        }
    }

    /**
    * Method stub for begin transaction
    * @param void
    * @return boolean true
    */
    function begin() {
        return true;
    }

    /**
    * Method stub for commit transaction
    * @param void
    * @return boolean true
    */
    function commit() {
        return true;
    }

    /**
    * Method stub for rollback transaction
    * @param void
    * @return boolean true
    */
    function rollback() {
        return true;
    }
}
?>