<?php
/**
 * Mapper class
 */
class Mapper {
    /**
     * Constructor
     * @param request $request
     * @return void
     */
    public function __construct(request $request){
	    $this->request=$request;
	}
		
    /**
     * Run method
     * @param void
     * @return void
     */
	public function run(){
	    global $smarty;
        $smarty->assign("request_uri", $_SERVER['REQUEST_URI']);
        $smarty->assign("uri", $_SERVER['REQUEST_URI']);
        
	    
		switch($this->request->getControllerName()){
			case 'gallery' : {
                require_once('./lib/driver.php');
		        require_once('./lib/backend/gallery/gallery_manager.php');
                require_once('./lib/frontend/gallery/gallery_frontend.php');
                $gallery_frontend = new Gallery_Frontend();
                $gallery_frontend->get_carousel(10);
    		    break;
			}
			    
			case 'serverresponse' : {
                require_once('./lib/driver.php');
				require_once('./lib/frontend/serverresponse/serverresponse.php');
                $serverresponse = new Serverresponse();
                $serverresponse->main();
    			break;
			}
			    
			case 'product' : {
                require_once('./lib/frontend/product/product.php');
                $product = new Product();
                $product->main();
    			break;
			}
			default : {
			    try {
			        require_once('./lib/backend/page/page_manager.php');
			        require_once('./lib/frontend/page/page_frontend.php');
                    $page_frontend = new Page_Frontend($this->request);
			        if ( $page_frontend->main() ) {
			            return;
			        }
			    } catch ( Exception $e ) {
                    echo $e->getMessage();
			    }
			    if ( $_SERVER['REQUEST_URI'] != '/' ) {
			        $smarty->assign('main_content_template', '404.tpl.html');
			    } else {
			        $smarty->assign('main_content_template', 'home.tpl.html');
			    }
			    return;
                require_once('./lib/backend/catalog/catalog_manager.php');
			    require_once('./lib/frontend/catalog/catalog.php');
			    require_once('./lib/frontend/product/product.php');
			    
			    $catalog_frontend = new Catalog_Frontend();
			    if ( $catalog_frontend->main() ) {
			        return;
			    }
			    
			    $categoryID = $catalog_frontend->get_category_id_by_seo_link($_SERVER['REQUEST_URI']);
			    
                $product = new Product();
			    
			    if ( $categoryID and $categoryID != 1 ) {
                    $product->main( $categoryID );
                    return;
			    }
			    
                if ( preg_match('/goods/', $_SERVER['REQUEST_URI']) ) {
                    $product->view();
                    return;
                }
			    $smarty->assign('main_content_template', 'home.tpl.html');
                
    			break;
			    
				//echo 'Call controller <b>'.$this->request->getControllerName().'</b> with parameter <i>'.$this->request->getControllerParam().'</i> and request params <u>'.print_r($this->request->getParams(),true).'</u>';
			}
		}
	}
}