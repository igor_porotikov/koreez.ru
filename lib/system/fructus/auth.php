<?php
/**
 * Fructus auth class 
 * @author Kondin Dmitriy <kondin@etown.ru>
 *
 */ 
class Fructus_Auth extends Fructus {

	private $status=NULL;

    /**
     * Constructor
     */
    function Fructus_Auth() {
        $this->Fructus();
    }
    
    /**
     * Main
     */
	function main () {
		if ((isset($_SESSION['user_id']))AND($_SESSION['user_id']!='')) {
			return true;
		}else{
			if($this->getRequestValue('login_submit')!=''){
				if($uid=$this->autentificateUser($this->getRequestValue('login'), $this->getRequestValue('password'))){
					$_SESSION['user_id']=$uid;
					//must be redirect anywhere
					return true;
				}else{
					//return $this->getAuthForm();
					$this->riseError('Non-existing user');
					return false;
				}
			}else{
				//return $this->getAuthForm();
				$this->riseError('No logged user');
				return false;
			}
		}
	}

    /**
     * Search user with 'login' and 'password' in DB
	 * @param string login
	 * @param string password
	 * @return bool false|string user_id
     */	
	function autentificateUser($login, $password){
		$login=$this->TransformStringToDataBase( $login );
		$password=$this->TransformStringToDataBase( $password );
		$q = "SELECT customerID, CID FROM ".CUSTOMERS_TABLE." WHERE Login='".$login."' AND cust_password='".$this->cryptPasswordCrypt($password, null)."'";
		
		$this->db->exec($q);
		if($this->db->num_rows()!=0){
			$this->db->fetch_assoc();
			// set session variables
			$_SESSION["log"] 	= $login;
			$_SESSION["pass"] 	= $this->cryptPasswordCrypt($password, null);
			$_SESSION["current_currency"] = $this->db->row["CID"];
			// update statistic
			$this->stAddCustomerLog( $this->db->row['customerID'] );
			return $this->db->row['customerID'];
		}else{
			return false;
		}
	}
	 
	/*
    function main () {
        if ( $_SESSION['user_id'] == '' ) {
            if ( $this->regAuthenticate($this->getRequestValue('login'), $this->getRequestValue('password')) ) {
                $_SESSION['user_id'] = 'true';
                return true;
            }
            $this->riseError('not login');
            return $this->getAuthForm();
        }
        return true;
    }
	*/
	
	/**
	 * Debug internal function
	*/
	function getCurrSession(){
		if(isset($_SESSION['user_id'])){
			return $_SESSION['user_id'];
		}else{
			//$_SESSION['user_id']=15;
			return 'No users';
		}
	}
	
	/**
	 * Debug internal function
	*/	
	function getStatus(){
		return $this->status;
	}
	
    
	/**
	 * Authenticate
	 * 
	 * @param unknown_type $login
	 * @param unknown_type $password
	 * @param unknown_type $Redirect
	 */
    function regAuthenticate($login, $password, $Redirect = true)
    {
	    $login		= $this->TransformStringToDataBase( $login );
	    $password	= $this->TransformStringToDataBase( $password );
	    $q = "SELECT cust_password, CID, ActivationCode FROM ".CUSTOMERS_TABLE.
			" WHERE Login='".$login."'";
	    $this->db->exec($q);
	    $this->db->fetch_assoc();

	    if ($this->db->row['cust_password'] && strlen( trim($login) ) > 0)
	    {
		    if ($this->db->row["cust_password"] == $this->cryptPasswordCrypt($password, null) )
		    {
			    // set session variables
			    $_SESSION["log"] 	= $login;
			    $_SESSION["pass"] 	= $this->cryptPasswordCrypt($password, null);

			
			    $_SESSION["current_currency"] = $this->db->row["CID"];
			
    			// update statistic
	    		$this->stAddCustomerLog( $login );

	    		return true;
    		}
	    	else
		    	return false;
	    }
	    else return false;
    }

    /**
     * Crypt
     * 
     * @param $password
     * @param $key
     * @return string
     */
    function cryptPasswordCrypt( $password, $key )
    {
	    return base64_encode( $password );
    }
    
    
    /**
     * Get group ID
     * @param string
     * @return int
     */
    function getGroupID ( $group_name ) {
        return 1;
    }
    
    /**
     * Get auth form
     * @param
     * @return
     */
    function getAuthForm () {
        $rs .= '
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        
                    <link rel=stylesheet type="text/css" href="/css/admin.css">
        <p>&nbsp;</p>        
                                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                            <td class="special">
                                                <h1>�����������</h1><br>
        ';
        $rs .= '<form method="post">';
        $rs .= '<table border="0">';
        $rs .= '<tr>';
        $rs .= '<td class="special">Login </td>';
        $rs .= '<td class="special"><input type="text" name="login" value=""></td>';
        $rs .= '</tr>';
        $rs .= '<tr>';
        $rs .= '<td class="special">Password </td>';
        $rs .= '<td class="special"><input type="password" name="password" value=""></td>';
        $rs .= '</tr>';
        $rs .= '<tr>';
        $rs .= '<td class="special"></td>';
        $rs .= '<td class="special"><input type="submit" name="login_submit" value="����"></td>';
        $rs .= '</tr>';
        $rs .= '</table>';
        $rs .= '</form>';
        $rs .= '
                                            </td>
                                        </tr>
                                    </table>
        ';
        return $rs;
    }
    
    
}
?>