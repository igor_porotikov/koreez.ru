<?php
/**
 * Uploadify class 
 * Store data into UPLOADIFY table
 */
if (  !defined('UPLOADIFY_TABLE')  ) 
{
    define('UPLOADIFY_TABLE', 'SS_uploadify');
}

class Sitebill_Uploadify extends Fructus {
    /**
     * Constructor
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        if (!empty($_FILES)) {
	        $tempFile = $_FILES['Filedata']['tmp_name'];
	        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';

	        $arr=split('\.',$_FILES['Filedata']['name']);
            $ext=strtolower($arr[count($arr)-1]);
            $this->writeLog('file size in megs = '.($_FILES['Filedata']['size'] / 1000000).' max file size (int) = '.( (int)str_replace('M', '', ini_get('upload_max_filesize'))) );
            if ( ($_FILES['Filedata']['size'] / 1000000) >   ( (int)str_replace('M', '', ini_get('upload_max_filesize')) ) ) {
            //if ( 1 ) {
                echo 'max_file_size';
                return;
            }
            if ( $ext != 'jpg' ) {
                echo 'wrong_ext';
                return;
            }
            $preview_name_tmp="jpg_".getmypid().'_'.time()."_".$i.".".$ext;
	        
	        $targetFile =  str_replace('//','/',$targetPath) . $preview_name_tmp;
		    echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
	        
		    move_uploaded_file($tempFile,$targetFile);
        }
        
        $this->addFile($_REQUEST['session'], $preview_name_tmp);
    }
    
    /**
     * Add file
     * @param string $session_code session code
     * @param string $targetFile target file
     * @return boolean
     */
    function addFile ( $session_code, $targetFile ) {
        $query = "insert into ".UPLOADIFY_TABLE." (session_code, file_name) values ('$session_code', '$targetFile')";
        $this->db->exec($query);
        return true;
    }
}
?>
