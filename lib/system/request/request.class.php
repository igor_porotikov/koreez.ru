<?php class Request {
	
	static private $instance=NULL;
	private $querystring;
	private $controller='default';
	private $controller_param=NULL;
	
	private function __construct(){
		$this->querystring=$_SERVER['QUERY_STRING'];
		$this->route=$_REQUEST['route'];
		//$request=;
		//print_r($_REQUEST);
		foreach($_REQUEST as $k=>$v){
			if($k!='route'){
				$this->params[$k]=$v;
			}
		}
		
		if($this->route!=''){
			$a=trim($this->route,'/');
			if(preg_match('/([A-Za-z\-\d*]*)/',$a,$matches)){
			    //echo '<pre>';
			    //print_r($matches);
			//if(preg_match('/product-(\d*)/',$a,$matches)){
				if(isset($matches[3])){
					$this->controller_param=$matches[3];
				}
			}
			//print_r($matches);
			
			$this->controller=$matches[1];
		}
		//echo $this->controller;
		//print_r($this->params);
	}
	
	private function __clone(){
	
	}
	
	static function getInstance(){
		if (self::$instance == NULL)
		{
		  self::$instance = new Request();
		}
		return self::$instance;
	}
	
	public function getControllerName(){
		return $this->controller;
	}
	
	public function getControllerParam(){
		return $this->controller_param;
	}
	
	public function getParams(){
		return $this->params;
	}

}