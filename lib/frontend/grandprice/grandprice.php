<?php
/**
 * Grand price frontend
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Grand_Price_Frontend extends Fructus {
    /**
     * Construct
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Main
     */
    function main () {
        return iconv('cp1251', 'utf8', $this->getPriceBySEO($_REQUEST['price']));
    }
    
    /**
     * Get price by SEO-url
     * @param string $url url
     * @return string
     */
    function getPriceBySEO ( $url ) {
        $query = "select * from ".AUX_PAGES_TABLE." where aux_page_url='$url'";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row['aux_page_text'];
    }
}
?>