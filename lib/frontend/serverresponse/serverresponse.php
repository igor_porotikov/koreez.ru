<?php
/**
 * Server-response class
 * Get url of the page and return HTTP-protocol detailed string response
 */
class Serverresponse extends Driver {
    /**
     * Constructor
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Main
     * @param void
     * @return string
     */
    function main () {
        global $smarty;
        
        $rs = $this->getForm();
        if ( $this->getRequestValue('url') != '' ) {
            require_once 'third/pear/HTTP/Request2.php';
            try {
                $uploader = new HTTP_Request2( $this->getRequestValue('url'), HTTP_Request2::METHOD_GET);
                $rs .= '<hr>';
                $rs .= '<b>HTTP STATUS</b> '.$uploader->send()->getStatus().'<br><br>';
                $response = $uploader->send()->getHeader();
                $rs .= '<b>SERVER RESPONSE</b> = ';
                $rs .= '<pre>';
                $rs .= var_export($response, true);
                $rs .= '</pre>';
                $rs .= '<hr>';
            } catch ( Exception $e ) {
                $rs .= $e->getMessage();
            }
        }
		
        $smarty->assign('main_content_template', 'simple.tpl.html');
		$smarty->assign('content', $rs);
        
        return $rs;
    }
    
    /**
     * Get form
     * @param
     * @return
     */
    function getForm () {
        $rs = '<form method="get">';
        $rs .= '<input type="text" name="url" value="'.$this->getRequestValue('url').'" />';
        $rs .= '<input type="submit" name="submit" value="Проверить" />';
        $rs .= '</form>';
        
        return $rs;
    }
}
?>
