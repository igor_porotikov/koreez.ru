<?php 
/**
 * Gallery frontend component
 */
if (  !defined('GALLERY_TABLE')  ) 
{
    define('GALLERY_TABLE', 'SS_gallery');
}

class Gallery_Frontend extends Gallery_Manager {
    /**
     * Constructor
     */
    function __construct() {
        //$params['component_url'] = '/portfolio_manager/';
        $this->Fructus($params);
    }
    
    /**
     * Get carousel
     * @param int $gallery_id gallery id
     * @param int $item_id item id
     * @return string 
     */
    function get_carousel ( $gallery_id, $item_id ) {
        $image_array = $this->getImageArray('gallery', 'gallery_id', $gallery_id);
        $rs = '
<div class="jcarousel-skin-tango">
<div class="jcarousel-container jcarousel-container-horizontal"
	style="position: relative; display: block;">
<div class="jcarousel-clip jcarousel-clip-horizontal"
	style="overflow-x: hidden; overflow-y: hidden; position: relative;">
<ul id="mycarousel'.$item_id.'" class="jcarousel-list jcarousel-list-horizontal"
	style="overflow-x: hidden; overflow-y: hidden; position: relative; top: 0px; margin-top: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-right: 0px; width: 2530px; left: -162px;">';
        
        $i = 0;
        foreach ( $image_array as $item_id => $item_array ) {
            $i++;
            
            $rs .= '	
			<li jcarouselindex="'.$i.'"
				class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal"
				style="list-style-type: none; list-style-position: initial; list-style-image: initial; float: left;"><a
				onclick="return hs.expand(this)" href="/storage/uploads/'.$item_array['normal'].'"><img
				src="/storage/uploads/'.$item_array['preview'].'" alt=""></a></li>';
        }
        

        $rs .= '
</ul>
</div>
<div disabled="false" style="display: block;"
	class="jcarousel-prev jcarousel-prev-horizontal"></div>
<div disabled="false" style="display: block;"
	class="jcarousel-next jcarousel-next-horizontal"></div>
</div>
</div>
        ';
        global $smarty;
        $smarty->assign('main_content_template', 'simple.tpl.html');
		$smarty->assign('content', $rs);
        
        return $rs;
        
        print_r($image_array);
        
    }
}
?>