<?php
/**
 * Meta frontend class
 * @author Kondin Dmitriy <kondin@etown.ru> http://www.sitebill.ru
 */
class Meta_Frontend extends Fructus {
    /**
     * Constructor
     */
    function __construct() {
        $this->Fructus();
        $this->loadCatalogStructure();
    }
    
    /**
     * Load catalog structure
     * @param void
     * @return string
     */
    function loadCatalogStructure () {
        $query = "SELECT * FROM ".CATEGORIES_TABLE." ";
        $this->db->exec($query);
        while ( $this->db->fetch_assoc() ) {
            $this->catalog[$this->db->row['categoryID']] = $this->db->row; 
            $this->childs[$this->db->row['parent']][] = $this->db->row['categoryID']; 
        }
    }
    
    /**
     * Get level by category ID
     * @param int $categoryID category id
     * @return int
     */
    function getCategoryLevel ( $categoryID ) {
        $level = 1;
        $parent_id = $this->catalog[$categoryID]['parent'];
        while ( $parent_id > 1 ) {
            if ( $level++ > 100 ) {
                return 1;
            }
            $parent_id = $this->catalog[$parent_id]['parent'];
        }
        return $level;
    }
    
    /**
     * Get meta title string template
     * @param void
     * @return string
     */
    function getMetaTitleTemplate () {
        return $this->getConfigValue('meta_title_rule');
    }
    
    /**
     * Get level meta
     * @param int $category_id category ID
     * @param int $product_id product ID
     * @return string
     */
    function getLevelMeta ( $category_id = 0, $product_id = 0 ) {
        //echo "category_id = $category_id, product_id = $product_id<br>";
        if ( $category_id == '' and $product_id == '' ) {
            return $this->getConfigValue('meta_title_rule_main');
        }
        $category_level = $this->getCategoryLevel($category_id);
        if ( $category_level == 1 and $product_id == 0 ) {
            return $this->getConfigValue('meta_title_catalog1');
        } elseif ( $category_level == 2 and $product_id == 0 ) {
            return $this->getConfigValue('meta_title_catalog2');
        } elseif ( $category_level == 3 and $product_id == 0 ) {
            return $this->getConfigValue('meta_title_catalog3');
        } elseif ( $product_id > 0 ) {
            return $this->getConfigValue('meta_title_product_detailed');
        }
        return $this->getConfigValue('meta_title_rule_main');
    }
    
    /**
     * Get level meta description
     * @param int $category_id category ID
     * @param int $product_id product ID
     * @return string
     */
    function getLevelMetaDescription ( $category_id = 0, $product_id = 0 ) {
        //echo "category_id = $category_id, product_id = $product_id<br>";
        if ( $category_id == '' and $product_id == '' ) {
            return $this->getConfigValue('meta_description_rule_main');
        }
        $category_level = $this->getCategoryLevel($category_id);
        if ( $category_level == 1 and $product_id == 0 ) {
            return $this->getConfigValue('meta_description_catalog1');
        } elseif ( $category_level == 2 and $product_id == 0 ) {
            return $this->getConfigValue('meta_description_catalog2');
        } elseif ( $category_level == 3 and $product_id == 0 ) {
            return $this->getConfigValue('meta_description_catalog3');
        } elseif ( $product_id > 0 ) {
            return $this->getConfigValue('meta_description_product_detailed');
        }
        return $this->getConfigValue('meta_description_rule_main');
    }
    
    /**
     * Get product manual meta
     * @param int $productID product id
     * @return mixed
     */
    function getProductManualMeta ( $productID ) {
        $query = "select meta_title, meta_description, meta_keywords from ".PRODUCTS_TABLE." where productID=$productID";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        if ( $this->db->row['meta_title'] != '' ) {
            return $this->db->row;
        }
        return false;
    }
    
    /**
     * Get category manual meta
     * @param int $categoryID category id
     * @return mixed
     */
    function getCategoryManualMeta ( $categoryID ) {
        $query = "select meta_title, meta_description, meta_keywords from ".CATEGORIES_TABLE." where categoryID=$categoryID";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        if ( $this->db->row['meta_title'] != '' ) {
            return $this->db->row;
        }
        return false;
    }
    
    /**
     * Get product name by ID
     * @param int $product_id product id
     * @return string
     */
    function getProductNameByID ( $product_id ) {
        $query = "select name from ".PRODUCTS_TABLE." where productID=$product_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row['name'];
    }
    
    /**
     * Get meta description and keywords string
     * @param string $description description
     * @param string $keywords keywords 
     * @return string
     */
    function getMetaDescriptionAndKeywords ( $description, $keywords ) {
		$meta_tags = "";
		$meta_tags .= "<meta name=\"Description\" content=\"".$description."\">\n";
		$meta_tags .= "<meta name=\"KeyWords\" content=\"".$keywords."\" >\n";
		return $meta_tags;
    }

    /**
     * Get category name by ID
     * @param int $product_id product id
     * @return string
     */
    function getCategoryNameByID ( $category_id ) {
        $query = "select name from ".CATEGORIES_TABLE." where categoryID=$category_id";
        $this->db->exec($query);
        $this->db->fetch_assoc();
        return $this->db->row['name'];
    }
}
?>
