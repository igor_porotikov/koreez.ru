<?php
/**
 * Navigation class
 * @author Kondin Dmitriy <kondin@etown.ru>
 */
class Navigation extends Map_Manager {
    /**
     * Constructor
     */
    function __construct() {
        $this->Fructus();
    }
    
    /**
     * Get breadcrumb
     * @param int $aux_page_ID
     * @return string 
     */
    function getBreadcrumb ( $aux_page_ID ) {
        //echo "aux_page_ID = $aux_page_ID";
        if ( $aux_page_ID > 0 ) {
            $page_info = $this->load($aux_page_ID);
            //$structure = $this->loadCategoryStructure();
            $i =  0;
            $page_structure = $this->loadPageStructure(true);
            //echo '<pre>';
            //print_r($page_structure);
            if ( $page_structure['page'][$aux_page_ID]['aux_page_short'] != '' ) {
                $path[$i]['item'] = $page_structure['page'][$aux_page_ID]['aux_page_short'];
            } elseif ( $page_structure['page'][$aux_page_ID]['aux_page_name'] != '' ) {
                $path[$i]['item'] = $page_structure['page'][$aux_page_ID]['aux_page_name'];
            }
            $path[$i]['end'] = 'true'; 
            
            $aux_page_ID = $page_structure['page'][$aux_page_ID]['parent_id'];
            if ( $aux_page_ID == '' ) {
                return '';
            }
            
            //echo "first aux_page_ID = $aux_page_ID<br>";
            //echo '<pre>';
            while ( $aux_page_ID > 0 ) {
                $i++;
                if ( $page_structure['page'][$aux_page_ID]['aux_page_short'] != '' ) {
                    $path[$i]['item'] = '<a href="/'.$page_structure['page'][$aux_page_ID]['aux_page_url'].'/">'.$page_structure['page'][$aux_page_ID]['aux_page_short'].'</a>';
                } elseif ( $page_structure['page'][$aux_page_ID]['aux_page_name'] != '' ) {
                    $path[$i]['item'] = '<a href="/'.$page_structure['page'][$aux_page_ID]['aux_page_url'].'/">'.$page_structure['page'][$aux_page_ID]['aux_page_name'].'</a>';
                }
                $aux_page_ID = $page_structure['page'][$aux_page_ID]['parent_id'];;
            }
            $i++;
            $path[$i]['item'] = '<a href="/">�������</a>';
            //echo count($path);
            //echo "i = $i";
            //if ( $i == 1 ) {
            //    return '';
            //}
            return array_reverse($path);

            $rs = implode(' / ', array_reverse($path) );
            //echo "rs = $rs<br>";
            //print_r($path);
            //echo '<br>';
            //print_r($structure);
            //exit;
            return $rs;
        }
        return '';
    }
    
    /**
     * Get map table box
     * @param int $current_page_id page ID
     * @return string
     */
    function getMapTable ( $current_page_id = 0 ) {
    	//echo '$current_category_id = '.$current_category_id;
        $page_structure = $this->loadPageStructure(true);
        //echo '<pre>';
        //print_r($page_structure);
        $level = 0;
        $rs = '';
        $rs .= '<table border="0">';
        $rs .= '<tr>';
        $rs .= '<td class="row1"><b><a href="/">�������</a></b></td>';
        $rs .= '<td></td>';
        $rs .= '</tr>';
        if ( is_array($page_structure) ) {
            foreach ( $page_structure['childs'][0] as $item_id => $page_id ) {
                //echo $categoryID;
                $rs .= $this->getChildNodesMapTable($page_id, $page_structure, $level + 1, $current_page_id);
            }
        }
        $rs .= '</table>';
        return $rs;
    }
    
    /**
     * Get child nodes pages for map table 
     * @param $pageID
     * @param $page_structure
     * @param $level
     * @param $current_page_id
     * @return string
     */
    function getChildNodesMapTable($page_id, $page_structure, $level, $current_page_id) {
        $this->j++;
        if ( ceil($this->j/2) > floor($this->j/2)  ) {
            $row_class = "row1";
        } else {
            $j = 0;
            $row_class = "row2";
        }
        	
        //print_r($category_structure['catalog'][$child_id]);
        //$rs .= '<option value="'.$child_id.'" '.$selected.'>'.str_repeat(' . ', $level).$page_structure['page'][$child_id]['aux_page_name'].'</option>';
        $rs .= '<tr>';
        $rs .= '<td class="'.$row_class.'">'.str_repeat('<img src="/img/16x16.gif" border="0" width="16" height="16">', $level);
            
        if ( $page_structure['page'][$page_id]['aux_page_short'] != '' ) {
            $rs .= '<a href="/'.$page_structure['page'][$page_id]['aux_page_url'].'/">'.$page_structure['page'][$page_id]['aux_page_short'].'</a>';
        } else {
            $rs .= '<a href="/'.$page_structure['page'][$page_id]['aux_page_url'].'/">'.$page_structure['page'][$page_id]['aux_page_name'].'</a>';
        }
        $rs .= '</td>';
        $rs .= '</tr>';
        
    	if ( is_array($page_structure['childs'][$page_id]) ) {
            foreach ( $page_structure['childs'][$page_id] as $child_id ) {
                //print_r($category_structure['childs'][$child_id]);
                $rs .= $this->getChildNodesMapTable($child_id, $page_structure, $level + 1, $current_page_id);
            }
    	}
        return $rs;
    }
}
?>
