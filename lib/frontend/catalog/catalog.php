<?php
/**
 * Catalog frontend class
 */
class Catalog_Frontend extends Catalog_Manager {
    /**
     * Constructor
     */
    function __construct() {
        $this->Catalog_Manager();
    }
    
    /**
     * Main
     * @param void
     * @return void
     */
    function main () {
	    global $smarty;
		
	    $categoryID = $this->get_category_id_by_seo_link($_SERVER['REQUEST_URI']);
	    //echo '$categoryID = '.$categoryID;
	    
        $catalog_structure = $this->loadCatalogStructure();
        if ( count($catalog_structure['childs'][$categoryID]) > 0 and $categoryID != 1 ) {
            foreach ( $catalog_structure['childs'][$categoryID] as $item_id => $current_catalog_id ) {
                $ra[] = $catalog_structure['catalog'][$current_catalog_id];
            }
            //print_r($catalog_structure['childs'][$categoryID]);
		    $smarty->assign('categories_tree', $ra);
            $smarty->assign('main_content_template', 'category_tree.tpl.html');
		    return true;
        }
	    
	    //if this is parent category with childs
    	    //return childs catalog list
    	//if this is category with products into
    	    //return product list
    	//else
    	    //return false
		$smarty->assign('HYUNDAI_ARRAY', $this->get_catalog_array(6312));
		
    }
    
    /**
     * Get catalog array
     * @param int $category_id category ID
     * @return
     */
    function get_catalog_array ( $category_id ) {
        $catalog_structure = $this->loadCatalogStructure();
        //echo '<pre>';
        foreach ( $catalog_structure['childs'] as $item_id => $child_id ) {
            //echo 'item_id = '.$item_id.'<br>';
            //echo 'child_id = '.$child_id.'<br>';
            $ra[] = $catalog_structure['catalog'][$item_id];
        }
        return $ra;
        //print_r($ra);
        
    }
    
    /**
     * Get category ID by SEO link
     * @param string $request_uri  request uri
     * @return mixed 
     */
    function get_category_id_by_seo_link ( $request_uri ) {
        $seo_link = str_replace('/', '', $request_uri);
        //echo '$request_uri = '.$request_uri.'<br>';
        //echo '$seo_link = '.$seo_link.'<br>';
		$query = "SELECT categoryID, parent, description, seolink FROM ".
					CATEGORIES_TABLE." where `seolink` = '".$seo_link."' LIMIT 0,1";
	    //echo $query;
		$this->db->exec($query);
		$this->db->fetch_assoc();
		if ( $this->db->row['categoryID'] > 0 ) {
		    return $this->db->row['categoryID'];
		}	
		return false;
        
        /*
		if($row = db_fetch_row($q)){
			$_GET["categoryID"] = $row['categoryID'];
			$_POST["categoryID"] = $row['categoryID'];
			$parent = $row['parent'];
			$description = $row['description'];
			$category_seolink = $row['seolink'];
		}
		*/
    }
}
?>
