<?php
/**
 * Review class
 */
if (  !defined('REVIEWS_TABLE')  ) 
{
    define('REVIEWS_TABLE', 'SS_reviews');
}
if (  !defined('CONF_PRODUCTS_PER_PAGE')  ) 
{
    define('CONF_PRODUCTS_PER_PAGE', 20);
}


/**
 * Product class
 */
class Product extends Fructus {
    /**
     * Constructor
     */
	public function __construct(){
		$this->Fructus();
		//print_r($this->route());
		
	}
	
	private function route(){
		$ret['module']='product';
		$ret['id']=NULL;
		$url=$_REQUEST;
		$a=trim($_REQUEST['route'],'/');
		if(preg_match('/product-(\d*)/',$a,$matches)){
			if(isset($matches[1])){
				$ret['id']=$matches[1];
			}
		}
		return $ret;
	}
	
	private function getProduct($id){
		$ret=NULL;
		$query='SELECT * FROM '.PRODUCTS_TABLE.' WHERE productID='.$id;
		$this->db->exec($query);
		if($this->db->success AND $this->db->num_rows()>0){
			$this->db->fetch_assoc();
			$ret=$this->db->row[0];
		}
		return $ret;
	}
	
	/**
	 * Get product list
	 * @param 
	 * @return array
	 */
	function get_product_list () {
	    $ra = array();
		$query='SELECT * FROM '.PRODUCTS_TABLE.' ';
		$this->db->exec($query);
		while ($this->db->fetch_assoc()) {
		    $ra[] = $this->db->row;
		}
		//echo '<pre>';
		//print_r($ra);
		return $ra;
	}
	
	// *****************************************************************************
	// Purpose	gets product
	// Inputs   $productID - product ID
	// Remarks
	// Returns	array of fieled value
	//			"name"				- product name
	//			"product_code"		- product code
	//			"description"		- description
	//			"brief_description"	- short description
	//			"customers_rating"	- product rating
	//			"in_stock"			- in stock (this parametr is persist if CONF_CHECKSTOCK == 1 )
	//			"option_values"		- array of
	//					"optionID"		- option ID
	//					"name"			- name
	//					"value"	- option value
	//					"option_type" - option type
	//			"ProductIsProgram"		- 1 if product is program, 0 otherwise
	//			"eproduct_filename"		- program filename
	//			"eproduct_available_days"	- program is available days to download
	//			"eproduct_download_times"	- attempt count download file
	//			"weight"			- product weigth
	//			"meta_description"		- meta tag description
	//			"meta_keywords"			- meta tag keywords
	//			"free_shipping"			- 1 product has free shipping,
	//							0 - otherwise
	//			"min_order_amount"		- minimum order amount
	//			"classID"			- tax class ID
	function GetProductSS( $productID, $AsIs = false )
	{
	    $productID = (int)$productID;
	    $q = "SELECT productID, categoryID, name, description, ".
				" customers_rating, Price, in_stock, ".
				" brief_description, list_price, ".
				" product_code, sort_order, date_added, ".
				" date_modified, default_picture, ".
				" eproduct_available_days, eproduct_download_times, ".
				" eproduct_filename, ".
				" weight, meta_description, meta_keywords, ".
				" free_shipping, min_order_amount, classID, shipping_freight, ".
				" customer_votes, enabled, seolink, meta_title FROM ".
	    PRODUCTS_TABLE.
				" WHERE productID='".$productID."'";
	    $this->db->exec($q);
	    $this->db->fetch_assoc();
	    if ( $product = $this->db->row )
	    {
	        //$q1=db_query("SELECT optionID, name FROM ". PRODUCT_OPTIONS_TABLE);
	        
	        $product["option_values"] = array();
	        /*
	        while($option = db_fetch_row($q1))
	        {
	            $q2="SELECT option_value, option_type FROM ".
	            PRODUCT_OPTIONS_VALUES_TABLE.
				" WHERE optionID='".$option["optionID"]."' AND ".
				" productID='".$productID."'";
	            
	            $this->db->exec($q);
	            $this->db->fetch_assoc();
	            
	            if ( $option_value = $this->db->row )
	            {
	                $new_item = array();
	                $new_item["optionID"]		= $option["optionID"];
	                $new_item["name"]			= $option["name"];
	                $new_item["value"]			= TransformDataBaseStringToText( $option_value["option_value"] );
	                $new_item["option_type"]	= $option_value["option_type"];
	                $product["option_values"][]	= $new_item;
	            }
	        }
	        */
	        if(!$AsIs)$product["name"]				= $this->TransformDataBaseStringToText( $product["name"] );
	        $product["description"]			= $this->TransformDataBaseStringToHTML_Text( $product["description"] );
	        $product["brief_description"]	= $this->TransformDataBaseStringToHTML_Text( $product["brief_description"] );
	        $product["product_code"]		= $this->TransformDataBaseStringToText( $product["product_code"] );
	        $product["eproduct_filename"]	= $this->TransformDataBaseStringToText( $product["eproduct_filename"] );
	        $product["meta_description"]	= $this->TransformDataBaseStringToText( $product["meta_description"] );
	        $product["meta_keywords"]		= $this->TransformDataBaseStringToText( $product["meta_keywords"] );

	        $product["date_added"]			= $this->format_datetime( $product["date_added"] );
	        $product["date_modified"]		= $this->format_datetime( $product["date_modified"] );

	        return $product;
	    }
	    $product["ProductIsProgram"] = 	(trim($product["eproduct_filename"]) != "");
	    return false;
	}

	/**
	 * Increment product viewed times
	 * @param $productID
	 * @return void
	 */
	function IncrementProductViewedTimes($productID)
	{
	    $query = "update ".PRODUCTS_TABLE." set viewed_times=viewed_times+1 ".
		" where productID='".$productID."'";
	    $this->db->exec($query);
	}
	
	// *****************************************************************************
	// Purpose	calculate a path to the category ( $categoryID )
	// Inputs
	// Remarks
	// Returns	path to category
	function catCalculatePathToCategory( $categoryID )
	{
	    $categoryID = (int)$categoryID;
	    if (!$categoryID) return NULL;

	    $path = array();

	    $q = "select count(*) from ".CATEGORIES_TABLE.
			" where categoryID=$categoryID ";
	    $row = $this->get_assoc_array_from_query($q);
	    
	    //$row = db_fetch_row($q);
	    if ( $row[0] == 0 )
	    return $path;

	    $curr = $categoryID;
	    do
	    {
	        $q = "SELECT categoryID, parent, name, seolink FROM ".
	        CATEGORIES_TABLE.
			" WHERE categoryID='$curr'";
	        $row = $this->get_assoc_array_from_query($q);
	        //$row = db_fetch_row($q);
	        $path[] = $row;

	        if ( $curr == 1 )
	        break;

	        $curr = $row["parent"];
	    }
	    while ( 1 );
	    //now reverse $path
	    $path = array_reverse($path);
	    return $path;
	}



	
	/**
	 * View
	 * @param 
	 * @return
	 */
	function view () {
	    global $smarty;

	    preg_match_all('/[a-zA-Z0-9\-]+/', $_SERVER['REQUEST_URI'], $matches);
	    //print_r($matches);
	    //echo '<br>';
	    $product_seolink = str_replace('-goods', '', $matches[0][0]);
	    //print_r($product_seolink);
	    
	    if(isset($product_seolink)){
		
	        $q = "SELECT productID FROM ".
					PRODUCTS_TABLE." where `seolink` = '".$product_seolink."' LIMIT 0,1";
		    //echo $q;
			$row = $this->get_assoc_array_from_query($q);	
		    if($row){
			    $_GET["productID"] = $row['productID'];
			    $_POST["productID"] = $row['productID'];
			    $productID = $row['productID'];
		    }
	    }
	    
	    //$productID = 83962;
	    //show product information
	    if (isset($productID) && $productID>=0 && !isset($_POST["add_topic"]) && !isset($_POST["discuss"]) )
	    {
	        $product=$this->GetProductSS($productID);
	        //echo 'test';
	        //print_r($product);

	        if (  !$product || $product["enabled"] == 0  )
	        {
	            header("Location: index.php");
	        }
	        else
	        {

	            if ( !isset($_GET["vote"]) )
	            $this->IncrementProductViewedTimes($productID);

	            $dontshowcategory = 1;

	            $smarty->assign("main_content_template", "product_detailed.tpl.html");

	            $a = $product;
	            $a["PriceWithUnit"] = $this->show_price( $a["Price"] );
	            $a["list_priceWithUnit"] = $this->show_price( $a["list_price"] );

	            if ( ((float)$a["shipping_freight"]) > 0 )
	            $a["shipping_freightUC"] = show_price( $a["shipping_freight"] );

	            if ( isset($_GET["picture_id"]) )
	            {
	                $query = "select filename, thumbnail, enlarged from ".
	                PRODUCT_PICTURES." where photoID=".(int)$_GET["picture_id"] ;
	                //$this->db->exec($query);
	                //$this->db->fetch_assoc();
	                //$picture = $this->db->row;
	                $picture_row = $this->get_assoc_array_from_query( $query );
	            }
	            else if ( !is_null($a["default_picture"]) )
	            {
	                $picture_row = $this->get_assoc_array_from_query("select filename, thumbnail, enlarged from ".PRODUCT_PICTURES." where photoID=".$a["default_picture"] );
	                //$picture = db_query();
	                //$picture_row = db_fetch_row( $picture );
	            }
	            else
	            {
					$query = "select filename, thumbnail, enlarged, photoID from ".PRODUCT_PICTURES." where productID='".$productID."'"; 
	                $picture_row = $this->get_assoc_array_from_query($query);
	                if ( $picture_row ) {
	                    $a["default_picture"]=$picture_row["photoID"];
	                } else {
	                    $picture_row=null;
	                }
	            }
	            if ( $picture_row )
	            {
	                $a["picture"]	= $picture_row[ 0 ];
	                $a["thumbnail"] = $picture_row[ 1 ];
	                $a["big_picture"]  = $picture_row[ 2 ];
	            }
	            else
	            {
	                $a["picture"]	= "";
	                $a["thumbnail"] = "";
	                $a["big_picture"]  = "";
	            }

	            if ($a) //product found
	            {
	                if (!isset($categoryID)) $categoryID = $a["categoryID"];

	                //get selected category info
	                $q = "SELECT categoryID, name, description, picture FROM ".CATEGORIES_TABLE." WHERE categoryID='$categoryID'";
	                $row = $this->get_assoc_array_from_query($q);
	                if ($row)
	                {
	                    if (!file_exists("./products_pictures/".$row[3])) $row[3] = "";
	                    $smarty->assign("selected_category", $row);
	                }
	                else
	                $smarty->assign("selected_category", NULL);

	                //calculate a path to the category
	                $smarty->assign("product_category_path",  $this->catCalculatePathToCategory( $categoryID ) );

	                //reviews number
	                $q = "SELECT count(*) FROM ".DISCUSSIONS_TABLE." WHERE productID='$productID'";
	                $k = $this->get_assoc_array_from_query($q);
	                //$k = db_fetch_row($q); 
	                $k = $k[0];

	                //extra parameters
	                $extra = $this->GetExtraParametrs($productID);

	                //related items
	                $related = array();
	                $q = "SELECT count(*) FROM ".RELATED_PRODUCTS_TABLE." WHERE Owner='$productID'";
	                //$cnt = db_fetch_row($q);
	                $cnt = $this->get_assoc_array_from_query($q);
	                
	                $smarty->assign("product_related_number", $cnt[0]);
	                if ($cnt[0] > 0)
	                {
	                    $q = "SELECT productID FROM ".RELATED_PRODUCTS_TABLE." WHERE Owner='$productID'";
	                    $this->db->exec($q);

	                    while ($this->db->fetch_assoc())
	                    {
	                        $row = $this->db->row;
	                        
	                        $p = db_query("SELECT productID, name, Price FROM ".PRODUCTS_TABLE." WHERE productID=$row[0]") or die (db_error());
	                        if ($r = db_fetch_row($p))
	                        {
	                            $r[2] = show_price($r[2]);
	                            $RelatedPictures = GetPictures($r['productID']);
	                            foreach($RelatedPictures as $_RelatedPicture){

	                                if(!$_RelatedPicture['default_picture'])continue;
	                                if(!file_exists("./products_pictures/".$_RelatedPicture['thumbnail']))break;
	                                $r['pictures'] = array('default' => $_RelatedPicture);
	                                break;
	                            }
	                            //						  $r['thumb'] =
	                            $related[] = $r;
	                        }
	                    }

	                }

	                //update several product fields
	                if (!file_exists("./products_pictures/".$a["picture"] )) $a["picture"] = 0;
	                if (!file_exists("./products_pictures/".$a["thumbnail"] )) $a["thumbnail"] = 0;
	                if (!file_exists("./products_pictures/".$a["big_picture"] )) $a["big_picture"] = 0;
	                else if ($a["big_picture"])
	                {
	                    $size = getimagesize("./products_pictures/".$a["big_picture"] );
	                    $a[16] = $size[0]+40;
	                    $a[17] = $size[1]+30;
	                }
	                $a[12] = $this->show_price( $a["Price"] );
	                $a[13] = $this->show_price( $a["list_price"] );
	                $a[14] = $this->show_price( $a["list_price"] - $a["Price"]); //you save (value)
	                $a["PriceWithOutUnit"]=$this->show_priceWithOutUnit( $a["Price"] );
	                if ( $a["list_price"] ) $a[15] =
	                ceil(((($a["list_price"]-$a["Price"])/
	                $a["list_price"])*100)); //you save (%)


	                if ( isset($_GET["picture_id"]) )
	                {
	                    $pictures = "select photoID, filename, thumbnail, enlarged from ".
	                    PRODUCT_PICTURES." where photoID!=".(int)$_GET["picture_id"].
						" AND productID=".(string)$productID;
	                }
	                else if ( !is_null($a["default_picture"]) )
	                {
	                    $pictures = "select photoID, filename, thumbnail, enlarged from ".
	                    PRODUCT_PICTURES." where photoID!=".$a["default_picture"].
						" AND productID=".(string)$productID;
	                }
	                else
	                {
	                    $pictures = "select photoID, filename, thumbnail, enlarged from ".
	                    PRODUCT_PICTURES." where productID=".(string)$productID;
	                }
	                $this->db->exec($pictures);
	                
	                $all_product_pictures = array();
	                $all_product_pictures_id = array();
	                
	                while( $this->db->fetch_assoc() )
	                {
	                    $picture = $this->db->row;
	                    
	                    if ( $picture[2] != "" )
	                    {
	                        if ( file_exists("./products_pictures/".$picture[2]) )
	                        {
	                            $all_product_pictures[]=$picture[2];
	                            $all_product_pictures_id[] = $picture[0];
	                        }
	                    }
	                    else if ( $picture[1] != "" )
	                    {
	                        if ( file_exists("./products_pictures/".$picture[1]) )
	                        {
	                            $all_product_pictures[]=$picture[1];
	                            $all_product_pictures_id[] = $picture[0];
	                        }
	                    }
	                }

	                //eproduct
	                if (strlen($a["eproduct_filename"]) > 0 && file_exists("products_files/".$a["eproduct_filename"]) )
	                {
	                    $size = filesize("products_files/".$a["eproduct_filename"]);
	                    if ($size > 1000) $size = round ($size / 1000);
	                    $a["eproduct_filesize"] = $size." Kb";
	                }
	                else
	                {
	                    $a["eproduct_filename"] = "";
	                }

	                //initialize product "request information" form in case it has not been already submitted
	                if (!isset($_POST["request_information"]))
	                {
	                    if (!isset($_SESSION["log"]))
	                    {
	                        $customer_name = "";
	                        $customer_email = "";
	                    }
	                    else
	                    {
	                        //$custinfo = regGetCustomerInfo2( $_SESSION["log"] );
	                        $customer_name = $custinfo["first_name"]." ".$custinfo["last_name"];
	                        $customer_email = $custinfo["Email"];
	                    }

	                    $message_text = "";
	                }

	                $smarty->assign("customer_name", $customer_name);
	                $smarty->assign("customer_email", $customer_email);
	                $smarty->assign("message_text", $message_text);

	                if (isset($_GET["sent"]))
	                $smarty->assign("sent",1);

	                $smarty->assign("all_product_pictures_id", $all_product_pictures_id );
	                $smarty->assign("all_product_pictures", $all_product_pictures );
	                $smarty->assign("product_info", $a);
	                $smarty->assign("product_reviews_count", $k);
	                $smarty->assign("product_extra", $extra);
	                $smarty->assign("product_related", $related);
	            }
	            else
	            {
	                //product not found
	                header("Location: index.php");
	            }
	        }
	    }
	     
	    
	    $smarty->assign('main_content_template', 'product_detailed.tpl.html');
	}

	
	/**
	 * Main
	 * @param int $categoryID
	 * @return void
	 */
	public function main( $categoryID ) {
	    global $smarty;
	    
		$rout_data=$this->route();
		if($rout_data['id'] AND $this->getProduct($rout_data['id'])){
			//draw product or seo-link
		}else{
		    
			$callBackParam["categoryID"]	= $categoryID;
			$callBackParam["enabled"]		= 1;

			if (  isset($_GET["search_in_subcategory"]) )
				if ( $_GET["search_in_subcategory"] == 1 )
				{
					$callBackParam["searchInSubcategories"] = true;
					$callBackParam["searchInEnabledSubcategories"] = true;
				}				

			if ( isset($_GET["sort"]) )
				$callBackParam["sort"] = $_GET["sort"];
			if ( isset($_GET["direction"]) )
				$callBackParam["direction"] = $_GET["direction"];

			// search parametrs to advanced search
			if ( $extraParametrsTemplate != null )
					$callBackParam["extraParametrsTemplate"] = $extraParametrsTemplate;
			if ( $searchParamName != null )
					$callBackParam["name"] = $searchParamName;
			if ( $rangePrice != null )
					$callBackParam["price"] = $rangePrice;

			if ( $category["show_subcategories_products"] )
				$callBackParam["searchInSubcategories"] = true;

			$count = 100;
			//$offset = 1;
		    
			$navigatorHtml = $this->GetNavigatorHtml( $this->_getUrlToNavigate( $categoryID, $category['seolink'] ), CONF_PRODUCTS_PER_PAGE,	'prdSearchProductByTemplate', $callBackParam, $products, $offset, $count );
		    
		    $smarty->assign('main_content_template', 'category.tpl.html');
		    //$smarty->assign('products_to_show', $this->get_product_list());
			$smarty->assign( "products_to_show", $products);
		    
		    $smarty->assign('catalog_navigator', $this->ShowNavigator($count, $offset, CONF_PRODUCTS_PER_PAGE, '/product/?'));
		    return;
			//draw products list
		}
	}
	
	function _getUrlToNavigate( $categoryID , $seolink ='')
	{
	    //die($seolink."asdf");
	    if($seolink) $url = "/$seolink/?";
	    else $url = "index.php?categoryID=$categoryID";
	    $data = $this->ScanGetVariableWithId( array("param") );
	    if ( isset($_GET["search_name"]) )
	    $url .= "&search_name=".$_GET["search_name"];
	    if ( isset($_GET["search_price_from"]) )
	    $url .= "&search_price_from=".$_GET["search_price_from"];
	    if ( isset($_GET["search_price_to"]) )
	    $url .= "&search_price_to=".$_GET["search_price_to"];
	    foreach( $data as $key => $val )
	    {
	        $url .= "&param_".$key;
	        $url .= "=".$val["param"];
	    }
	    if ( isset($_GET["search_in_subcategory"]) )
	    $url .= "&search_in_subcategory=1";
	    if ( isset($_GET["sort"]) )
	    $url .= "&sort=".$_GET["sort"];
	    if ( isset($_GET["direction"]) )
	    $url .= "&direction=".$_GET["direction"];
	    if ( isset($_GET["advanced_search_in_category"]) )
	    $url .= "&advanced_search_in_category=".$_GET["advanced_search_in_category"];
	    return $url;
	}
	
	// *****************************************************************************
	// Purpose	this functin does also as ScanPostVariableWithId
	//			but it uses GET variables
	// Inputs     	see ScanPostVariableWithId
	// Remarks	see ScanPostVariableWithId
	// Returns	see ScanPostVariableWithId
	function ScanGetVariableWithId( $varnames )
	{
	    $data=array();
	    foreach( $varnames as $name )
	    {
	        foreach( $_GET as $key => $value )
	        {
	            if ( strstr( $key, $name."_" ) )
	            {
	                $key = str_replace($name."_","",$key);
	                $data[$key][ $name ] = $value;
	            }
	        }
	    }
	    return $data;
	}


	
	/**
	 * Get navigation HTML
	 * @param $url
	 * @param $countRowOnPage
	 * @param $callBackFunction
	 * @param $callBackParam
	 * @param $tableContent
	 * @param $offset
	 * @param $count
	 * @return
	 */
	function GetNavigatorHtml(	$url, $countRowOnPage = CONF_PRODUCTS_PER_PAGE,
	$callBackFunction, $callBackParam, &$tableContent,
	&$offset, &$count )
	{
	    if ( isset($_GET["offset"]) )
	    $offset = (int)$_GET["offset"];
	    else
	    $offset = 0;
	    $offset -= $offset % $countRowOnPage;//CONF_PRODUCTS_PER_PAGE;
	    if ( $offset < 0 ) $offset = 0;
	    $count = 0;

	    if ( !isset($_GET["show_all"]) ) //show 'CONF_PRODUCTS_PER_PAGE' products on this page
	    {
	        $tableContent = $this->$callBackFunction( $callBackParam, $count,
	        array(
						"offset" => $offset, 
						"CountRowOnPage" => $countRowOnPage 
	        )
	        );
	    }
	    else //show all products
	    {
	        $tableContent = $callBackFunction( $callBackParam, $count, null );
	        $offset = "show_all";
	    }

	    $this->ShowNavigator( $count, $offset, $countRowOnPage,
	    $url."&", $out);
	    return $out;
	}
	
	// *****************************************************************************
	// Purpose	gets all products by categoryID
	// Inputs     	$callBackParam item
	//					"search_simple"				- string search simple
	//					"sort"					- column name to sort
	//					"direction"				- sort direction DESC - by descending,
	//												by ascending otherwise
	//					"searchInSubcategories" - if true function searches
	//						product in subcategories, otherwise it does not
	//					"searchInEnabledSubcategories"	- this parametr is actual when
	//											"searchInSubcategories" parametr is specified
	//											if true this function take in mind enabled categories only
	//					"categoryID"	- is not set or category ID to be searched
	//					"name"			- array of name template
	//					"product_code"		- array of product code template
	//					"price"			-
	//								array of item
	//									"from"	- down price range
	//									"to"	- up price range
	//					"enabled"		- value of column "enabled"
	//									in database
	//					"extraParametrsTemplate"
	// Remarks
	// Returns
	function prdSearchProductByTemplate(
	$callBackParam, &$count_row, $navigatorParams = null )
	{
	    //print_r($callBackParam);
	    // navigator params
	    if ( $navigatorParams != null )
	    {
	        $offset			= $navigatorParams["offset"];
	        $CountRowOnPage	= $navigatorParams["CountRowOnPage"];
	    }
	    else
	    {
	        $offset = 0;
	        $CountRowOnPage = 0;
	    }


	    // special symbol prepare
	    if ( isset($callBackParam["search_simple"]) )
	    {
	        for( $i=0; $i<count($callBackParam["search_simple"]); $i++ )
	        {
	            $callBackParam["search_simple"][$i] =
	            TransformStringToDataBase( $callBackParam["search_simple"][$i] );
	        }
	        _deletePercentSymbol( $callBackParam["search_simple"] );
	    }
	    if ( isset($callBackParam["name"]) )
	    {
	        for( $i=0; $i<count($callBackParam["name"]); $i++ )
	        $callBackParam["name"][$i] =
	        TransformStringToDataBase( $callBackParam["name"][$i] );
	        _deletePercentSymbol( $callBackParam["name"][$i] );
	    }
	    if ( isset($callBackParam["product_code"]) )
	    {
	        for( $i=0; $i<count($callBackParam["product_code"]); $i++ )
	        {
	            $callBackParam["product_code"][$i] =
	            TransformStringToDataBase( $callBackParam["product_code"][$i] );
	        }
	        _deletePercentSymbol( $callBackParam["product_code"] );
	    }

	    if ( isset($callBackParam["extraParametrsTemplate"]) )
	    {
	        foreach( $callBackParam["extraParametrsTemplate"] as $key => $value )
	        {
	            if ( is_int($key) )
	            {
	                $callBackParam["extraParametrsTemplate"][$key] =
	                TransformStringToDataBase( $callBackParam["extraParametrsTemplate"][$key] );
	                _deletePercentSymbol( $callBackParam["extraParametrsTemplate"][$key] );
	            }
	        }
	    }


	    $where_clause = "";

	    if ( isset($callBackParam["search_simple"]) )
	    {
	        if (!count($callBackParam["search_simple"])) //empty array
	        {
	            $where_clause = " where 0";
	        }
	        else //search array is not empty
	        {
	            foreach( $callBackParam["search_simple"] as $value )
	            {
	                if ( $where_clause != "" )
	                $where_clause .= " AND ";
	                $where_clause .= " ( LOWER(name) LIKE '%".strtolower($value)."%' OR ".
						 "   LOWER(description) LIKE '%".strtolower($value)."%' OR ".
						 "   LOWER(brief_description) LIKE '%".strtolower($value)."%' ) ";
	            }

	            if ( $where_clause != "" )
	            {
	                $where_clause = " where categoryID>1 and enabled=1 and ".$where_clause;
	            }
	            else
	            {
	                $where_clause = "where categoryID>1 and enabled=1";
	            }
	        }

	    }
	    else
	    {

	        // "enabled" parameter
	        if ( isset($callBackParam["enabled"]) )
	        {
	            if ( $where_clause != "" )
	            $where_clause .= " AND ";
	            $where_clause.=" enabled=".$callBackParam["enabled"];
	        }

	        // take into "name" parameter
	        if ( isset($callBackParam["name"]) )
	        {
	            foreach( $callBackParam["name"] as $name )
	            if (strlen($name)>0)
	            {
	                if ( $where_clause != "" )
	                $where_clause .= " AND ";
	                $where_clause .= " name LIKE '%".$name."%' ";
	            }
	        }

	        // take into "product_code" parameter
	        if ( isset($callBackParam["product_code"]) )
	        {
	            foreach( $callBackParam["product_code"] as $product_code )
	            {
	                if ( $where_clause != "" )
	                $where_clause .= " AND ";
	                $where_clause .= " product_code LIKE '%".$product_code."%' ";
	            }
	        }

	        // take into "price" parameter
	        if ( isset($callBackParam["price"]) )
	        {
	            $price = $callBackParam["price"];

	            if ( trim($price["from"]) != "" && $price["from"] != null )
	            {
	                if ( $where_clause != "" )
	                $where_clause .= " AND ";
	                $from	= ConvertPriceToUniversalUnit( $price["from"] );
	                $where_clause .= "$from<=Price ";
	            }
	            if ( trim($price["to"]) != "" && $price["to"] != null )
	            {
	                if ( $where_clause != "" )
	                $where_clause .= " AND ";
	                $to		= ConvertPriceToUniversalUnit( $price["to"] );
	                $where_clause .= " Price<=$to ";
	            }
	        }
	        
	        //echo 'test';

	        // categoryID
	        if ( isset($callBackParam["categoryID"]) )
	        {
	            //echo 'isset categoryID<br>';
	            //echo "where before = $where_clause<br>";
	            $searchInSubcategories = true;

	            if ( $searchInSubcategories )
	            {
	                //$where_clause = _getConditionWithCategoryConjWithSubCategories( $where_clause,	$callBackParam["categoryID"] );
	                $where_clause = ' ( enabled=1 AND categoryID='.$callBackParam["categoryID"].' ) ';
	            }
	            else
	            {
	                $where_clause = $this->_getConditionWithCategoryConj( $where_clause,
	                $callBackParam["categoryID"] );
	            }
	            //echo "$where_clause<br>";
	            	
	        }

	        if ( $where_clause != "" )
	        $where_clause = "where ".$where_clause;

	    }
	    //echo $where_clause;


	    $order_by_clause = "order by sort_order, name";
	    if ( isset($callBackParam["sort"]) )
	    {
	        if (	$callBackParam["sort"] == "categoryID"			||
	        $callBackParam["sort"] == "name"				||
	        $callBackParam["sort"] == "brief_description"	||
	        $callBackParam["sort"] == "in_stock"			||
	        $callBackParam["sort"] == "Price"				||
	        $callBackParam["sort"] == "customer_votes"		||
	        $callBackParam["sort"] == "customers_rating"	||
	        $callBackParam["sort"] == "list_price"			||
	        $callBackParam["sort"] == "sort_order"			||
	        $callBackParam["sort"] == "items_sold"			||
	        $callBackParam["sort"] == "product_code"		||
	        $callBackParam["sort"] == "shipping_freight"		)
	        {
	            $order_by_clause = " order by ".$callBackParam["sort"]." ASC ";
	            if (  isset($callBackParam["direction"]) )
	            if (  $callBackParam["direction"] == "DESC" )
	            $order_by_clause = " order by ".$callBackParam["sort"]." DESC ";
	        }
	    }

	    $sqlQueryCount = "select count(*) as cid from ".PRODUCTS_TABLE.
				" $where_clause $order_by_clause";
	    //$q = db_query( $sqlQueryCount );
	    $this->db->exec($sqlQueryCount);
	    $this->db->fetch_assoc();
	    //$products_count = db_fetch_row($q);
	    //$products_count = $products_count[0];
	    $products_count = $this->db->row['cid'];
	    
	    $sqlQuery = "select categoryID, name, brief_description, ".
		 		" customers_rating, Price, in_stock, ".
				" customer_votes, list_price, ".
				" productID, default_picture, sort_order, items_sold, enabled, ".
				" product_code, description, shipping_freight, seolink from ".PRODUCTS_TABLE.
				" $where_clause $order_by_clause";

	    //echo $sqlQuery;
	    //$q = db_query( $sqlQuery );
	    $this->db->exec($sqlQuery);
	    $result = array();
	    $i = 0;
	    
	    //$CountRowOnPage = 10;
	    
	    //echo 'offset = '.$offset.'<br>';
	    //echo '$CountRowOnPage = '.$CountRowOnPage.'<br>';
	    
	    if ($offset >= 0 && $offset <= $products_count )
	    {
	        while( $this->db->fetch_assoc() )
	        {
	            //echo 'test';
                //$row = db_fetch_row($q);
                $row = $this->db->row;
                
	            if ( isset($callBackParam["extraParametrsTemplate"]) )
	            {

	                // take into "extra" parametrs
	                $testResult = _testExtraParametrsTemplate( $row["productID"],
	                $callBackParam["extraParametrsTemplate"] );
	                if ( !$testResult )
	                continue;
	            }

	            if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
	            $navigatorParams == null  )
	            {
	            //echo 'test';
	                
	                $row["PriceWithUnit"]		= $this->show_price($row["Price"]);
	                $row["list_priceWithUnit"] 	= $this->show_price($row["list_price"]);
	                // you save (value)
	                $row["SavePrice"]		= $this->show_price($row["list_price"]-$row["Price"]);

	                // you save (%)
	                if ($row["list_price"])
	                $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));
	                $this->_setPictures( $row );
	                //$row["product_extra"]		= $this->GetExtraParametrs( $row["productID"] );
	                $row["PriceWithOutUnit"]	= $this->show_priceWithOutUnit( $row["Price"] );
	                if ( ((float)$row["shipping_freight"]) > 0 )
	                $row["shipping_freightUC"] = show_price( $row["shipping_freight"] );

	                $row["name"]				= $this->TransformDataBaseStringToText( $row["name"] );
	                $row["description"]			= $this->TransformDataBaseStringToHTML_Text( $row["description"] );
	                $row["brief_description"]	= $this->TransformDataBaseStringToHTML_Text( $row["brief_description"] );
	                $row["product_code"]		= $this->TransformDataBaseStringToText( $row["product_code"] );

	                $result[] = $row;
	            }
	            $i++;
	        }
	    }
	    $count_row = $i;
	    return $result;
	}
	
	function _getConditionWithCategoryConj( $condition, $categoryID ) //fetch products from current category
	{
	    $category_condition = "";
	    $q = "select productID from ".
	    CATEGORIY_PRODUCT_TABLE." where categoryID=$categoryID";
	    echo $q;
	    $this->db->exec($q);
	    while( $this->db->fetch_assoc() )
	    {
            $product = $this->db->row;
	        if ( $category_condition != "" )
	        $category_condition .= " OR ";
	        $category_condition .= " productID=".$product[0];
	    }
	    if (strlen($category_condition)>0) $category_condition = "(".$category_condition.")";

	    if ( $condition == "" )
	    {
	        if ( $category_condition == "" )
	        return "categoryID=".$categoryID;
	        else
	        return $category_condition." OR categoryID=".$categoryID;
	    }
	    else
	    {
	        if ( $category_condition == "" )
	        return $condition." AND categoryID=".$categoryID;
	        else
	        return "( $condition AND $category_condition ) OR ".
				" ( $condition AND categoryID=$categoryID )";
	    }
	}

	
	function show_priceWithOutUnit($price)
	{
	    global $selected_currency_details;

	    if (!isset($selected_currency_details) || !$selected_currency_details)
	    //no currency found
	    {
	        return $price;
	    }

	    //is exchange rate negative or 0?
	    if ($selected_currency_details[1] == 0) return "";

	    //now show price
	    $price = round(100*$price*$selected_currency_details[1])/100;
	    if (round($price*10) == $price*10 && round($price)!=$price)
	    $price = "$price"."0"; //to avoid prices like 17.5 - write 17.50 instead
	    return (float)$price;
	}

	
	function _setPictures( & $product )
	{
	    if ( !is_null($product["default_picture"]) )
	    {
	        $pictire=db_query("select filename, thumbnail, enlarged from ".
	        PRODUCT_PICTURES." where photoID=".$product["default_picture"] );
	        $pictire_row=db_fetch_row($pictire);
	        $product["picture"]=$pictire_row["filename"];
	        $product["thumbnail"]=$pictire_row["thumbnail"];
	        $product["big_picture"]=$pictire_row["enlarged"];
	        if (!file_exists("./products_pictures/".$product["picture"])) $product["picture"]=0;
	        if (!file_exists("./products_pictures/".$product["thumbnail"])) $product["thumbnail"]=0;
	        if (!file_exists("./products_pictures/".$product["big_picture"])) $product["big_picture"]=0;
	    }
	}


	function show_price($price, $custom_currency = 0) //show a number and selected currency sign
	//$price is in universal currency

	//if $custom_currency != 0 show price this currency with ID = $custom_currency
	{
	    global $selected_currency_details;

	    if ($custom_currency == 0)
	    {
	        if (!isset($selected_currency_details) || !$selected_currency_details) //no currency found
	        {
	            return $price;
	        }
	    }
	    else //show price in custom currency
	    {
	        $custom_currency = (int) $custom_currency;

	        $q = db_query("select code, currency_value, where2show, currency_iso_3, Name from ".CURRENCY_TYPES_TABLE." where CID=$custom_currency") or die (db_error());
	        if ($row = db_fetch_row($q))
	        {
	            $selected_currency_details = $row; //for show_price() function
	        }
	        else //no currency found. In this case check is there any currency type in the database
	        {
	            $q = db_query("select code, currency_value, where2show from ".CURRENCY_TYPES_TABLE) or die (db_error());
	            if ($row = db_fetch_row($q))
	            {
	                $selected_currency_details = $row; //for show_price() function
	            }
	        }

	    }

	    //is exchange rate negative or 0?
	    if ($selected_currency_details[1] == 0) return "";

	    //now show price
	    $price = round(100*$price*$selected_currency_details[1])/100;
	    if (round($price*10) == $price*10 && round($price)!=$price)
	    $price = "$price"."0"; //to avoid prices like 17.5 - write 17.50 instead

	    $price = _formatPrice( $price );
	    return $selected_currency_details[2] ?
	    $price.$selected_currency_details[0] :
	    $selected_currency_details[0].$price;
	}

	// *****************************************************************************
	// Purpose	gets extra parametrs
	// Inputs   $productID - product ID
	// Remarks
	// Returns	array of value extraparametrs
	//				each item of this array has next struture
	//					first type "option_type" = 0
	//						"name"					- parametr name
	//						"option_value"			- value
	//						"option_type"			- 0
	//					second type "option_type" = 1
	//						"name"					- parametr name
	//						"option_show_times"		- how times does show in client side this
	//												parametr to select
	//						"variantID_default"		- variant ID by default
	//						"values_to_select"		- array of variant value to select
	//							each item of "values_to_select" array has next structure
	//								"variantID"			- variant ID
	//								"price_surplus"		- to added to price
	//								"option_value"		- value
	function GetExtraParametrs( $productID )
	{
	    $extra = array();
	    $query = "select optionID, name from ".
	    PRODUCT_OPTIONS_TABLE." order by sort_order, name";
	    
	    //$q=db_query("select optionID, name from ".PRODUCT_OPTIONS_TABLE." order by sort_order, name");
	    $this->db->exec($query);
	    while ($this->db->fetch_assoc())
	    {
	        $row = $this->db->row;
	        //$row = db_fetch_row($q);
	        if ($row["name"]!="")
	        {
	            $q1=db_query("select option_value, option_type, ".
				    "option_show_times, variantID, optionID from ".
	            PRODUCT_OPTIONS_VALUES_TABLE.
				    " where productID='$productID' AND optionID=$row[0]");
	            $val = db_fetch_row($q1);
	            $b=null;
	            if ($val)
	            {
	                if ( ($val["option_type"]==0 || $val["option_type"]==NULL) &&
	                strlen( trim($val["option_value"]) ) > 0/* &&
	                $val["option_value"] != null*/  )
	                {
	                    $b = array();
	                    $b["option_type"] = $val["option_type"];
	                    $b["name"] = $row["name"];
	                    $b["option_value"] = $val["option_value"];
	                    $b["option_value"] = TransformDataBaseStringToText( $b["option_value"] );
	                }
	                else if ( $val["option_type"]==1 )
	                {
	                    $show_option_price=1;
	                    $b = array();
	                    $b["optionID"] = $val["optionID"];
	                    $b["option_type"] = $val["option_type"];
	                    $b["name"] = $row["name"];
	                    $b["option_show_times"] = $val["option_show_times"];
	                    $b["variantID"] = $val["variantID"];



	                    //fetch all option values variants
	                    $q2=db_query( "select option_value, variantID from ".
	                    PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE.
							" where optionID=".$row[0]." order by sort_order, option_value" );
	                    $b["values_to_select"]=array();
	                    $i=0;
	                    while( $value = db_fetch_row($q2)  )
	                    {
	                        //is this value is product's allowed values list
	                        $q3 = db_query("select variantID, price_surplus from ".
	                        PRODUCTS_OPTIONS_SET_TABLE.
						" where productID='$productID' AND optionID=".$row[0]." and variantID=".$value["variantID"] );
	                        $values_to_select = db_fetch_row($q3);

	                        if ($values_to_select)
	                        {
	                            $b["values_to_select"][$i]=array();
	                            $b["values_to_select"][$i]["option_value"] = TransformDataBaseStringToText( $value["option_value"] );
	                            if ( $values_to_select["price_surplus"] > 0 )
	                            $b["values_to_select"][$i]["option_value"] .= " (+ ".show_price($values_to_select["price_surplus"]).")";
	                            else if ( $values_to_select["price_surplus"] < 0 )
	                            $b["values_to_select"][$i]["option_value"] .= " (- ".show_price(-$values_to_select["price_surplus"]).")";

	                            $b["values_to_select"][$i]["option_valueWithOutPrice"] = $value["option_value"];
	                            $b["values_to_select"][$i]["price_surplus"]=
	                            show_priceWithOutUnit(
	                            $values_to_select["price_surplus"]);
	                            $b["values_to_select"][$i]["variantID"]=
	                            $values_to_select["variantID"];
	                            $i++;

	                        }
	                    }


	                }
	                if ( !is_null($b) )
	                {
	                    $extra[] = $b;
	                }
	            }
	        }
	    }
	    return $extra;
	}

	
}