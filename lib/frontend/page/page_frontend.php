<?php
/**
 * Page frontend component
 */
class Page_Frontend extends Page_Manager {
    /**
     * Constructor
     */
    function __construct( request $request ) {
	    $this->request = $request;
        //$params['component_url'] = '/portfolio_manager/';
        $this->Fructus($params);
    }
    
    /**
     * Main
     * @param
     * @return
     */
    function main () {
        global $smarty;
        
        require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/driver.php');
		require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/backend/gallery/gallery_manager.php');
        require_once(FRUCTUS_DOCUMENT_ROOT.'/lib/frontend/gallery/gallery_frontend.php');
        
        $gallery_frontend = new Gallery_Frontend();

        
        //throw new Exception('exception');
        $aux_page_url = $this->request->getControllerName();
        //echo "aux_page_url = $aux_page_url";
        $page = $this->auxpgGetAuxPageByUrl($aux_page_url);
        //echo $page['aux_page_ID'];
        $gallery_page_array = $this->load_gallery_page_array($page['aux_page_ID']);
        //print_r($gallery_page_array);
        
        //print_r($page);
        $smarty->assign('page_name', $page['aux_page_name']);
        $smarty->assign('meta_keywords', $page['meta_keywords']);
        $smarty->assign('meta_description', $page['meta_description']);
        $smarty->assign('page_h1', $page['aux_page_h1']);
        $smarty->assign('page_short', $page['aux_page_short']);
        
        $smarty->assign('main_content_template', 'simple.tpl.html');
        $rs = '';
        $rs .= $page['aux_page_text'];
        foreach ( $gallery_page_array as $item_id => $gallery_id ) {
            $document_ready_js_string .= '
    			jQuery(\'#mycarousel'.$item_id.'\').jcarousel({
    	            wrap: \'circular\'
                });
            ';
            $rs .= $gallery_frontend->get_carousel($gallery_id, $item_id);
        }
        $rs_final = '<script>';
        $rs_final .= 'jQuery(document).ready(function() {';
        $rs_final .= $document_ready_js_string;
        $rs_final .= '});';
        $rs_final .= '</script>';
        $rs_final .= $rs;
        
		$smarty->assign('content', $rs_final);
        if ( count($page) > 1 ) {
            return true;
        }
    }
    /**
     * Get page by SEO-url
     * @param string $aux_page_url
     * @return array
     */
    function auxpgGetAuxPageByUrl( $aux_page_url ) {
	    $aux_page_ID = (int) $aux_page_ID;
	    $query = "SELECT * FROM ".AUX_PAGES_TABLE." WHERE aux_page_url='$aux_page_url'";
	    //echo $query.'<br>';
	    $this->db->exec($query);
	    $this->db->fetch_assoc();
	    //print_r($this->db->row);
  	    //$q = db_query("SELECT * FROM ".AUX_PAGES_TABLE." WHERE aux_page_url='$aux_page_url'" );
	    if  ( $this->db->row ) {
		    $row["aux_page_ID"] = $this->db->row["aux_page_ID"];
	        $row["aux_page_name"] = $this->TransformDataBaseStringToText( $this->db->row["aux_page_name"] );
	        /*
		    if ( $this->db->row["aux_page_text_type"] == 1 )
			    $row["aux_page_text"] = $this->TransformDataBaseStringToHTML_Text( $this->db->row["aux_page_text"] );
		    else
			    $row["aux_page_text"] = $this->TransformDataBaseStringToText( $this->db->row["aux_page_text"] );
			*/
			
	        $row["aux_page_text"] = $this->db->row["aux_page_text"];
	        $row["aux_page_short"] = $this->db->row["aux_page_short"];
	        $row["aux_page_url"] = $this->TransformDataBaseStringToText( $this->db->row["aux_page_url"] );
		    $row["aux_page_h1"] = $this->TransformDataBaseStringToText( $this->db->row["aux_page_h1"] );
		    $row["aux_page_description"]	= $this->TransformDataBaseStringToText( $this->db->row["aux_page_description"] );
		    $row["meta_keywords"]		= $this->TransformDataBaseStringToText( $this->db->row["meta_keywords"] );
		    $row["meta_description"]	= $this->TransformDataBaseStringToText( $this->db->row["meta_description"] );
	    }
	    return $row;
    }

    /**
     * Transform data base string to text
     * @param string $str
     * @return string
     */
    function TransformDataBaseStringToText( $str ) {
	    $str = str_replace( "&", "&#38;", $str );
	    $str = str_replace( "'", "&#39;", $str );
	    $str = str_replace( "<", "&lt;",  $str );
	    $str = str_replace( ">", "&gt;",  $str );
	    return $str;
    }
}
?>
