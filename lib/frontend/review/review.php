<?php
/**
 * Review class
 */
if (  !defined('REVIEWS_TABLE')  ) 
{
    define('REVIEWS_TABLE', 'SS_reviews');
}

class Review extends Fructus {
    var $mark_items = array('�����������', '���������', '�� �����������');
    /**
     * Constructor
     */
	public function __construct(){
		$this->Fructus();
	}
	
	
	public function main(){
		if(isset($_POST['submit'])){
		    //echo $_SESSION['captcha'];
			$data=array();
			$data['user_nick']=$this->dataPrepare($this->getRequestValue('user_nick'));
			//$data['sex']=$this->dataPrepare($this->getRequestValue('sex'));
			$data['review']=$this->dataPrepare($this->getRequestValue('review'));
			$data['email']=$this->dataPrepare($this->getRequestValue('email'));
			$data['captcha']=$this->dataPrepare($this->getRequestValue('captcha'));
			$data['mark']=$this->dataPrepare($this->getRequestValue('mark'));
			
			//if($data['user_nick']!='' AND $data['review']!=''){
			if( $this->checkData($data) ){
			    $t=array();
				$t=explode("\n",$data['review']);
				$data['review']='';
				foreach($t as $tt){
					$data['review'].='<p>'.$tt.'</p>';
				}
				$answ=$this->addReview($data);
				if($answ){
					$ret.='<span><b>�������. ��� ����� ��������� �� ���������.</b></span>';
					//$ret.=$this->getForm();
					//$ret.=implode('',$this->getReviews());
				}else{
					$ret.='<span style="color: red;">��������� ������. ���������� ��� ���.</span>';
					$ret.=$this->getForm();
					//$ret.=implode('',$this->getReviews());
				}
			}else{
				$ret.='<span style="color: red;">'.$this->GetErrorMessage().'</span>';
				$ret.=$this->getForm($data);
				//$ret.=implode('',$this->getReviews());
				
			}
		}else{
			//$ret = implode('',$this->getReviews());
		    $ret .= $this->getForm();
		}
		
		return $ret;
		//return 1;
	}
	/**
	 * Check data
	 * @param array $data data
	 * @return boolean
	 */
	function checkData ( $data ) {
	    if ( $data['user_nick'] == '' ) {
	        $this->riseError('������� ���� ���');
	        return false;
	    }
	    if ( $data['review'] == '' ) {
	        $this->riseError('��������� �����');
	        return false;
	    }
	    if ( $data['captcha'] != $_SESSION['captcha'] ) {
	        $this->riseError('������� ������ �������� ���');
	        return false;
	    }
	    
	    return true;
	}
	
	/**
	 * Get mark title
	 * @param int $mark_id mark id
	 * @return string
	 */
	function getMarkTitle ( $mark_id ) {
	    return $this->mark_items[$mark_id];
	}
	
	
	/**
	 * Get reviews array
	 * @param void
	 * @return array
	 */
	public function getReviewsArray(){
		$ret=array();
		$query="SELECT * FROM ".REVIEWS_TABLE." WHERE is_published=1 ORDER BY add_date DESC";
		$this->db->exec($query);
		while($this->db->fetch_assoc()){
		    $tmp_arr = array();
		    $tmp_arr = $this->db->row;
		    $tmp_arr['mark'] = $this->getMarkTitle($tmp_arr['mark']);
		    $ra[] = $tmp_arr;
		    
		}
		return $ra;
	}
	
	/**
	 * Get reviews string
	 * @param void
	 * @return array
	 */
	public function getReviews(){
		$ret=array();
		$query="SELECT * FROM ".REVIEWS_TABLE." WHERE is_published=1 ORDER BY add_date DESC";
		$this->db->exec($query);
		while($this->db->fetch_assoc()){
		//print_r($this->db->row);
		    $rs = '';
		    $rs .= '
						<div id="comment">
						<div id="main_photo"><img
							src="/img/default.gif" border="0" alt="" width="89" height="90" /></div>
						<div id="main_text">
						<div id="main_fio"><strong>'.$this->db->row['user_nick'].'</strong></div>
						'.$this->db->row['review'].'
						</div>
						<div id="reply1">
						<div id="reply_photo"><img
							src="/img/default.png" border="0" alt="" width="48" height="48" /></div>
						<div id="reply_text"><strong>Admin</strong>';
			if($this->db->row['admin_review']!=''){
				$rs .= '<p>'.$this->db->row['admin_review'].'</p>';
			}
						$rs .= '
						</div>
						</div>
						</div>
						<div id="clear"></div>
		    ';
			$text='<div class="rev_block">';
			$text.='<div class="rev_us_name">'.$this->db->row['user_nick'].'</div>';
			$text.='<div class="rev_text">'.$this->db->row['review'].'</div>';
			if($this->db->row['admin_review']!=''){
				$text.='<div class="admin_rev_text">'.$this->db->row['admin_review'].'</div>';
			}
			$text.='</div>';
			$ret[]=$rs;
		}
		return $ret;
	}
	
	private function getForm($data=NULL){
	    $ret = '<h3 style="margin-left:300px;">�������� �����</h3>';
		$ret.='<form action="/review/" method="post">';
		$ret.='<table class="tovar100" cellspacing="10">';
		$ret.='<tr><td width="300px" align="right">���� ���</td><td><input type="text" name="user_nick" value="'.$data['user_nick'].'" style="width: 300px;"/></td></tr>';
		//$ret.='<tr><td>���� ���</td><td><select name="sex"><option value="n">�� ������</option><option value="m">�������</option><option value="f">�������</option></select></td></tr>';
		$ret.='<tr><td align="right">�����</td><td><textarea name="review" style="width: 300px;" rows="7">'.$data['review'].'</textarea></td></tr>';
		$ret.='<tr><td align="right">E-mail</td><td><input type="text" name="email" style="width: 300px;" value="'.$data['email'].'" /></td></tr>';
		$ret.='<tr><td align="right">������</td><td>';
		$ret .= ' <input type="radio" name="mark" value="0" checked/> �����������';
		$ret .= ' <input type="radio" name="mark" value="1" /> ���������';
		$ret .= ' <input type="radio" name="mark" value="2" /> �� �����������';
		$ret .= '</td></tr>';
		$ret.='<tr><td></td><td><img src="/captcha.php" border="0"></td></tr>';
		$ret.='<tr><td align="right"></td><td><input type="text" name="captcha" value="" /></td></tr>';
		$ret.='<tr><td></td><td><input type="submit" name="submit" value="��������" /></td></tr>';
		$ret.='</table>';
		$ret.='</form>';
		return $ret;
	}
	
	private function dataPrepare($data){
		$data=strip_tags($data,'<p>');
		if(!get_magic_quotes_gpc()){
			$data=addslashes($data);
		}
		return $data;
	}
	
	private function addReview($data){
	    $query="INSERT INTO ".REVIEWS_TABLE." SET user_nick='".$data['user_nick']."', review='".$data['review']."', email='".$data['email']."', is_published=0, mark=".$data['mark'].", add_date=".time();
		$this->db->exec($query);
		if($this->db->success){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	
}