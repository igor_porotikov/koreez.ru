<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');

session_start();
$include_path = ini_get("include_path");
$additionalPath = $_SERVER['DOCUMENT_ROOT'].'/third/pear';
if ( PHP_OS  == 'WINNT' ) {
    $include_path .= ';'.$additionalPath;
} else {
    $include_path .= ':'.$additionalPath;
}
ini_set("include_path", $include_path );

require_once('../cfg/tables.inc.php');														
require_once('../cfg/connect.inc.php');

require_once('../lib/db/MySQL.php');
require_once('../lib/fructus.php');
require_once('../lib/system/fructus/auth.php');
require_once('../lib/backend/main/main.php');

$fructus_auth = new Fructus_Auth();
?>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251"> 
<link rel=stylesheet type="text/css" href="/css/admin.css">
<link type="text/css" href="/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />	
<script type="text/javascript" src="/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.16.custom.min.js"></script>
 
</head>
<body> 
<?php 

$fructus_auth->main();

if ( $fructus_auth->getError() ) {
    echo $fructus_auth->getAuthForm();
    exit;
}
$admin_main = new Admin_Main();
echo $admin_main->main();
?>
</body>
</html>